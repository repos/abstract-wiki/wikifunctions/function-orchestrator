'use strict';

const http = require( 'http' );
const express = require( 'express' );
const compression = require( 'compression' );
const bodyParser = require( 'body-parser' );
const fs = require( 'node:fs/promises' );
const addShutdown = require( 'http-shutdown' );
const path = require( 'path' );
const packageInfo = require( './package.json' );

const sUtil = require( './lib/util.js' );
const { getEvaluatorAndOrchestratorConfigsForEnvironment } = require( './src/utils.js' );
const { orchestratorConfig } =
	getEvaluatorAndOrchestratorConfigsForEnvironment( process.env, /* useReentrance= */ false );
// eslint-disable-next-line no-unused-vars
const winston = require( 'winston' );
const ServiceUtils = require( '@wikimedia/service-utils' );

/**
 * service-utils functionality that is added to Express
 *
 * @typedef {Object} _ServiceUtils
 * @property {winston.Logger} logger
 * @property {ServiceUtils.metrics} Prometheus metrics
 * @property {Object} conf
 * @property {Function} close
 */

/**
 * Creates an express app and initialises it
 * Express app with service-utils functionality
 *
 * @param {Object?} options - options for ServiceUtils.initialize()
 * @return {Promise} ServiceUtilsExpress the promise resolving to the app object
 */
async function initApp( options ) {

	const serviceUtil = await ServiceUtils.getInstance( options );

	// the main application object
	const app = express();

	// Attach service-utils helpers and make them available in the app
	app.logger = serviceUtil.logger;
	app.metrics = serviceUtil.metrics; // the metrics
	const conf = serviceUtil.config;
	// Default to old service-runner config shape or override with top level
	// TODO: Make config top level in helmchart
	app.conf = {
		...conf.services[ 0 ].conf,
		...conf
	}; // this app's config options
	app.close = serviceUtil.teardown;

	app.info = packageInfo; // this app's package info

	// ensure some reasonable defaults
	if ( !app.conf.port ) {
		app.conf.port = 8888;
	}
	if ( !app.conf.interface ) {
		app.conf.interface = '0.0.0.0';
	}
	if ( app.conf.compression_level === undefined ) {
		app.conf.compression_level = 3;
	}
	if ( app.conf.cors === undefined ) {
		app.conf.cors = '*';
	}
	if ( app.conf.csp === undefined ) {
		app.conf.csp = "default-src 'self'; object-src 'none'; media-src 'none'; img-src 'none'; style-src 'none'; base-uri 'self'; frame-ancestors 'self'";
	}

	// set outgoing proxy
	if ( app.conf.proxy ) {
		process.env.HTTP_PROXY = app.conf.proxy;
		// if there is a list of domains which should
		// not be proxied, set it
		if ( app.conf.no_proxy_list ) {
			if ( Array.isArray( app.conf.no_proxy_list ) ) {
				process.env.NO_PROXY = app.conf.no_proxy_list.join( ',' );
			} else {
				process.env.NO_PROXY = app.conf.no_proxy_list;
			}
		}
	}

	// set up header whitelisting for logging
	if ( !app.conf.log_header_whitelist ) {
		app.conf.log_header_whitelist = [
			'cache-control', 'content-type', 'content-length', 'if-match',
			'user-agent', 'x-request-id'
		];
	}

	// eslint-disable-next-line security/detect-non-literal-regexp
	app.conf.log_header_whitelist = new RegExp( `^(?:${ app.conf.log_header_whitelist.map( ( item ) => item.trim() ).join( '|' ) })$`, 'i' );

	// set up the spec, probably unnecessarily
	app.conf.spec = {
		openapi: '3.0.0',
		info: {
			version: app.info.version,
			title: app.info.name,
			description: app.info.description
		},
		paths: []
	};

	// set the CORS and CSP headers
	app.all( '*', ( req, res, next ) => {
		// TODO (T366734): we never get inside here; does it work for other teams?

		if ( app.conf.cors !== false ) {
			res.header( 'access-control-allow-origin', app.conf.cors );
			res.header( 'access-control-allow-headers', 'accept, x-requested-with, content-type' );
			res.header( 'access-control-expose-headers', 'etag' );
		}
		if ( app.conf.csp !== false ) {
			res.header( 'x-xss-protection', '1; mode=block' );
			res.header( 'x-content-type-options', 'nosniff' );
			res.header( 'x-frame-options', 'SAMEORIGIN' );
			res.header( 'content-security-policy', app.conf.csp );
		}

		sUtil.initAndLogRequest( req, app );
		next();
	} );

	// set up the user agent header string to use for requests
	app.conf.user_agent = app.conf.user_agent || app.info.name;

	// disable the X-Powered-By header
	app.set( 'x-powered-by', false );
	// disable the ETag header - users should provide them!
	app.set( 'etag', false );
	// enable compression
	app.use( compression( { level: app.conf.compression_level } ) );
	// use the JSON body parser
	app.use( bodyParser.json( { limit: app.conf.max_body_size || '100kb' } ) );
	// use the application/x-www-form-urlencoded parser
	app.use( bodyParser.urlencoded( { extended: true } ) );

	return app;
}

/**
 * Loads all routes declared in routes/ into the app
 *
 * @param {app} app the application object to load routes into
 * @param {dir} dir routes folder
 * @return {Promise} a promise resolving to the app object
 */
async function loadRoutes( app, dir ) {
	// recursively load routes from .js files under routes/
	// eslint-disable-next-line security/detect-non-literal-fs-filename
	const dirContents = await fs.readdir( dir );
	await Promise.all(
		dirContents.map( async ( fname ) => {
			try {
				const resolvedPath = path.resolve( dir, fname );
				// eslint-disable-next-line security/detect-non-literal-fs-filename
				const isDirectory = ( await fs.stat( resolvedPath ) ).isDirectory();
				if ( isDirectory ) {
					await loadRoutes( app, resolvedPath );
				} else if ( /\.js$/.test( fname ) ) {
					// import the route file (should be a method) and call it
					// /**
					// eslint-disable-next-line max-len
					// @type {{path: string, skip_domain: string, api_version?: boolean, router: express.Router}}
					// */
					// eslint-disable-next-line security/detect-non-literal-require
					const route = await require( `${ dir }/${ fname }` )( app );
					if ( route === undefined ) {
						return undefined;
					}
					// check that the route exports the object we need
					if ( route.constructor !== Object || !route.path || !route.router ||
						!( route.api_version || route.skip_domain ) ) {
						throw new TypeError( `routes/${ fname } does not export the correct object!` );
					}
					// normalise the path to be used as the mount point
					if ( route.path[ 0 ] !== '/' ) {
						route.path = `/${ route.path }`;
					}
					if ( route.path[ route.path.length - 1 ] !== '/' ) {
						route.path = `${ route.path }/`;
					}
					if ( !route.skip_domain ) {
						route.path = `/:domain/v${ route.api_version }${ route.path }`;
					}
					// all good, use that route
					app.use( route.path, route.router );
				}
			} catch ( e ) {
				app.logger.error( e );
				throw new Error( e );
			}
		} )
	);
	// wrap the route handlers with Promise.try() blocks
	ServiceUtils.helpers.wrapExpressRouteHandlers(
		app, ( await ServiceUtils.getInstance() ).metrics
	);
	// catch errors
	sUtil.setErrorHandler( app );
	// route loading is now complete, return the app object
	return app;
}

/**
 * Creates and start the service's web server
 *
 * @param {app} app
 * @return {http.Server} a promise creating the web server
 */
function createServer( app ) {

	if ( orchestratorConfig.generateFunctionsMetrics ) {
		// set this up when app starts to capture buffer usage
		sUtil.logBufferAllocation( app.logger );
	}

	// return a promise which creates an HTTP server,
	// attaches the app to it, and starts accepting
	// incoming client requests
	let server = http.createServer( app ).listen(
		app.conf.port,
		app.conf.interface
	);
	server.on( 'close', () => {
		app.close();
	} );
	server = addShutdown( server );
	app.logger.info(
		`Worker ${ process.pid } listening on ${ app.conf.interface || '*' }:${ app.conf.port }` );

	if ( orchestratorConfig.generateFunctionsMetrics ) {
		sUtil.periodicResourceLogging( app.logger );
	}

	// Don't delay incomplete packets for 40ms (Linux default) on
	// pipelined HTTP sockets. We write in large chunks or buffers, so
	// lack of coalescing should not be an issue here.
	server.on( 'connection', ( socket ) => {
		socket.setNoDelay( true );
	} );

	return server;

}

/**
 * * @param {Object} options ServiceUtils configuration
 *  * @return {http.Server}
 */

const startApp = async ( options ) => initApp( {
	envName: process.env.NODE_ENV,
	configFile: 'config',
	...options
} )
	.then( ( app ) => loadRoutes( app, `${ __dirname }/routes` ) )
	.then( createServer );

module.exports = startApp;
