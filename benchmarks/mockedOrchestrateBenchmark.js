/**
 * Script to run a sample benchmark suite, testing main workflows
 * with the network calls mocked out.
 *
 * How to use:
 * `mocha path/to/mockedOrchestrateBenchmark.js`
 * OR
 * `npm run benchmark`
 * to run all benchmark suites
 *
 * Result will look like:
 * ```
 * Evaluate py3 function x 2.65 ops/sec ±7.03% (27 runs sampled)
 * Evaluate py3 function without validate x 21.93 ops/sec ±2.16% (70 runs sampled)
 * ```
 *
 * The higher the number (X ops/sec), the faster it is.
 */

'use strict';

const Benchmark = require( 'benchmark' );
const uuid = require( 'uuid' );
const assert = require( '../test/utils/assert.js' );
const { getServerWithMocks, getTestInvariants, EVAL_URI, WIKI_URI, WIKIDATA_URI } = require( '../test/utils/mockUtils.js' );
const { testInputsDir, testOutputsDir } = require( '../test/utils/testFileUtils.js' );
const { orchestrate } = require( '../src/orchestrate.js' );
const { readJSON } = require( '../src/utils.js' );
const canonicalize = require( '../function-schemata/javascript/src/canonicalize.js' );
const { makeMappedResultEnvelope } = require( '../function-schemata/javascript/src/utils.js' );

// Default benchmark options.
const defaultOptions = {
	defer: true, // for async calls.
	minSamples: 20, // ensure the result stability.
	initCount: 2 // default is one if not set.
};

async function runOrchestration( theInput, doValidate ) {
	const invariants = getTestInvariants(
		{
			doValidate: doValidate,
			remainingTime: 15,
			wikiUri: WIKI_URI,
			wikidataUri: WIKIDATA_URI,
			evalUri: EVAL_URI,
			requestId: uuid.v1()
		}
	);
	return await orchestrate( theInput, invariants );
}

describe( 'Function orchestrator benchmark test', () => {

	const { wikiStub, evaluatorStub, mockServiceWorker } = getServerWithMocks();

	let suite;

	before( () => {
		mockServiceWorker.listen();
		suite = new Benchmark.Suite(
			'Mocked orchestrate benchmark suite',
			{
				onCycle: ( event ) => {
					// This gets called between benchmarks.
					console.log( String( event.target ) );
					mockServiceWorker.resetHandlers();
				}
			}
		);
	} );

	afterEach( () => {
		mockServiceWorker.resetHandlers();
	} );

	after( () => {
		suite.run( { async: true } );
	} );

	/**
	 * Orchestrates theInput, checks that output is as expected, and benchmarks runtime.
	 *
	 * @param {string} name The name of the test and benchmark.
	 * @param {Object} theInput The input object.
	 * @param {Object} expectedOutput This should match the returned result's Z22K1 field.
	 * @param {Function|null} setUpStubs callback to set up stubs before running
	 * @param {boolean} doValidate Whether to validate the function call.
	 */
	function runBenchmarkTest(
		name, theInput, expectedOutput, setUpStubs = null, doValidate = true ) {
		it( name, async () => {
			if ( setUpStubs !== null ) {
				setUpStubs();
			}
			let result = { Z22K1: null };
			try {
				result = await runOrchestration( theInput, doValidate );
				result = canonicalize( result ).Z22K1;
			} catch ( err ) {
				console.trace();
				console.log( err );
			}
			const expStr = JSON.stringify( expectedOutput );
			const resStr = JSON.stringify( result.Z22K1 );
			// TODO (T335805): Also test this against past performance.
			assert.deepEqual(
				result.Z22K1,
				expectedOutput,
				`Expected ${ expStr } but got ${ resStr }.\
                Even though this is just a benchmark run, faulty results might indicate \
                the expected sequence wasn't run correctly.` );

			suite.add(
				name,
				async ( deferred ) => {
					// Orchestrate is run twice here. The original call to orchestrate
					// can't be added to benchmark suite :-/.
					result = await runOrchestration( theInput, doValidate );
					result = canonicalize( result ).Z22K1;
					deferred.resolve();
				},
				defaultOptions
			);
		} );
	}

	{
		const setUpStubs = () => {
			evaluatorStub.setZId(
				'Z410',
				() => makeMappedResultEnvelope( { Z1K1: 'Z6', Z6K1: '13' }, null ) );
		};
		runBenchmarkTest(
			'Evaluate py3 function',
			readJSON( testInputsDir( 'evaluated.json' ) ),
			'13',
			setUpStubs
		);
		runBenchmarkTest(
			'Evaluate py3 function without validating',
			readJSON( testInputsDir( 'evaluated.json' ) ),
			'13',
			setUpStubs,
			false
		);
	}

	{
		const setUpStubs = () => {
			const Z400 = readJSON( testInputsDir( 'generic-composition.json' ) );
			wikiStub.setZId( 'Z400', {
				Z1K1: 'Z2',
				Z2K1: { Z1K1: 'Z6', Z6K1: 'Z400' },
				Z2K2: Z400
			} );
			const Z401 = readJSON( testInputsDir( 'generic-composition-implementation.json' ) );
			wikiStub.setZId( 'Z401', {
				Z1K1: 'Z2',
				Z2K1: { Z1K1: 'Z6', Z6K1: 'Z401' },
				Z2K2: Z401
			} );
		};

		// A type containing K1: list of strings and K2: Boolean.
		const theType = {
			Z1K1: 'Z7',
			Z7K1: 'Z400',
			Z400K1: {
				Z1K1: 'Z7',
				Z7K1: 'Z881',
				Z881K1: 'Z6'
			},
			Z400K2: 'Z40'
		};

		// The input has the above-specified type.
		const theInput = {
			Z1K1: theType,
			K1: [ 'Z6' ],
			K2: {
				Z1K1: 'Z40',
				Z40K1: 'Z42'
			}
		};

		const expectedOutput = readJSON( testOutputsDir( 'type-returned-by-generic-composition.json' ) );

		// Call <Echo> (Z801) on the input.
		const theFunctionCall = {
			Z1K1: 'Z7',
			Z7K1: {
				Z1K1: 'Z8',
				Z8K1: [
					'Z17'
				],
				Z8K2: theType,
				Z8K3: [ 'Z20' ],
				Z8K4: [
					'Z14',
					{
						Z1K1: 'Z14',
						Z14K1: 'Z402',
						Z14K2: {
							Z1K1: 'Z7',
							Z7K1: 'Z801',
							Z801K1: theInput
						}
					}
				],
				Z8K5: 'Z402'
			}
		};
		runBenchmarkTest(
			'Evaluate generic defined as composition',
			theFunctionCall,
			expectedOutput,
			setUpStubs
		);
	}

} );
