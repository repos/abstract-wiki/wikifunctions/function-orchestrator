'use strict';

/*!
 * Wikifunctions orchestrator code for 'built-in' implementations of pre-defined functions.
 *
 * @copyright 2020– Abstract Wikipedia team; see LICENSE
 * @license MIT
 */

const normalize = require( '../function-schemata/javascript/src/normalize.js' );
const {
	readJSON,
	createSchema,
	createZObjectKey,
	isTrue,
	makeBoolean,
	traverseZList,
	responseEnvelopeContainsError,
	unquoteBrashly,
	unquoteVeryCarefully
} = require( './utils.js' );
const { makeErrorInNormalForm, error } = require( '../function-schemata/javascript/src/error.js' );
const {
	builtInTypes,
	convertArrayToKnownTypedList,
	convertZListToItemArray,
	deepEqual,
	findFunctionIdentity,
	findTypeIdentity,
	getHead,
	getTail,
	isEmptyZList,
	isGlobalKey,
	isString,
	isZBoolean,
	isZFunctionCall,
	isZReference,
	isZType,
	kidFromGlobalKey,
	makeMappedResultEnvelope,
	setMetadataValue,
	wrapInZ6,
	wrapInZ9,
	wrapInKeyReference,
	wrapInQuote,
	makeTrue,
	makeFalse
} = require( '../function-schemata/javascript/src/utils.js' );
const ErrorFormatter = require( '../function-schemata/javascript/src/errorFormatter.js' );
const { EmptyFrame } = require( './frame.js' );
const { Implementation } = require( './implementation.js' );
const { ZWrapper } = require( './ZWrapper.js' );
const fs = require( 'fs' );

/**
 * HELPER FUNCTIONS
 */

/**
 * Constructs a Z3 object with the given type and name.
 *
 * @param {Object} typeZ4 A Z4 object (in JSON form, not ZWrapper)
 * @param {Object} nameZ6 A Z6 object (in JSON form, not ZWrapper)
 * @param {Object} [labelZ12] A Z12 object (in JSON form, not ZWrapper)
 * @return {Object} Constructed Z3 / Key object (in JSON form, not ZWrapper)
 */
function Z3For( typeZ4, nameZ6, labelZ12 = undefined ) {
	return {
		Z1K1: wrapInZ9( 'Z3' ),
		Z3K1: typeZ4,
		Z3K2: nameZ6,
		Z3K3: labelZ12 === undefined ? Z12For( labelZ12 ) : labelZ12
	};
}

/**
 * Wraps English label in a Z12/Multilingual String object.
 *
 * @param {string} name The English label.
 * @return {Object} a Z12/Multilingual String containing a single Z11
 * wrapping the label (in JSON form, not ZWrapper)
 */
function Z12For( name ) {
	return {
		Z1K1: wrapInZ9( 'Z12' ),
		Z12K1: convertArrayToKnownTypedList(
			[ {
				Z1K1: wrapInZ9( 'Z11' ),
				Z11K1: wrapInZ9( 'Z1002' ),
				Z11K2: wrapInZ6( name )
			} ],
			wrapInZ9( 'Z11' )
		)
	};
}

/**
 * BUILTINS
 */

function BUILTIN_ECHO_( input ) {
	return makeMappedResultEnvelope( input, null );
}

function BUILTIN_IF_( antecedent, trueConsequent, falseConsequent ) {
	let result;
	if ( isTrue( antecedent ) ) {
		result = trueConsequent;
	} else {
		result = falseConsequent;
	}
	return makeMappedResultEnvelope( result, null );
}

function BUILTIN_VALUE_BY_KEY_( ZKeyReference, ZObject ) {
	// TODO (T296667): Add test for error case.
	let goodResult = null, badResult = null;
	const key = ZKeyReference.Z39K1.Z6K1;
	if ( ZObject[ key ] === undefined ) {
		badResult = makeErrorInNormalForm(
			error.key_not_found,
			[ ZKeyReference.asJSON(), wrapInQuote( ZObject.asJSON() ) ]
		);
	} else {
		goodResult = ZObject[ key ];
	}
	return makeMappedResultEnvelope( goodResult, badResult );
}

function BUILTIN_VALUES_BY_KEYS_( ZKeyReferences, ZObject ) {
	const keyrefs = ( ZKeyReferences === undefined ) ? [] :
		convertZListToItemArray( ZKeyReferences );
	const pairType = {
		Z1K1: wrapInZ9( 'Z7' ),
		Z7K1: wrapInZ9( 'Z882' ),
		Z882K1: wrapInZ9( 'Z39' ),
		Z882K2: wrapInZ9( 'Z1' )
	};
	const pairArray = [];
	const missing = [];
	for ( const keyref of keyrefs ) {
		const key = keyref.Z39K1.Z6K1;
		const value = ZObject[ key ];
		if ( value === undefined ) {
			missing.push( key );
		} else {
			pairArray.push( {
				Z1K1: pairType,
				K1: keyref,
				K2: value
			} );
		}
	}
	if ( missing.length > 0 ) {
		const quote = wrapInQuote( ZObject.asJSON() );
		const notFoundErrors = [];
		for ( const key of missing ) {
			const keyRef = wrapInKeyReference( key );
			notFoundErrors.push( makeErrorInNormalForm( error.key_not_found, [ keyRef, quote ] ) );
		}
		let badResult;
		if ( missing.length === 1 ) {
			badResult = notFoundErrors[ 0 ];
		} else {
			badResult = ErrorFormatter.createZErrorList( notFoundErrors );
		}
		return makeMappedResultEnvelope( null, badResult );
	} else {
		const pairList = convertArrayToKnownTypedList( pairArray, pairType );
		const mapType = {
			Z1K1: wrapInZ9( 'Z7' ),
			Z7K1: wrapInZ9( 'Z883' ),
			Z883K1: wrapInZ9( 'Z39' ),
			Z883K2: wrapInZ9( 'Z1' )
		};
		const goodResult = {
			Z1K1: mapType,
			K1: pairList
		};
		return makeMappedResultEnvelope( goodResult, null );
	}
}

function reifyRecursive( targetZObject ) {
	if ( isString( targetZObject ) ) {
		return wrapInZ6( targetZObject );
	}
	const pairType = {
		Z1K1: wrapInZ9( 'Z7' ),
		Z7K1: wrapInZ9( 'Z882' ),
		Z882K1: wrapInZ9( 'Z39' ),
		Z882K2: wrapInZ9( 'Z1' )
	};
	const result = [];
	for ( const key of targetZObject.keys() ) {
		const value = reifyRecursive( targetZObject.getName( key ) );
		result.push( {
			Z1K1: pairType,
			K1: wrapInKeyReference( key ),
			K2: value
		} );
	}
	return convertArrayToKnownTypedList( result, pairType );
}

function BUILTIN_REIFY_( targetZObject ) {
	return makeMappedResultEnvelope( reifyRecursive( targetZObject ), null );
}

function abstractRecursive( ZList ) {
	if ( ZList.Z1K1 === 'Z6' ) {
		return ZList.Z6K1;
	}
	const result = {};
	const arrayOfPairs = convertZListToItemArray( ZList || [] );
	for ( const pair of arrayOfPairs ) {
		const ZKeyReference = pair.K1;
		result[ ZKeyReference.Z39K1.Z6K1 ] = abstractRecursive( pair.K2 );
	}
	return result;
}

function BUILTIN_ABSTRACT_( ZList ) {
	// TODO (T296666): Validate that List is a reified list, i.e. that all
	// elements are Pairs(Key, ZObject).
	return makeZWrapperForTrustedObject(
		makeMappedResultEnvelope(
			abstractRecursive( ZList ),
			null ) );
}

function BUILTIN_CONS_( ZObject, list ) {
	let itemType = wrapInZ9( 'Z1' );

	// if validates as type, type is expanded, itemType is at list.Z1K1.Z4K1.Z881K1
	if ( isZType( list.Z1K1 ) && ( list.Z1K1.Z4K1.Z7K1.Z9K1 === 'Z881' ) ) {
		itemType = list.Z1K1.Z4K1.Z881K1;
	}

	// if validates as function call, type is not expanded, itemType is at list.Z1K1.Z881K1
	if ( isZFunctionCall( list.Z1K1 ) && ( list.Z1K1.Z7K1.Z9K1 === 'Z881' ) ) {
		itemType = list.Z1K1.Z881K1;
	}

	const typedList = convertArrayToKnownTypedList( [ ZObject ], itemType );
	typedList.K2 = list;

	const result = makeMappedResultEnvelope( typedList, null );
	const isValidated = ZObject.isValidated && list.isValidated;
	const isFullyResolved = ZObject.isFullyResolved && list.isFullyResolved;
	if ( isValidated && isFullyResolved ) {
		return makeZWrapperForTrustedObject( result );
	}
	return result;
}

function BUILTIN_HEAD_( list ) {
	if ( isEmptyZList( list ) ) {
		const argument = wrapInKeyReference( 'Z811K1' );
		if ( list instanceof ZWrapper ) {
			list = list.asJSON();
		}
		const quote = wrapInQuote( list );
		return makeMappedResultEnvelope(
			null,
			makeErrorInNormalForm(
				error.argument_value_error,
				[ argument, quote ] ) );
	}
	return makeMappedResultEnvelope( list.K1, null );
}

function BUILTIN_TAIL_( list ) {
	if ( isEmptyZList( list ) ) {
		const argument = wrapInKeyReference( 'Z812K1' );
		if ( list instanceof ZWrapper ) {
			list = list.asJSON();
		}
		const quote = wrapInQuote( list );
		return makeMappedResultEnvelope(
			null,
			makeErrorInNormalForm(
				error.argument_value_error,
				[ argument, quote ] ) );
	}
	return makeMappedResultEnvelope( list.K2, null );
}

function BUILTIN_EMPTY_( list ) {
	let result;
	if ( isEmptyZList( list ) ) {
		result = makeTrue();
	} else {
		result = makeFalse();
	}
	return makeZWrapperForTrustedObject( makeMappedResultEnvelope( result, null ) );
}

function BUILTIN_FIRST_( ZTypedPair ) {
	return makeMappedResultEnvelope( ZTypedPair.K1, null );
}

function BUILTIN_SECOND_( ZTypedPair ) {
	return makeMappedResultEnvelope( ZTypedPair.K2, null );
}

async function BUILTIN_GET_ENVELOPE_( QuotedZ7, invariants ) {
	const { orchestrate } = require( './orchestrate.js' );
	this.invariants_.logger.log( 'debug',
		{ message: 'calling orchestrator from BUILTIN_GET_ENVELOPE_()...', requestId: this.invariants_.requestId }
	);
	const envelope = await orchestrate(
		unquoteBrashly( QuotedZ7 ),
		invariants,
		/* implementationSelector= */ null );
	this.invariants_.logger.log( 'debug',
		{ message: '...finished calling orchestrator from BUILTIN_GET_ENVELOPE_()', requestId: this.invariants_.requestId }
	);
	const pairType = {
		Z1K1: wrapInZ9( 'Z7' ),
		Z7K1: wrapInZ9( 'Z882' ),
		Z882K1: wrapInZ9( 'Z1' ),
		Z882K2: wrapInZ9( 'Z1' )
	};
	const pair = { Z1K1: pairType, K1: envelope.Z22K1, K2: envelope.Z22K2 };
	return makeZWrapperForTrustedObject( makeMappedResultEnvelope( pair, null ) );
}

async function BUILTIN_FETCH_PERSISTENT_ZOBJECT_( quotedZ9, invariants ) {
	const referenceEnvelope = unquoteVeryCarefully( quotedZ9 );
	if ( responseEnvelopeContainsError( referenceEnvelope ) ) {
		return referenceEnvelope;
	}
	const ZID = referenceEnvelope.Z22K1.Z9K1;
	// For Builtins, we cannot call dereferenceZObjects, because it returns a modified
	// Z2 constructed from resolveBuiltinReference(), which is missing keys such as Z2K3
	// and fails validation.  So we go straight to the definitions file.
	if ( resolveBuiltinReference( ZID ) !== null ) {
		return normalize( getPersistentZObjectFromFile( ZID ) );
	}

	let envelope;
	try {
		envelope = ( await invariants.resolver.dereferenceZObjects( [ ZID ] ) ).get( ZID );
	} catch ( e ) {
		const message = `Error thrown by dereferenceZObjects(): ${ e }.`;

		invariants.logger.log( 'error', { message: message, requestId: invariants.requestId } );

		const zerror = ErrorFormatter.createZErrorInstance(
			error.unknown_error, { errorInformation: message } );

		return makeMappedResultEnvelope( null, zerror );
	}
	return envelope;
}

function makeZWrapperForTrustedObject( ZObject ) {
	return ZWrapper.create(
		ZObject,
		// Scope and parent information aren't needed because this object
		// is fully trusted, so no resolution can take place.
		/* scope= */ new EmptyFrame(),
		/* parentPointer= */ null,
		// Trusted objects spring into being fully validated and fully resolved.
		/* isValidated= */ true,
		/* isFullyResolved= */ true );
}

async function BUILTIN_FETCH_WIKIDATA_ITEM_( Z6091, invariants ) {
	const IID = Z6091.Z6091K1.Z6K1;
	const resolvedItem = await invariants.resolver.dereferenceItem( IID );
	return makeZWrapperForTrustedObject( resolvedItem );
}

async function BUILTIN_FETCH_WIKIDATA_PROPERTY_( Z6092, invariants ) {
	const PID = Z6092.Z6092K1.Z6K1;
	const resolvedItem = await invariants.resolver.dereferenceProperty( PID );
	return makeZWrapperForTrustedObject( resolvedItem );
}

async function BUILTIN_FETCH_WIKIDATA_LEXEME_FORM_( Z6094, invariants ) {
	const LFID = Z6094.Z6094K1.Z6K1;
	const resolvedForm = await invariants.resolver.dereferenceLexemeForm( LFID );
	return makeZWrapperForTrustedObject( resolvedForm );
}

async function BUILTIN_FETCH_WIKIDATA_LEXEME_( Z6095, invariants ) {
	const LID = Z6095.Z6095K1.Z6K1;
	const resolvedLexeme = await invariants.resolver.dereferenceLexeme( LID );
	return makeZWrapperForTrustedObject( resolvedLexeme );
}

async function BUILTIN_FETCH_WIKIDATA_LEXEME_SENSE_( Z6096, invariants ) {
	const LSID = Z6096.Z6096K1.Z6K1;
	const resolvedSense = await invariants.resolver.dereferenceLexemeSense( LSID );
	return makeZWrapperForTrustedObject( resolvedSense );
}

async function BUILTIN_FIND_LEXEMES_FOR_ITEM_( Z6091, Z6092, Z60, invariants ) {
	const QID = Z6091.Z6091K1.Z6K1;
	const PID = Z6092.Z6092K1.Z6K1;
	const resolvedListOfZ6095 = await invariants.resolver.findLexemesForItem( QID, PID, Z60 );
	return makeZWrapperForTrustedObject( resolvedListOfZ6095 );
}

function BUILTIN_EQUALS_WIKIDATA_ITEM_( firstItem, secondItem ) {
	return makeMappedResultEnvelope(
		makeBoolean( ( firstItem.Z6001K1.Z6091K1.Z6K1 === secondItem.Z6001K1.Z6091K1.Z6K1 ) ),
		null
	);
}

function BUILTIN_EQUALS_WIKIDATA_PROPERTY_( firstItem, secondItem ) {
	return makeZWrapperForTrustedObject(
		makeMappedResultEnvelope(
			makeBoolean( ( firstItem.Z6002K1.Z6092K1.Z6K1 === secondItem.Z6002K1.Z6092K1.Z6K1 ) ),
			null ) );
}

function BUILTIN_EQUALS_WIKIDATA_STATEMENT_( first, second ) {
	// Wikidata statements are small and simple - no container types – so deepEqual is suitable, and
	// allows for evolution in the content of statements (as constructed by convertStatement in
	// convert.js).
	return makeZWrapperForTrustedObject(
		makeMappedResultEnvelope(
			makeBoolean( deepEqual( first.asJSON(), second.asJSON() ) ),
			null ) );
}

function BUILTIN_EQUALS_WIKIDATA_LEXEME_FORM( firstItem, secondItem ) {
	return makeZWrapperForTrustedObject(
		makeMappedResultEnvelope(
			makeBoolean( ( firstItem.Z6004K1.Z6094K1.Z6K1 === secondItem.Z6004K1.Z6094K1.Z6K1 ) ),
			null ) );
}

function BUILTIN_EQUALS_WIKIDATA_LEXEME_SENSE( firstItem, secondItem ) {
	return makeZWrapperForTrustedObject(
		makeMappedResultEnvelope(
			makeBoolean( ( firstItem.Z6006K1.Z6096K1.Z6K1 === secondItem.Z6006K1.Z6096K1.Z6K1 ) ),
			null ) );
}

function BUILTIN_EQUALS_WIKIDATA_LEXEME( firstItem, secondItem ) {
	return makeZWrapperForTrustedObject(
		makeMappedResultEnvelope(
			makeBoolean( ( firstItem.Z6005K1.Z6095K1.Z6K1 === secondItem.Z6005K1.Z6095K1.Z6K1 ) ),
			null ) );
}

function BUILTIN_EQUALS_BOOLEAN_( firstBoolean, secondBoolean ) {
	const isFirstTrue = isTrue( firstBoolean );
	const isSecondTrue = isTrue( secondBoolean );
	const result = ( isFirstTrue && isSecondTrue ) || ( !isFirstTrue && !isSecondTrue );
	return makeZWrapperForTrustedObject(
		makeMappedResultEnvelope( makeBoolean( result ), null ) );
}

function getLanguageMap() {
	// TODO (T302342): switch to using require maybe?
	// TODO (T372692): Move getLanguageMap to utils.js & improve function coverage of builtins.js
	const path = 'function-schemata/data/definitions/naturalLanguages.json';
	return JSON.parse( fs.readFileSync( path, { encoding: 'utf8' } ) );
}

async function BUILTIN_LANGUAGE_CODE_TO_LANGUAGE_( ZString, invariants ) {
	const languages = getLanguageMap();
	const languageCode = ZString.Z6K1;

	let result = null;

	if ( !( languageCode in languages ) ) {
		result = makeErrorInNormalForm(
			error.language_code_not_found,
			[ wrapInZ6( languageCode ) ]
		);
		return makeMappedResultEnvelope( result );
	}
	const zid = languages[ languageCode ];
	const fetched = await invariants.resolver.dereferenceZObjects( [ zid ] );
	const resolvedZObject = fetched.get( zid );
	if ( responseEnvelopeContainsError( resolvedZObject ) ) {
		return resolvedZObject;
	}
	// Language objects shouldn't contain any internal references or anything,
	// so we should be able to treat them as trusted.
	return makeZWrapperForTrustedObject( makeMappedResultEnvelope( resolvedZObject.Z22K1.Z2K2 ) );
}

function BUILTIN_EQUALS_STRING_( firstZString, secondZString ) {
	return makeZWrapperForTrustedObject(
		makeMappedResultEnvelope(
			makeBoolean( ( firstZString.Z6K1 === secondZString.Z6K1 ) ),
			null ) );
}

function stringToCharsInternal( characterArray ) {
	const Z86Array = [];
	const typeZ86 = wrapInZ9( 'Z86' );
	for ( const character of characterArray ) {
		Z86Array.push( {
			Z1K1: wrapInZ9( 'Z86' ),
			Z86K1: wrapInZ6( character )
		} );
	}
	return convertArrayToKnownTypedList( Z86Array, typeZ86 );
}

function BUILTIN_STRING_TO_CHARS_( ZString ) {
	return makeZWrapperForTrustedObject(
		makeMappedResultEnvelope(
			stringToCharsInternal( Array.from( ZString.Z6K1 ) ),
			null ) );
}

function charsToStringInternal( list ) {
	const ZCodePoints = convertZListToItemArray( list || [] );
	const result = [];
	for ( const ZCodePoint of ZCodePoints ) {
		result.push( ZCodePoint.Z6K1 || ZCodePoint.Z86K1.Z6K1 );
	}
	return result;
}

function BUILTIN_CHARS_TO_STRING_( list ) {
	// TODO (T294482): Validate input is a List(Z86).
	return makeZWrapperForTrustedObject(
		makeMappedResultEnvelope(
			wrapInZ6( charsToStringInternal( list ).join( '' ) ),
			null ) );
}

async function applyFunctionToList( someFunction, itemArray, invariants ) {
	const promises = [];
	for ( const item of itemArray ) {
		const ZFunctionCall = {
			Z1K1: wrapInZ9( 'Z7' ),
			Z7K1: someFunction,
			K1: item
		};
		const wrappedZFunctionCall = ZWrapper.create( ZFunctionCall, item.getScope() );
		promises.push( invariants.executeWithCallCount( wrappedZFunctionCall, invariants ) );
	}
	return await Promise.all( promises );
}

async function BUILTIN_FILTER_FUNCTION_( someFunction, someList, invariants ) {
	const itemArray = convertZListToItemArray( someList || [] );
	const results = await applyFunctionToList( someFunction, itemArray, invariants );
	const goodResults = [];
	for ( let i = 0; i < results.length; ++i ) {
		const result = results[ i ];
		if ( responseEnvelopeContainsError( result ) ) {
			return result;
		}
		if ( !isZBoolean( result.Z22K1 ) ) {
			// Provided equality function did not return Boolean
			const argument = wrapInKeyReference( 'Z872K1' );
			const quotedValue = wrapInQuote( someFunction.asJSON() );
			return makeMappedResultEnvelope(
				null,
				makeErrorInNormalForm( error.argument_value_error, [ argument, quotedValue ] ) );
		}
		if ( isTrue( result.Z22K1 ) ) {
			goodResults.push( itemArray[ i ] );
		}
	}
	// TODO (T338407): Create a list of the same type as the input.
	const resultList = convertArrayToKnownTypedList( goodResults, wrapInZ9( 'Z1' ) );
	return makeMappedResultEnvelope( resultList );
}

async function BUILTIN_MAP_FUNCTION_( someFunction, someList, invariants ) {
	const itemArray = convertZListToItemArray( someList || [] );
	const results = await applyFunctionToList( someFunction, itemArray, invariants );
	const goodResults = [];
	for ( const result of results ) {
		if ( responseEnvelopeContainsError( result ) ) {
			return result;
		}
		goodResults.push( result.Z22K1 );
	}
	// TODO (T338407): Constrain list's type by return type of someFunction ...
	// TODO (T370028): ... BUT changing this will break a workaround currently
	// in use by the community, so tread carefully.
	const resultList = convertArrayToKnownTypedList( goodResults, wrapInZ9( 'Z1' ) );
	return makeMappedResultEnvelope( resultList );
}

async function BUILTIN_REDUCE_FUNCTION_( someFunction, someList, someObject, invariants ) {
	const itemArray = convertZListToItemArray( someList || [] );
	let resultSoFar = someObject;
	for ( const otherParameter of itemArray ) {
		const ZFunctionCall = {
			Z1K1: wrapInZ9( 'Z7' ),
			Z7K1: someFunction,
			K1: resultSoFar,
			K2: otherParameter
		};
		const wrappedZFunctionCall = ZWrapper.create( ZFunctionCall, resultSoFar.getScope() );
		const resultEnvelope =
			await invariants.executeWithCallCount( wrappedZFunctionCall, invariants );
		if ( responseEnvelopeContainsError( resultSoFar ) ) {
			return resultSoFar;
		}
		resultSoFar = resultEnvelope.Z22K1;
	}
	return makeMappedResultEnvelope( resultSoFar );
}

function BUILTIN_TRIGGER_METADATA_( keyZ6, valueZ1 ) {
	const response = makeMappedResultEnvelope( null, null );
	setMetadataValue( response, keyZ6, valueZ1 );
	return response;
}

function BUILTIN_SAME_( firstZCodePoint, secondZCodePoint ) {
	let result;
	if ( firstZCodePoint.Z86K1.Z6K1 === secondZCodePoint.Z86K1.Z6K1 ) {
		result = makeTrue();
	} else {
		result = makeFalse();
	}
	return makeZWrapperForTrustedObject( makeMappedResultEnvelope( result, null ) );
}

async function BUILTIN_EQUALS_LIST_( list1, list2, ZFunction, invariants ) {
	let trustedResult;
	while ( !isEmptyZList( list1 ) && !isEmptyZList( list2 ) ) {
		const headEqualityZ22 = await invariants.executeWithCallCount(
			ZWrapper.create(
				{
					Z1K1: wrapInZ9( 'Z7' ),
					Z7K1: ZFunction,
					K1: getHead( list1 ),
					K2: getHead( list2 )
				},
				new EmptyFrame()
			),
			invariants
		);
		if ( responseEnvelopeContainsError( headEqualityZ22 ) ) {
			trustedResult = headEqualityZ22;
			break;
		} else if ( !isZBoolean( headEqualityZ22.Z22K1 ) ) {
			// Provided equality function did not return Boolean
			const argument = wrapInKeyReference( 'Z889K3' );
			const quotedValue = wrapInQuote( ZFunction.asJSON() );
			return makeMappedResultEnvelope(
				null,
				makeErrorInNormalForm( error.argument_value_error, [ argument, quotedValue ] )
			);
		} else if ( !isTrue( headEqualityZ22.Z22K1 ) ) {
			trustedResult = makeMappedResultEnvelope( makeFalse() );
			break;
		}
		list1 = getTail( list1 );
		list2 = getTail( list2 );
	}
	if ( trustedResult === undefined ) {
		trustedResult = makeMappedResultEnvelope(
			makeBoolean( isEmptyZList( list1 ) === isEmptyZList( list2 ) )
		);
	}
	return makeZWrapperForTrustedObject( trustedResult );
}

function BUILTIN_UNQUOTE_( ZQuote ) {
	return unquoteVeryCarefully( ZQuote );
}

function makeValidatorResultEnvelope( ZObject, errors ) {
	if ( errors.length === 0 ) {
		// TODO (T383573): Consider not calling asJSON here.
		return makeMappedResultEnvelope( ZObject.asJSON(), null );
	} else if ( errors.length === 1 ) {
		return makeMappedResultEnvelope( null, errors[ 0 ] );
	} else {
		return makeMappedResultEnvelope( null, ErrorFormatter.createZErrorList( errors ) );
	}
}

async function resolveAllZ4Keys_( ZType, invariants ) {
	const resolvedKeys = new Set();
	const typesToResolve = [];
	typesToResolve.push( ZType );
	while ( typesToResolve.length > 0 ) {
		const toResolve = typesToResolve.pop( 0 );
		await traverseZList( toResolve.Z4K2, async ( Z3Tail ) => {
			await ( Z3Tail.resolveEphemeral( [ 'K1', 'Z3K1' ], invariants, /* ignoreList= */null, /* resolveInternals= */false ) );
			const nextType = Z3Tail.K1.getNameEphemeral( 'Z3K1' );
			const key = createZObjectKey( nextType ).asString();
			if ( resolvedKeys.has( key ) ) {
				return;
			}
			resolvedKeys.add( key );
			typesToResolve.push( nextType );
		} );
	}
}

async function BUILTIN_SCHEMA_VALIDATOR_( quotedObject, quotedType, invariants ) {
	// TODO (T290698): Use this instead of BUILTIN_EMPTY_VALIDATOR_.
	const ZObject = unquoteBrashly( quotedObject );
	const ZType = ( await ( unquoteBrashly( quotedType ).resolve(
		invariants, /* originalObject= */null, /* ignoreList= */null,
		/* resolveInternals= */ false, /* doValidate= */ false ) ) ).Z22K1;

	// Ensure all internal type references are resolved.
	// TODO (T297904): Also need to resolve generic types.
	await resolveAllZ4Keys_( ZType, invariants );
	// TODO (T381597): asJSON() will be unnecessary once JSON schema is gone.
	const theSchema = createSchema( { Z1K1: ZType.asJSONEphemeral() } );

	// TODO (T294289): Return validationStatus Z5s as Z22K2.
	// TODO (T381597): asJSON() will be unnecessary once JSON schema is gone.
	const theStatus = theSchema.validateStatus( ZObject.asJSON() );
	let errors;
	if ( theStatus.isValid() ) {
		errors = [];
	} else {
		errors = [ theStatus.getZ5() ];
	}

	return makeValidatorResultEnvelope( ZObject, errors );
}

function BUILTIN_EMPTY_VALIDATOR_( targetZObject ) {
	return makeMappedResultEnvelope( targetZObject, null );
}

/**
 * Validates the keys of a normal Typed List. This functions looks for duplicate or non-sequential
 * keys and keys that don't follow the expected format of (Z)?<identity>Kn.
 *
 * @param {Object} list the Typed List being validated.
 * @param {Function} key a function to get the key of a list element.
 * @param {string} identity the identity of the list's parent.
 *
 * @return {Object} a Typed List of Z5/Error.
 */
function arrayValidator( list, key, identity ) {
	const keys = convertZListToItemArray( list || [] ).map( key );
	const errorInfo = [];

	const seen = new Set();
	for ( let i = 0; i < keys.length; ++i ) {
		const originalKey = keys[ i ];
		let key = originalKey;
		if ( isGlobalKey( key ) ) {
			if ( !originalKey.startsWith( identity ) ) {
				errorInfo.push( { indexInt: i, key: originalKey } );
				continue;
			}
			key = kidFromGlobalKey( key );
		}
		const expectedIndex = i + 1;
		const actualIndex = Number( key.replace( 'K', '' ) );
		if ( seen.has( originalKey ) ) {
			errorInfo.push( { indexInt: i, key: originalKey } );
			continue;
		} else {
			seen.add( originalKey );
		}

		if ( actualIndex !== expectedIndex ) {
			errorInfo.push( { indexInt: i, key: originalKey } );
		}
	}

	return errorInfo.map(
		( info ) => {
			const index = wrapInZ6( info.indexInt.toString() );
			const innerError = makeErrorInNormalForm( error.invalid_key, [ wrapInZ6( info.key ) ] );
			return makeErrorInNormalForm(
				error.array_element_not_well_formed,
				[ index, innerError ]
			);
		}
	);
}

function BUILTIN_FUNCTION_VALIDATOR_( ZQuote ) {
	const ZObject = unquoteBrashly( ZQuote );
	let functionIdentity = findFunctionIdentity( ZObject );
	if ( functionIdentity ) {
		functionIdentity = functionIdentity.Z9K1;
	}
	const errors = arrayValidator(
		ZObject.Z8K1,
		( x ) => x.Z17K2.Z6K1,
		functionIdentity
	);

	return makeValidatorResultEnvelope( ZQuote, errors );
}

function BUILTIN_Z4_TYPE_VALIDATOR_( ZQuote ) {
	const ZObject = unquoteBrashly( ZQuote );
	let typeIdentity = findTypeIdentity( ZObject );
	if ( typeIdentity && isZReference( typeIdentity ) ) {
		typeIdentity = typeIdentity.Z9K1;
	}
	const errors = arrayValidator(
		ZObject.Z4K2,
		( x ) => x.Z3K2.Z6K1,
		typeIdentity
	);

	return makeValidatorResultEnvelope( ZQuote, errors );
}

async function BUILTIN_FUNCTION_CALL_VALIDATOR_INTERNAL_( ZQuote, errors, invariants ) {
	const ZObject = unquoteBrashly( ZQuote );
	const { getArgumentStates } = require( './execute.js' );
	const argumentStates = await getArgumentStates( ZObject, invariants );
	const dictDict = {};
	for ( const argumentState of argumentStates ) {
		if ( argumentState.state === 'ERROR' ) {
			// This is probably because Z8K1 couldn't be dereferenced and is
			// fine.
			return;
		}
		const argumentDict = argumentState.argumentDict;
		dictDict[ argumentDict.name ] = argumentDict;
		const localKey = argumentDict.name.replace( /^Z\d+/, '' );
		dictDict[ localKey ] = argumentDict;
	}

	const keysToSkip = new Set( [ 'Z1K1', 'Z7K1' ] );

	// TODO (T296668): Also check declared arguments that are absent from the Z7.
	// TODO (T296668): Also check local keys.
	for ( const key of ZObject.keys() ) {
		if ( keysToSkip.has( key ) ) {
			continue;
		}
		const argumentDict = dictDict[ key ];
		if ( argumentDict === undefined ) {
			errors.push(
				makeErrorInNormalForm(
					// Invalid key for function call
					error.invalid_key,
					[ wrapInZ6( key ) ]
				)
			);
			continue;
		}
		let type = ZObject[ key ].Z1K1.Z9K1 || ZObject[ key ].Z1K1;
		// TODO (T383575): These calls to asJSON could be unneeded if we compared
		// declaredtype to type with compareTypes.
		if ( type instanceof ZWrapper ) {
			type = type.asJSON();
		}
		let declaredType = argumentDict.declaredType;
		if ( declaredType instanceof ZWrapper ) {
			declaredType = declaredType.asJSON();
		}
		// TODO (T296669): Fix type semantics below; do something when declaredType is a Z4.
		if ( isZType( declaredType ) ) {
			continue;
		}
		if ( isZReference( declaredType ) ) {
			declaredType = declaredType.Z9K1;
		}

		// Type mismatches for Z7, Z9, and Z18 will be caught at runtime.
		const skippableTypes = new Set( [ 'Z18', 'Z9', 'Z7' ] );
		// TODO (T296669): More intricate subtype semantics once we have generic
		// types (just checking for Z1 is not sufficient).
		if ( !( declaredType === type || declaredType === 'Z1' || skippableTypes.has( type ) ) ) {
			errors.push(
				makeErrorInNormalForm(
					error.argument_type_mismatch,
					[ declaredType, type, ZObject[ key ].asJSON() ]
				)
			);
		}
	}
}

async function BUILTIN_FUNCTION_CALL_VALIDATOR_( ZQuote, invariants ) {
	const errors = [];
	await BUILTIN_FUNCTION_CALL_VALIDATOR_INTERNAL_( ZQuote, errors, invariants );

	return makeValidatorResultEnvelope( ZQuote, errors );
}

async function BUILTIN_MULTILINGUAL_TEXT_VALIDATOR_( ZQuote, invariants ) {
	const ZObject = unquoteBrashly( ZQuote );
	const errors = [];
	const ZMonolingualStrings = convertZListToItemArray( ZObject.Z12K1 || [] );
	const languages = await Promise.all( ZMonolingualStrings.map( async ( ZMonolingualString ) => ( await ZMonolingualString.resolveKey( [ 'Z11K1', 'Z60K1', 'Z6K1' ], invariants ) ).Z22K1.Z6K1
	) );

	const seen = new Set();
	for ( let i = 0; i < languages.length; ++i ) {
		if ( seen.has( languages[ i ] ) ) {
			const argument = wrapInKeyReference( 'Z112K1' );
			errors.push(
				makeErrorInNormalForm( error.argument_value_error, [ argument, ZQuote ] )
			);
		}

		seen.add( languages[ i ] );
	}

	return makeValidatorResultEnvelope( ZQuote, errors );
}

function BUILTIN_MULTILINGUAL_STRINGSET_VALIDATOR_( ZQuote ) {
	const ZObject = unquoteBrashly( ZQuote );
	const errors = [];
	const ZMonolingualStringSets = convertZListToItemArray( ZObject.Z32K1 || [] );
	const languages = ZMonolingualStringSets.map(
		( ZMonolingualStringSet ) => ZMonolingualStringSet.Z31K1.Z60K1.Z6K1
	);

	const seen = new Set();
	for ( let i = 0; i < languages.length; ++i ) {
		if ( seen.has( languages[ i ] ) ) {
			const argument = wrapInKeyReference( 'Z132K1' );
			errors.push(
				makeErrorInNormalForm( error.argument_value_error, [ argument, ZQuote ] )
			);
		}

		seen.add( languages[ i ] );
	}

	return makeValidatorResultEnvelope( ZQuote, errors );
}

function BUILTIN_ERROR_TYPE_VALIDATOR_( ZQuote ) {
	return makeValidatorResultEnvelope( ZQuote, [] );
}

function resolveListType( typeZ4 ) {
	const itsMe = {
		Z1K1: wrapInZ9( 'Z7' ),
		Z7K1: wrapInZ9( 'Z881' ),
		Z881K1: typeZ4
	};
	const ZType = {
		Z1K1: wrapInZ9( 'Z4' ),
		Z4K1: itsMe,
		Z4K2: convertArrayToKnownTypedList(
			[
				Z3For( typeZ4, wrapInZ6( 'K1' ), Z12For( 'head' ) ),
				Z3For( itsMe, wrapInZ6( 'K2' ), Z12For( 'tail' ) )
			],
			wrapInZ9( 'Z3' )
		),
		Z4K3: wrapInZ9( 'Z831' )
	};
	return makeMappedResultEnvelope( ZType, null );
}

function BUILTIN_GENERIC_LIST_TYPE_( typeZ4 ) {
	return resolveListType( typeZ4 );
}

function BUILTIN_GENERIC_PAIR_TYPE_( firstType, secondType ) {
	const itsMe = {
		Z1K1: wrapInZ9( 'Z7' ),
		Z7K1: wrapInZ9( 'Z882' ),
		Z882K1: firstType,
		Z882K2: secondType
	};
	const ZType = {
		Z1K1: wrapInZ9( 'Z4' ),
		Z4K1: itsMe,
		Z4K2: convertArrayToKnownTypedList( [
			Z3For( firstType, wrapInZ6( 'K1' ), Z12For( 'first' ) ),
			Z3For( secondType, wrapInZ6( 'K2' ), Z12For( 'second' ) )
		], wrapInZ9( 'Z3' ) ),
		Z4K3: wrapInZ9( 'Z831' )
	};
	return makeMappedResultEnvelope( ZType, null );
}

function BUILTIN_GENERIC_MAP_TYPE_( keyType, valueType, invariants ) {
	// TODO (T302015) When ZMap keys are extended beyond Z6/String, update accordingly
	const allowedKeyTypes = [ 'Z6', 'Z39' ];
	let identity = keyType;
	while ( identity.Z4K1 !== undefined ) {
		identity = identity.Z4K1;
	}
	if ( !allowedKeyTypes.includes( identity.Z9K1 ) ) {
		const argument = wrapInKeyReference( 'Z883K1' );
		const quotedValue = wrapInQuote( keyType.asJSON() );
		const newError = makeErrorInNormalForm(
			error.argument_value_error,
			[ argument, quotedValue ]
		);
		return makeMappedResultEnvelope( null, newError );
	}
	const itsMe = {
		Z1K1: wrapInZ9( 'Z7' ),
		Z7K1: wrapInZ9( 'Z883' ),
		Z883K1: keyType,
		Z883K2: valueType
	};
	const pairType = BUILTIN_GENERIC_PAIR_TYPE_( keyType, valueType ).Z22K1;
	const listType = BUILTIN_GENERIC_LIST_TYPE_( pairType ).Z22K1;
	const ZType = {
		Z1K1: wrapInZ9( 'Z4' ),
		Z4K1: itsMe,
		Z4K2: convertArrayToKnownTypedList(
			[ Z3For( listType, wrapInZ6( 'K1' ), Z12For( 'elements' ) ) ],
			wrapInZ9( 'Z3' )
		),
		Z4K3: wrapInZ9( 'Z831' )
	};
	return makeMappedResultEnvelope( ZType, null );
}

async function BUILTIN_ERRORTYPE_TO_TYPE_( errorType, invariants ) {
	const resolutionEnvelope = await errorType.Z50K1.resolve(
		invariants,
		/* ignoreList= */ null,
		/* resolveInternals= */ false,
		/* doValidate= */ false );
	const theKeys = resolutionEnvelope.Z22K1;
	const itsMe = {
		Z1K1: wrapInZ9( 'Z7' ),
		Z7K1: wrapInZ9( 'Z885' ),
		Z885K1: errorType
	};
	const theType = {
		Z1K1: wrapInZ9( 'Z4' ),
		Z4K1: itsMe,
		Z4K2: theKeys,
		Z4K3: wrapInZ9( 'Z101' )
	};
	return makeMappedResultEnvelope( theType, null );
}

const builtinFunctions = new Map();

builtinFunctions.set( 'Z901', BUILTIN_ECHO_ );
builtinFunctions.set( 'Z902', BUILTIN_IF_ );
builtinFunctions.set( 'Z903', BUILTIN_VALUE_BY_KEY_ );
builtinFunctions.set( 'Z904', BUILTIN_VALUES_BY_KEYS_ );
builtinFunctions.set( 'Z905', BUILTIN_REIFY_ );
builtinFunctions.set( 'Z908', BUILTIN_ABSTRACT_ );
builtinFunctions.set( 'Z910', BUILTIN_CONS_ );
builtinFunctions.set( 'Z911', BUILTIN_HEAD_ );
builtinFunctions.set( 'Z912', BUILTIN_TAIL_ );
builtinFunctions.set( 'Z913', BUILTIN_EMPTY_ );
builtinFunctions.set( 'Z920', BUILTIN_TRIGGER_METADATA_ );
builtinFunctions.set( 'Z921', BUILTIN_FIRST_ );
builtinFunctions.set( 'Z922', BUILTIN_SECOND_ );
builtinFunctions.set( 'Z923', BUILTIN_GET_ENVELOPE_ );
builtinFunctions.set( 'Z928', BUILTIN_FETCH_PERSISTENT_ZOBJECT_ );
builtinFunctions.set( 'Z931', BUILTIN_SCHEMA_VALIDATOR_ );
builtinFunctions.set( 'Z944', BUILTIN_EQUALS_BOOLEAN_ );
builtinFunctions.set( 'Z960', BUILTIN_LANGUAGE_CODE_TO_LANGUAGE_ );
builtinFunctions.set( 'Z966', BUILTIN_EQUALS_STRING_ );
builtinFunctions.set( 'Z968', BUILTIN_STRING_TO_CHARS_ );
builtinFunctions.set( 'Z972', BUILTIN_FILTER_FUNCTION_ );
builtinFunctions.set( 'Z973', BUILTIN_MAP_FUNCTION_ );
builtinFunctions.set( 'Z976', BUILTIN_REDUCE_FUNCTION_ );
builtinFunctions.set( 'Z981', BUILTIN_GENERIC_LIST_TYPE_ );
builtinFunctions.set( 'Z982', BUILTIN_GENERIC_PAIR_TYPE_ );
builtinFunctions.set( 'Z983', BUILTIN_GENERIC_MAP_TYPE_ );
builtinFunctions.set( 'Z985', BUILTIN_ERRORTYPE_TO_TYPE_ );
builtinFunctions.set( 'Z986', BUILTIN_CHARS_TO_STRING_ );
builtinFunctions.set( 'Z988', BUILTIN_SAME_ );
builtinFunctions.set( 'Z989', BUILTIN_EQUALS_LIST_ );
builtinFunctions.set( 'Z999', BUILTIN_UNQUOTE_ );

// Wikidata-related Builtins
builtinFunctions.set( 'Z6921', BUILTIN_FETCH_WIKIDATA_ITEM_ );
builtinFunctions.set( 'Z6922', BUILTIN_FETCH_WIKIDATA_PROPERTY_ );
builtinFunctions.set( 'Z6924', BUILTIN_FETCH_WIKIDATA_LEXEME_FORM_ );
builtinFunctions.set( 'Z6925', BUILTIN_FETCH_WIKIDATA_LEXEME_ );
builtinFunctions.set( 'Z6926', BUILTIN_FETCH_WIKIDATA_LEXEME_SENSE_ );
builtinFunctions.set( 'Z6930', BUILTIN_FIND_LEXEMES_FOR_ITEM_ );
builtinFunctions.set( 'Z6901', BUILTIN_EQUALS_WIKIDATA_ITEM_ );
builtinFunctions.set( 'Z6902', BUILTIN_EQUALS_WIKIDATA_PROPERTY_ );
builtinFunctions.set( 'Z6903', BUILTIN_EQUALS_WIKIDATA_STATEMENT_ );
builtinFunctions.set( 'Z6904', BUILTIN_EQUALS_WIKIDATA_LEXEME_FORM );
builtinFunctions.set( 'Z6905', BUILTIN_EQUALS_WIKIDATA_LEXEME );
builtinFunctions.set( 'Z6906', BUILTIN_EQUALS_WIKIDATA_LEXEME_SENSE );

// validators
builtinFunctions.set( 'Z201', BUILTIN_EMPTY_VALIDATOR_ );
builtinFunctions.set( 'Z202', BUILTIN_EMPTY_VALIDATOR_ );
builtinFunctions.set( 'Z203', BUILTIN_EMPTY_VALIDATOR_ );
builtinFunctions.set( 'Z204', BUILTIN_Z4_TYPE_VALIDATOR_ );
builtinFunctions.set( 'Z205', BUILTIN_EMPTY_VALIDATOR_ );
builtinFunctions.set( 'Z206', BUILTIN_EMPTY_VALIDATOR_ );
builtinFunctions.set( 'Z207', BUILTIN_FUNCTION_CALL_VALIDATOR_ );
builtinFunctions.set( 'Z208', BUILTIN_FUNCTION_VALIDATOR_ );
builtinFunctions.set( 'Z209', BUILTIN_EMPTY_VALIDATOR_ );
builtinFunctions.set( 'Z211', BUILTIN_EMPTY_VALIDATOR_ );
builtinFunctions.set( 'Z212', BUILTIN_MULTILINGUAL_TEXT_VALIDATOR_ );
builtinFunctions.set( 'Z213', BUILTIN_EMPTY_VALIDATOR_ );
builtinFunctions.set( 'Z214', BUILTIN_EMPTY_VALIDATOR_ );
builtinFunctions.set( 'Z216', BUILTIN_EMPTY_VALIDATOR_ );
builtinFunctions.set( 'Z217', BUILTIN_EMPTY_VALIDATOR_ );
builtinFunctions.set( 'Z218', BUILTIN_EMPTY_VALIDATOR_ );
builtinFunctions.set( 'Z220', BUILTIN_EMPTY_VALIDATOR_ );
builtinFunctions.set( 'Z221', BUILTIN_EMPTY_VALIDATOR_ );
builtinFunctions.set( 'Z222', BUILTIN_EMPTY_VALIDATOR_ );
builtinFunctions.set( 'Z223', BUILTIN_EMPTY_VALIDATOR_ );
builtinFunctions.set( 'Z231', BUILTIN_EMPTY_VALIDATOR_ );
builtinFunctions.set( 'Z232', BUILTIN_MULTILINGUAL_STRINGSET_VALIDATOR_ );
builtinFunctions.set( 'Z239', BUILTIN_EMPTY_VALIDATOR_ );
builtinFunctions.set( 'Z240', BUILTIN_EMPTY_VALIDATOR_ );
builtinFunctions.set( 'Z241', BUILTIN_EMPTY_VALIDATOR_ );
builtinFunctions.set( 'Z242', BUILTIN_EMPTY_VALIDATOR_ );
builtinFunctions.set( 'Z250', BUILTIN_ERROR_TYPE_VALIDATOR_ );
builtinFunctions.set( 'Z260', BUILTIN_EMPTY_VALIDATOR_ );
builtinFunctions.set( 'Z261', BUILTIN_EMPTY_VALIDATOR_ );
builtinFunctions.set( 'Z270', BUILTIN_EMPTY_VALIDATOR_ );
builtinFunctions.set( 'Z280', BUILTIN_EMPTY_VALIDATOR_ );
builtinFunctions.set( 'Z286', BUILTIN_EMPTY_VALIDATOR_ );
builtinFunctions.set( 'Z289', BUILTIN_EMPTY_VALIDATOR_ );
builtinFunctions.set( 'Z299', BUILTIN_EMPTY_VALIDATOR_ );

/**
 * Retrieves an in-memory JS function implementing a builtin.
 *
 * @param {Object} ZID the function to retrieve an implementation for
 * @return {Implementation} an implementation
 */
function getFunction( ZID ) {
	const result = builtinFunctions.get( ZID );
	if ( result === undefined ) {
		return null;
	}
	return result;
}

const lazyFunctions = new Map();
lazyFunctions.set( 'Z902', [ 'Z802K2', 'Z802K3' ] );

/**
 * Retrieves lazy variables for the given function.
 *
 * @param {string} ZID the function
 * @return {Array} an array of variables which are lazy for the given function
 */
function getLazyVariables( ZID ) {
	let lazy = lazyFunctions.get( ZID );
	if ( lazy === undefined ) {
		lazy = [];
	}
	return lazy;
}

const lazyReturns = new Set();
lazyReturns.add( 'Z902' );

/**
 * For a given ZID, determine whether return value should be evaluated after execution.
 *
 * @param {string} ZID the function
 * @return {boolean} whether the function is lazy
 */
function getLazyReturn( ZID ) {
	return lazyReturns.has( ZID );
}

const builtinReferences = new Map();

function definitionFileForZid( ZID ) {
	return `function-schemata/data/definitions/${ ZID }.json`;
}

function getPersistentZObjectFromFile( ZID ) {
	const fileName = definitionFileForZid( ZID );
	return readJSON( fileName );
}

function getDefinitionFromFile( ZID ) {
	const fileName = definitionFileForZid( ZID );
	return readJSON( fileName ).Z2K2;
}

// Built-in implementations.
const implementationZIDs = [
	'Z901', 'Z902', 'Z903', 'Z904', 'Z905', 'Z908', 'Z910', 'Z911', 'Z912',
	'Z913', 'Z920', 'Z921', 'Z922', 'Z923', 'Z928', 'Z944', 'Z960', 'Z966', 'Z968',
	'Z972', 'Z973', 'Z976',
	// TODO (T314383): Add these ZIDs to the list of implementations. See below.
	/*
	 * 'Z981', 'Z982',
	 */
	// TODO (T314364): Add this ZID to the list of implementations. See below.
	/*
	 * 'Z983',
	 */
	'Z986', 'Z988', 'Z989', 'Z999', 'Z931',
	// Wikidata-related implementations
	'Z6901', 'Z6902', 'Z6903', 'Z6904', 'Z6905', 'Z6906',
	'Z6921', 'Z6922', 'Z6924', 'Z6925', 'Z6926', 'Z6930'
];

// Built-in functions.
const functionZIDs = [
	'Z801', 'Z802', 'Z803', 'Z804', 'Z805', 'Z808', 'Z810', 'Z811', 'Z812',
	'Z813', 'Z820', 'Z821', 'Z822', 'Z823', 'Z828', 'Z844', 'Z860', 'Z866', 'Z868',
	'Z872', 'Z873', 'Z876',
	'Z881', 'Z882', 'Z883', 'Z886', 'Z888', 'Z889', 'Z899', 'Z831',
	'Z6801', 'Z6802', 'Z6803', 'Z6804', 'Z6805', 'Z6806',
	'Z6821', 'Z6822', 'Z6824', 'Z6825', 'Z6826', 'Z6830'
];

// Validators for core types.
const validatorZIDs = [
	'Z101', 'Z102', 'Z103', 'Z104', 'Z105', 'Z106', 'Z107', 'Z108',
	'Z109', 'Z111', 'Z112', 'Z114', 'Z116', 'Z117', 'Z118', 'Z120',
	'Z121', 'Z122', 'Z123', 'Z139', 'Z140', 'Z150', 'Z160',
	'Z161', 'Z180', 'Z186', 'Z189', 'Z199',
	'Z201', 'Z202', 'Z203', 'Z204', 'Z205', 'Z206', 'Z207', 'Z208',
	'Z209', 'Z211', 'Z212', 'Z214', 'Z216', 'Z217', 'Z218', 'Z220',
	'Z221', 'Z222', 'Z223', 'Z239', 'Z240', 'Z250', 'Z260',
	'Z261', 'Z280', 'Z286', 'Z289', 'Z299'
];

( function setBuiltinReferences() {
	const implementations = new Map();
	const definitions = new Map();
	for ( const ZID of implementationZIDs ) {
		const theDefinition = getDefinitionFromFile( ZID );
		implementations.set( ZID, theDefinition );
		definitions.set( ZID, theDefinition );
	}

	// TODO (T314364): Undo this special case for Typed Map.
	// We do this because Map has some special validation logic which can't be
	// expressed in the JSON definition without Unions. UNIONIZE NOW
	// TODO (T314383): Undo these special cases for generic List and Pair.
	// Compositions don't currently execute in time for schema validation, so
	// the composition implementations of built-in generic functions produce
	// types which allow anything to validate.
	for ( const ZIDMod100 of [ 81, 82, 83 ] ) {
		const functionZID = 'Z' + ( 800 + ZIDMod100 );
		const implementationZID = 'Z' + ( 900 + ZIDMod100 );
		const theImplementation = {
			Z1K1: 'Z14',
			Z14K1: functionZID,
			Z14K4: wrapInZ6( implementationZID )
		};
		implementations.set( implementationZID, theImplementation );
		definitions.set( implementationZID, theImplementation );
	}

	for ( const ZID of functionZIDs ) {
		const theDefinition = getDefinitionFromFile( ZID );
		const Z8K4 = [];
		for ( const element of theDefinition.Z8K4 ) {
			const implementationDefinition = implementations.get( element );
			if ( implementationDefinition === undefined ) {
				Z8K4.push( element );
			} else {
				Z8K4.push( implementationDefinition );
			}
		}
		theDefinition.Z8K4 = Z8K4;
		definitions.set( ZID, theDefinition );
	}
	for ( const ZID of validatorZIDs ) {
		const theDefinition = getDefinitionFromFile( ZID );
		definitions.set( ZID, theDefinition );
	}
	for ( const ZID of builtInTypes() ) {
		const theDefinition = getDefinitionFromFile( ZID );
		definitions.set( ZID, theDefinition );
	}
	for ( const entry of definitions.entries() ) {
		const ZID = entry[ 0 ];
		const definition = entry[ 1 ];
		const normalizedDefinition = ( normalize( definition ) ).Z22K1;
		builtinReferences.set( ZID, normalizedDefinition );
	}
}() );

/**
 * Creates a Z8 corresponding to a bulitin function.
 *
 * @param {string} ZID reference to a builtin function
 * @return {Object} a Z8 or null
 */
function resolveBuiltinReference( ZID ) {
	const result = builtinReferences.get( ZID );
	if ( result === undefined ) {
		return null;
	}
	return result;
}

module.exports = {
	builtinReferences, getFunction, getLazyVariables, getLazyReturn, resolveBuiltinReference,
	resolveListType, getLanguageMap
};
