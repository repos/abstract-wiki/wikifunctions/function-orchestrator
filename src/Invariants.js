'use strict';

/*!
 * Wikifunctions orchestrator class to handle items that do not vary during execution.
 *
 * @copyright 2020– Abstract Wikipedia team; see LICENSE
 * @license MIT
 */

const { dataDir } = require( '../function-schemata/javascript/src/fileUtils.js' );
const { rateLimits } = require( './rateLimits.js' );
const { readJSON } = require( './utils.js' );
const softwareLanguages = readJSON( dataDir( 'definitions/softwareLanguages.json' ) );

/**
 * Encapsulates objects which will not change over the course of a function execution.
 */
class Invariants {
	constructor(
		resolver, evaluators, orchestratorConfig, getRemainingTime,
		requestId, logger, req = null, rateLimiter = null,
		validateTypesWithCallCount = null, eagerlyEvaluateWithCallCount = null,
		resolveDanglingReferencesWithCallCount = null, executeWithCallCount = null ) {
		this.resolver_ = resolver;
		this.languageToEvaluatorIndex_ = new Map();
		this.orchestratorConfig_ = Object.freeze( orchestratorConfig );
		this.getRemainingTime_ = getRemainingTime;
		this.requestId_ = requestId;
		this.logger_ = logger;
		this.req_ = req;
		this.executeWithCallCount_ = executeWithCallCount;

		// We rely on the ordering of these evaluators, so we prohibit modifications
		// of the list.
		// evaluators get decided based on idx/mapping
		this.evaluators_ = Object.freeze( evaluators );
		if ( rateLimiter === null ) {
			rateLimiter = rateLimits;
		}
		this.rateLimiter_ = rateLimiter;
		for ( let i = 0; i < evaluators.length; ++i ) {
			const evaluator = this.evaluators_[ i ];
			for ( const programmingLanguage of evaluator.programmingLanguages ) {
				this.languageToEvaluatorIndex_.set( programmingLanguage, i );
				this.languageToEvaluatorIndex_.set( softwareLanguages[ programmingLanguage ], i );
			}
			// Evaluators need access to invariants for re-entrant calls.
			evaluator.setInvariants( this );
		}
		// Resolver wraps MediaWiki for the purpose of calling wikilambda-fetch.
		Object.defineProperty( this, 'resolver', {
			get: function () {
				return resolver;
			}
		} );
		Object.defineProperty( this, 'orchestratorConfig', {
			get: function () {
				return this.orchestratorConfig_;
			}
		} );
		Object.defineProperty( this, 'requestId', {
			get: function () {
				return this.requestId_;
			}
		} );
		Object.defineProperty( this, 'logger', {
			get: function () {
				return this.logger_;
			}
		} );
		Object.defineProperty( this, 'req', {
			get: function () {
				return this.req_;
			}
		} );
		Object.defineProperty( this, 'rateLimiter', {
			get: function () {
				return this.rateLimiter_;
			}
		} );
		if ( validateTypesWithCallCount === null ) {
			const { runTypeValidatorDynamic } = require( './validation.js' );
			validateTypesWithCallCount = runTypeValidatorDynamic;
		}
		Object.defineProperty( this, 'validateTypesWithCallCount', {
			get: function () {
				return validateTypesWithCallCount;
			}
		} );
		if ( eagerlyEvaluateWithCallCount === null ) {
			const { eagerlyEvaluate } = require( './execute.js' );
			eagerlyEvaluateWithCallCount = eagerlyEvaluate;
		}
		Object.defineProperty( this, 'eagerlyEvaluateWithCallCount', {
			get: function () {
				return eagerlyEvaluateWithCallCount;
			}
		} );
		if ( resolveDanglingReferencesWithCallCount === null ) {
			const { resolveDanglingReferences } = require( './execute.js' );
			resolveDanglingReferencesWithCallCount = resolveDanglingReferences;
		}
		Object.defineProperty( this, 'resolveDanglingReferencesWithCallCount', {
			get: function () {
				return resolveDanglingReferencesWithCallCount;
			}
		} );
		if ( executeWithCallCount === null ) {
			const { execute } = require( './execute.js' );
			executeWithCallCount = execute;
		}
		Object.defineProperty( this, 'executeWithCallCount', {
			get: function () {
				return executeWithCallCount;
			}
		} );
	}

	/**
	 * Finds the corresponding Evaluator for the given programming language
	 * and returns the Evaluator object
	 *
	 * @param {string} programmingLanguage e.g. 'javascript'
	 * @return {Object|null} Evaluator object or null if none found
	 */
	evaluatorFor( programmingLanguage ) {
		const evaluatorIndex = this.languageToEvaluatorIndex_.get( programmingLanguage );
		if ( evaluatorIndex === undefined ) {
			return null;
		}
		return this.evaluators_[ evaluatorIndex ];
	}

	/**
	 * Finds the mapped/corresponding ZID of the programming language literal.
	 *
	 * @param {Object} codingLanguageObject a ZObject
	 * @return {string|null} String the ZID of the programming language (e.g. 'Z600') or null
	 */
	mappedProgrammingLangObj( codingLanguageObject ) {
		const explicitStringLiteral = codingLanguageObject.Z61K1.Z6K1;
		if ( explicitStringLiteral !== null ) {
			// map string literal to its ZID via schemata
			return softwareLanguages[ explicitStringLiteral ] || null;
		}
		return null;
	}

	getRemainingTime() {
		return this.getRemainingTime_();
	}
}

module.exports = { Invariants };
