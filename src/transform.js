'use strict';

/*!
 * Wikifunctions orchestrator code to convert raw Wikidata entities into ZObjects.
 *
 * @copyright 2020– Abstract Wikipedia team; see LICENSE
 * @license MIT
 */

const {
	convertArrayToKnownTypedList,
	wrapInZ6,
	wrapInZ9,
	isItemId,
	isPropertyId,
	isLexemeId,
	isLexemeFormId,
	isLexemeSenseId,
	isObject
} = require( '../function-schemata/javascript/src/utils.js' );
const { getLanguageMap } = require( './builtins.js' );

const languageMap = getLanguageMap();

// ZIDs for the Wikidata-based types and their keys
const ITEM_TYPE = 'Z6001';
const ITEM_IDENTITY = 'Z6001K1';
const ITEM_LABELS = 'Z6001K2';
const ITEM_DESCRIPTIONS = 'Z6001K3';
const ITEM_ALIASES = 'Z6001K4';
const ITEM_CLAIMS = 'Z6001K5';
const PROPERTY_TYPE = 'Z6002';
const PROPERTY_IDENTITY = 'Z6002K1';
const PROPERTY_LABELS = 'Z6002K2';
const PROPERTY_DESCRIPTIONS = 'Z6002K3';
const PROPERTY_ALIASES = 'Z6002K4';
const PROPERTY_CLAIMS = 'Z6002K5';
const STATEMENT_TYPE = 'Z6003';
const STATEMENT_SUBJECT = 'Z6003K1';
const STATEMENT_PREDICATE = 'Z6003K2';
const STATEMENT_VALUE = 'Z6003K3';
const STATEMENT_RANK = 'Z6003K4';
const LEXEME_FORM_TYPE = 'Z6004';
const LEXEME_FORM_IDENTITY = 'Z6004K1';
const LEXEME_FORM_LEXEME = 'Z6004K2';
const LEXEME_FORM_REPRESENTATIONS = 'Z6004K3';
const LEXEME_FORM_GRAMMATICAL_FEATURES = 'Z6004K4';
const LEXEME_FORM_CLAIMS = 'Z6004K5';
const LEXEME_TYPE = 'Z6005';
const LEXEME_IDENTITY = 'Z6005K1';
const LEXEME_LEMMAS = 'Z6005K2';
const LEXEME_LANGUAGE = 'Z6005K3';
const LEXEME_LEXICAL_CATEGORY = 'Z6005K4';
const LEXEME_CLAIMS = 'Z6005K5';
const LEXEME_SENSES = 'Z6005K6';
const LEXEME_FORMS = 'Z6005K7';
const LEXEME_SENSE_TYPE = 'Z6006';
const LEXEME_SENSE_IDENTITY = 'Z6006K1';
const LEXEME_SENSE_GLOSSES = 'Z6006K2';
const LEXEME_SENSE_CLAIMS = 'Z6006K3';

// ZIDs for the instances of Z6040 / Wikidata statement rank
const STATEMENT_RANK_PREFERRED = 'Z6041';
const STATEMENT_RANK_NORMAL = 'Z6042';
const STATEMENT_RANK_DEPRECATED = 'Z6043';

// ZIDs for the Wikidata reference types
const ITEM_REF = 'Z6091';
const PROPERTY_REF = 'Z6092';
const LEXEME_FORM_REF = 'Z6094';
const LEXEME_REF = 'Z6095';
const LEXEME_SENSE_REF = 'Z6096';

/**
 * Given the JSON returned from Wikidata for an item, construct the corresponding ZObject
 * of type Wikidata item.
 *
 * The thrown errors, indicating an invalid input, are defensive programming;
 * not expected to happen.
 *
 * See also:
 *   https://www.mediawiki.org/wiki/Wikibase/DataModel
 *   https://www.wikidata.org/wiki/Wikidata:Data_access
 *   https://phabricator.wikimedia.org/T368654
 *
 * @param {Object} wikidataItem The JSON returned from Wikidata for an item
 * @param {Function} logThis Logging function taking 2 arguments, level and message
 * @return {Object} A ZObject of type Wikidata item, in normal form
 * @throws {Error} If wikidataItem is invalid (shouldn't happen)
 */
function convertItem( wikidataItem, logThis = null ) {
	const wikifunctionsItem = { Z1K1: wrapInZ9( ITEM_TYPE ) };

	if ( !wikidataItem.id ) {
		throwInvalidJsonError( 'Item from Wikidata has no id' );
	}
	if ( !isItemId( wikidataItem.id ) ) {
		throwInvalidJsonError( `Item from Wikidata has an invalid id: <${ wikidataItem.id }>` );
	}
	const wikifunctionsItemRef = makeWikidataRef( ITEM_REF, wikidataItem.id );
	wikifunctionsItem[ ITEM_IDENTITY ] = wikifunctionsItemRef;

	// Not sure whether there is guaranteed to be at least one label; we provide
	// an empty object if needed, to avoid having a missing property.
	const wikidataLabels = wikidataItem.labels || {};
	wikifunctionsItem[ ITEM_LABELS ] =
		convertToMultilingualText( wikidataLabels, logThis );

	// Not sure whether there is guaranteed to be at least one description; we provide
	// an empty object if needed, to avoid having a missing property.
	const wikidataDescriptions = wikidataItem.descriptions || {};
	wikifunctionsItem[ ITEM_DESCRIPTIONS ] =
		convertToMultilingualText( wikidataDescriptions, logThis );

	// Not sure whether there is guaranteed to be at least one alias; we provide
	// an empty object if needed, to avoid having a missing property.
	const wikidataAliases = wikidataItem.aliases || {};
	wikifunctionsItem[ ITEM_ALIASES ] =
		convertToMultilingualStringset( wikidataAliases, logThis );

	// Not sure whether there is guaranteed to be a claims property; we provide
	// an empty object if needed, to avoid having a missing ITEM_CLAIMS.
	const wikidataClaims = wikidataItem.claims || {};
	wikifunctionsItem[ ITEM_CLAIMS ] = convertClaims( wikifunctionsItemRef, wikidataClaims );

	// TODO (T382795): Add site links
	return wikifunctionsItem;
}

/**
 * Given the JSON returned from Wikidata for a property, construct the corresponding ZObject
 * of type Wikidata property.
 *
 * The thrown errors, indicating an invalid input, are defensive programming;
 * not expected to happen.
 *
 * See also:
 *   https://www.mediawiki.org/wiki/Wikibase/DataModel
 *   https://www.wikidata.org/wiki/Wikidata:Data_access
 *   https://phabricator.wikimedia.org/T368654
 *
 * @param {Object} wikidataProperty The JSON returned from Wikidata for a property
 * @param {Function} logThis Logging function taking 2 arguments, level and message
 * @return {Object} A ZObject of type Wikidata property, in normal form
 * @throws {Error} If wikidataProperty is invalid (shouldn't happen)
 */
function convertProperty( wikidataProperty, logThis = null ) {
	const wikifunctionsProperty = { Z1K1: wrapInZ9( PROPERTY_TYPE ) };

	if ( !wikidataProperty.id ) {
		throwInvalidJsonError( 'Property from Wikidata has no id' );
	}
	if ( !isPropertyId( wikidataProperty.id ) ) {
		throwInvalidJsonError( `Property from Wikidata has an invalid id: <${ wikidataProperty.id }>` );
	}
	const wikifunctionsPropertyRef = makeWikidataRef( PROPERTY_REF, wikidataProperty.id );
	wikifunctionsProperty[ PROPERTY_IDENTITY ] = wikifunctionsPropertyRef;

	// Not sure whether there is guaranteed to be at least one label; we provide
	// an empty object if needed, to avoid having a missing key/value.
	const wikidataLabels = wikidataProperty.labels || {};
	wikifunctionsProperty[ PROPERTY_LABELS ] =
		convertToMultilingualText( wikidataLabels, logThis );

	// Not sure whether there is guaranteed to be at least one description; we provide
	// an empty object if needed, to avoid having a missing key/value.
	const wikidataDescriptions = wikidataProperty.descriptions || {};
	wikifunctionsProperty[ PROPERTY_DESCRIPTIONS ] =
		convertToMultilingualText( wikidataDescriptions, logThis );

	// Not sure whether there is guaranteed to be at least one alias; we provide
	// an empty object if needed, to avoid having a missing key/value.
	const wikidataAliases = wikidataProperty.aliases || {};
	wikifunctionsProperty[ PROPERTY_ALIASES ] =
		convertToMultilingualStringset( wikidataAliases, logThis );

	// Not sure whether there is guaranteed to be a claims property; we provide
	// an empty object if needed, to avoid having a missing key/value.
	const wikidataClaims = wikidataProperty.claims || {};
	wikifunctionsProperty[ PROPERTY_CLAIMS ] =
		convertClaims( wikifunctionsPropertyRef, wikidataClaims );

	return wikifunctionsProperty;
}

/**
 * Given the JSON returned from Wikidata for a lexeme, construct the corresponding ZObject
 * of type Wikidata lexeme.
 *
 * The thrown errors, indicating an invalid input, are defensive programming;
 * not supposed to happen.  A Lexeme should always have an ID, language, lexical
 * category, and at least one lemma. (But there can be Lexemes without forms and/or
 * senses.)
 *
 * See also:
 *   https://www.mediawiki.org/wiki/Wikibase/DataModel
 *   https://www.wikidata.org/wiki/Wikidata:Lexicographical_data/Documentation
 *   https://www.wikidata.org/wiki/Wikidata:Data_access
 *   https://www.mediawiki.org/wiki/Extension:WikibaseLexeme/Data_Model
 *   https://doc.wikimedia.org/WikibaseLexeme/master/php/md_docs_2topics_2json.html
 *   https://phabricator.wikimedia.org/T368654
 *
 * @param {Object} wikidataLexeme The JSON returned from Wikidata for a lexeme
 * @param {Function} logThis Logging function taking 2 arguments, level and message
 * @return {Object} A ZObject of type Wikidata lexeme, in normal form
 * @throws {Error} If wikidataLexeme is invalid (shouldn't happen)
 */
function convertLexeme( wikidataLexeme, logThis = null ) {
	const wikifunctionsLexeme = { Z1K1: wrapInZ9( LEXEME_TYPE ) };

	if ( !wikidataLexeme.id ) {
		throwInvalidJsonError( 'Lexeme from Wikidata has no id' );
	}
	if ( !isLexemeId( wikidataLexeme.id ) ) {
		throwInvalidJsonError( `Lexeme from Wikidata has an invalid id: <${ wikidataLexeme.id }>` );
	}
	const wikifunctionsLexemeRef = makeWikidataRef( LEXEME_REF, wikidataLexeme.id );
	wikifunctionsLexeme[ LEXEME_IDENTITY ] = wikifunctionsLexemeRef;

	if ( !wikidataLexeme.lemmas || Object.keys( wikidataLexeme.lemmas ).length === 0 ) {
		throwInvalidJsonError( `Lexeme <${ wikidataLexeme.id }> has no lemmas` );
	}
	wikifunctionsLexeme[ LEXEME_LEMMAS ] =
		convertToMultilingualText( wikidataLexeme.lemmas, logThis );

	// We ignore the language element & take the language of the first lemma.
	wikifunctionsLexeme[ LEXEME_LANGUAGE ] = getLexemeLanguage( wikidataLexeme );

	if ( !wikidataLexeme.lexicalCategory ) {
		throwInvalidJsonError( `Lexeme <${ wikidataLexeme.id }> has no lexicalCategory` );
	}
	wikifunctionsLexeme[ LEXEME_LEXICAL_CATEGORY ] =
		makeWikidataRef( ITEM_REF, wikidataLexeme.lexicalCategory );

	// Not sure whether there is guaranteed to be a claims property; we provide
	// an empty object if needed, to avoid having a missing LEXEME_CLAIMS.
	const wikidataClaims = wikidataLexeme.claims || {};
	wikifunctionsLexeme[ LEXEME_CLAIMS ] = convertClaims( wikifunctionsLexemeRef, wikidataClaims );

	const wikifunctionsLexemeSenses = [];
	if ( wikidataLexeme.senses ) {
		for ( const wikidataLexemeSense of wikidataLexeme.senses ) {
			wikifunctionsLexemeSenses.push( convertLexemeSense( wikidataLexemeSense ) );
		}
	}
	wikifunctionsLexeme[ LEXEME_SENSES ] =
		convertArrayToKnownTypedList( wikifunctionsLexemeSenses, wrapInZ9( LEXEME_SENSE_TYPE ) );

	const wikifunctionsLexemeForms = [];
	if ( wikidataLexeme.forms ) {
		for ( const wikidataLexemeForm of wikidataLexeme.forms ) {
			wikifunctionsLexemeForms.push( convertLexemeForm( wikidataLexemeForm ) );
		}
	}
	wikifunctionsLexeme[ LEXEME_FORMS ] =
		convertArrayToKnownTypedList( wikifunctionsLexemeForms, wrapInZ9( LEXEME_FORM_TYPE ) );

	return wikifunctionsLexeme;
}

/**
 * Given the JSON returned from Wikidata for the value of a "claims" property, extract statements
 * we currently handle and transform them into a list of Z6003/'Wikidata statement'.
 *
 * The Wikidata JSON (wikidataClaims) is organized by property ID; e.g.:
 *   { "P123": [ list of statement objects involving P123 ]
 *     "P456": [ list of statement objects involving P456 ],
 *     etc. }
 * The statement objects do not mention the subject ID (which comes from the object containing the
 * claims), so we pass in wikifunctionsSubjectRef from above.
 *
 * Each lexeme, lexeme form, lexeme sense, and possibly other entity types, can contain a "claims"
 * property, which can be an empty object {}.
 *
 * @param {Object} wikifunctionsSubjectRef Instance of reference type, for the subject of the
 * statements
 * @param {Object} wikidataClaims JSON returned from Wikidata for the value of a "claims" property
 * @return {Object} A ZList of Z6003/'Wikidata statement'
 */
function convertClaims( wikifunctionsSubjectRef, wikidataClaims ) {
	const wikifunctionsStatements = [];
	for ( const key of Object.keys( wikidataClaims ) ) {
		for ( const wikidataStatement of wikidataClaims[ key ] ) {
			const wikifunctionsStatement =
				convertStatement( wikifunctionsSubjectRef, wikidataStatement );
			if ( wikifunctionsStatement ) {
				wikifunctionsStatements.push( wikifunctionsStatement );
			}
		}
	}
	return convertArrayToKnownTypedList( wikifunctionsStatements, wrapInZ9( STATEMENT_TYPE ) );
}

function convertStatement( wikifunctionsSubjectRef, wikidataStatement ) {
	// For now, we ignore statements with qualifiers.  We also ignore statements without a rank (we
	// don't expect any, but if we encounter one we consider it to be a defective statement.)
	if ( wikidataStatement.type === 'statement' && wikidataStatement.mainsnak &&
		wikidataStatement.rank && !wikidataStatement.qualifiers ) {
		const snak = wikidataStatement.mainsnak;

		// See if there's a value we want to keep
		let statementValue = null;
		if ( snak.snaktype === 'value' && snak.datavalue && isObject( snak.datavalue ) ) {
			const datavalue = snak.datavalue;
			if ( datavalue.type === 'string' ) {
				statementValue = wrapInZ6( snak.datavalue.value );
			} else if ( datavalue.type === 'monolingualtext' && isObject( datavalue.value ) ) {
				statementValue =
					convertToMonolingualText( datavalue.value.language, datavalue.value.text );
			} else if ( datavalue.type === 'wikibase-entityid' && isObject( datavalue.value ) ) {
				switch ( datavalue.value[ 'entity-type' ] ) {
				case 'item':
					statementValue = makeWikidataRef( ITEM_REF, datavalue.value.id );
					break;
				case 'form':
					statementValue = makeWikidataRef( LEXEME_FORM_REF, datavalue.value.id );
					break;
				case 'lexeme':
					statementValue = makeWikidataRef( LEXEME_REF, datavalue.value.id );
					break;
				case 'sense':
					statementValue = makeWikidataRef( LEXEME_SENSE_REF, datavalue.value.id );
					break;
				}
			}
		}

		// If there's a value, construct & return the statement
		if ( statementValue ) {
			const wikifunctionsStatement = { Z1K1: wrapInZ9( STATEMENT_TYPE ) };
			wikifunctionsStatement[ STATEMENT_SUBJECT ] = wikifunctionsSubjectRef;
			wikifunctionsStatement[ STATEMENT_PREDICATE ] =
				makeWikidataRef( PROPERTY_REF, snak.property );
			wikifunctionsStatement[ STATEMENT_VALUE ] = statementValue;

			let wikifunctionsRank;
			switch ( wikidataStatement.rank ) {
			case 'preferred':
				wikifunctionsRank = STATEMENT_RANK_PREFERRED;
				break;
			case 'normal':
				wikifunctionsRank = STATEMENT_RANK_NORMAL;
				break;
			case 'deprecated':
				wikifunctionsRank = STATEMENT_RANK_DEPRECATED;
				break;
			}
			wikifunctionsStatement[ STATEMENT_RANK ] = wrapInZ9( wikifunctionsRank );

			return wikifunctionsStatement;
		}
	}
	return null;
}

/**
 * Given the JSON returned from Wikidata for a lexeme form, construct the corresponding
 * ZObject of type Wikidata lexeme form.
 *
 * @param {Object} wikidataLexemeForm The JSON returned from Wikidata for a lexeme form
 * @param {Function} logThis Logging function taking 2 arguments, level and message
 * @return {Object} A ZObject of type Wikidata lexeme form, in normal form
 */
function convertLexemeForm( wikidataLexemeForm, logThis = null ) {
	const wikifunctionsLexemeForm = { Z1K1: wrapInZ9( LEXEME_FORM_TYPE ) };

	if ( !wikidataLexemeForm.id ) {
		throwInvalidJsonError( 'Lexeme form from Wikidata has no id' );
	}
	if ( !isLexemeFormId( wikidataLexemeForm.id ) ) {
		throwInvalidJsonError( `Lexeme form from Wikidata has an invalid id: <${ wikidataLexemeForm.id }>` );
	}
	const wikifunctionsLexemeFormRef = makeWikidataRef( LEXEME_FORM_REF, wikidataLexemeForm.id );
	wikifunctionsLexemeForm[ LEXEME_FORM_IDENTITY ] = wikifunctionsLexemeFormRef;

	const lexemeId = wikidataLexemeForm.id.match( /^(L[1-9]\d*)-(F[1-9]\d*)$/ )[ 1 ];
	wikifunctionsLexemeForm[ LEXEME_FORM_LEXEME ] = makeWikidataRef( LEXEME_REF, lexemeId );

	// Not sure whether there is guaranteed to be at least one representation; we provide
	// an empty object if needed, to avoid having a missing K3.
	const wikidataRepresentations = wikidataLexemeForm.representations || {};
	wikifunctionsLexemeForm[ LEXEME_FORM_REPRESENTATIONS ] =
		convertToMultilingualText( wikidataRepresentations, logThis );

	// Each grammatical feature is a QID
	const wikifunctionsFeatures = [];
	if ( wikidataLexemeForm.grammaticalFeatures ) {
		for ( const feature of wikidataLexemeForm.grammaticalFeatures ) {
			wikifunctionsFeatures.push( makeWikidataRef( ITEM_REF, feature ) );
		}
	}
	wikifunctionsLexemeForm[ LEXEME_FORM_GRAMMATICAL_FEATURES ] =
		convertArrayToKnownTypedList( wikifunctionsFeatures, wrapInZ9( ITEM_REF ) );

	// Not sure whether there is guaranteed to be a claims property; we provide
	// an empty object if needed, to avoid having a missing LEXEME_FORM_CLAIMS.
	const wikidataClaims = wikidataLexemeForm.claims || {};
	wikifunctionsLexemeForm[ LEXEME_FORM_CLAIMS ] =
		convertClaims( wikifunctionsLexemeFormRef, wikidataClaims );

	return wikifunctionsLexemeForm;
}

/**
 * Given the JSON returned from Wikidata for a lexeme sense, construct the corresponding
 * ZObject of type Wikidata lexeme sense.
 *
 * @param {Object} wikidataLexemeSense The JSON returned from Wikidata for a lexeme sense
 * @param {Function} logThis Logging function taking 2 arguments, level and message
 * @return {Object} A ZObject of type Wikidata lexeme sense, in normal form
 */
function convertLexemeSense( wikidataLexemeSense, logThis = null ) {
	const wikifunctionsLexemeSense = { Z1K1: wrapInZ9( LEXEME_SENSE_TYPE ) };

	if ( !wikidataLexemeSense.id ) {
		throwInvalidJsonError( 'Lexeme sense from Wikidata has no id' );
	}
	if ( !isLexemeSenseId( wikidataLexemeSense.id ) ) {
		throwInvalidJsonError( `Lexeme sense from Wikidata has an invalid id: <${ wikidataLexemeSense.id }>` );
	}
	const wikifunctionsLexemeSenseRef = makeWikidataRef( LEXEME_SENSE_REF, wikidataLexemeSense.id );
	wikifunctionsLexemeSense[ LEXEME_SENSE_IDENTITY ] = wikifunctionsLexemeSenseRef;

	// Not sure whether there is guaranteed to be at least one gloss; we provide
	// an empty object if needed, to avoid having a missing K2.
	const wikidataGlosses = wikidataLexemeSense.glosses || {};
	wikifunctionsLexemeSense[ LEXEME_SENSE_GLOSSES ] =
		convertToMultilingualText( wikidataGlosses, logThis );

	// Not sure whether there is guaranteed to be a claims property; we provide
	// an empty object if needed, to avoid having a missing LEXEME_SENSE_CLAIMS.
	const wikidataClaims = wikidataLexemeSense.claims || {};
	wikifunctionsLexemeSense[ LEXEME_SENSE_CLAIMS ] =
		convertClaims( wikifunctionsLexemeSenseRef, wikidataClaims );

	return wikifunctionsLexemeSense;
}

/**
 * Given the JSON returned from Wikidata for a group of lemmas, representations, or glosses,
 * construct the corresponding ZObject of type Z12 / Multilingual text.  Each such group
 * is a JavaScript Object, and each top-level value is an Object (wikidataPair) containing
 * a "language" key and a "value" key - and the values of those are strings. The top-level keys
 * duplicate the language keys.
 *
 * @param {Object} wikidataObject JSON returned from Wikidata
 * @param {Function} logThis Logging function taking 2 arguments, level and message
 * @return {Object} An instance of Z12 / Multilingual text, in normal form
 */
function convertToMultilingualText( wikidataObject, logThis = null ) {
	const monolingualTexts = [];
	for ( const key of Object.keys( wikidataObject ) ) {
		const wikidataPair = wikidataObject[ key ];
		monolingualTexts.push(
			convertToMonolingualText( wikidataPair.language, wikidataPair.value, logThis )
		);
	}
	return {
		Z1K1: wrapInZ9( 'Z12' ), // Multilingual text
		Z12K1: convertArrayToKnownTypedList( monolingualTexts, wrapInZ9( 'Z11' ) )
	};
}

function convertToMonolingualText( languageCode, text, logThis = null ) {
	let wikifunctionsLanguage;
	if ( languageCode in languageMap ) {
		wikifunctionsLanguage = wrapInZ9( languageMap[ languageCode ] );
	} else {
		wikifunctionsLanguage = makeZ60( languageCode );
		if ( logThis ) {
			const logMessage = `No persistent Z60 for <${ languageCode }>; making an inline Z60`;
			logThis( 'info', logMessage );
		}
	}
	return {
		Z1K1: wrapInZ9( 'Z11' ), // Monolingual text
		Z11K1: wikifunctionsLanguage,
		Z11K2: wrapInZ6( text )
	};
}

/**
 * Given the JSON returned from Wikidata for a group of aliases,
 * construct the corresponding ZObject of type Z32 / Multilingual stringset.  The group of aliases
 * is a JavaScript Object, and each top-level value is a list of Objects containing
 * a "language" key and a "value" key - and the values of those are strings. Within each list, all
 * the language keys are the same. Each top-level key duplicates the language keys in its list.
 *
 * @param {Object} wikidataObject JSON returned from Wikidata
 * @param {Function} logThis Logging function taking 2 arguments, level and message
 * @return {Object} An instance of Z32 / Multilingual stringset, in normal form
 */
function convertToMultilingualStringset( wikidataObject, logThis = null ) {
	const monolingualStringsets = [];
	for ( const key of Object.keys( wikidataObject ) ) {
		const wikidataListOfPairs = wikidataObject[ key ];
		monolingualStringsets.push(
			convertToMonolingualStringset( key, wikidataListOfPairs, logThis )
		);
	}
	return {
		Z1K1: wrapInZ9( 'Z32' ), // Multilingual stringset
		Z32K1: convertArrayToKnownTypedList( monolingualStringsets, wrapInZ9( 'Z31' ) )
	};
}

function convertToMonolingualStringset( languageCode, wikidataListOfPairs, logThis = null ) {
	let wikifunctionsLanguage;
	if ( languageCode in languageMap ) {
		wikifunctionsLanguage = wrapInZ9( languageMap[ languageCode ] );
	} else {
		wikifunctionsLanguage = makeZ60( languageCode );
		if ( logThis ) {
			const logMessage = `No persistent Z60 for <${ languageCode }>; making an inline Z60`;
			logThis( 'info', logMessage );
		}
	}
	const wikifunctionsStrings = [];
	for ( const pair of wikidataListOfPairs ) {
		wikifunctionsStrings.push( wrapInZ6( pair.value ) );
	}
	return {
		Z1K1: wrapInZ9( 'Z31' ), // Monolingual stringset
		Z31K1: wikifunctionsLanguage,
		Z31K2: convertArrayToKnownTypedList( wikifunctionsStrings, wrapInZ9( 'Z6' ) )
	};
}

/**
 * Given the JSON returned from Wikidata for a lexeme, determine the most appropriate
 * Wikifunctions ZObject (of type Z60) representing the language of the lexeme.
 *
 * wikidataLexeme.language contains a QID. Because there's no perfect mapping from
 * wikidataLexeme.language to our Z60 instances, and it would be some effort to maintain
 * such a mapping anyway, we currently use a heuristic that should work for the vast
 * majority of lexemes.
 *
 * If we fail to determine a known ZID (which should be very unusual), we return a
 * non-persistent Z60.
 *
 * See also: https://www.wikidata.org/wiki/Wikidata:Lexicographical_data/Documentation/Lexeme_languages
 * TODO (T373598): consider whether any other possible approaches might be better.
 *
 * @param {Object} wikidataLexeme The JSON returned from Wikidata for a lexeme
 * @param {Function} logThis Logging function taking 2 arguments, level and message
 * @return {Object} Z9/Reference or an instance of Z60 / Natural language
 */
function getLexemeLanguage( wikidataLexeme, logThis = null ) {
	const wikidatalemmas = wikidataLexeme.lemmas;
	// Get the IETF code from the first Lemma
	const code = Object.keys( wikidatalemmas )[ 0 ];
	if ( code in languageMap ) {
		return wrapInZ9( languageMap[ code ] );
	}

	if ( logThis ) {
		const logMessage = `No persistent Z60 for <${ code }> (lexeme <${ wikidataLexeme.id }>); making an inline Z60`;
		logThis( 'info', logMessage );
	}
	return makeZ60( code );
}

function makeZ60( code ) {
	return {
		Z1K1: wrapInZ9( 'Z60' ), // Natural language
		Z60K1: wrapInZ6( code )
	};
}

/**
 * @param {string} refTypeZID The ZID for a Wikidata reference type (Z669x)
 * @param {string} wikidataID A Wikidata ID (e.g. L12345, L12345-F1, etc.)
 * @return {Object} An instance of the given reference type, containing the given ID
 */
function makeWikidataRef( refTypeZID, wikidataID ) {
	const wikidataIDKey = refTypeZID + 'K1';
	const ref = {
		Z1K1: wrapInZ9( refTypeZID )
	};
	ref[ wikidataIDKey ] = wrapInZ6( wikidataID );
	return ref;
}

function throwInvalidJsonError( message ) {
	const theError = new Error( message );
	// Note that this isn't a standard property, we're just mucking with the Error object
	theError.type = 'invalid-json';
	throw theError;
}

function conversionFunctionForWikidataID( ID ) {
	if ( isItemId( ID ) ) {
		return convertItem;
	}
	if ( isLexemeFormId( ID ) ) {
		return convertLexemeForm;
	}
	if ( isLexemeId( ID ) ) {
		return convertLexeme;
	}
	if ( isLexemeSenseId( ID ) ) {
		return convertLexemeSense;
	}
	return null;
}

module.exports = {
	convertItem,
	convertProperty,
	convertLexeme,
	convertLexemeForm,
	convertLexemeSense,
	makeWikidataRef,
	LEXEME_REF,
	conversionFunctionForWikidataID
};
