'use strict';

/*!
 * Wikifunctions orchestrator class to model the frame of a function call.
 *
 * @copyright 2020– Abstract Wikipedia team; see LICENSE
 * @license MIT
 */

const { ArgumentState } = require( './argumentState.js' );
const { error, makeErrorInNormalForm } = require( '../function-schemata/javascript/src/error.js' );
const { wrapInKeyReference } = require( '../function-schemata/javascript/src/utils.js' );

class BaseFrame {

	constructor( lastFrame = null ) {
		this.lastFrame_ = lastFrame;
		this.names_ = new Map();
		this.metadata_ = new Map();
		Object.defineProperty( this, 'metadata', {
			get: function () {
				return this.metadata_;
			}
		} );
		if ( this.lastFrame_ !== null ) {
			const parentMetadata = this.lastFrame_.metadata;
			if ( !parentMetadata.has( 'nestedMetadataArray' ) ) {
				parentMetadata.set( 'nestedMetadataArray', [] );
			}
			parentMetadata.get( 'nestedMetadataArray' ).push( this.metadata_ );
		}
		this.size_ = 1 + ( this.lastFrame_ && this.lastFrame_.size || 0 );
		Object.defineProperty( this, 'size', {
			get: function () {
				return this.size_;
			}
		} );
	}

	isEmpty() {
		return false;
	}

	// Returns a view of the Frame object suitable for debugging:
	// * ZObjects are canonicalized
	// * Scopes are flattened
	// See also `ZWrapper.debugObject()` and `ZWrapper.debug()`.
	debugObject() {
		const result = ( this.isEmpty() ) ? {} : { lastFrame_: this.lastFrame_.debugObject() };
		for ( const [ name, value ] of this.names_ ) {
			result[ name ] = value.argumentDict.argument.debugObject();
		}
		return result;
	}

	hasVariable( variableName ) {
		const argumentState = this.names_.get( variableName );
		if ( argumentState !== undefined ) {
			return true;
		}
		return this.lastFrame_.hasVariable( variableName );
	}
}

class EmptyFrame extends BaseFrame {
	constructor() {
		super();
	}

	async retrieveArgument( argumentName ) {
		return ArgumentState.ERROR(
			makeErrorInNormalForm(
				// TODO (T287919): Reconsider error type.
				error.invalid_key,
				[ wrapInKeyReference( argumentName ) ] ) );
	}

	isEmpty() {
		return true;
	}

	hasVariable( variableName ) {
		return false;
	}
}

module.exports = { BaseFrame, EmptyFrame };
