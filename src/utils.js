'use strict';

/*!
 * Wikifunctions orchestrator utility code.
 *
 * @copyright 2020– Abstract Wikipedia team; see LICENSE
 * @license MIT
 */

const fs = require( 'fs' );

const normalize = require( '../function-schemata/javascript/src/normalize.js' );
const {
	SchemaFactory,
	validatesAsZObject
} = require( '../function-schemata/javascript/src/schema.js' );
const {
	createZObjectKey,
	findFunctionIdentity,
	findTypeIdentity,
	isUserDefined,
	getHead,
	getTail,
	makeMappedResultEnvelope,
	isString,
	isVoid,
	isZFunctionCall,
	isZMap,
	isZReference,
	isZString,
	makeTrue,
	getZMapValue
} = require( '../function-schemata/javascript/src/utils.js' );
const { EmptyFrame } = require( './frame.js' );

const normalFactory = SchemaFactory.NORMAL();

/**
 * Accesses the quoted internals of a Z99/Quote and attempts to normalize them.
 * Returns the envelope intact; thus, if the original object was not a valid
 * ZObject, the envelope will contain an error.
 *
 * @param {Object} someQuote a Z99/Quote
 * @return {Object} Z22 containing either the normalized quote internals or an error
 */
function unquoteVeryCarefully( someQuote ) {
	return normalize( someQuote.Z99K1 );
}

/**
 * Accesses the quoted internals of a Z99/Quote directly. This function should
 * only be used on Quotes generated internally to the orchestrator (e.g. by
 * validation functions), when we know that the internal object is already in
 * normal form.
 *
 * @param {Object} someQuote a Z99/Quote
 * @return {Object} the quote internals
 */
function unquoteBrashly( someQuote ) {
	return someQuote.Z99K1;
}

/**
 * Determines whether argument is a Z6 or Z9. These two types' Z1K1s are
 * strings instead of Z9s, so some checks below need to special-case their
 * logic.
 *
 * @param {Object} Z1 a ZObject
 * @return {boolean} true if Z1 validates as either Z6 or Z7
 */
function isRefOrString( Z1 ) {
	return isZString( Z1 ) || isZReference( Z1 );
}

function createSchema( Z1 ) {
	// TODO (T381597): asJSON() will be unnecessary once JSON schema is gone.
	let Z1K1 = Z1.Z1K1;
	const { ZWrapper } = require( './ZWrapper.js' );
	if ( Z1K1 instanceof ZWrapper ) {
		Z1K1 = Z1K1.asJSONEphemeral();
	}
	if ( isRefOrString( Z1 ) ) {
		return normalFactory.create( Z1K1 );
	}
	if ( isZReference( Z1K1 ) ) {
		if ( isUserDefined( Z1K1.Z9K1 ) ) {
			throw new Error( `Tried to create schema for unrecognized ZID ${ Z1K1.Z9K1 }` );
		}
		return normalFactory.create( Z1K1.Z9K1 );
	}
	const result = normalFactory.createUserDefined( [ Z1K1 ] );
	const key = createZObjectKey( Z1K1 );
	return result.get( key );
}

/**
 * Validates a ZObject against the Error schema.
 *
 * @param {Object} Z1 object to be validated
 * @return {boolean} whether Z1 can validate as an Error
 */
function isError( Z1 ) {
	// TODO (T287921): Assay that Z1 validates as Z5 but not as Z9 or Z18.
	try {
		return Z1.Z1K1 === 'Z5' || Z1.Z1K1.Z9K1 === 'Z5';
	} catch ( error ) {
		return false;
	}
}

/**
 * Validates a ZObject against the GENERIC schema.
 *
 * @param {Object} Z1 object to be validated
 * @return {boolean} whether Z1 can validate as a generic type instantiation
 */
function isGenericType( Z1 ) {
	// TODO (T296658): Use the GENERIC schema.
	try {
		const { ZWrapper } = require( './ZWrapper.js' );
		let keys;
		if ( Z1 instanceof ZWrapper ) {
			keys = Z1.keys();
		} else {
			keys = Object.keys( Z1 );
		}
		if ( !isZFunctionCall( Z1.Z1K1 ) ) {
			return false;
		}
		const localKeyRegex = /K[1-9]\d*$/;
		for ( const key of keys ) {
			if ( key === 'Z1K1' || key === 'Z7K1' ) {
				continue;
			}
			if ( key.match( localKeyRegex ) === null ) {
				return false;
			}
		}
		return true;
	} catch ( err ) {
		return false;
	}
}

function isGenericListType( Z1K1 ) {
	const typeIdentity = findTypeIdentity( Z1K1 );
	const theFunction = typeIdentity.Z7K1;
	if ( theFunction === undefined ) {
		return false;
	}
	const functionIdentity = findFunctionIdentity( theFunction );
	if ( functionIdentity && functionIdentity.Z9K1 === 'Z881' ) {
		return true;
	}
	return false;
}

/**
 * Determines whether a Z22 / Evaluation result contains an error.
 *
 * @param {Object} envelope a Z22
 * @return {boolean} true if Z22K2 contains an error; false otherwise
 */
function responseEnvelopeContainsError( envelope ) {
	const { ZEnvelopeWrapper, ZWrapper } = require( './ZWrapper.js' );
	if ( envelope instanceof ZWrapper ) {
		if ( envelope instanceof ZEnvelopeWrapper ) {
			return envelope.containsError();
		}
		// If we've been called with a non-response envelope,
		// it's not a Z22 with an error in it
		return false;
	}
	if ( !envelope || !envelope.Z1K1 || !( envelope.Z22K1 && envelope.Z22K2 ) ) {
		return false;
	}

	const metadata = envelope.Z22K2;
	if ( metadata === undefined || isVoid( metadata ) ) {
		// envelope wasn't a Z22 or Z22 wasn't a ZMap
		return false;
	} else if ( isZMap( metadata ) ) {
		const errorValue = getZMapValue( metadata, { Z1K1: 'Z6', Z6K1: 'errors' } );
		return ( errorValue !== undefined && !isVoid( errorValue ) );
	} else {
		throw new Error( `Invalid value for Z22K2: "${ metadata }"` );
	}
}

/**
 * Determines whether a responseEnvelope contains a value (i.e., a non-Void first element).
 * The input responseEnvelope should be in normal form.
 *
 * FIXME (T311055): responseEnvelopeContainsValue might require normal form, as validateAsZObject
 * is a normal validator. Check and document.
 *
 * @param {Object} responseEnvelope a Z22
 * @return {boolean} true if Z22K1 is not Z24 / Void; false otherwise
 */
function responseEnvelopeContainsValue( responseEnvelope ) {
	// TODO (T381597): Use the findBadPath algorithm to implement isZObject;
	// eliminate asJSON here.
	const Z22K1 = responseEnvelope.Z22K1.asJSON();
	return (
		validatesAsZObject( Z22K1 ).isValid() &&
		!( isVoid( Z22K1 ) )
	);
}

function makeBoolean( truthy = false, canonical = false ) {
	const zobject = {};
	if ( canonical ) {
		zobject.Z1K1 = 'Z40';
	} else {
		zobject.Z1K1 = {
			Z1K1: 'Z9',
			Z9K1: 'Z40'
		};
	}

	if ( truthy ) {
		if ( canonical ) {
			zobject.Z40K1 = 'Z41';
		} else {
			zobject.Z40K1 = {
				Z1K1: 'Z9',
				Z9K1: 'Z41'
			};
		}
	} else {
		if ( canonical ) {
			zobject.Z40K1 = 'Z42';
		} else {
			zobject.Z40K1 = {
				Z1K1: 'Z9',
				Z9K1: 'Z42'
			};
		}
	}

	return zobject;
}

// TODO (T292650): This needs to generate an actual error instead of Z6s.
function generateError( errorString = 'An unknown error occurred' ) {
	return {
		Z1K1: {
			Z1K1: 'Z9',
			Z9K1: 'Z5'
		},
		Z5K2: {
			Z1K1: {
				Z1K1: 'Z7',
				Z7K1: 'Z881',
				Z881K1: 'Z6'
			},
			K1: {
				Z1K1: 'Z6',
				Z6K1: errorString
			},
			K2: {
				Z1K1: {
					Z1K1: 'Z7',
					Z7K1: 'Z881',
					Z881K1: 'Z6'
				}
			}
		}
	};
}

/**
 * Convenience function for running a callback over every element of a ZList.
 *
 * Note that the callbacks are run using Promise.all, so do not make assumptions
 * about the order in which they are called.
 *
 * @param {Object} ZList a Typed List (instance of Z881)
 * @param {Function} callback a function to be called over every element of the list
 * @return {Promise<Array[Object]>} the results of running the callback on all list elements
 */
async function traverseZList( ZList, callback ) {
	let tail = ZList;
	if ( tail === undefined ) {
		return;
	}
	const callbacks = [];
	while ( getHead( tail ) !== undefined ) {
		callbacks.push( callback( tail ) );
		tail = getTail( tail );
	}
	return await Promise.all( callbacks );
}

/**
 * Runs several functions in sequence; returns first one whose Z22K2 is an error.
 *
 * @param {Object} Z22 a Z22/ResultEnvelope
 * @param {Array} callTuples an array whose elements are also arrays of the form
 *  [ function, argument list, name ]
 *  every function accepts Z22 as its first argument and will be called with the
 *  result of the previous function (starting with input Z22). If the resulting Z22
 *  contains an error (Z22K2), this function returns immediately; otherwise, it
 *  calls the next function with the output of the previous.
 * @param {?Function} callback optional callback to be called on every element of
 *  callTuples; arguments are of the form ( current Z22, current call tuple)
 * @param {boolean} addZ22 whether to inject Z22.Z22K1 as first argument to callables
 * @return {Promise<Object>} a Z22
 */
async function returnOnFirstError( Z22, callTuples, callback = null, addZ22 = true ) {
	let currentResponseEnvelope = Z22;
	for ( const callTuple of callTuples ) {
		if (
			responseEnvelopeContainsError( currentResponseEnvelope ) ||
			isVoid( currentResponseEnvelope.Z22K1 )
		) {
			break;
		}
		if ( callback !== null ) {
			await callback( currentResponseEnvelope, callTuple );
		}
		const callable = callTuple[ 0 ];
		const args = [];
		if ( addZ22 ) {
			args.push( currentResponseEnvelope.Z22K1 );
		}
		for ( const arg of callTuple[ 1 ] ) {
			args.push( arg );
		}
		currentResponseEnvelope = await callable( ...args );
	}
	return currentResponseEnvelope;
}

function quoteZObject( ZObject ) {
	const { ZWrapper } = require( './ZWrapper.js' );
	return ZWrapper.create(
		{
			Z1K1: {
				Z1K1: 'Z9',
				Z9K1: 'Z99'
			},
			Z99K1: ZObject
		},
		// Use an empty scope for the outer object, the nested object should already have its own
		// scope, if any.
		new EmptyFrame()
	);
}

function makeWrappedResultEnvelope( ...args ) {
	const { ZWrapper } = require( './ZWrapper.js' );
	return ZWrapper.create(
		makeMappedResultEnvelope( ...args ),
		// Use an empty scope for the outer object, the nested object should already have its own
		// scope, if any.
		new EmptyFrame()
	);
}

function getEvaluatorAndOrchestratorConfigsForEnvironment( environment, useReentrance ) {
	const evaluatorWs = environment.FUNCTION_EVALUATOR_WS || null;
	const evaluatorUri = environment.FUNCTION_EVALUATOR_URL || null;
	const orchestratorConfig = JSON.parse( environment.ORCHESTRATOR_CONFIG || '{}' );

	let evaluatorConfigs = orchestratorConfig.evaluatorConfigs;
	if ( evaluatorConfigs === undefined ) {
		// Legacy request: request does not supply evaluatorConfigs.
		evaluatorConfigs = [];
		evaluatorConfigs.push(
			{
				programmingLanguages: [
					'javascript-es2020', 'javascript-es2019', 'javascript-es2018',
					'javascript-es2017', 'javascript-es2016', 'javascript-es2015',
					'javascript' ],
				evaluatorUri: evaluatorUri,
				evaluatorWs: evaluatorWs,
				useReentrance: useReentrance } );
		evaluatorConfigs.push(
			{
				programmingLanguages: [
					'python-3-9', 'python-3-8', 'python-3-7', 'python-3',
					'python' ],
				evaluatorUri: evaluatorUri,
				evaluatorWs: evaluatorWs,
				useReentrance: useReentrance } );
		evaluatorConfigs.push(
			{
				softwareLanguages: [
					'Z600', 'Z601', 'Z602', 'Z603', 'Z604', 'Z605', 'Z606', 'Z607', 'Z608' ],
				evaluatorUri: evaluatorUri,
				evaluatorWs: evaluatorWs,
				useReentrance: useReentrance } );
		evaluatorConfigs.push(
			{
				softwareLanguages: [
					'Z610', 'Z611', 'Z612', 'Z613', 'Z614', 'Z615', 'Z620' ],
				evaluatorUri: evaluatorUri,
				evaluatorWs: evaluatorWs,
				useReentrance: useReentrance } );
	}
	// Orchestrator config doesn't need evaluator configs.
	delete orchestratorConfig.evaluatorConfigs;
	return { evaluatorConfigs, orchestratorConfig };
}

function getTimeoutForEnvironment( environment, defaultTimeout, logger ) {
	const millisecondsString = environment.ORCHESTRATOR_TIMEOUT_MS || '';
	let timeout = Number( millisecondsString );
	let logMessage = null;
	if ( isNaN( timeout ) ) {
		logMessage = `ORCHESTRATOR_TIMEOUT_MS was not a numerical value: ${ millisecondsString }; `;
	} else if ( timeout <= 0 ) {
		logMessage = 'Timeout must be greater than 0 ms; ';
	}
	if ( logMessage !== null ) {
		timeout = defaultTimeout;
		logMessage += `setting to default of ${ defaultTimeout } ms.`;
		logger.log( 'warn', { message: logMessage } );
	}
	return timeout;
}

function getBooleanIdentity( ZBoolean ) {
	if ( ZBoolean === undefined ) {
		return null;
	}
	if ( isZReference( ZBoolean ) ) {
		return ZBoolean;
	}
	return getBooleanIdentity( ZBoolean.Z40K1 );
}

/**
 * Returns true if the input is equivalent to the builtin true value.
 *
 * @param {Object} ZBoolean A Z40/Boolean
 * @return {boolean} Whether the input corresponds to Z41 (true) or not
 */
function isTrue( ZBoolean ) {
	const identity = getBooleanIdentity( ZBoolean );
	if ( identity === null ) {
		return false;
	}
	return identity.Z9K1 === makeTrue().Z40K1.Z9K1;
}

function isZ24( ZObject ) {
	let ZID = ZObject;
	if ( !isString( ZObject ) ) {
		ZID = ZObject.Z9K1;
	}
	return ZID === 'Z24';
}

function safeJsonStringify( ZObject ) {
	const { ZWrapper } = require( './ZWrapper.js' );
	if ( ZObject instanceof ZWrapper ) {
		ZObject = ZObject.asJSON();
	}
	return JSON.stringify( ZObject );
}

function readJSON( fileName ) {
	// eslint-disable-next-line security/detect-non-literal-fs-filename
	return JSON.parse( fs.readFileSync( fileName, { encoding: 'utf8' } ) );
}

module.exports = {
	responseEnvelopeContainsError,
	responseEnvelopeContainsValue,
	createSchema,
	generateError,
	getEvaluatorAndOrchestratorConfigsForEnvironment,
	getTimeoutForEnvironment,
	isError,
	isGenericListType,
	isGenericType,
	isRefOrString,
	isTrue,
	isZ24,
	makeBoolean,
	makeWrappedResultEnvelope,
	quoteZObject,
	returnOnFirstError,
	traverseZList,
	unquoteBrashly,
	unquoteVeryCarefully,
	safeJsonStringify,
	readJSON
};
