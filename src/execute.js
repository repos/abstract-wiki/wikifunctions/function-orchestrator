'use strict';

/*!
 * Wikifunctions orchestrator code to commission evaluation of function calls.
 *
 * @copyright 2020– Abstract Wikipedia team; see LICENSE
 * @license MIT
 */

const { ArgumentState } = require( './argumentState.js' );
const { Invariants } = require( './Invariants.js' );
const ImplementationSelector = require( './implementationSelector.js' );
const { BaseFrame, EmptyFrame } = require( './frame.js' );
const { Composition, Implementation, Evaluated, EvaluatorError, ZResponseError } = require( './implementation.js' );
const { FirstImplementationSelector } = require( './implementationSelector.js' );
const {
	isGenericListType,
	isTrue,
	isZ24,
	makeWrappedResultEnvelope,
	safeJsonStringify,
	quoteZObject,
	returnOnFirstError,
	responseEnvelopeContainsError,
	responseEnvelopeContainsValue,
	traverseZList
} = require( './utils.js' );
const { MutationType, ZWrapper } = require( './ZWrapper.js' );
const { resolveListType } = require( './builtins.js' );
const sUtil = require( '../lib/util' );
const { error, makeErrorInNormalForm } = require( '../function-schemata/javascript/src/error.js' );
const {
	createZObjectKey,
	findFunctionIdentity,
	findTypeIdentity,
	getError,
	isGlobalKey,
	isString,
	isZArgumentReference,
	isZFunctionCall,
	isZReference,
	isZString,
	isZType,
	setMetadataValues,
	wrapInQuote,
	wrapInKeyReference
} = require( '../function-schemata/javascript/src/utils.js' );
const { compareTypes } = require( '../function-schemata/javascript/src/compareTypes.js' );
const ErrorFormatter = require( '../function-schemata/javascript/src/errorFormatter.js' );

let execute = null;

const GENERIC_SCHEMA_VALIDATOR_ZID_ = 'Z831';

async function maybeAddValidationFunction( Z1, resolvedType, invariants, callTuples ) {
	if ( Z1.isValidated() ) {
		return;
	}
	if ( !isZType( resolvedType ) ) {
		return;
	}
	await ( resolvedType.resolveEphemeral( [ 'Z4K3' ], invariants ) );
	const validatorZ8 = resolvedType.Z4K3;
	if ( findFunctionIdentity( validatorZ8 ).Z9K1 === GENERIC_SCHEMA_VALIDATOR_ZID_ ) {
		return;
	}
	callTuples.push(
		[
			invariants.validateTypesWithCallCount,
			[ Z1, resolvedType, invariants ],
			'runTypeValidator' ] );
}

async function validateAsType( Z1, invariants, typeZObject = null ) {
	const wrapInZ9 = ( ZID ) => ZWrapper.create(
		{
			Z1K1: 'Z9',
			Z9K1: ZID
		},
		// A lone reference doesn't need any scope.
		new EmptyFrame()
	);
	const callTuples = [];
	let resolvedType = Z1.getNameEphemeral( 'Z1K1' );

	// TODO (T292787): Make this more elegant--should be possible to avoid
	// passing strings in the first place.
	if ( isString( typeZObject ) ) {
		typeZObject = wrapInZ9( typeZObject );
	}
	if ( isString( resolvedType ) ) {
		resolvedType = wrapInZ9( resolvedType );
	}

	// Run type comparison if typeZObject is provided.
	if ( typeZObject !== null ) {
		const runTypeComparison = ( comparand, comparator ) => {
			const typeComparison = compareTypes( comparand, comparator );
			if ( typeComparison ) {
				return makeWrappedResultEnvelope( Z1, null );
			} else {
				return makeWrappedResultEnvelope(
					null,
					makeErrorInNormalForm(
						error.unexpected_zobject_type,
						[ comparator, comparand ]
					)
				);
			}
		};
		callTuples.push(
			[
				runTypeComparison,
				[ resolvedType, typeZObject ],
				'runTypeComparison' ] );
	}

	// TODO (T301532): Find a more reliable way to signal that no additional
	// validation needs to be run. Here we just make sure that we won't run the
	// same function twice by comparing Z8K5 references.
	//
	// TODO (T327870): Also run the validator for typeZObject?
	await maybeAddValidationFunction( Z1, resolvedType, invariants, callTuples );

	const Z22 = makeWrappedResultEnvelope( Z1, null );
	return await returnOnFirstError( Z22, callTuples, /* callback= */null, /* addZ22= */false );
}

/**
 * Traverses a ZObject and resolves all Z1K1s.
 *
 * @param {Object} Z1 object whose Z1K1s are to be resolved
 * @param {Invariants} invariants evaluator, resolver: invariants preserved over all function calls
 * @param {boolean} doValidate whether to validate types of arguments and return values
 * @return {Promise<ArgumentState|null>} error state or null if no error encountered
 */
async function resolveTypes( Z1, invariants, doValidate = true ) {
	const objectQueue = [ Z1 ];
	while ( objectQueue.length > 0 ) {
		const nextObject = objectQueue.shift();
		if ( !( nextObject instanceof ZWrapper ) ) {
			continue;
		}
		await ( nextObject.resolveEphemeral(
			[ 'Z1K1' ], invariants, /* ignoreList= */ null,
			/* resolveInternals= */ false, doValidate ) );
		const typeEnvelope = nextObject.Z1K1;
		if ( responseEnvelopeContainsError( typeEnvelope ) ) {
			return ArgumentState.ERROR( getError( typeEnvelope ) );
		}
		for ( const key of nextObject.keys() ) {
			if ( key === 'Z1K1' ) {
				continue;
			}
			objectQueue.push( nextObject[ key ] );
		}
	}
	return null;
}

class KeyList {

	constructor( key, lastList, lastType ) {
		this.key = key;
		this.lastList = lastList;
		this.seenKeys = new Set( lastList === null ? undefined : lastList.seenKeys );
		this.seenKeys.add( this.key );
		this.seenTypes = new Set( lastList === null ? undefined : lastList.seenTypes );
		this.seenTypes.add( lastType );
		this.length = 1 + ( lastList === null ? 0 : lastList.length );
	}

	getAllKeys() {
		let result;
		if ( this.lastList !== null ) {
			result = new Array( this.lastList.getAllKeys() );
		} else {
			result = [];
		}
		result.push( this.key );
		return result;
	}

}

async function getIdentityKeys( zobject, invariants ) {
	const identityKeys = new Set();
	const keysEnvelope = await ( zobject.resolveEphemeral(
		[ 'Z1K1', 'Z4K2' ],
		invariants,
		/* ignoreList= */ null,
		/* resolveInternals= */ false,
		/* doValidate= */ false ) );
	if ( responseEnvelopeContainsError( keysEnvelope ) ) {
		return identityKeys;
	}
	await traverseZList(
		zobject.getNameEphemeral( 'Z1K1' ).getNameEphemeral( 'Z4K2' ),
		async ( tail ) => {
			const keyEnvelope = await tail.resolveEphemeral(
				[ 'K1' ],
				invariants,
				/* ignoreList= */ null,
				/* resolveInternals= */ false,
				/* doValidate= */ false );
			if ( responseEnvelopeContainsError( keyEnvelope ) ) {
				return;
			}
			const theKey = tail.getNameEphemeral( 'K1' );
			if ( isTrue( theKey.Z3K4 ) ) {
				identityKeys.add( theKey.Z3K2.Z6K1 );
			}
		}
	);
	return identityKeys;
}

async function maybeEagerlyEvaluate(
	zobject, invariants, ignoreList, resolveInternals, doValidate, keyList = null ) {
	if ( !( zobject instanceof ZWrapper ) ) {
		return null;
	}
	if ( zobject.isFullyResolved() ) {
		return null;
	}
	if ( isZString( zobject ) || isZReference( zobject ) ) {
		zobject.setIsFullyResolved( true );
		return null;
	}
	const result = await invariants.eagerlyEvaluateWithCallCount(
		zobject, invariants, ignoreList, resolveInternals, doValidate, keyList );
	if ( result === null ) {
		zobject.setIsFullyResolved( true );
	}
	return result;
}

async function eagerlyEvaluate(
	zobject, invariants, ignoreList, resolveInternals, doValidate, keyList = null ) {
	if (
		!( zobject instanceof ZWrapper ) ||
			isZString( zobject ) ||
			isZReference( zobject ) ) {
		return null;
	}

	if ( ignoreList === null ) {
		ignoreList = new Set();
	}
	const ignoreKeys = new Set( [
		// These are the identity fields defined on built-in types.
		// If we supported the `evenBuiltIns` argument in `resolveEphemeral`,
		// then we could avoid specifying these keys and identify these keys
		// using `getIdentityKeys`. However, this is inefficient and
		// unnecessary.
		'Z4K1', 'Z8K5', 'Z40K1',
		// Z1K1/Type should not be recursively expanded.
		'Z1K1',
		// Non-identity members of a Type should not be expanded.
		'Z4K2', 'Z4K3', 'Z4K4', 'Z4K5', 'Z4K6', 'Z4K7', 'Z4K8',
		// Non-identity members of a Function should not be expanded.
		'Z8K1', 'Z8K2', 'Z8K3', 'Z8K4',
		// Just say, "no," to Quotes.
		'Z99K1'
	] );

	function doResolve( key, someObject ) {
		if (
			isZArgumentReference( someObject ) &&
			!( ignoreList.has( MutationType.ARGUMENT_REFERENCE ) ) ) {
			if ( someObject.getScope().hasVariable( someObject.Z18K1.Z6K1 ) ) {
				return true;
			}
			return false;
		}
		if (
			isZReference( someObject ) &&
			!( ignoreList.has( MutationType.REFERENCE ) ) ) {
			return true;
		}
		if (
			isZFunctionCall( someObject ) &&
			!( ignoreList.has( MutationType.FUNCTION_CALL ) ) ) {
			return true;
		}
		return false;
	}

	const subResultPromises = [];
	let typeIdentity, identityKeys;
	if ( isString( zobject.Z1K1 ) ) {
		typeIdentity = zobject.Z1K1;
		identityKeys = new Set();
	} else {
		// TODO (T383574): This should use createZObjectKey.
		typeIdentity = JSON.stringify( findTypeIdentity( zobject.Z1K1 ).asJSON() );
		identityKeys = await getIdentityKeys( zobject, invariants );
	}

	for ( const key of zobject.keys() ) {
		if ( identityKeys.has( key ) || ignoreKeys.has( key ) ) {
			continue;
		}
		if ( isGlobalKey( key ) && keyList !== null ) {
			if ( keyList.seenKeys.has( key ) && keyList.seenTypes.has( typeIdentity ) ) {
				continue;
			} else if ( keyList.length > 100 ) {
				invariants.logger.log(
					'warn',
					{ message: 'Recursion limit: stack limit reached or exceeded', requestId: invariants.requestId }
				);
				return makeWrappedResultEnvelope(
					null,
					makeErrorInNormalForm(
						error.orchestrator_recursion_limit,
						[ '100', 'eagerlyEvaluate' ] ) );
			}
		}
		const nextKeyList = new KeyList( key, keyList, typeIdentity );
		const oldValue = zobject[ key ];
		if ( doResolve( key, oldValue ) ) {
			const valueEnvelope = await ( oldValue.resolve(
				invariants, ignoreList, resolveInternals, doValidate ) );
			// It's okay for some Z18s not to have values assigned.
			// TODO (T305981): We should formally distinguish between unbound
			// and unassigned variables. This will constrain further the errors
			// that we let slide here.
			if ( responseEnvelopeContainsError( valueEnvelope ) ) {
				return valueEnvelope;
			} else {
				const newValue = valueEnvelope.Z22K1;
				zobject.setName( key, newValue );
				if ( newValue instanceof ZWrapper ) {
					let newScope, newParent;
					if ( oldValue instanceof ZWrapper ) {
						newScope = oldValue.scope_;
						newParent = oldValue.parent_;
					} else {
						newScope = zobject.scope_;
						newParent = zobject.parent_;
					}
					zobject[ key ].scope_ = newScope;
					zobject[ key ].parent_ = newParent;
				}
			}
		}
		subResultPromises.push( maybeEagerlyEvaluate(
			zobject[ key ], invariants, ignoreList, resolveInternals, doValidate, nextKeyList ) );
	}

	for ( const subResult of await ( Promise.all( subResultPromises ) ) ) {
		if ( subResult !== null ) {
			return subResult;
		}
	}

	// zobject.setIsFullyResolved( true );
	return null;
}

class Frame extends BaseFrame {

	constructor( lastFrame = null ) {
		if ( lastFrame === null ) {
			lastFrame = new EmptyFrame();
		}
		super( lastFrame );
	}

	/**
	 * Add new name and argument to this frame.
	 *
	 * @param {string} name
	 * @param {ArgumentState} argumentState an ArgumentState, what else?
	 */
	setArgument( name, argumentState ) {
		this.names_.set( name, argumentState );
	}

	async processArgument(
		argumentDict, invariants, doValidate, resolveInternals, eagerlyEvaluateWithCallCount,
		ignoreList ) {
		const argumentEnvelope = await ( argumentDict.argument.resolve(
			invariants, ignoreList, resolveInternals, doValidate,
			/* evenBuiltins= */ true ) );
		if ( responseEnvelopeContainsError( argumentEnvelope ) ) {
			return ArgumentState.ERROR( getError( argumentEnvelope ) );
		}
		const argument = argumentEnvelope.Z22K1;

		// First evaluate the argument. Evaluation may be eager or shallow
		// depending on the provided flag.
		if ( eagerlyEvaluateWithCallCount ) {
			const eagerEvaluationEnvelope = await maybeEagerlyEvaluate(
				argument, invariants, /* ignoreList= */ null,
				resolveInternals, doValidate );
			if ( eagerEvaluationEnvelope !== null ) {
				return ArgumentState.ERROR( getError( eagerEvaluationEnvelope ) );
			}
		}

		// TODO (T296675): "doValidate" is a heavy-handed hack to avoid infinite
		// recursion. Better solutions include
		//  -   validating directly with schemata if the type is built-in,
		//      otherwise using a Function;
		//  -   validating directly with schemata in all the cases where
		//      doValidate is currently false, otherwise using a Function;
		//  -   caching and reusing the results of function calls
		// Validate the argument as its declared type.
		if ( doValidate && resolveInternals ) {
			const typeError = await resolveTypes( argument, invariants );
			if ( typeError !== null ) {
				return typeError;
			}
			const declaredType = argumentDict.declaredType;
			const declaredResult = await validateAsType( argument, invariants, declaredType );
			if ( responseEnvelopeContainsError( declaredResult ) ) {
				return ArgumentState.ERROR(
					makeErrorInNormalForm(
						error.argument_type_mismatch,
						[ declaredType, argument.Z1K1, argument,
							getError( declaredResult ) ] ) );
			}
		}

		return ArgumentState.EVALUATED( {
			name: argumentDict.name,
			argument: argument
		} );
	}

	/**
	 * Ascend enclosing scopes to find instantiation of argument with provided name.
	 *
	 * @param {string} argumentName
	 * @param {Invariants} invariants evaluator, resolver: invariants preserved
	 *      over all function calls
	 * @param {boolean} lazily
	 * @param {boolean} doValidate if false, then the argument will be executed
	 *      without validating return type (if it's a Z7)
	 * @param {boolean} resolveInternals if false, will evaluate typed lists via shortcut
	 *      and will not validate attributes of Z7s
	 * @param {boolean} eagerlyEvaluateWithCallCount whether to eagerly evaluate the argument
	 * @param {Set(MutationType)} ignoreList which types of mutations to ignore
	 *      when resolving function calls and references
	 * @return {Object} argument instantiated with given name in lowest enclosing scope
	 * along with enclosing scope
	 */
	async retrieveArgument(
		argumentName, invariants, lazily = false,
		doValidate = true, resolveInternals = true, eagerlyEvaluateWithCallCount = true,
		ignoreList = null ) {
		let boundValue = this.names_.get( argumentName );
		let doSetBoundValue = false;

		// Name does not exist in this scope; look in the previous one
		// (or return null if no previous scope).
		if ( boundValue === undefined ) {
			doSetBoundValue = true;
			boundValue = await this.lastFrame_.retrieveArgument(
				argumentName, invariants, lazily, doValidate,
				resolveInternals, eagerlyEvaluateWithCallCount, ignoreList );
		} else if ( boundValue.state === 'UNEVALUATED' && !lazily ) {
			doSetBoundValue = true;
			// If boundValue is in the ERROR or EVALUATED state, it has already
			// been evaluated and can be returned directly.
			// If state is UNEVALUATED and evaluation is not lazy, the argument
			// may need to be evaluated before returning (e.g., if a Z9, Z18,
			// or Z7).
			const argumentDict = boundValue.argumentDict;
			const evaluatedArgument = await this.processArgument(
				argumentDict, invariants, doValidate, resolveInternals,
				eagerlyEvaluateWithCallCount, ignoreList );
			if ( evaluatedArgument.state === 'ERROR' ) {
				boundValue = evaluatedArgument;
			} else if ( evaluatedArgument.state === 'EVALUATED' ) {
				const newDict = {
					name: argumentName,
					argument: evaluatedArgument.argumentDict.argument,
					declaredType: argumentDict.declaredType
				};
				boundValue = ArgumentState.EVALUATED( newDict );
			} else {
				// TODO (T296676): Throw error here, since this shouldn't happen.
			}
		}
		if ( doSetBoundValue ) {
			this.setArgument( argumentName, boundValue );
		}
		return boundValue;
	}

}

/**
 * Resolves function eternals, particularly the Z7K1/Function and Z8K1/argument declarations.
 *
 * @param {ZWrapper} zobject
 * @param {Invariants} invariants evaluator, resolver: invariants preserved over all function calls
 * @param {boolean} doValidate whether to validate types of arguments and return values
 * @return {Object|null} either a Z22 containing an error state or null
 */
async function resolveFunctionInternals( zobject, invariants, doValidate ) {
	const Z7K1Envelope = await ( zobject.resolveEphemeral(
		[ 'Z7K1' ], invariants, /* ignoreList= */ null, /* resolveInternals= */ true, doValidate ) );
	if ( responseEnvelopeContainsError( Z7K1Envelope ) ) {
		return Z7K1Envelope;
	}
	const Z8K1Envelope = await ( zobject.Z7K1.resolveEphemeral(
		[ 'Z8K1' ], invariants, /* ignoreList= */ null, /* resolveInternals= */ false, doValidate ) );
	// This usually happens because dereferencing can't occur during validation
	// (and is expected).
	if ( responseEnvelopeContainsError( Z8K1Envelope ) ) {
		return Z8K1Envelope;
	}

	// If resolution of any Z17/Argument declaration goes badly, we should return the
	// first error. We will return null if no error occurs.
	let badEnvelope = null;
	await traverseZList( zobject.Z7K1.Z8K1, async ( tail ) => {
		// No reason to keep going if an error has already occurred.
		if ( badEnvelope !== null ) {
			return;
		}
		const Z17 = tail.K1;
		for ( const key of [ 'Z17K1', 'Z17K2' ] ) {
			const keyEnvelope = await ( Z17.resolveEphemeral(
				[ key ], invariants,
				/* ignoreList= */ null, /* resolveInternals= */ false, doValidate ) );
			if ( responseEnvelopeContainsError( keyEnvelope ) ) {
				badEnvelope = keyEnvelope;
			}
		}
	} );

	// This will be either the first error encountered when resolving Z17s or null.
	return badEnvelope;
}

/**
 * Validates the Z7K1/Function and the labels of argument declarations.
 *
 * @param {ZWrapper} zobject
 * @param {Invariants} invariants evaluator, resolver: invariants preserved over all function calls
 * @return {Object|null} either a Z22 containing an error state or null
 */
async function validateFunctionInternals( zobject, invariants ) {
	async function runBuiltinTypeValidator( someObject ) {
		const theTypeEnvelope = await someObject.Z1K1.resolve(
			invariants,
			/* ignoreList= */ null,
			/* resolveInternals= */ false,
			/* doValidate= */ false,
			/* evenBuiltins= */ true );
		if ( responseEnvelopeContainsError( theTypeEnvelope ) ) {
			return theTypeEnvelope;
		}
		return await invariants.validateTypesWithCallCount(
			quoteZObject( someObject ), theTypeEnvelope.Z22K1, invariants );
	}
	const functionValidationResult = await runBuiltinTypeValidator( zobject.Z7K1 );
	if ( responseEnvelopeContainsError( functionValidationResult ) ) {
		return functionValidationResult;
	}
	return null;
}

/**
 * Retrieve argument declarations and instantiations from a Z7.
 *
 * @param {Object} zobject
 * @return {Array} list of ArgumentStates, containing argument names or errors
 */
async function getArgumentStates( zobject ) {
	const argumentStates = [];
	const foundKeys = new Set( zobject.keys() );
	foundKeys.delete( 'Z1K1' );
	foundKeys.delete( 'Z7K1' );
	await traverseZList( zobject.Z7K1.Z8K1, ( tail ) => {
		const Z17 = tail.K1;
		const argumentDict = {};
		const argumentName = Z17.Z17K2.Z6K1;
		argumentDict.name = argumentName;
		// TODO (T292787): This is flaky to rely on; find a better way to determine type.
		argumentDict.declaredType = Z17.Z17K1;
		let key = argumentName;
		if ( zobject[ key ] === undefined ) {
			const localKeyRegex = /K[1-9]\d*$/;
			key = key.match( localKeyRegex )[ 0 ];
		}

		const argument = zobject[ key ];

		if ( argument === undefined ) {
			const subError = makeErrorInNormalForm(
				error.key_not_found,
				[ wrapInKeyReference( argumentName ), wrapInQuote( zobject.asJSON() ) ]
			);
			argumentStates.push( ArgumentState.ERROR( subError ) );
		} else {
			foundKeys.delete( key );
			argumentDict.argument = argument;
			argumentStates.push( ArgumentState.UNEVALUATED( argumentDict ) );
		}
	} );

	for ( const extraKey of foundKeys ) {
		const subError = makeErrorInNormalForm(
			error.invalid_key,
			[ wrapInKeyReference( extraKey ) ]
		);
		argumentStates.push( ArgumentState.ERROR( subError ) );
	}

	return argumentStates;
}

/**
 * Ensure that result of a function call comports with declared type.
 *
 * FIXME (T311055): validateReturn might require normal form. Check and document.
 *
 * @param {Object} result
 * @param {Object} zobject
 * @param {Invariants} invariants evaluator, resolver: invariants preserved over all function calls
 * @return {Object} zobject if validation succeeds; error tuple otherwise
 */
async function validateReturnType( result, zobject, invariants ) {
	if ( !responseEnvelopeContainsError( result ) ) {
		const emptyResponseEnvelopeOk = ( findFunctionIdentity( zobject.Z7K1 ).Z9K1 === 'Z820' );
		if ( !responseEnvelopeContainsValue( result ) && !emptyResponseEnvelopeOk ) {
			// Neither value nor error.
			// TODO (T318293): Can we add modification of the Z22 internals to the ZWrapper concept?
			const resultJSON = result.asJSON();
			return makeWrappedResultEnvelope(
				null,
				makeErrorInNormalForm(
					error.invalid_evaluation_result,
					[ wrapInQuote( resultJSON ) ]
				)
			);
		}

		// Value returned; validate its return type..
		await ( zobject.resolveEphemeral( [ 'Z7K1', 'Z8K2' ], invariants ) );
		const returnType = zobject.Z7K1.Z8K2;
		await resolveTypes( result.Z22K1, invariants, /* doValidate= */ true );
		const returnTypeValidation = await validateAsType(
			result.Z22K1, invariants, returnType
		);
		if ( responseEnvelopeContainsError( returnTypeValidation ) ) {
			return makeWrappedResultEnvelope(
				null,
				makeErrorInNormalForm(
					error.return_type_mismatch,
					[
						returnType,
						result.Z22K1.Z1K1,
						result.Z22K1,
						getError( returnTypeValidation )
					]
				)
			);
		}
		// If we got here, it's got a value, no error, and validates, so return as-is.
		return result;
	}

	if ( responseEnvelopeContainsValue( result ) ) {
		// Both value and error.
		const resultJSON = result.asJSON();
		return makeWrappedResultEnvelope(
			null,
			makeErrorInNormalForm( error.invalid_evaluation_result, [ wrapInQuote( resultJSON ) ] )
		);
	}

	// No value but some error.
	return result;
}

/**
 * Add implementation-specific metadata elements to the metadata map in the
 * Evaluation result (response envelope).
 *
 * @param {Implementation} implementation
 * @param {ZWrapper} result (Z22 / Evaluation result)
 */
function addImplementationMetadata( implementation, result ) {
	const implementationId = implementation.getZID(); // Can be null
	let implementationType;
	if ( implementation instanceof Composition ) {
		implementationType = 'Z14K2';
	} else if ( implementation instanceof Evaluated ) {
		implementationType = 'Z14K3';
	} else {
		implementationType = 'Z14K4';
	}
	const newPairs = [];
	if ( implementationId !== null ) {
		newPairs.push( [ { Z1K1: 'Z6', Z6K1: 'implementationId' }, { Z1K1: 'Z6', Z6K1: implementationId } ] );
	}
	newPairs.push( [ { Z1K1: 'Z6', Z6K1: 'implementationType' }, { Z1K1: 'Z6', Z6K1: implementationType } ] );
	setMetadataValues( result, newPairs );
}

/**
 * Run an implementation. Raise an exception if the implmentation was Evaluated
 * and the evaluator service fails or is not running.
 *
 * @param {ZWrapper} zobject the Function Call to be run
 * @param {Invariants} invariants
 * @param {boolean} doValidate
 * @param {Implementation} implementation
 * @param {boolean} resolveInternals
 * @param {Map} argumentStates
 * @param {boolean} eagerlyEvaluateWithCallCount
 * @throws {EvaluatorError}
 * @return {ZWrapper}
 */
async function runThatImplementation(
	zobject,
	invariants,
	doValidate,
	implementation,
	resolveInternals,
	argumentStates,
	eagerlyEvaluateWithCallCount
) {
	if ( implementation === null ) {
		return makeWrappedResultEnvelope(
			null,
			makeErrorInNormalForm(
				error.not_implemented_yet,
				[ findFunctionIdentity( zobject.Z7K1 ) ]
			)
		);
	}

	// This additional scope allows us to capture arguments at the right time.
	// Without this, closures are bound late, and undesirable behavior emerges.
	// For example, recursive functions that operate elementwise over lists
	// will end up being called with the final list element on every iteration.
	const theScope = new Frame( implementation.getZ14().getScope() );
	implementation.getZ14().setScope( theScope );
	for ( const argumentState of argumentStates ) {
		theScope.setArgument( argumentState.argumentDict.name, argumentState );
	}

	const argumentInstantiations = [];
	if ( !( implementation instanceof Composition ) ) {
		// Populate arguments from scope.
		const instantiationPromises = [];
		for ( const argumentState of argumentStates ) {
			const argumentDict = argumentState.argumentDict;
			instantiationPromises.push( async function () {
				const instantiation = await theScope.retrieveArgument(
					argumentDict.name, invariants,
					implementation.hasLazyVariable( argumentDict.name ),
					doValidate,
					resolveInternals,
					eagerlyEvaluateWithCallCount );
				return instantiation;
			}() );
		}
		for ( const instantiation of await Promise.all( instantiationPromises ) ) {
			if ( instantiation.state === 'ERROR' ) {
				return makeWrappedResultEnvelope( null, instantiation.error );
			}
			argumentInstantiations.push( instantiation.argumentDict );
		}
	}

	// Equip the implementation for its journey and execute.
	implementation.invariants = invariants;
	implementation.doValidate = doValidate;
	return await implementation.execute( zobject, argumentInstantiations );
}

/**
 * Same as {@link execute} but assumes a new frame has already been created in the scope and does
 * not recursively resolve the subobjects.
 *
 * @param {ZWrapper} zobject
 * @param {Invariants} invariants
 * @param {boolean} doValidate
 * @param {ImplementationSelector} implementationSelector
 * @param {boolean} resolveInternals
 * @param {boolean} topLevel whether this is the top-level Z7 sent to the orchestrator
 * @param {boolean} eagerlyEvaluateWithCallCount whether to expand arguments fully
 * @return {ZWrapper}
 */
async function executeInternal(
	zobject, invariants, doValidate = true,
	implementationSelector = null, resolveInternals = true,
	topLevel = false, eagerlyEvaluateWithCallCount = true ) {

	if ( isGenericListType( zobject ) && !resolveInternals ) {
		// TODO (T305459): Tighten number of cases where `resolveInternals` is set to false.
		return ZWrapper.create(
			await resolveListType( zobject.Z881K1 ),
			// Use an empty scope for the outer object, the nested object should already have its
			// own scope, if any.
			new EmptyFrame()
		);
	}

	const newScope = new Frame( zobject.getScope() );
	zobject.setScope( newScope );
	const zObjectKey = createZObjectKey( zobject.asJSONEphemeral() );
	if ( invariants && invariants.orchestratorConfig.addNestedMetadata ) {
		newScope.metadata.set( 'zObjectKey', zObjectKey );
	}
	// Resolve all argument declarations.
	const resolutionEnvelope = await resolveFunctionInternals( zobject, invariants, doValidate );
	if ( resolutionEnvelope !== null ) {
		return resolutionEnvelope;
	}

	// Validate Function and Labels for Argument Declarations.
	if ( doValidate ) {
		const validationEnvelope = await validateFunctionInternals( zobject, invariants );
		if ( validationEnvelope !== null ) {
			return validationEnvelope;
		}
	}

	// Retrieve argument declarations and instantiations.
	const argumentStates = await getArgumentStates( zobject );
	for ( const argumentState of argumentStates ) {
		if ( argumentState.state === 'ERROR' ) {
			return makeWrappedResultEnvelope(
				null,
				makeErrorInNormalForm(
					error.error_in_evaluation,
					[ wrapInQuote( zobject.asJSON() ), argumentState.error ]
				)
			);
		}
	}

	const Z7K1 = zobject.Z7K1;
	Z7K1.Z8K4.resolveEphemeral(
		[ 'Z8K4' ], invariants, /* ignoreList= */ null, /* resolveInternals= */ false,
		doValidate );
	const Z8K4 = Z7K1.Z8K4;

	let implementations = await traverseZList( Z8K4, async ( tail ) => {
		// ZID captures the persistent ID when Z14 is a Z9 / Reference,
		// for Composition and Evaluated implementations.
		// TODO (T321998): If an ID key is added to Z14, this can be removed
		let ZID = null;
		if ( isZReference( tail.K1 ) ) {
			ZID = tail.K1.Z9K1;
		}
		const Z14Envelope = await tail.resolveEphemeral(
			[ 'K1' ],
			invariants, /* ignoreList= */null, /* resolveInternals= */ false,
			doValidate );
		let impl;
		try {
			impl = await ( Implementation.create( tail.K1, Z14Envelope, ZID ) );
		} catch ( err ) {
			if ( err instanceof ZResponseError ) {
				return { error: err.envelope };
			} else {
				throw err; // unknown error; rethrow
			}
		}
		if ( impl ) {
			return { implementation: impl };
		} else {
			const stringifiedResult = safeJsonStringify( tail.K1 );
			invariants.logger.log( 'error',
				{
					message: 'Could not create implementation from Z14',
					requestId: invariants.requestId,
					info: stringifiedResult
				}
			);
		}
	} );
	implementations = implementations
		.map( ( implementation ) => implementation.implementation )
		.filter( ( implementation ) => implementation !== undefined );

	if ( implementations.length === 0 ) {
		return makeWrappedResultEnvelope(
			null,
			makeErrorInNormalForm(
				error.not_implemented_yet,
				[ findFunctionIdentity( zobject.Z7K1 ) ]
			)
		);
	}

	if ( implementationSelector === null ) {
		implementationSelector = new FirstImplementationSelector();
	}

	let implementation = null, result = null, lastError = null;
	for ( const generatedImplementation of implementationSelector.generate( implementations ) ) {
		implementation = generatedImplementation;
		invariants.implementationZID = implementation.ZID_;
		try {
			result = await runThatImplementation(
				zobject, invariants, doValidate, implementation, resolveInternals,
				argumentStates, eagerlyEvaluateWithCallCount );
			break;
		} catch ( err ) {
			if ( invariants.orchestratorConfig.generateFunctionsMetrics ) {
				const errorMessage = `function implementation: ${ invariants.implementationZID } error: ${ err }`;
				invariants.logger.log( 'error',
					{
						message: errorMessage,
						requestId: invariants.requestId
					}
				);
				sUtil.sendImplementationErrorMetrics( invariants.req, errorMessage );
			}
			if ( err instanceof EvaluatorError ) {
				lastError = err;
				continue;
			} else {
				throw err; // unknown error; rethrow
			}
		}
	}

	if ( result === null ) {
		// lastError is guaranteed to be set.
		// TODO (T350716): Test this code path.
		return makeWrappedResultEnvelope(
			null,
			ErrorFormatter.wrapMessageInEvaluationError(
				lastError.message,
				zobject.asJSONEphemeral()
			)
		);
	}

	// Execute result if implementation is lazily evaluated.
	if ( implementation.returnsLazy() && !isZ24( result.Z22K1 ) ) {
		await ( result.resolveKey(
			[ 'Z22K1' ], invariants, /* ignoreList= */ null,
			/* resolveInternals= */ true, doValidate ) );
		if ( eagerlyEvaluateWithCallCount ) {
			const subResult = await maybeEagerlyEvaluate(
				result.Z22K1, invariants, /* ignoreList= */ null,
				resolveInternals, doValidate );
			if ( subResult !== null ) {
				return subResult;
			}
		}
	}

	if ( topLevel ) {
		addImplementationMetadata( implementation, result );
		// TODO (T367787): Figure out a more robust way to attach function call's
		// nested metadata to the result's metadata, and apply that solution
		// to non-top-level objects.
		const resultMetadata = result.getScope().metadata;
		let resultNestedMetadata = resultMetadata.get( 'nestedMetadataArray' );
		if ( resultNestedMetadata === undefined ) {
			resultNestedMetadata = [];
			resultMetadata.set( 'nestedMetadataArray', resultNestedMetadata );
		}
		const zObjectMetadata = zobject.getScope().metadata;
		const zObjectNestedMetadata = zObjectMetadata.get( 'nestedMetadataArray' );
		if ( zObjectNestedMetadata ) {
			for ( const nestedMetadatum of zObjectNestedMetadata ) {
				if ( nestedMetadatum !== resultMetadata ) {
					resultNestedMetadata.push( nestedMetadatum );
				}
			}
		}
		const zObjectKey = zObjectMetadata.get( 'zObjectKey' );
		if ( zObjectKey ) {
			resultMetadata.set( 'zObjectKey', zObjectKey );
		}
	}
	return result;
}

async function maybeResolveDanglingReferences( zobject, invariants ) {
	if ( !( zobject instanceof ZWrapper ) ) {
		return;
	}
	if ( zobject.isFullyResolved() ) {
		return null;
	}
	await invariants.resolveDanglingReferencesWithCallCount( zobject, invariants );
}

/**
 * Helper recursive function that 'resolves' any remaining unresolved nested zobjects
 *
 * @param {Object} zobject
 * @param {Object} invariants
 * @return {ZWrapper} zobject instance of ZWrapper, key (i.e. Z22) of evaluated ZObject
 */
async function resolveDanglingReferences( zobject, invariants ) {
	for ( const key of zobject.keys() ) {
		const oldValue = zobject[ key ];
		if ( isZArgumentReference( oldValue ) ) {
			const valueEnvelope = await ( oldValue.resolve(
				invariants, /* ignoreList= */ new Set( [
					MutationType.REFERENCE, MutationType.FUNCTION_CALL,
					MutationType.GENERIC_INSTANCE
				] ), /* resolveInternals= */ false, /* doValidate= */ true ) );
			// It's okay for some Z18s not to have values assigned.
			// TODO (T305981): We should formally distinguish between unbound
			// and unassigned variables. This will constrain further the errors
			// that we let slide here.
			if ( !responseEnvelopeContainsError( valueEnvelope ) ) {
				const newValue = valueEnvelope.Z22K1;
				zobject.setName( key, newValue );
				if ( newValue instanceof ZWrapper ) {
					let newScope, newParent;
					if ( oldValue instanceof ZWrapper ) {
						newScope = oldValue.scope_;
						newParent = oldValue.parent_;
					} else {
						newScope = zobject.scope_;
						newParent = zobject.parent_;
					}
					zobject[ key ].scope_ = newScope;
					zobject[ key ].parent_ = newParent;
				}
			}
		}
		await maybeResolveDanglingReferences( zobject[ key ], invariants );
	}
}

/**
 * Given ZWrapper representing a function call ZObject, resolves the function, selects an
 * implementation, and executes it with the supplied arguments.
 *
 * @param {ZWrapper} zobject object describing a function call
 * @param {Invariants} invariants evaluator, resolver: invariants preserved over all function calls
 * @param {boolean} doValidate whether to validate types of arguments and return value
 * @param {ImplementationSelector} implementationSelector
 * @param {boolean} resolveInternals if false, will evaluate typed lists via shortcut
 *      and will not validate attributes of Z7s
 * @param {boolean} topLevel whether this is the top-level Z7 sent to the orchestrator
 * @param {boolean} eagerlyEvaluateWithCallCount whether to expand arguments fully
 * @return {ZWrapper} result of executing function call
 */
execute = async function (
	zobject, invariants = null, doValidate = true,
	implementationSelector = null, resolveInternals = true, topLevel = false,
	eagerlyEvaluateWithCallCount = true ) {

	if ( invariants !== null ) {
		const rateLimitError = invariants.rateLimiter.increment( invariants.requestId );
		if ( rateLimitError !== null ) {
			invariants.logger.log(
				'warn',
				{ message: 'Rate limit: stack limit reached or exceeded', requestId: invariants.requestId }
			);
			return rateLimitError;
		}
	}

	let functionIdentity, functionMetricsInstance, startTime;
	if ( invariants.orchestratorConfig.generateFunctionsMetrics ) {
		functionIdentity = findFunctionIdentity( zobject.Z7K1 ).Z9K1;
		// instantiate metrics data on function performance
		startTime = performance.now();
		functionMetricsInstance = sUtil.createFunctionDurationMetrics( invariants.req.app );
	}

	let result = await executeInternal(
		zobject, invariants, doValidate,
		implementationSelector, resolveInternals, topLevel, eagerlyEvaluateWithCallCount );
	if ( topLevel ) {
		await maybeResolveDanglingReferences( result.Z22K1, invariants );
		result = await validateReturnType( result, zobject, invariants );
	}
	if ( invariants !== null ) {
		const rateLimitError = invariants.rateLimiter.decrement( invariants.requestId );
		if ( rateLimitError !== null ) {
			return rateLimitError;
		}
	}

	if ( invariants.orchestratorConfig.generateFunctionsMetrics ) {
		// finish creating metrics on function performance
		sUtil.submitFunctionDurationMetricsAndLog(
			invariants,
			functionMetricsInstance,
			startTime,
			functionIdentity
		);
		sUtil.sendFunctionExecutionCounterMetrics( invariants.req );
	}

	return result;
};

module.exports = {
	eagerlyEvaluate, execute, getArgumentStates, resolveDanglingReferences
};
