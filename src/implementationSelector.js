'use strict';

/*!
 * Wikifunctions orchestrator code to select which Implementation of a Function to run.
 *
 * @copyright 2020– Abstract Wikipedia team; see LICENSE
 * @license MIT
 */

/**
 * Implementation of Knuth's Algorithm P for random shuffling. We don't want to
 * reorder implementations in-place (for now), so this function returns indices
 * into the original list.
 *
 * @param {number} numberOfElements an array of implementations
 * @return {Array[Number]} randomly-sorted indices
 */
function randomlyShuffledIndices( numberOfElements ) {
	const resultIndices = [ ...Array( numberOfElements ).keys() ];
	for ( let i = resultIndices.length - 1; i > 0; --i ) {
		const j = Math.ceiling( Math.random() * i );
		[ resultIndices[ j ], resultIndices[ i ] ] = [ resultIndices[ i ], resultIndices[ j ] ];
	}
	return resultIndices;
}

class RandomImplementationSelector {

	/**
	 * Generator function that invokes random shuffling for implementations.
	 * Per shuffle, it returns (yields) the index value of the implementation shuffled.
	 *
	 * @param {Array} implementations array of implementations
	 * @yield {number} index of each implementation per execution of shuffle
	 */
	* generate( implementations ) {
		for ( const index of randomlyShuffledIndices( implementations.length ) ) {
			yield implementations[ index ];
		}
	}

}

class FirstImplementationSelector {

	/**
	 * Generator function that invokes and then returns (yields) the implementation result.
	 *
	 * @param {Array} implementations array of implementations
	 * @yield {Object} result from the implementation
	 */
	* generate( implementations ) {
		for ( const implementation of implementations ) {
			yield implementation;
		}
	}

}

module.exports = { RandomImplementationSelector, FirstImplementationSelector };
