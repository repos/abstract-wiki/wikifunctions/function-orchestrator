'use strict';

/*!
 * Wikifunctions orchestrator class to model Implementation instances.
 *
 * @copyright 2020– Abstract Wikipedia team; see LICENSE
 * @license MIT
 */

const { isGenericListType, makeWrappedResultEnvelope, responseEnvelopeContainsError, traverseZList } = require( './utils.js' );
const { ZEnvelopeWrapper, ZWrapper } = require( './ZWrapper.js' );
const { convertItemArrayToZList, isZReference } = require( '../function-schemata/javascript/src/utils.js' );
const { error, makeErrorInNormalForm } = require( '../function-schemata/javascript/src/error.js' );

/**
 * Error class for throwing a Z22/'Evaluation response' (envelope) that
 * contains an error (in Z22K2/metadata).
 */
class ZResponseError extends Error {
	constructor( message, envelope ) {
		super( message );
		this.name = 'ZResponseError';
		this.envelope = envelope;
	}
}

class EvaluatorError extends Error { }

class Implementation {

	constructor( Z14, ZID ) {
		this.invariants_ = null;
		this.lazyVariables_ = new Set();
		this.lazyReturn_ = false;
		this.doValidate_ = true;
		this.Z14_ = Z14;
		// Persistent ID for the implementation; null if there is none
		this.ZID_ = ZID;

		Object.defineProperty( this, 'invariants', {
			get: function () {
				return this.invariants_;
			},
			set: function ( invariants ) {
				this.invariants_ = invariants;
			}
		} );
		Object.defineProperty( this, 'doValidate', {
			get: function () {
				return this.doValidate_;
			},
			set: function ( doValidate ) {
				this.doValidate_ = doValidate;
			}
		} );
	}

	hasLazyVariable( variableName ) {
		return this.lazyVariables_.has( variableName );
	}

	returnsLazy() {
		return this.lazyReturn_;
	}

	getZID() {
		return this.ZID_;
	}

	getZ14() {
		return this.Z14_;
	}

	async execute( zobject, argumentList ) {
		const executionResult = await this.executeInternal( zobject, argumentList );
		try {
			if ( !( executionResult instanceof ZEnvelopeWrapper ) ) {
				// TODO (T365194): This extra call to ZWrapper.create is just
				// to "verify" (-ish) that the Z22K2/Metadata is in normal form
				// and doesn't contain anything weird. If it does contain
				// something weird, ZWrapper.create() throws an error here.
				// We should either strengthen the evaluator's post-conditions
				// to obviate this or do a proper validation check here.
				ZWrapper.create(
					executionResult.Z22K2, this.getZ14().scope_, this.getZ14().parent_ );
			}
			const result = ZWrapper.create(
				executionResult, this.getZ14().scope_, this.getZ14().parent_ );
			return result;
		} catch ( err ) {
			return makeWrappedResultEnvelope(
				null,
				makeErrorInNormalForm(
					error.invalid_format,
					[ JSON.stringify( executionResult ) ] ) );
		}
	}

	/**
	 * Creates and returns a function implementation for the given Z14,
	 * as an instance of one of the subclasses Composition, Evaluated,
	 * or BuiltIn.  If an error occurs, an instance of subclass
	 * ImplementationError is returned.
	 *
	 * invariants and doValidate are used locally in this method. Otherwise, they
	 * can be set and retrieved via their accessors (e.g. invariants).
	 *
	 * @param {Object} Z14 the implementation
	 * @param {Object} Z14Envelope
	 * @param {string} ZID
	 * @return {Implementation}
	 * @throws {ZResponseError} If the call to resolve() returns an error
	 */
	static async create( Z14, Z14Envelope, ZID ) {
		if ( typeof Z14 === 'undefined' ) {
			return null;
		}
		if ( responseEnvelopeContainsError( Z14Envelope ) ) {
			throw new ZResponseError( 'Error returned from call to resolve', Z14Envelope );
		}

		if ( Z14.Z14K4 !== undefined ) {
			const BuiltInZID = Z14.Z14K4.Z6K1;
			const { getFunction, getLazyVariables, getLazyReturn } = require( './builtins.js' );
			const builtin = getFunction( BuiltInZID );
			const lazyVariables = getLazyVariables( BuiltInZID );
			const lazyReturn = getLazyReturn( BuiltInZID );
			// eslint-disable-next-line no-use-before-define
			return new BuiltIn( Z14, BuiltInZID, builtin, lazyVariables, lazyReturn );
		}
		if ( Z14.Z14K2 !== undefined ) {
			// eslint-disable-next-line no-use-before-define
			return new Composition( Z14, ZID );
		}

		if ( Z14.Z14K3 !== undefined ) {
			// eslint-disable-next-line no-use-before-define
			return new Evaluated( Z14, ZID );
		}

		throw new ZResponseError( 'Implementation did not specify Z14K[234]', Z14Envelope );
	}
}

class BuiltIn extends Implementation {

	constructor( Z14, ZID, functor, lazyVariables, lazyReturn ) {
		super( Z14, ZID );
		for ( const variable of lazyVariables ) {
			this.lazyVariables_.add( variable );
		}
		this.lazyReturn_ = lazyReturn;
		this.functor_ = functor;
	}

	/**
	 * Calls this implementation's functor with the provided arguments.
	 *
	 * @param {Object} zobject
	 * @param {Array} argumentList
	 * @return {Object} the result of calling this.functor_ with provided arguments
	 */
	executeInternal( zobject, argumentList ) {
		const keys = [];
		const nameToArgument = new Map();
		for ( const argumentDict of argumentList ) {
			keys.push( argumentDict.name );
			nameToArgument.set( argumentDict.name, argumentDict.argument );
		}
		keys.sort();
		const callArgs = [];
		for ( const key of keys ) {
			callArgs.push( nameToArgument.get( key ) );
		}
		callArgs.push( this.invariants_ );
		return this.functor_( ...callArgs );
	}
}

/**
 * Used for resolving code in an implementation
 *
 * @param {Object} codeObject an unresolved ZObject
 * @param {Object} implementation the implementation object of the ZObject to resolve
 * @return {Object} resolved ZObject
 */
async function resolveCodeObject( codeObject, implementation ) {
	// Code string.
	const envelope = await codeObject.resolveEphemeral(
		[ 'Z16K1', 'Z61K1' ],
		implementation.invariants,
		/* ignoreList= */ null,
		/* resolveInternals= */ false,
		implementation.doValidate );
	if ( responseEnvelopeContainsError( envelope ) ) {
		return envelope;
	}
	// Programming language string.
	return await codeObject.resolveEphemeral(
		[ 'Z16K2' ],
		implementation.invariants,
		/* ignoreList= */ null,
		/* resolveInternals= */ false,
		implementation.doValidate );
}

/**
 * Used for resolving objects in a List Type during implementation
 *
 * @param {Object} codeConverterList the List object
 * @param {string} converterKey e.g. 'Z64K1'
 * @param {Object} implementation the implementation object of the ZObject to resolve
 * @return {Promise<Array[Object]>} the result of resolving objects and keys in the list,
 * caching them for future calls, and finally running callback on the List elements
 */
async function resolveCodeConverterList( codeConverterList, converterKey, implementation ) {
	return await traverseZList(
		codeConverterList,
		async ( tail ) => {
			await tail.resolveEphemeral(
				[ 'K1', converterKey ],
				implementation.invariants,
				/* ignoreList= */ null,
				/* resolveInternals= */ false,
				implementation.doValidate );
			// TODO (T360243): Error early here if resolution failed.
			await resolveCodeObject(
				tail.getNameEphemeral( 'K1' ).getNameEphemeral( converterKey ),
				implementation );
		} );
}

/**
 * Resolves any remaining unresolved elements in List type,
 * returning null or the Object containing error if any arise
 *
 * @param {Object} sourceZWrapper ZObject, an instance of ZWrapper
 * @param {Object} destinationObject
 * @param {string} destinationObjectKey
 * @param {string} codeConverterListKey
 * @param {string} converterKey
 * @param {Object} implementation the implementation object of the ZObject to resolve
 * @return {Object|null} null or ZObject with an error
 */
async function maybeResolveListElementType(
	sourceZWrapper, destinationObject, destinationObjectKey,
	codeConverterListKey, converterKey, implementation ) {
	if ( !isGenericListType( sourceZWrapper ) ) {
		return null;
	}
	const elementTypeEnvelope = await sourceZWrapper.Z4K2.K1.Z3K1.resolve(
		implementation.invariants, /* ignoreList= */ null, /* resolveInternals= */ true,
		implementation.doValidate );
	if ( responseEnvelopeContainsError( elementTypeEnvelope ) ) {
		return elementTypeEnvelope;
	}
	const elementType = elementTypeEnvelope.Z22K1;
	const codeConverterList = elementType[ codeConverterListKey ];
	if ( codeConverterList === undefined ) {
		// Type does not have type converters; return.
		return null;
	}
	await resolveCodeConverterList( codeConverterList, converterKey, implementation );
	// asJSONEphemeral is needed here because original code converters may have
	// been supplied as e.g. Z9/References.
	destinationObject[ destinationObjectKey ].Z4K2.K1.Z3K1[
		codeConverterListKey
	] = codeConverterList.asJSONEphemeral();
	return null;
}

class Evaluated extends Implementation {

	constructor( Z14, ZID ) {
		super( Z14, ZID );
	}

	/**
	 * Calls this implementation's functor with the provided arguments.
	 *
	 * @param {Object} zobject
	 * @param {Array} argumentList
	 * @return {Object} the result of calling this.functor_ with provided arguments
	 */
	async executeInternal( zobject, argumentList ) {
		// Arguments should already be fully resolved, but any other attributes
		// of the Z7 which are Z9s/Z18s must be resolved before dispatching
		// to the function evaluator.
		const Z7 = {};
		await ( zobject.resolveEphemeral(
			[ 'Z7K1', 'Z8K2' ], this.invariants, /* ignoreList= */ null,
			/* resolveInternals= */ true, this.doValidate ) );
		const Z7K1Envelope = await ( zobject.Z7K1.resolve(
			this.invariants, /* ignoreList= */ null,
			/* resolveInternals= */ true, this.doValidate ) );
		const Z7K1 = Z7K1Envelope.Z22K1;

		// Create a minimal Z7K1/Z8 to generate the executor request.
		Z7.Z7K1 = {};

		// Z8K5 needs to be resolved fully.
		Z7.Z7K1.Z8K5 = Z7K1.Z8K5.asJSONEphemeral();

		// Populate Z8K2/Return type.
		// Return type may be a function call and must be resolved to allow for binary encoding.
		const returnTypeEnvelope = await ( Z7K1.Z8K2.resolve(
			this.invariants, /* ignoreList= */ null,
			/* resolveInternals= */ true, this.doValidate ) );
		if ( responseEnvelopeContainsError( returnTypeEnvelope ) ) {
			return returnTypeEnvelope;
		}
		Z7.Z7K1.Z8K2 = returnTypeEnvelope.Z22K1.asJSONEphemeral();
		for ( const argumentDict of argumentList ) {
			Z7[ argumentDict.name ] = argumentDict.argument.asJSON();
		}

		// Populate Z8K4/Implementation.
		Z7.Z7K1.Z8K4 = convertItemArrayToZList( [] );

		// Get the implementation, which may need to be dereferenced.
		let firstImplementation = this.Z14_;

		// Code implementation.
		const implementation = this;
		await ( firstImplementation.resolveEphemeral(
			[ 'Z14K3' ], implementation.invariants,
			/* ignoreList= */ null,
			/* resolveInternals= */ false, implementation.doValidate ) );
		const codeObjectEnvelope = await resolveCodeObject(
			firstImplementation.Z14K3, implementation );
		if ( responseEnvelopeContainsError( codeObjectEnvelope ) ) {
			return codeObjectEnvelope;
		}
		Z7.Z7K1.Z8K4.K1 = firstImplementation.asJSONEphemeral();
		firstImplementation = Z7.Z7K1.Z8K4.K1;
		const codeObject = firstImplementation.Z14K3;
		const codingLanguageObject = codeObject.Z16K1;
		const referenceObj = codingLanguageObject.Z9K1;
		const referencedExplicitObj =
			this.invariants.mappedProgrammingLangObj( codingLanguageObject );
		const programmingLanguage =
			isZReference( codingLanguageObject ) ?
				referenceObj : referencedExplicitObj;
		const evaluator = this.invariants.evaluatorFor( programmingLanguage );
		if ( evaluator === null ) {
			throw new EvaluatorError( `No evaluator is available for ${ programmingLanguage }` );
		}

		// Do type conversion of inputs, if necessary.
		for ( const argumentDict of argumentList ) {
			const argumentType = argumentDict.argument.Z1K1;
			if ( argumentType instanceof ZWrapper ) {
				const typeResult = await argumentDict.argument.Z1K1.resolve(
					implementation.invariants
				);
				if ( responseEnvelopeContainsError( typeResult ) ) {
					return typeResult;
				}
				const theType = typeResult.Z22K1;
				const toCodeConverterList = theType.getNameEphemeral( 'Z4K7' );
				if ( toCodeConverterList !== undefined ) {
					await resolveCodeConverterList( toCodeConverterList, 'Z46K3', implementation );
				}
				Z7[ argumentDict.name ].Z1K1 = theType.asJSONEphemeral();
				const listResolutionEnvelope = await maybeResolveListElementType(
					argumentDict.argument.Z1K1, Z7[ argumentDict.name ], 'Z1K1',
					'Z4K7', 'Z46K3', this );
				if (
					listResolutionEnvelope !== null &&
                        responseEnvelopeContainsError( listResolutionEnvelope ) ) {
					return listResolutionEnvelope;
				}
			}
		}
		const returnType = Z7K1.getNameEphemeral( 'Z8K2' );
		if ( returnType instanceof ZWrapper ) {
			const typeResult = await returnType.resolve(
				implementation.invariants
			);
			if ( responseEnvelopeContainsError( typeResult ) ) {
				return typeResult;
			}
			const theType = typeResult.Z22K1;
			const fromCodeConverterList = theType.getNameEphemeral( 'Z4K8' );
			if ( fromCodeConverterList !== undefined ) {
				await resolveCodeConverterList( fromCodeConverterList, 'Z64K3', implementation );
			}
			Z7.Z7K1.Z8K2 = theType.asJSONEphemeral();
			const listResolutionEnvelope = await maybeResolveListElementType(
				zobject.Z7K1.Z8K2, Z7.Z7K1, 'Z8K2', 'Z4K8', 'Z64K3', this );
			if (
				listResolutionEnvelope !== null &&
                    responseEnvelopeContainsError( listResolutionEnvelope ) ) {
				return listResolutionEnvelope;
			}
		}

		let fetchedResult;
		try {
			fetchedResult = await evaluator.evaluate( Z7 );
		} catch ( e ) {
			throw new EvaluatorError( e.message );
		}
		if ( fetchedResult.ok ) {
			// Assume the evaluator is returning Z22s.
			const resultEnvelope = await fetchedResult.json();
			return resultEnvelope;
		}
		const statusCode = fetchedResult.status;
		const errorText = await fetchedResult.text();
		throw new EvaluatorError( `Function evaluation failed with status ${ statusCode }: ${ errorText }` );
	}

}

class Composition extends Implementation {

	constructor( Z14, ZID ) {
		super( Z14, ZID );
		this.composition_ = Z14.Z14K2.asJSON();
	}

	/**
	 * Creates a ZObject, i.e. a ZWrapper instance, and resolves it.
	 *
	 * @return {Promise<ZWrapper>} A result envelope zobject representing the result.
	 */
	async executeInternal() {
		return await ZWrapper.create(
			this.composition_, this.getZ14().scope_, this.getZ14().parent_
		).resolve(
			this.invariants_, /* ignoreList= */ null, /* resolveInternals= */ true,
			/* doValidate= */ this.doValidate_ );
	}

}

module.exports = {
	BuiltIn,
	Composition,
	Evaluated,
	EvaluatorError,
	Implementation,
	ZResponseError
};
