'use strict';

/*!
 * Wikifunctions orchestrator class to model a ZObject's state of evaluation and concreteness.
 *
 * @copyright 2020– Abstract Wikipedia team; see LICENSE
 * @license MIT
 */

const {
	createSchema,
	isGenericType,
	makeWrappedResultEnvelope,
	responseEnvelopeContainsError
} = require( './utils.js' );
const { BaseFrame } = require( './frame.js' );
const { error, makeErrorInNormalForm } = require( '../function-schemata/javascript/src/error.js' );
const {
	convertArrayToKnownTypedList,
	convertZListToItemArray,
	isString,
	isUserDefined,
	isVoid,
	isZArgumentReference,
	isZEnvelope,
	isZFunctionCall,
	isZReference,
	isZType,
	makeVoid,
	wrapInZ9,
	wrapInKeyReference,
	ZWrapperBase
} = require( '../function-schemata/javascript/src/utils.js' );
const canonicalize = require( '../function-schemata/javascript/src/canonicalize.js' );
const util = require( 'util' );
const { Invariants } = require( './Invariants.js' );
const ErrorFormatter = require( '../function-schemata/javascript/src/errorFormatter.js' );

const MutationType = Object.freeze( {
	REFERENCE: Symbol( 'REFERENCE' ),
	ARGUMENT_REFERENCE: Symbol( 'ARGUMENT_REFERENCE' ),
	FUNCTION_CALL: Symbol( 'FUNCTION_CALL' ),
	GENERIC_INSTANCE: Symbol( 'GENERIC_INSTANCE' )
} );

/**
 * Wrapper around ZObjects that should be used instead of bare ZObjects during evaluation.
 *
 * The wrapper keeps track of the scope under which the object should be evaluated, and caches
 * the results of resolving subobjects for future use.
 */
class ZWrapper extends ZWrapperBase {

	/**
	 * Private. Use {@link ZWrapper#create} instead.
	 */
	constructor() {
		super();
		// Each value in original_ (and eventually in resolved_) points to another ZWrapper
		// representing the corresponding subobject with its own scope. Initially they all point
		// to the same scope, but they diverge as subobjects get resolved.
		this.original_ = new Map();
		this.resolved_ = new Map();
		this.resolvedEphemeral_ = new Map();
		this.keys_ = new Set();
		this.scope_ = null;
		this.parent_ = null;
		this.isValidated_ = false;
		this.isFullyResolved_ = false;
	}

	/**
	 * Creates an equivalent ZWrapper representation for the given ZObject and its subobjects.
	 * The resulting ZWrapper has the same fields as the ZObject, each of which is itself a
	 * ZWrapper, and so on.
	 *
	 * @param {string|Object|ZWrapper} zobjectJSON The JSON representation of the ZObject)
	 * @param {BaseFrame} scope The ZObject with whose scope this wrapped object exists
	 * @param {ZWrapper|null} parentPointer pointer pointing to current ZWrapper to parent scope
	 * @param {boolean} isValidated whether the resulting object is pre-validated
	 * @param {boolean} isFullyResolved whether the resulting object is fully validated
	 * @return {ZWrapper|string} new creation of ZWrapper
	 */
	static create(
		zobjectJSON, scope = null, parentPointer = null, isValidated = false,
		isFullyResolved = false
	) {
		if ( !scope && !parentPointer ) {
			throw new Error( 'Missing scope argument' );
		}

		return ZWrapper.createInternal_(
			zobjectJSON, scope, parentPointer, isValidated, isFullyResolved );
	}

	static createInternal_( zobjectJSON, scope, parentPointer, isValidated, isFullyResolved ) {
		if ( isString( zobjectJSON ) || zobjectJSON instanceof ZWrapper ) {
			return zobjectJSON;
		}
		let result;
		if ( isZEnvelope( zobjectJSON ) ) {
			result = new ZEnvelopeWrapper(); // eslint-disable-line no-use-before-define
		} else {
			result = new ZWrapper();
		}
		result.setScope( scope );
		result.setIsValidated( isValidated );
		result.setIsFullyResolved( isFullyResolved );
		result.parent_ = parentPointer; // will use parent in case scope does not exist
		result.populateKeys_( zobjectJSON, isValidated, isFullyResolved );
		return result;
	}

	/**
	 * Get the scope ZWrapper set for this ZObject, if any.
	 *
	 * @return {ZWrapper|null} scope that is an instance of ZWrapper
	 */
	getScope() {
		return this.scope_ || this.parent_.getScope();
	}

	/**
	 * Set the scope ZWrapper for this ZObject.
	 *
	 * @param {ZWrapper} scope
	 */
	setScope( scope ) {
		this.scope_ = scope;
	}

	isValidated() {
		return this.isValidated_;
	}

	setIsValidated( isValidated ) {
		this.isValidated_ = isValidated;
	}

	/**
	 * If a ZObject loses "pre-validated" status, then all parent objects
	 * must also lose that status.
	 */
	invalidate() {
		if ( !this.isValidated_ ) {
			return;
		}
		this.isValidated_ = false;
		if ( this.parent_ !== null ) {
			this.parent_.invalidate();
		}
	}

	isFullyResolved() {
		return this.isFullyResolved_;
	}

	setIsFullyResolved( isFullyResolved ) {
		this.isFullyResolved_ = isFullyResolved;
	}

	/**
	 * If a ZObject loses "fully resolved" status, then all parent objects
	 * must also lose that status.
	 */
	unresolve() {
		if ( !this.isFullyResolved_ ) {
			return;
		}
		this.isFullyResolved_ = false;
		if ( this.parent_ !== null ) {
			this.parent_.unresolve();
		}
	}

	/**
	 * Create the keys for a ZObject and throw an error if faulty input
	 *
	 * @param {JSON} zobjectJSON JSON-ified ZObject
	 * @param {boolean} isValidated whether the resulting object is pre-validated
	 * @param {boolean} isFullyResolved whether the resulting object is fully validated
	 */
	populateKeys_( zobjectJSON, isValidated, isFullyResolved ) {
		let keys;
		try {
			keys = Object.keys( zobjectJSON );
		} catch ( error ) {
			keys = [];
		}

		if ( keys.length === 0 ) {
			// This implies the original object was not an Object, get me? An integer,
			// a Boolean, a list. Something just awful.
			throw new Error( 'Non-Object input: ' + JSON.stringify( zobjectJSON ) );
		}
		for ( const key of keys ) {
			let value = zobjectJSON[ key ];
			// Do not create ZWrappers for Z99/Quotes.
			// TODO (T341522): Validate zobjectJSON against the Z99 validator.
			if ( key !== 'Z99K1' ) {
				value = ZWrapper.createInternal_( value, null, this, isValidated, isFullyResolved );
			}
			this.original_.set( key, value );
		}
		ZWrapper.defineProperties( this, keys );
	}

	/**
	 * Retrieves and sets the resolved objects for a given ZWrapper instance
	 *
	 * @param {Object} theWrapper ZObject
	 * @param {Array} keys array of keys of a ZObject
	 */
	static defineProperties( theWrapper, keys ) {
		for ( const key of keys ) {
			theWrapper.keys_.add( key );
			Object.defineProperty( theWrapper, key, {
				get: function () {
					return theWrapper.getNameEphemeral( key );
				},
				set: function ( newValue ) {
					theWrapper.setName( key, newValue );
				}
			} );
		}
	}

	/**
	 * Get a copy of this object.
	 *
	 * @param {ZWrapper|null} parentPointer pointer pointing to current ZWrapper to parent scope
	 * @return {ZWrapper}
	 */
	copy( parentPointer = null ) {
		const result = new ZWrapper();
		result.scope_ = this.scope_;
		result.parent_ = parentPointer || this.parent_;
		result.isValidated_ = this.isValidated_;
		result.isFullyResolved_ = this.isFullyResolved_;
		for ( const mapName of [ 'original_', 'resolved_', 'resolvedEphemeral_' ] ) {
			const thisMap = this[ mapName ];
			const copyMap = result[ mapName ];
			for ( const entry of thisMap.entries() ) {
				const key = entry[ 0 ];
				let value = entry[ 1 ];
				if ( value instanceof ZWrapper ) {
					value = value.copy( /* parentPointer= */ result );
				}
				copyMap.set( key, value );
			}
		}
		ZWrapper.defineProperties( result, this.keys() );
		return result;
	}

	/**
	 * Get the resolved object for the given key, including the ephemeral resolution, if
	 * available.
	 *
	 * @param {string} key The key for the object to fetch
	 * @return {ZWrapper}
	 */
	getNameEphemeral( key ) {
		if ( this.resolvedEphemeral_.has( key ) ) {
			return this.resolvedEphemeral_.get( key );
		}
		if ( this.resolved_.has( key ) ) {
			return this.resolved_.get( key );
		}
		return this.original_.get( key );
	}

	runInvalidateAndUnresolveHeuristics( key, value ) {
		if ( !( value instanceof ZWrapper ) ) {
			return;
		}
		// TODO (T385476): More heuristics to skip invalidation/unresolution.
		if ( key === 'Z1K1' ) {
			return;
		}
		this.invalidate();
		if ( value.isFullyResolved() ) {
			return;
		}
		this.unresolve();
	}

	/**
	 * Set the resolved object for the given key.
	 *
	 * WARNING: Do not call this function with keys that were not part of the original object.
	 *
	 * @param {string} key The key to set
	 * @param {ZWrapper} value The value to set
	 */
	setName( key, value ) {
		this.resolved_.set( key, value );
		this.runInvalidateAndUnresolveHeuristics( key, value );
	}

	/**
	 * Set the ephemerally-resolved object for the given key.
	 *
	 * WARNING: Do not call this function with keys that were not part of the original object.
	 *
	 * @param {string} key The key to set
	 * @param {ZWrapper} value The value to set
	 */
	setNameEphemeral( key, value ) {
		this.resolvedEphemeral_.set( key, value );
		this.runInvalidateAndUnresolveHeuristics( key, value );
	}

	/**
	 * Get the resolved object for the given key, if available.
	 *
	 * @param {string} key The key for the object to fetch
	 * @return {ZWrapper}
	 */
	getName( key ) {
		if ( this.resolved_.has( key ) ) {
			return this.resolved_.get( key );
		}
		return this.original_.get( key );
	}

	/**
	 * Private.
	 *
	 * @param {Invariants} invariants
	 * @param {Set(MutationType)} ignoreList
	 * @param {boolean} resolveInternals
	 * @param {boolean} doValidate
	 * @param {boolean} evenBuiltins
	 * @return {Promise<ZWrapper>}
	 */
	async resolveInternal_( invariants, ignoreList, resolveInternals, doValidate, evenBuiltins ) {
		if ( ignoreList === null ) {
			ignoreList = new Set();
		}
		let nextObject = this;
		while ( true ) {
			if ( !ignoreList.has( MutationType.ARGUMENT_REFERENCE ) ) {
				if ( isZArgumentReference( nextObject ) ) {
					const refKey = nextObject.Z18K1.Z6K1;
					const dereferenced = await this.getScope().retrieveArgument(
						refKey, invariants, /* lazily= */ false, doValidate,
						resolveInternals, ignoreList );
					if ( dereferenced.state === 'ERROR' ) {
						return makeWrappedResultEnvelope( null, dereferenced.error );
					}
					nextObject = dereferenced.argumentDict.argument;
					continue;
				}
			}
			if ( !ignoreList.has( MutationType.REFERENCE ) ) {
				// TODO (T296686): isUserDefined call here is only an
				// optimization/testing expedient; it would be better to pre-populate
				// the cache with builtin types.
				if ( isZReference( nextObject ) ) {
					if ( isUserDefined( nextObject.Z9K1 ) || evenBuiltins ) {
						const refKey = nextObject.Z9K1;
						const dereferenced =
							await invariants.resolver.dereferenceZObjects( [ refKey ] );
						const Z22 = dereferenced.get( refKey );
						if ( responseEnvelopeContainsError( Z22 ) ) {
							return Z22;
						}
						nextObject = ZWrapper.create(
							Z22.Z22K1.Z2K2, this.scope_, this.parent_, this.isValidated_,
							this.isFullyResolved_ );
						continue;
					}
				}
			}
			if ( !ignoreList.has( MutationType.FUNCTION_CALL ) ) {
				if ( isZFunctionCall( nextObject ) ) {
					const Z22 = await invariants.executeWithCallCount(
						nextObject, invariants, doValidate,
						/* implementationSelector= */ null, resolveInternals );
					if ( responseEnvelopeContainsError( Z22 ) ) {
						return Z22;
					}
					nextObject = Z22.Z22K1;
					continue;
				}
			}
			if ( isGenericType( nextObject ) ) {
				const executionResult = await nextObject.resolveEphemeral(
					[ 'Z1K1' ], invariants, ignoreList, resolveInternals, doValidate );
				if ( responseEnvelopeContainsError( executionResult ) ) {
					return executionResult;
				}
				const Z4 = nextObject.getNameEphemeral( 'Z1K1' );
				if ( !isZType( Z4 ) ) {
					const subError = ErrorFormatter.createZErrorInstance(
						error.unknown_error,
						{ errorInformation: 'Generic type function did not return a Z4' } );
					// TODO (T296681): Return typeStatus.getZ5() as part of this result.
					return makeWrappedResultEnvelope(
						null,
						makeErrorInNormalForm(
							error.return_type_mismatch,
							[ wrapInZ9( 'Z4' ), Z4.asJSON(), nextObject.asJSON(), subError ] ) );
				}
				continue;
			}
			break;
		}
		return makeWrappedResultEnvelope( nextObject, null, this.scope_, this.parent_ );
	}

	/**
	 * Private.
	 *
	 * @param {Object} key
	 * @param {Invariants} invariants
	 * @param {Set(MutationType)} ignoreList
	 * @param {boolean} resolveInternals
	 * @param {boolean} doValidate
	 * @param {Function} getNameFunction
	 * @param {Function} setNameFunction
	 * @return {Promise<ZWrapper|string>}
	 */
	async resolveInternalHelper_(
		key, invariants, ignoreList, resolveInternals, doValidate, getNameFunction, setNameFunction
	) {
		let newValue, resultEnvelope;
		if ( !this.keys_.has( key ) ) {
			return makeWrappedResultEnvelope(
				null,
				makeErrorInNormalForm(
					error.invalid_key,
					[ wrapInKeyReference( key ) ] ) );
		}
		const currentValue = getNameFunction( key );
		if ( currentValue instanceof ZWrapper ) {
			resultEnvelope = await ( currentValue.resolveInternal_(
				invariants, ignoreList, resolveInternals, doValidate, /* evenBuiltins */ false
			) );
			if ( responseEnvelopeContainsError( resultEnvelope ) ) {
				return resultEnvelope;
			}
			newValue = resultEnvelope.Z22K1;
		} else {
			resolveInternals = false;
			resultEnvelope = makeWrappedResultEnvelope( this, null, this.scope_, this.parent_ );
			newValue = currentValue;
		}
		if ( resolveInternals ) {
			// Validate that the newly-mutated object validates in accordance with the
			// original object's key declaration.
			// TODO (T381597): asJSON() will be unnecessary once JSON schema is gone.
			const theSchema = createSchema( this.asJSON() );
			// We validate elsewhere that Z1K1 must be a type, so the schemata do not
			// surface separate validators for Z1K1.
			if ( key !== 'Z1K1' ) {
				const subValidator = theSchema.subValidator( key );
				if ( subValidator === undefined ) {
					// Should never happen?
					return makeWrappedResultEnvelope(
						null,
						makeErrorInNormalForm(
							error.invalid_key,
							[ wrapInKeyReference( key ) ] ) );
				}

				let toValidate;
				if ( newValue instanceof ZWrapper ) {
					// TODO (T381597): asJSON() will be unnecessary once JSON schema is gone.
					toValidate = newValue.asJSON();
				} else {
					toValidate = newValue;
				}
				const theStatus = subValidator.validateStatus( toValidate );
				if ( !theStatus.isValid() ) {
					// TODO (T302015): Find a way to incorporate information about where this
					// error came from.
					return makeWrappedResultEnvelope( null, theStatus.getZ5() );
				}
			}
		}
		setNameFunction( key, newValue );
		return resultEnvelope;
	}

	/**
	 * Private.
	 *
	 * @param {Object} key
	 * @param {Invariants} invariants
	 * @param {Set(MutationType)} ignoreList
	 * @param {boolean} resolveInternals
	 * @param {boolean} doValidate
	 * @return {Promise<ZWrapper>}
	 */
	async resolveKeyInternal_(
		key, invariants, ignoreList, resolveInternals, doValidate
	) {
		const selfReference = this;
		function getNameFunction( key ) {
			return selfReference.getName( key );
		}
		function setNameFunction( key, value ) {
			return selfReference.setName( key, value );
		}
		return await this.resolveInternalHelper_(
			key, invariants, ignoreList, resolveInternals, doValidate,
			getNameFunction, setNameFunction
		);
	}

	/**
	 * Private.
	 *
	 * TODO: Collapse common functionality with resolveKeyInternal_.
	 *
	 * @param {Object} key
	 * @param {Invariants} invariants
	 * @param {Set(MutationType)} ignoreList
	 * @param {boolean} resolveInternals
	 * @param {boolean} doValidate
	 * @return {Promise<ZWrapper>}
	 */
	async resolveEphemeralInternal_(
		key, invariants, ignoreList, resolveInternals, doValidate ) {
		const selfReference = this;
		function getNameFunction( key ) {
			return selfReference.getNameEphemeral( key );
		}
		function setNameFunction( key, value ) {
			return selfReference.setNameEphemeral( key, value );
		}
		return await this.resolveInternalHelper_(
			key, invariants, ignoreList, resolveInternals, doValidate,
			getNameFunction, setNameFunction );
	}

	/**
	 * Repeatedly evaluates the top-level object according to its evaluation rules.
	 *
	 * The returned object does not have any evaluation rule that applies to it (i.e. it is not a
	 * reference, argument reference, function call, etc.) but the same is not true for its
	 * subobjects; they should be resolved separately. Moreover, it is wrapped in a result envelope
	 * to indicate any errors.
	 *
	 * @param {Invariants} invariants
	 * @param {Set(MutationType)} ignoreList
	 * @param {boolean} resolveInternals
	 * @param {boolean} doValidate
	 * @param {boolean} evenBuiltins Whether to resolve references to built-in types.
	 * @return {Promise<ZWrapper>} A result envelope zobject representing the result.
	 */
	async resolve(
		invariants, ignoreList = null, resolveInternals = true, doValidate = true,
		evenBuiltins = false
	) {
		// TODO: Remove this intermediate call?
		return this.resolveInternal_(
			invariants, ignoreList, resolveInternals, doValidate, evenBuiltins );
	}

	/**
	 * Recursively traverses and resolves the current object along the given keys, caching the
	 * results for future calls.
	 *
	 * The returned object does not have any evaluation rule that applies to it (i.e. it is not a
	 * reference, argument reference, function call, etc.) but the same is not true for its
	 * subobjects; they should be resolved separately. Moreover, it is wrapped in a result envelope
	 * to indicate any errors.
	 *
	 * @param {Array(string)} keys Path of subobjects to resolve
	 * @param {Invariants} invariants
	 * @param {Set(MutationType)} ignoreList
	 * @param {boolean} resolveInternals
	 * @param {boolean} doValidate
	 * @return {Promise<ZWrapper>} A result envelope zobject representing the result.
	 */
	async resolveKey(
		keys, invariants, ignoreList = null,
		resolveInternals = true, doValidate = true ) {
		let result;
		if ( keys.length <= 0 ) {
			return makeWrappedResultEnvelope( this, null );
		}
		const key = keys.shift();
		if ( !this.resolved_.has( key ) ) {
			result = await this.resolveKeyInternal_(
				key, invariants, ignoreList, resolveInternals, doValidate );
			if ( responseEnvelopeContainsError( result ) ) {
				return result;
			}
		}
		const nextValue = this.getNameEphemeral( key );
		if ( nextValue instanceof ZWrapper ) {
			result = await (
				nextValue.resolveKey(
					keys, invariants, ignoreList, resolveInternals, doValidate )
			);
		}
		return result;
	}

	/**
	 * Recursively traverses and resolves the current object along the given keys, caching the
	 * results for future calls.
	 *
	 * This differs from resolveKey() in that the resolved values will not be
	 * represented when asJSON() is called. resolveEphemeral is useful for cases
	 * where it's helpful to cache results, but those results will not be needed
	 * outside the orchestrator.
	 *
	 * The returned object does not have any evaluation rule that applies to it (i.e. it is not a
	 * reference, argument reference, function call, etc.) but the same is not true for its
	 * subobjects; they should be resolved separately. Moreover, it is wrapped in a result envelope
	 * to indicate any errors.
	 *
	 * @param {Array(string)} keys Path of subobjects to resolve
	 * @param {Invariants} invariants
	 * @param {Set(MutationType)} ignoreList
	 * @param {boolean} resolveInternals
	 * @param {boolean} doValidate
	 * @return {Promise<ZWrapper>} A result envelope zobject representing the result.
	 */
	async resolveEphemeral(
		keys, invariants, ignoreList = null,
		resolveInternals = true, doValidate = true ) {
		let result;
		if ( keys.length <= 0 ) {
			return makeWrappedResultEnvelope( this, null );
		}
		const key = keys.shift();
		if ( !( this.keys_.has( key ) ) ) {
			// TODO (T309809): Return an error in this case.
			return makeWrappedResultEnvelope( this, null );
		}
		if ( !this.resolvedEphemeral_.has( key ) ) {
			result = await this.resolveEphemeralInternal_(
				key, invariants, ignoreList, resolveInternals, doValidate );
			if ( responseEnvelopeContainsError( result ) ) {
				return result;
			}
		}
		const nextValue = this.getNameEphemeral( key );
		if ( nextValue instanceof ZWrapper ) {
			result = await (
				nextValue.resolveEphemeral(
					keys, invariants, ignoreList, resolveInternals, doValidate )
			);
		}
		return result;
	}

	/**
	 * Returns the JSON representation of the zobject.
	 *
	 * WARNING: The resulting object loses all information in the attached scopes and may therefore
	 * have unbound argument references. In particular, if subobjects have already been resolved
	 * with `resolveKey()`, their scope might have diverged. This means that `zwrapper.asJSON()` may
	 * have argument references that are not captured even in `zwrapper.getScope()`. Using this
	 * method should hence only be used judiciously where such effects can be taken into account.
	 * You have been warned.
	 *
	 * @return {Object}
	 */
	asJSON() {
		const result = {};
		for ( const key of this.keys() ) {
			let value = this.getName( key );
			if ( value instanceof ZWrapper ) {
				value = value.asJSON();
			}
			result[ key ] = value;
		}
		return result;
	}

	/**
	 * Returns the JSON representation of the zobject, based only on the keys' ephemeral values.
	 *
	 * @return {Object}
	 */
	asJSONEphemeral() {
		const result = {};
		for ( const key of this.keys() ) {
			let value = this.getNameEphemeral( key );
			if ( value instanceof ZWrapper ) {
				value = value.asJSONEphemeral();
			}
			result[ key ] = value;
		}
		return result;
	}

	/**
	 * Returns a view of the ZWrapper object of a ZResultEnvelope suitable for debugging:
	 * - ZObjects are canonicalized
	 * - Scopes are flattened
	 *
	 * See also {@link ZWrapper#debug}.
	 *
	 * @return {Object}
	 */
	debugObject() {
		const object_ = canonicalize( this.asJSON() ).Z22K1;
		const scope_ = this.scope_.debugObject();
		return { object_, scope_ };
	}

	/**
	 * Helper function for logging the debug representation of the ZWrapper.
	 *
	 * With it, one can write:
	 *
	 *   `console.log( 'executing:', await zwrapper.debug() );`
	 *
	 * … to log the debug representation of the ZWrapper object without truncating it due to depth.
	 *
	 * See also {@link ZWrapper#debug}.
	 *
	 * @return {Object}
	 */
	debug() {
		const debugObject = this.debugObject();
		return { [ util.inspect.custom ]:
			( _, options, _inspect ) => util.inspect(
				debugObject,
				Object.assign( {}, options, { depth: null } )
			)
		};
	}

	/* eslint-disable jsdoc/no-undefined-types */
	/**
	 * Get all the keys set for this ZObject.
	 *
	 * @return {IterableIterator<string>}
	 */
	keys() {
		return this.keys_.values();
	}
	/* eslint-enable jsdoc/no-undefined-types */
}

function makeMapType( keyType, valType ) {
	return {
		Z1K1: makeRef( 'Z7' ),
		Z7K1: makeRef( 'Z883' ),
		Z883K1: keyType,
		Z883K2: valType
	};
}

function makePairType( keyType, valType ) {
	return {
		Z1K1: makeRef( 'Z7' ),
		Z7K1: makeRef( 'Z882' ),
		Z882K1: keyType,
		Z882K2: valType
	};
}

function makePair( key, value ) {
	const keyType = makeRef( 'Z6' );
	const valType = makeRef( 'Z1' );
	const pairType = makePairType( keyType, valType );
	return {
		Z1K1: pairType,
		K1: key,
		K2: value
	};
}

function makeString( key ) {
	return { Z1K1: 'Z6', Z6K1: key };
}

function makeRef( key ) {
	return { Z1K1: 'Z9', Z9K1: key };
}

function metadataFromMap( originalMap, addNestedMetadata ) {
	const mapEntries = [];
	let wasEmpty = true;
	const keyType = makeRef( 'Z6' );
	const valType = makeRef( 'Z1' );
	const mapType = makeMapType( keyType, valType );
	for ( let [ key, value ] of originalMap.entries() ) {
		if ( key === 'nestedMetadataArray' && !addNestedMetadata ) {
			continue;
		}
		wasEmpty = false;
		if ( value instanceof ZWrapperBase ) {
			value = value.asJSON();
		}
		if ( key === 'nestedMetadataArray' ) {
			const nestedMetadata = [];
			for ( const nestedMetadatum of value ) {
				nestedMetadata.push(
					metadataFromMap( nestedMetadatum, addNestedMetadata )
				);
			}
			value = convertArrayToKnownTypedList( nestedMetadata, mapType );
			key = 'nestedMetadata';
		}
		// Here, we assume that all metadata keys are strings, so we just
		// turn them into Z6s. This assumption may be unjustified in the future.
		mapEntries.push( makePair( makeString( key ), value ) );
	}
	if ( wasEmpty ) {
		return {
			Z1K1: 'Z9',
			Z9K1: 'Z24'
		};
	}
	const pairType = makePairType( keyType, valType );
	const mapList = convertArrayToKnownTypedList( mapEntries, pairType );
	return { Z1K1: mapType, K1: mapList };
}

class ZEnvelopeWrapper extends ZWrapper {

	constructor() {
		super();
		// TODO (T337589): this.metadata_ is necessary because sometimes the
		// ZWrapper is inialized without a scope. Once that bug has been fixed,
		// we can assume that the scope can contain all of the metadata.
		const instance = this;
		Object.defineProperty( this, 'metadata', {
			get: function () {
				return instance.getScope().metadata;
			}
		} );
	}

	populateKeys_( zobjectJSON, isValidated, isFullyResolved ) {
		const intermediateObject = {
			Z1K1: zobjectJSON.Z1K1,
			Z22K1: zobjectJSON.Z22K1
		};
		super.populateKeys_( intermediateObject, isValidated, isFullyResolved );
		const envelopeMetadata = zobjectJSON.Z22K2;
		if ( envelopeMetadata === undefined || isVoid( envelopeMetadata ) ) {
			return;
		}
		const scopeMetadata = this.getScope().metadata;
		const entries = convertZListToItemArray( envelopeMetadata.K1 || [] );
		for ( const entry of entries ) {
			const key = entry.K1.Z6K1;
			const value = entry.K2;
			scopeMetadata.set( key, value );
		}
	}

	asJSON( addNestedMetadata = false ) {
		const result = super.asJSON();
		result.Z22K2 = metadataFromMap( this.getScope().metadata, addNestedMetadata );
		return result;
	}

	asJSONEphemeral( addNestedMetadata = false ) {
		const result = super.asJSONEphemeral();
		result.Z22K2 = metadataFromMap( this.getScope().metadata, addNestedMetadata );
		return result;
	}

	setMetadata( key, value ) {
		const metadata = this.metadata;
		// Here, we assume that all metadata keys are strings, so we just
		// turn them into Z6s. This assumption may be unjustified in the future.
		metadata.set( key, value );
	}

	getErrorInternal_() {
		const thisScope = this.getScope();
		if ( thisScope !== null ) {
			const result = thisScope.metadata.get( 'errors' );
			if ( result !== undefined ) {
				return result;
			}
		}
		return undefined;
	}

	containsError() {
		return this.getErrorInternal_() !== undefined;
	}

	getError() {
		const theError = this.getErrorInternal_();
		if ( theError === undefined ) {
			return makeVoid();
		}
		return theError;
	}

}

module.exports = { MutationType, ZEnvelopeWrapper, ZWrapper };
