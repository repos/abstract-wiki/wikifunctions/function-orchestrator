'use strict';

/*!
 * Wikifunctions orchestrator class to implement rate liming on a per-request basis.
 *
 * @copyright 2020– Abstract Wikipedia team; see LICENSE
 * @license MIT
 */

const NodeCache = require( 'node-cache' );
const { makeWrappedResultEnvelope } = require( './utils.js' );
const { error, makeErrorInNormalForm } = require( '../function-schemata/javascript/src/error.js' );
const { wrapInZ6 } = require( '../function-schemata/javascript/src/utils.js' );

function rateLimitErrorFor( limit ) {
	return makeWrappedResultEnvelope(
		null,
		makeErrorInNormalForm(
			error.orchestrator_rate_limit,
			[ wrapInZ6( limit.toString() ) ] ) );
}

const DEFAULT_TTL_ = 15 * 60;

function getTTLForEnvironment( environment ) {
	const ttlString = environment.FUNCTION_ORCHESTRATOR_RATE_LIMIT_TTL_SECONDS;
	let ttl = Number.parseInt( ttlString );
	if ( Number.isNaN( ttl ) ) {
		ttl = DEFAULT_TTL_;
	}
	return ttl;
}

/**
 * Implements a simple cache to count how many operations are currently attached to
 * a single request ID.
 */
class RateLimitCache {

	constructor( limit = 300, environment = null ) {
		if ( !environment ) {
			environment = process.env;
		}
		const ttl = getTTLForEnvironment( environment );
		this.limit_ = limit;
		// TODO (T340561): Parameterize the TTL; ensure it's much longer than the request timeout.
		this.idCounter_ = new NodeCache( { stdTTL: ttl } );
	}

	/**
	 * Remove a request ID from the cache upon orchestration completion.
	 *
	 * @param {string} requestId ID for a request
	 */
	evict( requestId ) {
		if ( !requestId ) {
			return;
		}
		this.idCounter_.del( requestId );
	}

	/**
	 * Increase by one the count for a given request ID.
	 *
	 * @param {string} requestId ID for a request
	 * @return {Object|null} Z22 containing error if the request should be
	 *  rate-limited; null otherwise
	 */
	increment( requestId ) {
		if ( !requestId ) {
			return null;
		}
		if ( !this.idCounter_.has( requestId ) ) {
			this.idCounter_.set( requestId, 0 );
		}
		const newValue = this.idCounter_.get( requestId ) + 1;
		this.idCounter_.set( requestId, newValue );
		if ( newValue > this.limit_ ) {
			return rateLimitErrorFor( this.limit_ );
		}
		return null;
	}

	/**
	 * Decrease by one the count for a given request ID.
	 *
	 * @param {string} requestId ID for a request
	 * @return {Object|null} Z22 containing error if the request should be
	 *  rate-limited; null otherwise
	 */
	decrement( requestId ) {
		if ( !requestId ) {
			return null;
		}
		if ( !this.idCounter_.has( requestId ) ) {
			this.idCounter_.set( requestId, 0 );
		}
		const newValue = this.idCounter_.get( requestId ) - 1;
		this.idCounter_.set( requestId, newValue );
		if ( newValue > this.limit_ ) {
			return rateLimitErrorFor( this.limit_ );
		}
		return null;
	}

}

const rateLimits = new RateLimitCache();

module.exports = { rateLimits, RateLimitCache };
