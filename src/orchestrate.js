'use strict';

/*!
 * Wikifunctions orchestrator primary entry point.
 *
 * @copyright 2020– Abstract Wikipedia team; see LICENSE
 * @license MIT
 */

const normalize = require( '../function-schemata/javascript/src/normalize.js' );
const { isZFunctionCall, makeMappedResultEnvelope, setMetadataValues, wrapInZ9, wrapInQuote } = require( '../function-schemata/javascript/src/utils.js' );
const { error, makeErrorInNormalForm } = require( '../function-schemata/javascript/src/error.js' );
const ErrorFormatter = require( '../function-schemata/javascript/src/errorFormatter.js' );
const { isError, makeWrappedResultEnvelope, safeJsonStringify, returnOnFirstError } = require( './utils.js' );
const { Invariants } = require( './Invariants.js' );
const { ZWrapper } = require( './ZWrapper.js' );
const ImplementationSelector = require( './implementationSelector.js' );

const sUtil = require( '../lib/util' );
const { cpuUsage, memoryUsage } = require( 'node:process' );
const os = require( 'os' );

/**
 * Returns the pair <original ZObject, Unit> if the input object is a Z7;
 * otherwise returns the pair <Unit, Z5>.
 *
 * @param {Object} zobject
 * @return {Object} a Z22 as described above
 */
function Z7OrError( zobject ) {
	if ( isZFunctionCall( zobject ) ) {
		return makeMappedResultEnvelope( zobject, null );
	}
	return makeMappedResultEnvelope(
		null,
		makeErrorInNormalForm( error.object_type_mismatch, [ wrapInZ9( 'Z7' ), wrapInQuote( zobject ) ] )
	);
}

/**
 * Main orchestration workflow. Executes an input Z7 and returns either the
 * results of function evaluation or the relevant error(s).
 *
 * Takes and returns JSON representation; not ZWrapper.
 *
 * @param {Object} zobject the function call
 * @param {Invariants} invariants encapsulates global orchestrator config and wrappers
 *      for evaluator and Wiki services
 * @param {ImplementationSelector} implementationSelector
 * @return {Object} a Z22 containing the result of function evaluation or a Z5 (in Z22K2/metadata)
 */
async function orchestrate( zobject, invariants, implementationSelector = null ) {
	const startTime = new Date();
	const startUsage = cpuUsage();

	let currentResponseEnvelope;
	if ( isError( zobject ) ) {
		currentResponseEnvelope = makeMappedResultEnvelope(
			null, zobject, /* canonicalize= */true
		);
	} else {
		currentResponseEnvelope = makeMappedResultEnvelope(
			zobject, null, /* canonicalize= */true
		);
	}

	const stringifiedResult = safeJsonStringify( zobject );
	invariants.logger.log(
		'info',
		{ message: 'Incoming orchestrator request', requestId: invariants.requestId, info: stringifiedResult }
	);

	const callTuples = [
		[ normalize, [ /* generically= */true, /* withVoid= */ true ], 'normalize' ],
		// TODO (T296685): Dereference top-level object if it is a Z9?
		[ Z7OrError, [], 'Z7OrError' ],
		[ makeWrappedResultEnvelope, [], 'wrapAsZObject' ],
		[
			invariants.executeWithCallCount, [
				invariants, /* doValidate= */invariants.orchestratorConfig.doValidate,
				/* implementationSelector= */implementationSelector,
				/* resolveInternals= */true, /* topLevel= */true ],
			'execute'
		]
	];

	if ( invariants.orchestratorConfig.generateFunctionsMetrics ) {
		sUtil.logResourceUsage( invariants.logger, invariants.requestId, 'DURING orchestrate()' );
	}
	try {
		currentResponseEnvelope = await returnOnFirstError( currentResponseEnvelope, callTuples );
		if ( invariants.orchestratorConfig.generateFunctionsMetrics ) {
			sUtil.logResourceUsage( invariants.logger, invariants.requestId, 'AFTER orchestrate()' );
		}
	} catch ( e ) {
		const errorMessage = `Call tuples failed in returnOnFirstError. Error: ${ e }.`;
		invariants.logger.log( 'error', { message: errorMessage, requestId: invariants.requestId } );
		// The zobject provides context for a Z507/Evaluation error (and will be quoted there)
		const zerror = ErrorFormatter.wrapMessageInEvaluationError( errorMessage, zobject );
		// This currentResponseEnvelope will be JSON, not a ZWrapper.
		// makeMappedResultEnvelope will put zerror into a metadata map.
		currentResponseEnvelope = makeMappedResultEnvelope( null, zerror );
	}

	const cpuUsageStats = cpuUsage( startUsage );
	const cpuUsageStr = ( ( cpuUsageStats.user + cpuUsageStats.system ) / 1000 ) + ' ms';
	const memoryUsageStr = Math.round( memoryUsage.rss() / 1024 / 1024 * 100 ) / 100 + ' MiB';
	const endTime = new Date();
	const startTimeStr = startTime.toISOString();
	const endTimeStr = endTime.toISOString();
	const durationStr = ( endTime.getTime() - startTime.getTime() ) + ' ms';
	const hostname = os.hostname();

	// Note: Keep this block in sync with the 'standardMetaData' list in mswOrchestrateTest
	setMetadataValues( currentResponseEnvelope, [
		[ { Z1K1: 'Z6', Z6K1: 'orchestrationMemoryUsage' }, { Z1K1: 'Z6', Z6K1: memoryUsageStr } ],
		[ { Z1K1: 'Z6', Z6K1: 'orchestrationCpuUsage' }, { Z1K1: 'Z6', Z6K1: cpuUsageStr } ],
		[ { Z1K1: 'Z6', Z6K1: 'orchestrationStartTime' }, { Z1K1: 'Z6', Z6K1: startTimeStr } ],
		[ { Z1K1: 'Z6', Z6K1: 'orchestrationEndTime' }, { Z1K1: 'Z6', Z6K1: endTimeStr } ],
		[ { Z1K1: 'Z6', Z6K1: 'orchestrationDuration' }, { Z1K1: 'Z6', Z6K1: durationStr } ],
		[ { Z1K1: 'Z6', Z6K1: 'orchestrationHostname' }, { Z1K1: 'Z6', Z6K1: hostname } ]
	] );

	if ( currentResponseEnvelope instanceof ZWrapper ) {
		currentResponseEnvelope = currentResponseEnvelope.asJSON(
			invariants.orchestratorConfig.addNestedMetadata
		);
	}

	return currentResponseEnvelope;
}

module.exports = {
	orchestrate
};
