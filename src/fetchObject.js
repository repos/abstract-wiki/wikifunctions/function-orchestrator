'use strict';

/*!
 * Wikifunctions orchestrator code to fetch ZObjects and Wikidata entities from MediaWiki instances.
 *
 * @copyright 2020– Abstract Wikipedia team; see LICENSE
 * @license MIT
 */

const { error, makeErrorInNormalForm } = require( '../function-schemata/javascript/src/error.js' );
const fetch = require( '../lib/fetch.js' );
const {
	makeMappedResultEnvelope,
	wrapInQuote,
	isItemId,
	isPropertyId,
	isLexemeId,
	isLexemeFormId,
	isLexemeSenseId,
	isEntityId,
	convertArrayToKnownTypedList,
	wrapInZ9
} = require( '../function-schemata/javascript/src/utils.js' );
const normalize = require( '../function-schemata/javascript/src/normalize.js' );
const {
	convertItem,
	convertProperty,
	convertLexeme,
	convertLexemeForm,
	convertLexemeSense,
	makeWikidataRef,
	LEXEME_REF,
	conversionFunctionForWikidataID
} = require( './transform.js' );

const ReferenceType = Object.freeze( {
	WIKIDATA_OBJECT: Symbol( 'ReferenceType.WIKIDATA_OBJECT' ),
	ZOBJECT: Symbol( 'ReferenceType.ZOBJECT' )
} );

class ReferenceResolver {

	constructor(
		wikiUri,
		wikidataUri,
		wikifunctionsVirtualHost = null,
		wikidataVirtualHost = null,
		logThis = null,
		useWikidata = false
	) {
		this.wikiUri_ = wikiUri;
		this.wikidataUri_ = wikidataUri;
		this.wikifunctionsVirtualHost_ = wikifunctionsVirtualHost;
		this.wikidataVirtualHost_ = wikidataVirtualHost;
		this.referencePreCaches = new Map();
		this.referenceCaches = new Map();
		for ( const referenceType of Object.values( ReferenceType ) ) {
			this.referencePreCaches.set( referenceType, new Map() );
			this.referenceCaches.set( referenceType, new Map() );
		}
		this.useWikidata_ = useWikidata;
		this.logThis_ = logThis;
	}

	/**
	 * Dereferences an iterable of IDs, consulting the local cache and pre-
	 * cache in case any of the IDs has already been resolved
	 * (or is in the process of being resolved).
	 *
	 * @param {Array} theIDs IDs to be resolved.
	 * @param {symbol} referenceType ReferenceType, as defined above
	 * @param {Function} fetchCallback Callback returning an Array of Promises
	 *      to be called for IDs that cannot be resolved locally (e.g., by
	 *      consulting the cache)
	 * @return {Map} Map ID -> resolved object
	 */
	async dereferenceWithCaching( theIDs, referenceType, fetchCallback ) {
		const unresolved = new Set( theIDs );
		const toResolve = new Set();
		const dereferenced = new Map();
		const resolutionPromises = [];
		const referencePreCache = this.referencePreCaches.get( referenceType );
		const referenceCache = this.referenceCaches.get( referenceType );

		for ( const theID of unresolved ) {
			// Case 1: Object is already in the cache, so just return that.
			const previouslyDereferenced = referenceCache.get( theID );
			if ( previouslyDereferenced !== undefined ) {
				dereferenced.set( theID, previouslyDereferenced );
				continue;
			}

			if ( referenceType === ReferenceType.ZOBJECT ) {
				// Importing here instead of at top-level to avoid circular reference.
				const { resolveBuiltinReference } = require( './builtins.js' );
				// Case 2: Object is a built-in, so the reference can be resolved
				// directly within the orchestrator without performing a fetch.
				const builtin = resolveBuiltinReference( theID );
				if ( builtin !== null ) {
					const builtinZ2 = makeMappedResultEnvelope( { Z2K1: { Z1K1: 'Z6', Z6K1: theID }, Z2K2: builtin } );
					referenceCache.set( theID, builtinZ2 );
					dereferenced.set( theID, builtinZ2 );
					continue;
				}
			}

			// Case 3: Object is already being dereferenced elsewhere.
			// Wait for that to finish, then retrieve from the regular cache.
			const preCachePromise = referencePreCache.get( theID );
			if ( preCachePromise !== undefined ) {
				const { promiseForID } = preCachePromise;
				resolutionPromises.push( ( async () => {
					await promiseForID;
					dereferenced.set( theID, referenceCache.get( theID ) );
				} )() );
				continue;
			}

			// Case 4: This is the first time we've heard of this ZID.
			// Mark it for resolution.
			toResolve.add( theID );
			let resolveForID;
			const promiseForID = new Promise( ( resolve ) => {
				resolveForID = resolve;
			} );
			referencePreCache.set( theID, {
				resolveForID,
				promiseForID
			} );
		}

		if ( toResolve.size > 0 ) {
			const newlyResolved = await Promise.all( fetchCallback( toResolve ) );

			for ( const [ theID, dereferencedZObject ] of newlyResolved ) {
				dereferenced.set( theID, dereferencedZObject );
				referenceCache.set( theID, dereferencedZObject );
				// Value for theID is now set in referenceCache,
				// so we can resolve the corresponding pre-cache Promise.
				const { resolveForID } = referencePreCache.get( theID );
				resolveForID();
			}
		}

		await Promise.all( resolutionPromises );
		return dereferenced;
	}

	/**
	 * Assembles the wikilambda_fetch request for unresolved ZIDs.
	 * This function is intended to be passed as a callback to
	 * dereferenceWithCaching.
	 *
	 * @param {Set} toResolve A Set of unresolved ZIDs.
	 * @return {Array[Promise]} An Array of Promises, each of which will return
	 *      the pair [ZID, dereferencedZObject]
	 */
	fetchZIDs( toResolve ) {
		const resultPromises = [];
		if ( this.wikiUri_ === null ) {
			return resultPromises;
		}
		const url = new URL( this.wikiUri_ );
		url.searchParams.append( 'action', 'wikilambda_fetch' );
		url.searchParams.append( 'format', 'json' );
		// (T362271) Manually set the ignored 'uselang' value to 'content',
		// so requests cache
		url.searchParams.append( 'uselang', 'content' );
		const zIDsToFetch = [ ...toResolve ].join( '|' );
		url.searchParams.append( 'zids', zIDsToFetch );

		const fetchOptions = { method: 'GET' };
		if ( this.wikifunctionsVirtualHost_ !== null ) {
			fetchOptions.headers = { Host: this.wikifunctionsVirtualHost_ };
		}

		const fetchPromise = ( async () => {
			if ( this.logThis_ ) {
				const logMessage = `orchestrator start fetching ZIDs <${ zIDsToFetch }> from URI <${ url.toString() }> with host <${ this.wikifunctionsVirtualHost_ }> ...`;
				this.logThis_( 'info', logMessage );
			}
			const fetched = await fetch( url, fetchOptions );
			if ( this.logThis_ ) {
				const logMessage = `... orchestrator final fetching ZIDs <${ zIDsToFetch }>`;
				this.logThis_( 'info', logMessage );
			}
			const fetchedJSON = await fetched.json();
			return fetchedJSON;
		} )();

		for ( const ZID of toResolve ) {
			resultPromises.push( ( async () => {
				const result = await fetchPromise;
				const fetchResult = result[ ZID ];
				let dereferencedZObject;
				if ( fetchResult === undefined ) {
					dereferencedZObject = makeMappedResultEnvelope(
						null,
						makeErrorInNormalForm(
							error.zid_not_found,
							[ ZID ]
						)
					);
				} else {
					const zobject = JSON.parse( fetchResult.wikilambda_fetch );
					dereferencedZObject = normalize( zobject );
				}
				return [ ZID, dereferencedZObject ];
			} )() );
		}
		return resultPromises;
	}

	/**
	 * Gets the ZObjects of a list of ZIDs.
	 *
	 * @param {Array} ZIDs A list of ZIDs to fetch.
	 * @return {Object} An object mapping ZIDs to Z22s (envelopes)
	 */
	async dereferenceZObjects( ZIDs ) {
		return await this.dereferenceWithCaching(
			ZIDs, ReferenceType.ZOBJECT, this.fetchZIDs.bind( this ) );
	}

	/**
	 * Gets the JSON for a given Wikidata entity identifier, which must exist on Wikidata.
	 * The JSON returned has the form { entities: { ID: { <entity> } } }
	 *
	 * If the ID doesn't exist in Wikidata, Wikidata returns a 404, so we throw a custom Error with
	 * type "not-found". If it returns a 200 but isn't valid JSON, we throw one with "invalid-json".
	 *
	 * @param {string} ID A Wikidata entity identifier to fetch
	 * @return {Promise} An object containing the JSON representation of the given entity
	 * @throws {Error} If wikidataUri_ is null, or if fetch returns invalid JSON
	 */
	async retrieveWikidataEntityFromLODAPI( ID ) {
		if ( this.wikidataUri_ === null ) {
			throw new Error( 'wikidataUri has no value' );
		}

		const url = new URL( this.wikidataUri_ );
		url.pathname = '/wiki/Special:EntityData/' + ID + '.json';
		const fetchOptions = { method: 'GET' };
		if ( this.wikidataVirtualHost_ !== null ) {
			fetchOptions.headers = { Host: this.wikidataVirtualHost_ };
		}

		if ( this.logThis_ ) {
			const logMessage = `orchestrator start fetching Wikidata entity <${ ID }> from URI <${ url.toString() }> with host <${ this.wikidataVirtualHost_ }> ...`;
			this.logThis_( 'info', logMessage );
		}
		const fetched = await fetch( url, fetchOptions );
		if ( this.logThis_ ) {
			const logMessage = `... orchestrator finish fetching Wikidata entity <${ ID }>`;
			this.logThis_( 'info', logMessage );
		}

		// If we got back a 404, return a proper error
		if ( fetched.status === 404 ) {
			const theError = new Error( `Fetched Wikidata entity <${ ID }> not found` );
			// Note that this isn't a standard property, we're just mucking with the Error object
			theError.type = 'not-found';
			throw theError;
		}

		const fetchedString = await fetched.text();
		try {
			return JSON.parse( fetchedString );
		} catch ( e ) {
			// If we didn't get JSON back over the wire, e.g. a network issue, return an error
			const logMessage = `Fetched Wikidata entity for <${ ID }> isn't JSON; value: <${ fetchedString }>`;
			if ( this.logThis_ ) {
				this.logThis_( 'error', logMessage );
			}

			const theError = new Error( logMessage );
			// Note that this isn't a standard property, we're just mucking with the Error object
			theError.type = 'invalid-json';
			throw theError;
		}
	}

	/**
	 * Fetches JSON for the given Wikidata identifier ID, which must exist on Wikidata, using
	 * the Linked Open Data API (Special:EntityData), and constructs the corresponding ZObject.
	 *
	 * @param {string} ID Wikidata entity identifier to fetch.
	 * @param {Function} isValidId string=>boolean function to check the ID's lexical correctness
	 * @param {Function} convertToZObject Object=>Object fn to convert Wikidata JSON => ZObject
	 * @return {Promise} Z22 (envelope), containing the desired ZObject or an error
	 */
	async fetchWikidataEntityFromLODAPI( ID, isValidId, convertToZObject ) {
		if ( this.useWikidata_ === false ) {
			return makeMappedResultEnvelope(
				null,
				// TODO (T373413): Consider whether a Wikidata-specific error should be used here.
				makeErrorInNormalForm( error.unknown_error, [ 'Wikidata retrieval is currently disabled' ] )
			);
		}
		if ( !isValidId( ID ) ) {
			if ( this.logThis_ ) {
				const logMessage = `User requested invalid Wikidata entity ID <${ ID }>`;
				this.logThis_( 'info', logMessage );
			}
			return makeMappedResultEnvelope(
				null,
				// TODO (T373413): Consider whether a Wikidata-specific error should be used here.
				makeErrorInNormalForm( error.invalid_zreference, [ ID ] )
			);
		}

		let result;
		try {
			result = await this.retrieveWikidataEntityFromLODAPI( ID );
			result = result.entities[ ID ];
		} catch ( err ) {
			if ( this.logThis_ ) {
				const logMessage = `Caught error in fetchWikidataEntityFromLODAPI:\n  ${ err }`;
				this.logThis_( 'error', logMessage );
			}
			let zerror;
			if ( err.type === 'not-found' ) {
				// TODO (T373413): Consider whether a Wikidata-specific error should be used here.
				zerror = makeErrorInNormalForm( error.zid_not_found, [ ID ] );
			} else if ( err.type === 'invalid-json' ) {
				// TODO (T386312): Include the correct properties for invalid_json
				zerror = makeErrorInNormalForm( error.invalid_json, [ ID ] );
			} else {
				zerror = makeErrorInNormalForm( error.unknown_error, [ err.message ] );
			}
			return makeMappedResultEnvelope( null, zerror );
		}

		try {
			const zobject = convertToZObject( result, this.logThis_ );
			return makeMappedResultEnvelope( zobject, null );
		} catch ( err ) {
			if ( this.logThis_ ) {
				const logMessage = `Caught JSON error in fetchWikidataEntityFromLODAPI:\n  ${ err }`;
				this.logThis_( 'error', logMessage );
			}
			return makeMappedResultEnvelope(
				null,
				// TODO (T386312): Include the correct properties for invalid_json
				makeErrorInNormalForm( error.invalid_json, [ err.message, wrapInQuote( result ) ] )
			);
		}
	}

	/**
	 * Assembles the fetch request for unresolved Wikidata IDs,
	 * using the Linked Open Data API (Special:EntityData).
	 * This function is intended to be passed as a callback to
	 * dereferenceWithCaching.
	 *
	 * TODO (T386314): Consider replacing uses of this function with fetchWikidataEntities. This
	 *   will also allow for removing fetchWikidataEntityFromLODAPI and several other functions.
	 *
	 * @param {Function} isValidId string=>boolean function to check the ID's lexical correctness
	 * @param {Function} convertToZObject Object=>Object fn to convert Wikidata JSON => ZObject
	 * @param {Set} toResolve A Set of unresolved Wikidata IDs.
	 * @return {Array[Promise]} An Array of Promises, each of which will return
	 *      the pair [ID, dereferencedWikidataObject]
	 */
	fetchWikidataEntitiesFromLODAPI( isValidId, convertToZObject, toResolve ) {
		const resultPromises = [];
		for ( const ID of toResolve ) {
			resultPromises.push( ( async () => {
				const wikidataObject = await this.fetchWikidataEntityFromLODAPI(
					ID, isValidId, convertToZObject );
				return [ ID, wikidataObject ];
			} )() );
		}
		return resultPromises;
	}

	async dereferenceSingleWikidataObject( ID, isValidId, convertToZObject ) {
		const fetchWikidataEntitiesFromLODAPI = this.fetchWikidataEntitiesFromLODAPI.bind(
			this, isValidId, convertToZObject );
		const resultMap = await this.dereferenceWithCaching(
			[ ID ], ReferenceType.WIKIDATA_OBJECT, fetchWikidataEntitiesFromLODAPI );
		return resultMap.get( ID );
	}

	/**
	 * Gets the ZObject (instance of Z6001) corresponding to the item with the given ID.
	 *
	 * @param {string} IID Wikidata entity identifier to fetch.
	 * @return {Promise} Z22 (envelope), containing the desired ZObject or an error
	 */
	async dereferenceItem( IID ) {
		return await this.dereferenceSingleWikidataObject( IID, isItemId, convertItem );
	}

	/**
	 * Gets the ZObject (instance of Z6002) corresponding to the property with the given ID.
	 *
	 * @param {string} PID Wikidata property identifier to fetch.
	 * @return {Promise} Z22 (envelope), containing the desired ZObject or an error
	 */
	async dereferenceProperty( PID ) {
		return await this.dereferenceSingleWikidataObject( PID, isPropertyId, convertProperty );
	}

	/**
	 * Gets the ZObject (instance of Z6004) corresponding to the lexeme form with the given ID.
	 *
	 * @param {string} LFID Wikidata entity identifier to fetch.
	 * @return {Promise} Z22 (envelope), containing the desired ZObject or an error
	 */
	async dereferenceLexemeForm( LFID ) {
		return await this.dereferenceSingleWikidataObject(
			LFID, isLexemeFormId, convertLexemeForm );
	}

	/**
	 * Gets the ZObject (instance of Z6005) corresponding to the lexeme with the given ID.
	 *
	 * @param {string} LID to fetch.
	 * @return {Promise} Z22 (envelope), containing the desired ZObject or an error
	 */
	async dereferenceLexeme( LID ) {
		return await this.dereferenceSingleWikidataObject( LID, isLexemeId, convertLexeme );
	}

	/**
	 * Gets the ZObject (instance of Z6006) corresponding to the lexeme sense with the given ID.
	 *
	 * @param {string} LSID Wikidata entity identifier to fetch.
	 * @return {Promise} Z22 (envelope), containing the desired ZObject or an error
	 */
	async dereferenceLexemeSense( LSID ) {
		return await this.dereferenceSingleWikidataObject(
			LSID, isLexemeSenseId, convertLexemeSense );
	}

	/**
	 * Gets the JSON from Wikidata for each of the given entity identifiers,
	 * using the action API (action=wbgetentities). The return from this function has the form
	 *   { ID1: <entity1_JSON>, ID2: <entity2_JSON>, ... }
	 *
	 * For a valid but unused ID, Wikidata returns JSON containing "missing":""
	 * at the top-level.
	 *
	 * @param {Array} IDs An array of unresolved IDs of Wikidata entities.
	 * @return {Promise} A JavaScript object containing ID:JSON for each ID
	 * @throws {Error} If wikidataUri_ is null, or if fetch returns 404 or invalid JSON
	 */
	async retrieveWikidataEntities( IDs ) {
		if ( IDs.length === 0 ) {
			return {};
		}
		if ( this.wikidataUri_ === null ) {
			throw new Error( 'wikidataUri has no value' );
		}

		const url = new URL( this.wikidataUri_ );
		url.pathname = '/w/api.php';
		url.searchParams.append( 'action', 'wbgetentities' );
		url.searchParams.append( 'format', 'json' );
		const fetchOptions = { method: 'GET' };
		if ( this.wikidataVirtualHost_ !== null ) {
			fetchOptions.headers = { Host: this.wikidataVirtualHost_ };
		}

		const fetchPromises = [];
		let start = 0, end;
		// Make a promise for each retrieval, retrieving up to 50 IDs at a time
		while ( start < IDs.length ) {
			end = Math.min( IDs.length, start + 50 );
			const iDsToFetch = IDs.slice( start, end ).join( '|' );
			url.searchParams.set( 'ids', iDsToFetch );
			fetchPromises.push( ( async () => {
				if ( this.logThis_ ) {
					const logMessage = `orchestrator start fetching Wikidata entities <${ iDsToFetch }> from URI <${ url.toString() }> with host <${ this.wikidataVirtualHost_ }> ...`;
					this.logThis_( 'debug', logMessage );
				}
				const fetched = await fetch( url, fetchOptions );
				if ( this.logThis_ ) {
					const logMessage = `... orchestrator final fetching Wikidata entities <${ iDsToFetch }>`;
					this.logThis_( 'debug', logMessage );
				}

				if ( fetched.status !== 200 ) {
					const logMessage = `Status <${ fetched.status }> returned from Wikidata wbgetentities API with IDs <${ iDsToFetch }>`;
					throw new Error( logMessage );
				}

				const fetchedJSON = await fetched.json();

				if ( fetchedJSON.error ) {
					// We validated the IDs, but this can still happen (e.g., with 'L34355555555'),
					// and with status 200
					const logMessage = 'Error from Wikidata wbgetentities API: ' + JSON.stringify( fetchedJSON.error );
					throw new Error( logMessage );
				}
				if ( fetchedJSON.warning ) {
					// This can happen with invalid parameter values
					if ( this.logThis_ ) {
						const logMessage = 'Warning from Wikidata wbgetentities API: ' + JSON.stringify( fetchedJSON.warning );
						this.logThis_( 'warning', logMessage );
					}
				}
				// The result is an object containing ID:<JSON> for each ID
				return fetchedJSON.entities;
			} )() );
			start = end;
		}

		const fetchResults = await Promise.all( fetchPromises );
		// Merge the results into a single object
		const target = fetchResults[ 0 ];
		for ( let i = 1; i < fetchResults.length; i++ ) {
			Object.assign( target, fetchResults[ i ] );
		}
		return target;
	}

	/**
	 * Assembles the wbgetentities request for unresolved entity IDs.
	 * This function is intended to be passed as a callback to
	 * dereferenceWithCaching.
	 *
	 * Retrieves JSON for the given Wikidata entity IDs,
	 * and constructs the corresponding ZObject (or a ZError) for each.
	 *
	 * @param {Set} toResolve A Set of unresolved IDs of Wikidata entities.
	 * @return {Array[Promise]} An Array of Promises, each of which will return
	 *      the pair [ID, Z22 ], where the Z22 contains either the entity
	 *      converted to a ZObject or an error.  Z22s are in normal form.
	 * @throws Errors thrown from retrieveWikidataEntities
	 */
	fetchWikidataEntities( toResolve ) {
		const resultPromises = [];
		if ( this.wikidataUri_ === null ) {
			return resultPromises;
		}

		const fetchPromise =
			( async () => await this.retrieveWikidataEntities( [ ...toResolve ] ) )();

		for ( const ID of toResolve ) {
			resultPromises.push( ( async () => {
				const result = await fetchPromise;
				const fetchResult = result[ ID ];
				let dereferencedZObject;
				// Valid but unused IDs return an object with "missing":""
				if ( fetchResult === undefined || fetchResult.missing === '' ) {
					dereferencedZObject = makeMappedResultEnvelope(
						null,
						makeErrorInNormalForm(
							// TODO (T373413): Consider using a Wikidata-specific error here.
							error.zid_not_found,
							[ ID ]
						)
					);
				} else {
					const functor = conversionFunctionForWikidataID( ID );
					let zobject;
					try {
						zobject = functor( fetchResult, this.logThis_ );
						dereferencedZObject = makeMappedResultEnvelope( zobject, null );
					} catch ( err ) {
						if ( this.logThis_ ) {
							const logMessage = `Caught error in fetchWikidataEntities:\n  ${ err }`;
							this.logThis_( 'error', logMessage );
						}
						let zerror;
						if ( err.type === 'invalid-json' ) {
							// TODO (T386312): Include the correct properties for invalid_json
							zerror = makeErrorInNormalForm( error.invalid_json, [ ID ] );
						} else {
							zerror = makeErrorInNormalForm( error.unknown_error, [ err.message ] );
						}
						dereferencedZObject = makeMappedResultEnvelope( null, zerror );
					}
				}
				return [ ID, dereferencedZObject ];
			} )() );
		}
		return resultPromises;
	}

	/**
	 * Fetch Wikidata entities for a list of IDs, and convert them into ZObjects.
	 *
	 * TODO (T386598): Define a built-in function so this is available on-wiki
	 *
	 * @param {Array} IDs A list of Wikidata entity IDs to fetch.
	 * @return {Object} An object mapping ZIDs to Z22s (envelopes), OR
	 *     a single Z22 containing an error. Each Z22 is in normal form.
	 */
	async dereferenceWikidataEntities( IDs ) {
		// An invalid ID yields a top-level error from Wikidata, so no point in sending the request
		for ( const ID of IDs ) {
			if ( !isEntityId( ID ) ) {
				return makeMappedResultEnvelope(
					null,
					// TODO (T373413): Consider if a Wikidata-specific error should be used here.
					makeErrorInNormalForm( error.invalid_zreference, [ ID ] )
				);
			}
		}
		try {
			return await this.dereferenceWithCaching( IDs, ReferenceType.WIKIDATA_OBJECT,
				this.fetchWikidataEntities.bind( this ) );
		} catch ( err ) {
			if ( this.logThis_ ) {
				const logMessage = `Caught error in dereferenceWikidataEntities:\n  ${ err }`;
				this.logThis_( 'error', logMessage );
			}
			const zerror = makeErrorInNormalForm( error.unknown_error, [ err.message ] );
			return makeMappedResultEnvelope( null, zerror );
		}
	}

	/**
	 * Call Wikidata API to find entities that are related by the given property to the given value.
	 *
	 * Example: for PID=P5137/'item for this lexeme' and value=Q144/dog, we find entities which
	 * appear as *subject* in a statement with predicate P5137 and value Q144. Each search
	 * result looks like this:
	 *   {
	 *     "ns": 146,
	 *     "title": "Lexeme:L31499",
	 *     "pageid": 57430911,
	 *     "size": 28169,
	 *     "wordcount": 9,
	 *     "snippet": "hund hund hunden hunde hundene hunds hundens hundes hundenes",
	 *     "timestamp": "2025-01-16T17:11:10Z"
	 *   }
	 *
	 * If limit > 500, the API (and this function) behave as if it were 500.
	 *
	 * @param {string} value Value that could appear in a Wikidata statement, typically
	 *     an entity ID
	 * @param {string} PID Identifier for a Wikidata property
	 * @param {string} namespace Wikidata namespace in which the search should occur
	 *     default is 146 for Lexemes
	 * @param {number} limit Max number of search results requested; not to exceed 500
	 * @return {Promise} An array of search results, each a JSON object
	 * @throws {Error} If wikidataUri_ is null, fetch returns 404, or fetch returns invalid JSON
	 */
	async findEntitiesByStatements( value, PID, namespace = '146', limit = 500 ) {
		if ( this.wikidataUri_ === null ) {
			throw new Error( 'wikidataUri has no value' );
		}

		const url = new URL( this.wikidataUri_ );
		url.pathname = '/w/api.php';
		url.searchParams.append( 'action', 'query' );
		url.searchParams.append( 'format', 'json' );
		url.searchParams.append( 'list', 'search' );
		url.searchParams.append( 'formatversion', '2' );
		url.searchParams.append( 'srsearch', 'haswbstatement:' + PID + '=' + value );
		url.searchParams.append( 'srnamespace', namespace );
		url.searchParams.append( 'srlimit', limit.toString() );

		const fetchOptions = { method: 'GET' };
		if ( this.wikidataVirtualHost_ !== null ) {
			fetchOptions.headers = { Host: this.wikidataVirtualHost_ };
		}

		if ( this.logThis_ ) {
			const logMessage = `orchestrator start haswbstatement search (value <${ value }>, PID <${ PID }>) from URI <${ url.toString() }> with virtual host <${ this.wikidataVirtualHost_ }> ...`;
			this.logThis_( 'debug', logMessage );
		}
		const fetched = await fetch( url, fetchOptions );
		if ( this.logThis_ ) {
			const logMessage = `... orchestrator finish haswbstatement search (value <${ value }>, PID <${ PID }>)`;
			this.logThis_( 'debug', logMessage );
		}

		if ( fetched.status === 404 ) {
			throw new Error( `Status 404 from haswbstatement search (value <${ value }>, PID <${ PID }>)` );
		}

		const fetchedString = await fetched.text();
		try {
			return JSON.parse( fetchedString ).query.search;
		} catch ( e ) {
			// If we didn't get JSON back over the wire, e.g. a network issue, return an error
			const logMessage = `Malformed result from haswbstatement search (value <${ value }>, PID <${ PID }>): <${ fetchedString }>`;
			throw new Error( logMessage );
		}
	}

	/**
	 * Call Wikidata API to find lexemes that are related by the given property to the given item,
	 * and have the given language.
	 *
	 * We determine a lexeme's language by the language code of its first lemma;
	 * see also comments in getLexemeLanguage (transform.js).
	 * TODO (T373598): consider whether some other approach might be better.
	 *
	 * @param {string} QID Identifier for a Wikidata item
	 * @param {string} PID Identifier for a Wikidata property
	 * @param {Object} language an instance of Z60
	 * @return {Promise} Z22 with a list of Z6095/'Wikidata lexeme reference' or an error,
	 *     normal form
	 */
	async findLexemesForItem( QID, PID, language ) {
		try {
			const lexemeResults = await this.findEntitiesByStatements( QID, PID, '146' );

			// Get the lexeme IDs from the search results
			const LIDs = [];
			for ( const result of lexemeResults ) {
				if ( result.title && result.title.match( /^Lexeme:L[1-9]\d*$/ ) ) {
					LIDs.push( result.title.match( /^Lexeme:(L[1-9]\d*)$/ )[ 1 ] );
				}
			}

			// Retrieve the lexemes
			// TODO (T271776): Use inlanguage parameter in findEntitiesByStatements,
			//     when available; then remove this brute-force approach
			const languageCode = language.Z60K1.Z6K1;
			const lexemes = ( await this.retrieveWikidataEntities( LIDs ) );

			// Return the IDs of lexemes having the given language
			const lexemeRefs = [];
			for ( const key of Object.keys( lexemes ) ) {
				if ( Object.keys( lexemes[ key ].lemmas )[ 0 ] === languageCode ) {
					lexemeRefs.push( makeWikidataRef( LEXEME_REF, key ) );
				}
			}
			return makeMappedResultEnvelope(
				convertArrayToKnownTypedList( lexemeRefs, wrapInZ9( LEXEME_REF ) ),
				null
			);
		} catch ( err ) {
			if ( this.logThis_ ) {
				const logMessage = `Caught error in findLexemesForItem:\n  ${ err }`;
				this.logThis_( 'error', logMessage );
			}
			const zerror = makeErrorInNormalForm( error.unknown_error, [ err.message ] );
			return makeMappedResultEnvelope( null, zerror );
		}
	}
}

module.exports = { ReferenceResolver };
