'use strict';

/*!
 * Wikifunctions orchestrator root route manager.
 *
 * These are low-value, but k8s uses this route to determine if we're up.
 *
 * @copyright 2020– Abstract Wikipedia team; see LICENSE
 * @license MIT
 */

const sUtil = require( '../lib/util' );

/**
 * The main router object
 */
const router = sUtil.router();

/**
 * The main application object reported when this module is require()d
 */
let app;

/**
 * GET /robots.txt
 * Instructs robots no indexing should occur on this domain.
 */
router.get( '/robots.txt', ( req, res ) => {

	res.type( 'text/plain' ).send( 'User-agent: *\nDisallow: /\n' );

} );

/**
 * GET /
 * Main entry point. Currently it only responds if the spec or doc query
 * parameter is given, otherwise lets the next middleware handle it
 */
router.get( '/', ( req, res, next ) => {

	if ( {}.hasOwnProperty.call( req.query || {}, 'spec' ) ) {
		res.json( app.conf.spec );
	} else {
		next();
	}

} );

module.exports = ( appObj ) => {

	app = appObj;

	return {
		path: '/',
		skip_domain: true,
		router
	};

};
