'use strict';

/*!
 * Wikifunctions orchestrator route manager for inbound language availability requests.
 *
 * @copyright 2020– Abstract Wikipedia team; see LICENSE
 * @license MIT
 */

const sUtil = require( '../lib/util' );
const { getEvaluatorAndOrchestratorConfigsForEnvironment } = require( '../src/utils.js' );

/**
 * The main router object
 */
const router = sUtil.router();

router.get( '/', ( req, res ) => {
	const { evaluatorConfigs } = getEvaluatorAndOrchestratorConfigsForEnvironment(
		process.env, /* useReentrance= */ false );
	const programmingLanguages = [];
	for ( const evaluatorConfig of evaluatorConfigs ) {
		if ( evaluatorConfig.programmingLanguages !== undefined ) {
			for ( const language of evaluatorConfig.programmingLanguages ) {
				programmingLanguages.push( language );
			}
		}
		if ( evaluatorConfig.softwareLanguages !== undefined ) {
			for ( const language of evaluatorConfig.softwareLanguages ) {
				programmingLanguages.push( language );
			}
		}
	}
	res.json( programmingLanguages );
} );

module.exports = function ( appObj ) {

	// the returned object mounts the routes on
	// /{domain}/vX/mount/path
	return {
		path: '/supported-programming-languages',
		api_version: 1, // must be a number!
		router: router
	};

};
