'use strict';

/*!
 * Wikifunctions orchestrator route manager for inbound evaluation requests.
 *
 * @copyright 2020– Abstract Wikipedia team; see LICENSE
 * @license MIT
 */

const sUtil = require( '../lib/util' );
const canonicalize = require( '../function-schemata/javascript/src/canonicalize.js' );
const { error, makeErrorInNormalForm } = require( '../function-schemata/javascript/src/error.js' );
const { wrapInZ6 } = require( '../function-schemata/javascript/src/utils.js' );
const { ReferenceResolver } = require( '../src/fetchObject.js' );
const { Evaluator } = require( '../src/Evaluator.js' );
const { Invariants } = require( '../src/Invariants.js' );
const { orchestrate } = require( '../src/orchestrate.js' );
const {
	getEvaluatorAndOrchestratorConfigsForEnvironment,
	getTimeoutForEnvironment,
	makeWrappedResultEnvelope,
	responseEnvelopeContainsError
} = require( '../src/utils.js' );
const { ZWrapper } = require( '../src/ZWrapper.js' );

const wikiUri = process.env.WIKI_API_URL || null;
const wikidataUri = process.env.WIKIDATA_API_URL || null;
const wikifunctionsVirtualHost = process.env.WIKI_VIRTUAL_HOST || null;
const wikidataVirtualHost = process.env.WIKIDATA_VIRTUAL_HOST || null;
const defaultTimeout = 20000;

/**
 * The main router object
 */
const router = sUtil.router();

async function propagateResult( requestId, res, result, invariants ) {
	invariants.rateLimiter.evict( requestId );

	if ( res.writableEnded ) {
		return;
	}

	const logData = {
		// We repeat the incoming request ID so we can match up load
		'x-request-id': requestId
	};

	// result should be bare JSON (never a ZWrapper), so convert to JSON and log
	// an error if it is a ZWrapper.
	if ( result instanceof ZWrapper ) {
		logData.response = result;
		invariants.logger.log(
			'error',
			{ message: 'propagateResult has erroneously received a ZWrapper', requestId: requestId, info: logData }
		);
		result = result.asJSON();
	}
	// (T343176) Make sure to not log the object; it'll explode in logstash
	logData.response = JSON.stringify( result );
	invariants.logger.log(
		'info',
		{ message: 'Outgoing orchestrator response', requestId: requestId, info: logData }
	);

	const canonicalized = canonicalize( result );

	if ( responseEnvelopeContainsError( canonicalized ) ) {
		// If canonicalization fails, return normalized form instead.
		invariants.logger.log(
			'warn',
			{ message: 'Could not canonicalize; returning the error from function-schemata.', info: logData }
		);
		result = canonicalized;
	} else {
		result = canonicalized.Z22K1;
	}
	res.json( result );
}

/** ROUTE DECLARATIONS GO HERE */
router.post( '/', async ( req, res ) => {
	sUtil.sendOutgoingResponseMetrics( req );

	const logger = req.app.logger;
	const requestId = req.context.reqId;

	const timeoutInMilliseconds = getTimeoutForEnvironment( process.env, defaultTimeout, logger );

	// TODO (T373030): This is brittle and unmaintainable; we should restructure
	// this and eliminate all calls to propagateResult.
	let invariants; // eslint-disable-line prefer-const
	const timer = setTimeout(
		async () => {
			await propagateResult(
				requestId,
				res,
				makeWrappedResultEnvelope(
					null,
					makeErrorInNormalForm(
						error.orchestrator_time_limit,
						[ wrapInZ6( timeoutInMilliseconds.toString() + ' ms' ) ]
					)
				).asJSON(),
				invariants
			);
		},
		timeoutInMilliseconds
	);

	function getRemainingTime() {
		return timer._idleStart + timer._idleTimeout - Date.now();
	}

	// Capture all stray config.
	// TODO (T367080): These bits of config shouldn't actually be passed in.
	// Verify that this code can all be deleted with impunity, then delete it.
	const useReentrance = req.body.useReentrance || false;
	const {
		evaluatorConfigs,
		orchestratorConfig
	} = getEvaluatorAndOrchestratorConfigsForEnvironment( process.env, useReentrance );
	// TODO (T367080): This, too.
	orchestratorConfig.doValidate = req.body.doValidate === undefined ? true : req.body.doValidate;

	// Initialize invariants.
	const referenceLogFunction = ( level, message ) => {
		logger.log( level, { message, requestId } );
	};
	const useWikidata = orchestratorConfig.useWikidata || false;
	const resolver = new ReferenceResolver(
		wikiUri, wikidataUri, wikifunctionsVirtualHost, wikidataVirtualHost,
		referenceLogFunction, useWikidata );
	const evaluators = evaluatorConfigs.map(
		( evaluatorConfig ) => new Evaluator( evaluatorConfig, logger ) );
	invariants = new Invariants(
		resolver, evaluators, orchestratorConfig, getRemainingTime,
		requestId, logger, req );

	try {
		// Orchestrate!
		invariants.logger.log( 'info', { message: 'calling orchestrator...', requestId: invariants.requestId } );
		const response = await orchestrate( req.body.zobject, invariants );

		clearTimeout( timer );
		await propagateResult( requestId, res, response, invariants );

		invariants.logger.log( 'info', { message: '...finished calling orchestrator', requestId: invariants.requestId } );
	} catch ( error ) {
		invariants.logger.log( 'error', { message: error, requestId: invariants.requestId } );
		await propagateResult(
			requestId,
			res,
			makeWrappedResultEnvelope(
				null,
				{
					Z1K1: {
						Z1K1: 'Z9',
						Z9K1: 'Z5'
					},
					// TODO (T327275): Figure out what error this should actually be.
					Z5K1: {
						Z1K1: 'Z9',
						Z9K1: 'Z558'
					},
					Z5K2: {
						Z1K1: 'Z6',
						Z6K1: 'Orchestration generally failed.'
					}
				}
			).asJSON(),
			invariants
		);
	}
} );

router.get( '/', ( req, res ) => {
	res.sendStatus( 200 );
} );

module.exports = function ( appObj ) {

	// the returned object mounts the routes on
	// /{domain}/vX/mount/path
	return {
		path: '/evaluate',
		api_version: 1, // must be a number!
		router: router
	};

};
