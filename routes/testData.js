'use strict';

/*!
 * Wikifunctions orchestrator route manager for inbound test-data requests.
 *
 * @copyright 2020– Abstract Wikipedia team; see LICENSE
 * @license MIT
 */

const fs = require( 'fs' );
const sUtil = require( '../lib/util' );
const { deepCopy } = require( '../function-schemata/javascript/src/utils.js' );

/**
 * The main router object
 */
const router = sUtil.router();

/**
 * GET /test-data/:fileName
 * Retrieves test data JSON file as a string.
 */
router.get( '/test-data/:fileName', ( req, res ) => {
	const jsonFile = `${ __dirname }/../test/features/v1/test_data/${ req.params.fileName }.json`;
	// eslint-disable-next-line security/detect-non-literal-fs-filename
	if ( !fs.existsSync( jsonFile ) ) {
		res.status( 404 ).end( `No file named ${ req.params.fileName } exists.` );
		return;
	}
	// eslint-disable-next-line security/detect-non-literal-fs-filename
	const contents = fs.readFileSync( jsonFile, { encoding: 'utf8' } );
	const result = { value: deepCopy( contents ) };
	res.json( result );
} );

module.exports = ( appObj ) => ( {
	path: '/',
	skip_domain: true,
	router
} );
