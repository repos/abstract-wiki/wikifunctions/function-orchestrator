'use strict';

const fetch = require( 'node-fetch' );
const version = require( '../package.json' ).version;

// Define the wrapper function with async/await
module.exports = async ( url, args = {} ) => {
	// Set up headers
	args.headers = args.headers || {};
	args.headers[ 'user-agent' ] = 'wikifunctions-function-orchestrator/' + version;

	// Await the fetch call and return the result
	const response = await fetch( url, args );
	return response;
};
