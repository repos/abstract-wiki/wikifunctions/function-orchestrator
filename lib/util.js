'use strict';

const express = require( 'express' );
const uuidv1 = require( 'uuid/v1' );

const process = require( 'process' );
const os = require( 'os' );
const v8 = require("v8");

const { safeJsonStringify } = require( '../src/utils.js' );
const { isZFunctionCall } = require( '../function-schemata/javascript/src/utils.js' );
const { builtinReferences, resolveListType } = require( '../src/builtins.js' );

/**
 * Error instance wrapping HTTP error responses
 */
class HTTPError extends Error {

	constructor( response ) {
		super();
		Error.captureStackTrace( this, HTTPError );

		if ( response.constructor !== Object ) {
			// just assume this is just the error message
			response = {
				status: 500,
				type: 'internal_error',
				title: 'InternalError',
				detail: response
			};
		}

		this.name = this.constructor.name;
		this.message = `${ response.status }`;
		if ( response.type ) {
			this.message += `: ${ response.type }`;
		}

		Object.assign( this, response );
	}
}

/**
 * Generates an object suitable for logging out of a request object
 *
 * @param {!Request} req          the request
 * @param {?RegExp}  whitelistRE  the RegExp used to filter headers
 * @return {!Object} an object containing the key components of the request
 */
function reqForLog( req, whitelistRE ) {

	const ret = {
		url: req.originalUrl,
		headers: {},
		method: req.method,
		params: req.params,
		query: req.query,
		body: req.body,
		remoteAddress: req.connection.remoteAddress,
		remotePort: req.connection.remotePort
	};

	if ( req.headers && whitelistRE ) {
		Object.keys( req.headers ).forEach( ( hdr ) => {
			if ( whitelistRE.test( hdr ) ) {
				ret.headers[ hdr ] = req.headers[ hdr ];
			}
		} );
	}

	return ret;

}

/**
 * Serialises an error object in a form suitable for logging.
 *
 * @param {!Error} err error to serialise
 * @return {!Object} the serialised version of the error
 */
function errForLog( err ) {

	const ret = {};
	ret.status = err.status;
	ret.type = err.type;
	ret.detail = err.detail;

	// log the stack trace only for 500 errors
	if ( Number.parseInt( ret.status, 10 ) !== 500 ) {
		ret.stack = undefined;
	}

	return ret;

}

/**
 * Wraps all of the given router's handler functions with
 * promised try blocks so as to allow catching all errors,
 * regardless of whether a handler returns/uses promises
 * or not.
 *
 * @param {!Object} route the object containing the router and path to bind it to
 * @param {!Application} app the application object
 */
async function wrapRouteHandlers( route, app ) {

	route.router.stack.forEach( ( routerLayer ) => {
		const path = ( route.path + routerLayer.route.path.slice( 1 ) )
			.replace( /\/:/g, '/--' )
			.replace( /^\//, '' )
			.replace( /[/?]+$/, '' );
		routerLayer.route.stack.forEach( ( layer ) => {
			const origHandler = layer.handle;
			const metric = sendRequestDurationMetric( app );

			layer.handle = async ( req, res, next ) => {
				const startTime = Date.now();
				try {
					// Await the original handler execution
					await origHandler( req, res, next );
				} catch ( err ) {
					// Pass the error to next() if it occurs
					next( err );
				} finally {
					// Record the timing after the handler is done
					let statusCode = parseInt( res.statusCode, 10 ) || 500;
					if ( statusCode < 100 || statusCode > 599 ) {
						statusCode = 500;
					}
					metric.endTiming ( startTime, [ path || 'root', req.method, statusCode ] );
				};
			};
		} );
	} );

}

/**
 * Generates an error handler for the given applications and installs it.
 *
 * @param {!Application} app the application object to add the handler to
 */
function setErrorHandler( app ) {

	app.use( ( err, req, res, next ) => {
		let errObj;
		// ensure this is an HTTPError object
		if ( err.constructor === HTTPError ) {
			errObj = err;
		} else if ( err instanceof Error ) {
			// is this an HTTPError defined elsewhere?
			if ( err.constructor.name === 'HTTPError' ) {
				const o = { status: err.status };
				if ( err.body && err.body.constructor === Object ) {
					Object.keys( err.body ).forEach( ( key ) => {
						o[ key ] = err.body[ key ];
					} );
				} else {
					o.detail = err.body;
				}
				o.message = err.message;
				errObj = new HTTPError( o );
			} else {
				// this is a standard error, convert it
				errObj = new HTTPError( {
					status: 500,
					type: 'internal_error',
					title: err.name,
					detail: err.detail || err.message,
					stack: err.stack
				} );
			}
		} else if ( err.constructor === Object ) {
			// this is a regular object, suppose it's a response
			errObj = new HTTPError( err );
		} else {
			// just assume this is just the error message
			errObj = new HTTPError( {
				status: 500,
				type: 'internal_error',
				title: 'InternalError',
				detail: err
			} );
		}
		// ensure some important error fields are present
		errObj.status = errObj.status || 500;
		errObj.type = errObj.type || 'internal_error';
		// add the offending URI and method as well
		errObj.method = errObj.method || req.method;
		errObj.uri = errObj.uri || req.url;
		// some set 'message' or 'description' instead of 'detail'
		errObj.detail = errObj.detail || errObj.message || errObj.description || '';
		// adjust the log level based on the status code
		let level = 'error';
		if ( Number.parseInt( errObj.status, 10 ) < 400 ) {
			level = 'verbose';
		} else if ( Number.parseInt( errObj.status, 10 ) < 500 ) {
			level = 'info';
		}
		// log the error
		const component = ( errObj.component ? errObj.component : errObj.status );
		// Encode the error object into JSON so the log doesn't just show `500: [object Object]`
		const message = `Otherwise Unhandled error ${ component }: ${ JSON.stringify( errForLog( errObj ) ) }`;
		( req.logger || app.logger ).log( level, { message: message } );
		// let through only non-sensitive info
		const respBody = {
			status: errObj.status,
			type: errObj.type,
			title: errObj.title,
			detail: errObj.detail,
			method: errObj.method,
			uri: errObj.uri
		};
		res.status( errObj.status ).json( respBody );
	} );

}

/**
 * Creates a new router with some default options.
 *
 * @param {?Object} [opts] additional options to pass to express.Router()
 * @return {!Router} a new router object
 */
function createRouter( opts ) {

	const options = {
		mergeParams: true
	};

	if ( opts && opts.constructor === Object ) {
		Object.assign( options, opts );
	}

	return new express.Router( options );

}

/**
 * Adds logger to the request and logs it.
 *
 * @param {!*} req request object
 * @param {!Application} app application object
 */
function initAndLogRequest( req, app ) {

	req.headers = req.headers || {};
	req.headers[ 'x-request-id' ] = req.headers[ 'x-request-id' ] || uuidv1();
	req.logger = app.logger.child( {
		request_id: req.headers[ 'x-request-id' ],
		request: reqForLog( req, app.conf.log_header_whitelist )
	} );
	req.context = { reqId: req.headers[ 'x-request-id' ] };
	if ( 'traceparent' in req.headers ) {
		req.context.traceparent = req.headers.traceparent;
	}
	if ( 'tracestate' in req.headers ) {
		req.context.tracestate = req.headers.tracestate;
	}

	sendIncomingRequestCountMetric( app )
}

function sendRequestDurationMetric( app ) {
	return app.metrics.makeMetric( {
		type: 'Histogram',
		name: 'router',
		prometheus: {
			name: 'function_orchestrator_router_request_duration_seconds',
			help: 'request duration handled by router in seconds',
			staticLabels: app.metrics.getServiceLabel(),
			buckets: [ 0.25, 0.5, 1, 2, 4, 8 ]
		},
		labels: {
			names: [ 'path', 'method', 'status' ],
			omitLabelNames: true
		}
	} );
}

function sendIncomingRequestCountMetric( app ) {
	return app.metrics.makeMetric( {
		type: 'Counter',
		name: 'function-orchestrator.incomingRequestCount',
		prometheus: {
			name: 'function_orchestrator_incomingrequestcount',
			help: 'Wikifunctions function-orchestrator request count'
		}
	} ).increment( 1 );
}

function sendOutgoingResponseMetrics( req ) {
	const app = req.app;

	// (T343176) Make sure to not log the object; it'll explode in logstash
	const requestedZObj = req.body.zobject;
	const stringifiedResult = safeJsonStringify( requestedZObj );
	app.logger.info(
		'Outgoing orchestrator response',
		{ requestId: req.context.reqId, info: stringifiedResult }
	);

	if ( !isZFunctionCall ( requestedZObj ) ) {
		return app.metrics.makeMetric( {
			type: 'Counter',
			name: 'function-orchestrator.nonRequestError',
			prometheus: {
				name: 'function_orchestrator_nonrequesterror',
				help: 'Wikifunctions function-orchestrator non-request error count'
			}
		} ).increment( 1 );
	}

	return app.metrics.makeMetric( {
		type: 'Counter',
		name: 'function-orchestrator.outgoingResponseCount',
		prometheus: {
			name: 'function_orchestrator_outgoingresponsecount',
			help: 'Wikifunctions function-orchestrator response count'
		}
	} ).increment( 1 );

	req.method = req.method || 'get';
	req.headers = req.headers || {};
	Object.assign( req.headers, {
		'user-agent': app.conf.user_agent,
		'x-request-id': req.context.reqId
	} );
}

function createFunctionDurationMetrics( app ) {
	return app.metrics.makeMetric( {
		type: 'Summary',
		name: 'marker',
		prometheus: {
			name: 'function_orchestrator_function_duration_milliseconds',
			help: 'function call duration in milliseconds',
			percentiles: [ 0.1, 0.25, 0.5, 0.9, 0.95, 0.99 ]
		}
	} )
}

function submitFunctionDurationMetricsAndLog( invariantsInstance, metricsInstance, startTime, functionIdentity ) {
	let isBuiltIn = true;
	if  ( ( builtinReferences.get( functionIdentity ) === null ) || ( builtinReferences.get( functionIdentity ) === undefined ) ) {
		isBuiltIn = false;
	}

	invariantsInstance.logger.log( 'info', {
		// eslint-disable-next-line max-len
		message: `function_duration_milliseconds, identity: ${ functionIdentity }, isBuiltIn: ${ isBuiltIn }, implementation: ${ invariantsInstance.implementationZID }`,
		requestId: invariantsInstance.requestId
	} );

	metricsInstance.observe( performance.now() - startTime );
}

function sendImplementationErrorMetrics( req, errorMessage ) {
	return req.app.metrics.makeMetric( {
		type: 'Counter',
		name: 'function_orchestrator_function_implementation_error_count',
		prometheus: {
			name: 'function_orchestrator_function_implementation_error_count',
			help: 'function call implementation errors count'
		}
	} ).increment( 1 );
}

function sendFunctionExecutionCounterMetrics( req ) {
	return req.app.metrics.makeMetric( {
		type: 'Counter',
		name: 'function_orchestrator_function_execute_count',
		prometheus: {
			name: 'function_orchestrator_function_execute_count',
			help: 'function execution counter'
		}
	} ).increment( 1 );
}

/**
 * Calculates heap, memory, CPU usage and logs
 * whenever invoked from a specific function,
 * or when invoked from periodicResourceLogging(), only if limit(s) is about to be reached
 *
 * @param {Object} logger Logger object
 * @param {string} reqId 'x-request-id' string
 * @param {string} invokedLocation string that names which function invoked this, e.g. 'orchestrate()'
 */
function logResourceUsage( logger, reqId = null, invokedLocation = null ) {
	// thresholds
	const HEAP_THRESHOLD_PERCENTAGE = 80; // 80% heap usage
	const CPU_THRESHOLD = os.cpus().length; // Load avg exceeding number of CPU cores

	// Process Memory Usage
	const memoryUsage = process.memoryUsage();
	const processHeapUsed = memoryUsage.heapUsed / 1024 / 1024; // Convert to MB
	const processHeapTotal = memoryUsage.heapTotal / 1024 / 1024;
	const processHeapUsagePercent = ( ( memoryUsage.heapUsed / memoryUsage.heapTotal ) * 100 ).toFixed( 2 );

	// V8 Memory Usage
	const v8HeapStats = v8.getHeapStatistics();
	const v8HeapUsed = v8HeapStats.used_heap_size / 1024 / 1024;
	const v8HeapLimit = v8HeapStats.heap_size_limit / 1024 / 1024;
	const v8HeapUsagePercent = ( ( v8HeapUsed / v8HeapLimit ) * 100 ).toFixed( 2 );

	if ( reqId || invokedLocation ) { // present when functionally invoked
		const logObject =
			`V8 heap usage: ${ v8HeapUsagePercent }%, ` +
			`Process heap usage: ${ processHeapUsagePercent }% ${ invokedLocation }`;
		processHeapUsagePercent >= HEAP_THRESHOLD_PERCENTAGE ? logger.warn( logObject ): logger.info( logObject );
		return; // return out of this utility function so doesn't overlap with it running periodically
	}

	// Total OS memory usage by Node.js process
	const rss = ( memoryUsage.rss / 1024 / 1024 ).toFixed( 2 );
	// represents the portion of memory allocated to process is held in physical memory (RAM)
	const maxRSS = 900 * 1024 * 1024; // 900MB given current 1GB k8 limit
	if ( rss > maxRSS ) {
	// Check if OS memory (RSS) is too high
		logger.warn(
			`Total process memory exceeds ${ maxRSS } MB! ` +
			`Node.js process is using too much memory: ${ rss } MB!`,
		)
	}

	// CPU Usage
	const cpuLoad = ( os.loadavg()[0] ).toFixed( 2 ); // 1-minute load average
	const cpu = process.cpuUsage();

	if ( cpuLoad > CPU_THRESHOLD ) {
		const cpuUser = cpu.user / 1000; // convert from microseconds to milliseconds
		// amount of CPU time spent executing the app (JS) code
		const cpuSystem = cpu.system / 1000;
		// amount of CPU time spent on system-level tasks, where app interacts with the os
		logger.warn(
			`High CPU load: ${ cpuLoad }, ` +
			`User: ${ cpuUser } ms, `+
			`System: ${ cpuSystem } ms, `+
			`Rss: ${ rss } mb`
		);
	}

	if ( processHeapUsagePercent > HEAP_THRESHOLD_PERCENTAGE ) {
		logger.warn(
			`V8 heap usage: ${ v8HeapUsagePercent }%, ` +
			`Process heap usage: ${ processHeapUsagePercent }% used, ` +
			`Rss: ${ rss } mb`
		);
	}

	// ( the following may be handy during debugging: )
	// max buffer size allowed by NodeJS
	// const bufferPoolSize = require( 'buffer' ).constants.MAX_LENGTH / 1024 / 1024;
	// check open handles
	// const processActiveHandles = process._getActiveHandles();

	// how much memory is being used by NodeJS that is not v8-related
	const processMemoryExternal = ( process.memoryUsage().external / 1024 / 1024 ).toFixed( 2 );
	// memory that v8 directly allocates outside of itself that it alone manages and allocates
	const v8MemoryExternal = ( v8HeapStats.external_memory / 1024 / 1024 ).toFixed( 2 );
	const EXTERNAL_MEMORY_THRESHOLD = 50; // MB

	if ( ( processMemoryExternal > EXTERNAL_MEMORY_THRESHOLD ) || ( v8MemoryExternal > EXTERNAL_MEMORY_THRESHOLD ) ) {
		logger.warn(
			`Process External Memory: ${ processMemoryExternal } MB, ` +
			`v8 External Memory: ${ v8MemoryExternal } MB `
		);
	}
}

/**
 * Function that logs resource consumption periodically
 *
 * @param {Object} logger Logger object
 * @param {Number} intervalMilliseconds frequency in milliseconds
 */
function periodicResourceLogging( logger, intervalMilliseconds = 300000 ) {
	setInterval( () => {
		logResourceUsage( logger );
	}, intervalMilliseconds );
}

/**
 * Function that logs where buffers are being allocated
 * in order to detect large buffer usage
 *
 * @param {Object} logger Logger object
 */
function logBufferAllocation( logger ) {
	const originalBufferAlloc = Buffer.alloc;
	const originalBufferFrom = Buffer.from;
	const BUFFER_THRESHOLD = 1024 * 1024; // 1 MB

	Buffer.alloc = function ( size, ...args ) {
		if ( size > BUFFER_THRESHOLD ) { // log only when large buffer gets allocated
			logger.info( `Allocating Buffer size: ${ size } bytes, ${ size / 1024 / 1024 } MB` );
		};
		return originalBufferAlloc.call( Buffer, size, ...args );
	}

	Buffer.from = function ( data, ...args ) {
		if ( data.length > BUFFER_THRESHOLD ) {
			logger.info( `Creating Buffer data size: ${ data.length } bytes, ${ data.length / 1024 / 1024 } MB` );
		};
		return originalBufferFrom.call( Buffer, data, ...args );
	}
}

module.exports = {
	createFunctionDurationMetrics,
	HTTPError,
	initAndLogRequest,
	logBufferAllocation,
	logResourceUsage,
	periodicResourceLogging,
	router: createRouter,
	setErrorHandler,
	sendFunctionExecutionCounterMetrics,
	sendImplementationErrorMetrics,
	sendOutgoingResponseMetrics,
	setErrorHandler,
	submitFunctionDurationMetricsAndLog,
	router: createRouter,
	wrapRouteHandlers
};
