'use strict';

const {
	getError,
	getZMapValue,
	isZMap,
	makeMappedResultEnvelope,
	makeTrue,
	makeVoid,
	setZMapValue
} = require( '../../../function-schemata/javascript/src/utils.js' );
const assert = require( '../../utils/assert.js' );
const { readJSON } = require( '../../../src/utils.js' );
const normalize = require( '../../../function-schemata/javascript/src/normalize.js' );
const { testInputsDir, testOutputsDir, testObjectsDir, testWikidataDir, schemataDefinitionsDir } = require( '../../utils/testFileUtils.js' );
const { makeErrorInNormalForm, error } = require( '../../../function-schemata/javascript/src/error.js' );
const { getServerWithMocks, getTestInvariants, EVAL_URI, WIKI_URI, WIKIDATA_URI } = require( '../../utils/mockUtils.js' );
const { attemptOrchestration } = require( './mswTestRunner.js' );
const { orchestrate } = require( '../../../src/orchestrate.js' );

describe( 'orchestrate', () => {
	const { wikiStub, wikidataStub, wikidataQueryStub,
		evaluatorStub, mockServiceWorker } = getServerWithMocks();

	before( () => {
		mockServiceWorker.listen();
	} );

	afterEach( () => {
		wikiStub.reset();
		evaluatorStub.reset();
		mockServiceWorker.resetHandlers();
	} );

	after( () => {
		mockServiceWorker.close();
	} );

	// TODO: Eliminate all of these tests and split instead into more specific test suite files

	{
		const setUpStubs = () => {
			wikiStub.setZId( 'Z444', {
				Z1K1: 'Z2',
				Z2K1: { Z1K1: 'Z6', Z6K1: 'Z444' },
				Z2K2: readJSON( testObjectsDir( 'map_function_Z444.json' ) )
			} );
			wikiStub.setZId( 'Z445', {
				Z1K1: 'Z2',
				Z2K1: { Z1K1: 'Z6', Z6K1: 'Z445' },
				Z2K2: readJSON( testObjectsDir( 'map_implementation_Z445.json' ) )
			} );
		};

		const mapOverSparseList = {
			Z1K1: 'Z7',
			Z7K1: 'Z444',
			Z444K1: 'Z813',
			Z444K2: [
				{
					Z1K1: 'Z7',
					Z7K1: 'Z881',
					Z881K1: 'Z6'
				},
				[ 'Z6' ],
				[ 'Z6', 'I am here!' ],
				[ 'Z6', 'I am not :(' ],
				[ 'Z6' ]
			]
		};

		attemptOrchestration(
			/* testName= */ 'map (emptiness of lists)',
			/* functionCall= */ mapOverSparseList,
			/* expectedResultFile= */ testOutputsDir( 'map_emptiness-of-lists_expected.json' ),
			/* expectedErrorFile= */ null,
			setUpStubs
		);
	}

	{
		const setUpStubs = () => {
			evaluatorStub.setZId( 'Z401', () => makeMappedResultEnvelope( { Z1K1: 'Z6', Z6K1: '13' }, null ) );
		};
		attemptOrchestration(
			/* testName= */ 'evaluated string function call',
			/* functionCall= */ readJSON( testInputsDir( 'evaluated-string.json' ) ),
			/* expectedResultFile= */ testOutputsDir( 'evaluated-string-13.json' ),
			/* expectedErrorFile= */ null,
			setUpStubs,
			/* expectedExtraMetadata= */ [],
			/* expectedMissingMetadata= */ [ 'implementationId' ],
			/* implementationSelector= */ null,
			/* doValidate= */ false,
			/* skip= */ false,
			/* requestId= */ 'byebye'
		);
	}

	{
		// Here the functionCall is the same as for 'evaluated function call', except with Z1000
		// replaced by Z1099.  Z1099 doesn't appear in evaluatorStub, so an exception is thrown.
		attemptOrchestration(
			/* testName= */ 'evaluated function call throwing an exception',
			/* functionCall= */ readJSON( testInputsDir( 'evaluated-with-499.json' ) ),
			/* expectedResultFile= */ null,
			/* expectedErrorFile= */ testOutputsDir( 'evaluated-with-499_expected.json' ),
			/* setUpStubs= */ null,
			/* expectedExtraMetadata= */ [],
			// Error gets returned before implementation metadata is recorded
			/* expectedMissingMetadata= */ [ 'implementationId', 'implementationType' ]
		);
	}

	{
		const setUpStubs = () => {
			evaluatorStub.setZId( 'Z420', () => 'naw', 500 );
		};
		attemptOrchestration(
			/* testName= */ 'failed evaluated function call',
			/* functionCall= */ readJSON( testInputsDir( 'evaluated-failed.json' ) ),
			/* expectedResultFile= */ null,
			/* expectedErrorFile= */ testOutputsDir( 'evaluated-failed_expected.json' ),
			setUpStubs,
			/* expectedExtraMetadata= */ [],
			/* expectedMissingMetadata= */ [ 'implementationId', 'implementationType' ]
		);
	}

	{
		const setUpStubs = () => {
			evaluatorStub.setZId( 'Z401', () => readJSON( testObjectsDir( 'Z22-map-result-only.json' ) ) );
		};
		attemptOrchestration(
			/* testName= */ 'evaluated function call, result and empty map',
			/* functionCall= */ readJSON( testInputsDir( 'evaluated-map-result-only.json' ) ),
			/* expectedResultFile= */ testOutputsDir( 'evaluated-map-13.json' ),
			/* expectedErrorFile= */ null,
			setUpStubs,
			/* expectedExtraMetadata= */ [],
			/* expectedMissingMetadata= */ [ 'implementationId' ]
		);
	}

	{
		const setUpStubs = () => {
			evaluatorStub.setZId( 'Z402', () => readJSON( testObjectsDir( 'Z22-map-basic.json' ) ) );
		};
		attemptOrchestration(
			/* testName= */ 'evaluated function call, result and simple map',
			/* functionCall= */ readJSON( testInputsDir( 'evaluated-map-basic.json' ) ),
			/* expectedResultFile= */ testOutputsDir( 'evaluated-map-basic-13.json' ),
			/* expectedErrorFile= */ null,
			setUpStubs,
			/* expectedExtraMetadata= */ [],
			/* expectedMissingMetadata= */ [ 'implementationId' ]
		);
	}

	{
		const setUpStubs = () => {
			const evaluatorResponseZ403 = readJSON( testObjectsDir( 'Z22-map-error.json' ) );
			const errorTermZ403 = makeErrorInNormalForm( error.not_wellformed_value, [ 'Error placeholder' ] );
			setZMapValue( evaluatorResponseZ403.Z22K2, { Z1K1: 'Z6', Z6K1: 'errors' }, errorTermZ403 );
			evaluatorStub.setZId( 'Z403', () => evaluatorResponseZ403 );
		};
		attemptOrchestration(
			/* testName= */ 'evaluated function call, void result',
			/* functionCall= */ readJSON( testInputsDir( 'evaluated-map-error.json' ) ),
			/* expectedResultFile= */ null,
			/* expectedErrorFile= */ testOutputsDir( 'evaluated-map-error_expected.json' ),
			setUpStubs,
			/* expectedExtraMetadata= */ [],
			/* expectedMissingMetadata= */ [ 'implementationId' ]
		);
	}

	{
		const setUpStubs = () => {
			const evaluatorResponseZ430 = normalize( readJSON( testObjectsDir( 'Z22-map-error.json' ) ) ).Z22K1;
			setZMapValue( evaluatorResponseZ430.Z22K2, { Z1K1: 'Z6', Z6K1: 'errors' }, { Z1K1: 'Z6', Z6K1: 13 } );
			evaluatorStub.setZId( 'Z430', () => evaluatorResponseZ430, /* statusCode= */ 200, /* doNormalize= */ false );
		};
		attemptOrchestration(
			/* testName= */ 'evaluated function call returns degenerate ZObject',
			/* functionCall= */ readJSON( testInputsDir( 'evaluated-returns-degenerate.json' ) ),
			/* expectedResultFile= */ null,
			/* expectedErrorFile= */ testOutputsDir( 'evaluated-returns-degenerate_expected.json' ),
			setUpStubs,
			/* expectedExtraMetadata= */ [],
			/* expectedMissingMetadata= */ [ 'implementationId' ]
		);
	}

	{
		const setUpStubs = () => {
			wikiStub.setZId( 'Z429', readJSON( testObjectsDir( 'empty_string_Z429.json' ) ) );
			wikiStub.setZId( 'Z431', readJSON( testObjectsDir( 'one_character_Z431.json' ) ) );
		};

		const falseInput = {
			Z1K1: 'Z7',
			Z7K1: 'Z431',
			Z431K1: 'ab'
		};
		attemptOrchestration(
			/* testName= */ 'one character("ab")',
			/* functionCall= */ falseInput,
			/* expectedResultFile= */ testOutputsDir( 'one_character_ab_expected.json' ),
			/* expectedErrorFile= */ null,
			setUpStubs,
			/* expectedExtraMetadata= */ [],
			/* expectedMissingMetadata= */ [ 'implementationId' ]
		);

		const trueInput = {
			Z1K1: 'Z7',
			Z7K1: 'Z431',
			Z431K1: 'a'
		};
		attemptOrchestration(
			/* testName= */ 'one character("a")',
			/* functionCall= */ trueInput,
			/* expectedResultFile= */ testOutputsDir( 'one_character_a_expected.json' ),
			/* expectedErrorFile= */ null,
			setUpStubs,
			/* expectedExtraMetadata= */ [],
			/* expectedMissingMetadata= */ [ 'implementationId' ]
		);

		const emptyInput = {
			Z1K1: 'Z7',
			Z7K1: 'Z431',
			Z431K1: ''
		};
		attemptOrchestration(
			/* testName= */ 'one character(<empty>)',
			/* functionCall= */ emptyInput,
			/* expectedResultFile= */ testOutputsDir( 'one_character_empty_expected.json' ),
			/* expectedErrorFile= */ null,
			setUpStubs,
			/* expectedExtraMetadata= */ [],
			/* expectedMissingMetadata= */ [ 'implementationId' ]
		);
	}

	{
		// FIXME: This file is also used in typeErrorTests and may fit better there.
		const mapCall = readJSON( testInputsDir( 'invalid_key_type_passed_to_Z883.json' ) );
		const z6Call = { ...mapCall };
		z6Call.Z883K1 = 'Z6';
		attemptOrchestration(
			/* testName= */ 'map key can be Z6/String',
			/* functionCall= */ z6Call,
			/* expectedResultFile= */ testOutputsDir( 'map-key-z6-expected.json' )
		);
		const z39Call = { ...mapCall };
		z39Call.Z883K1 = 'Z39';
		attemptOrchestration(
			/* testName= */ 'map key can be Z39/Key Reference',
			/* functionCall= */ z39Call,
			/* expectedResultFile= */ testOutputsDir( 'map-key-z39-expected.json' )
		);
	}

	{
		const setUpStubs = () => {
			wikiStub.setZId( 'Z444', {
				Z1K1: 'Z2',
				Z2K1: { Z1K1: 'Z6', Z6K1: 'Z444' },
				Z2K2: readJSON( testObjectsDir( 'map-Z444.json' ) )
			} );
		};
		const mapCall = readJSON( testInputsDir( 'map-Z443.json' ) );
		attemptOrchestration(
			/* testName= */ 'map "echo" function to a list of items',
			/* functionCall= */ mapCall,
			/* expectedResultFile= */ testOutputsDir( 'map_echo_expected.json' ),
			/* expectedErrorFile= */ null,
			setUpStubs,
			/* expectedExtraMetadata= */ [],
			/* expectedMissingMetadata= */ [ 'implementationId' ]
		);
	}

	{
		const setUpStubs = () => {
			wikiStub.setZId( 'Z401', {
				Z1K1: 'Z2',
				Z2K1: { Z1K1: 'Z6', Z6K1: 'Z401' },
				Z2K2: readJSON( testObjectsDir( 'Z401-user-defined.json' ) )
			} );
			wikiStub.setZId( 'Z402', {
				Z1K1: 'Z2',
				Z2K1: { Z1K1: 'Z6', Z6K1: 'Z402' },
				Z2K2: readJSON( testObjectsDir( 'Z402-user-defined.json' ) )
			} );
			wikiStub.setZId( 'Z403', {
				Z1K1: 'Z2',
				Z2K1: { Z1K1: 'Z6', Z6K1: 'Z403' },
				Z2K2: readJSON( testObjectsDir( 'Z403-user-defined.json' ) )
			} );
		};
		const userDefinedIf = readJSON( testInputsDir( 'user-defined-type.json' ) );

		const goodUserDefinedIf = { ...userDefinedIf };
		goodUserDefinedIf.Z1802K2 = 'Z403';
		attemptOrchestration(
			/* testName= */ 'good user-defined type',
			/* functionCall= */ goodUserDefinedIf,
			/* expectedResultFile= */ testOutputsDir( 'Z403-user-defined-expected.json' ),
			/* expectedErrorFile= */ null,
			setUpStubs
		);

		const badUserDefinedIf = { ...userDefinedIf };
		badUserDefinedIf.Z1802K2 = 'Z404';
		attemptOrchestration(
			/* testName= */ 'bad user-defined type',
			/* functionCall= */ badUserDefinedIf,
			/* expectedResultFile= */ null,
			/* expectedErrorFile= */ testOutputsDir( 'bad_user_defined_type_expected.json' ),
			setUpStubs,
			/* expectedExtraMetadata= */ [],
			// Error gets returned before implementation is selected
			/* expectedMissingMetadata= */ [ 'implementationId', 'implementationType' ],
			/* implementationSelector= */ null,
			/* doValidate= */ true,
			// TODO (T327412): Re-enable this test once type comparison is stricter.
			/* skip= */ true );
	}

	{
		const setUpStubs = () => {
			const Z405 = readJSON( testObjectsDir( 'Z405.json' ) );
			wikiStub.setZId( 'Z405', {
				Z1K1: 'Z2',
				Z2K1: { Z1K1: 'Z6', Z6K1: 'Z405' },
				Z2K2: Z405
			} );
		};
		const userDefinedEcho = readJSON( testInputsDir( 'user-defined-type-as-reference.json' ) );
		const typeOnly = readJSON( testInputsDir( 'type-only.json' ) );
		userDefinedEcho.Z1903K1 = typeOnly;
		attemptOrchestration(
			/* testName= */ 'reference to user-defined type',
			/* functionCall= */ userDefinedEcho,
			/* expectedResultFile= */ testOutputsDir( 'type-only_expected.json' ),
			/* expectedErrorFile= */ null,
			setUpStubs
		);
	}

	{
		const basicMetadataInsertionCall = {
			Z1K1: 'Z7',
			Z7K1: 'Z820',
			Z820K1: {
				Z1K1: 'Z6',
				Z6K1: 'test'
			},
			Z820K2: {
				Z1K1: 'Z6',
				Z6K1: 'Test value!'
			}
		};

		attemptOrchestration(
			/* testName= */ 'basic meta-data creation call',
			/* functionCall= */ basicMetadataInsertionCall,
			/* expectedResultFile= */ null,
			/* expectedErrorFile= */ testOutputsDir( 'basic-metadata-insertion-expected.json' ),
			/* setUpStubs= */ null,
			/* expectedExtraMetadata= */ [ 'test' ]
		);
	}

	{
		const callToThrow = readJSON( testInputsDir( 'throw.json' ) );
		attemptOrchestration(
			/* testName= */ 'throw throws Z5s',
			/* functionCall= */ callToThrow,
			/* expectedResultFile= */ null,
			/* expectedErrorFile= */ testOutputsDir( 'throw_expected.json' )
		);
	}

	{
		const setUpStubs = () => {
			wikiStub.setZId( 'Z401', {
				Z1K1: 'Z2',
				Z2K1: { Z1K1: 'Z6', Z6K1: 'Z401' },
				Z2K2: 'just an ol string'
			} );
		};
		attemptOrchestration(
			/* testName= */ 'referenced object is not correct type',
			/* functionCall= */ readJSON( testInputsDir( 'bad-reference.json' ) ),
			/* expectedResultFile= */ null,
			/* expectedErrorFile= */ testOutputsDir( 'bad-reference_expected.json' ),
			setUpStubs,
			/* expectedExtraMetadata= */ [],
			// Error gets returned before implementation is selected
			/* expectedMissingMetadata= */ [ 'implementationId', 'implementationType' ]
		);
	}

	{
		const setUpStubs = () => {
			wikiStub.setZId( 'Z401', null, /* error= */ true );
		};
		const call = {
			Z1K1: 'Z7',
			Z7K1: 'Z801',
			Z801K1: 'Z401'
		};
		attemptOrchestration(
			/* testName= */ 'attempt to retrieve object not present on wiki',
			/* functionCall= */ call,
			/* expectedResultFile= */ null,
			/* expectedErrorFile= */ testOutputsDir( 'reference-to-unused-ZID_expected.json' ),
			setUpStubs
		);
	}

	{
		const setUpStubs = () => {
			wikiStub.setZId( 'Z481', readJSON( testObjectsDir( 'Z481.json' ) ) );
			wikiStub.setZId( 'Z484', readJSON( testObjectsDir( 'Z484.json' ) ) );
			wikiStub.setZId( 'Z485', readJSON( testObjectsDir( 'Z485.json' ) ) );
			wikiStub.setZId( 'Z486', readJSON( testObjectsDir( 'Z486.json' ) ) );
		};
		const validateNonempty = {
			Z1K1: 'Z7',
			Z7K1: 'Z484',
			Z484K1: {
				Z1K1: 'Z481',
				Z481K1: {
					Z1K1: 'Z6',
					Z6K1: 'x'
				}
			}
		};
		attemptOrchestration(
			/* testName= */ 'Nonempty string with Z484 validator',
			/* functionCall= */ validateNonempty,
			/* expectedResultFile= */ testOutputsDir( 'Z484_nonempty_string_expected.json' ),
			/* expectedErrorFile= */ null,
			setUpStubs
		);

		const validateEmpty = {
			Z1K1: 'Z7',
			Z7K1: 'Z484',
			Z484K1: {
				Z1K1: 'Z481',
				Z481K1: {
					Z1K1: 'Z6',
					Z6K1: ''
				}
			}
		};
		attemptOrchestration(
			/* testName= */ 'Empty string with Z484 validator',
			/* functionCall= */ validateEmpty,
			/* expectedResultFile= */ null,
			/* expectedErrorFile= */ testOutputsDir( 'Z484_empty_string_expected.json' ),
			setUpStubs
		);
	}

	{
		const setUpStubs = () => {
			wikiStub.setZId( 'Z488', {
				Z1K1: 'Z2',
				Z2K1: { Z1K1: 'Z6', Z6K1: 'Z488' },
				Z2K2: readJSON( testObjectsDir( 'curry-implementation-Z488.json' ) )
			} );
			wikiStub.setZId( 'Z487', {
				Z1K1: 'Z2',
				Z2K1: { Z1K1: 'Z6', Z6K1: 'Z487' },
				Z2K2: readJSON( testObjectsDir( 'curry-Z487.json' ) )
			} );
			wikiStub.setZId( 'Z486', {
				Z1K1: 'Z2',
				Z2K1: { Z1K1: 'Z6', Z6K1: 'Z486' },
				Z2K2: readJSON( testObjectsDir( 'curry-call-Z486.json' ) )
			} );
			wikiStub.setZId( 'Z437', {
				Z1K1: 'Z2',
				Z2K1: { Z1K1: 'Z6', Z6K1: 'Z437' },
				Z2K2: readJSON( testObjectsDir( 'and-Z437.json' ) )
			} );
		};
		const curryCall = {
			Z1K1: 'Z7',
			Z7K1: 'Z486',
			Z486K1: 'Z437',
			Z486K2: makeTrue(),
			Z486K3: makeTrue()
		};
		attemptOrchestration(
			/* testName= */ 'curry',
			/* functionCall= */ curryCall,
			/* expectedResultFile= */ testOutputsDir( 'curry_expected.json' ),
			/* expectedErrorFile= */ null,
			setUpStubs,
			/* expectedExtraMetadata= */ [],
			/* expectedMissingMetadata= */ [ 'implementationId' ]
		);
	}

	{
		// Given:
		// g(f) = if(f(false),f(false),f(false)
		// (calling argument f multiple times to make sure nothing funny is happening with the
		// caching)
		// h(x) = lambda y: x
		const setUpStubs = () => {
			wikiStub.setZId( 'Z401', {
				Z1K1: 'Z2',
				Z2K1: { Z1K1: 'Z6', Z6K1: 'Z401' },
				Z2K2: readJSON( testObjectsDir( 'save-argument-scope-Z401.json' ) )
			} );
			wikiStub.setZId( 'Z402', {
				Z1K1: 'Z2',
				Z2K1: { Z1K1: 'Z6', Z6K1: 'Z402' },
				Z2K2: readJSON( testObjectsDir( 'save-argument-scope-Z402.json' ) )
			} );
		};

		// Expect:
		// g(h(true)) = true
		const call = {
			Z1K1: 'Z7',
			Z7K1: 'Z401',
			Z401K1: {
				Z1K1: 'Z7',
				Z7K1: 'Z402',
				Z402K1: 'Z41'
			}
		};
		attemptOrchestration(
			/* testName= */ 'save argument scope',
			/* functionCall= */ call,
			/* expectedResultFile= */ testOutputsDir( 'save-scope_expected.json' ),
			/* expectedErrorFile= */ null,
			setUpStubs,
			/* expectedExtraMetadata= */ [],
			/* expectedMissingMetadata= */ [ 'implementationId' ]
		);
	}

	{
		const setUpStubs = () => {
			wikiStub.setZId(
				'Z492',
				readJSON( testObjectsDir( 'Z492-wrap.json' ) ) );
			wikiStub.setZId(
				'Z493',
				readJSON( testObjectsDir( 'Z493-wrap-implementation.json' ) )
			);
		};
		const wrapCall = {
			Z1K1: 'Z7',
			Z7K1: 'Z492',
			Z492K1: 'Z6'
		};
		attemptOrchestration(
			/* testName= */ 'wrap type',
			/* functionCall= */ wrapCall,
			/* expectedResultFile= */ testOutputsDir( 'wrap_expected.json' ),
			/* expectedErrorFile= */ null,
			setUpStubs
		);
	}

	{
		const setUpStubs = () => {
			wikiStub.setZId(
				'Z422',
				readJSON( testObjectsDir( 'Z422-natural-number-type.json' ) ) );
			wikiStub.setZId(
				'Z495',
				readJSON( testObjectsDir( 'Z495-natural-number-from-string.json' ) ) );
			wikiStub.setZId(
				'Z496',
				readJSON( testObjectsDir( 'Z496-nnfs-implementation.json' ) ) );
		};
		const naturalNumberCall = {
			Z1K1: 'Z7',
			Z7K1: 'Z495',
			Z495K1: '15'
		};
		attemptOrchestration(
			/* testName= */ 'construct positive integer from string',
			/* functionCall= */ naturalNumberCall,
			/* expectedResultFile= */ testOutputsDir( 'positive-integer-15.json' ),
			/* expectedErrorFile= */ null,
			setUpStubs
		);
	}

	{
		const setUpStubs = () => {
			wikiStub.setZId( 'Z430', {
				Z1K1: 'Z2',
				Z2K1: { Z1K1: 'Z6', Z6K1: 'Z430' },
				Z2K2: readJSON( testObjectsDir( 'bind-binary-Z430.json' ) )
			} );
			wikiStub.setZId( 'Z431', {
				Z1K1: 'Z2',
				Z2K1: { Z1K1: 'Z6', Z6K1: 'Z431' },
				Z2K2: readJSON( testObjectsDir( 'bind-binary-implementation-Z431.json' ) )
			} );
			wikiStub.setZId( 'Z437', {
				Z1K1: 'Z2',
				Z2K1: { Z1K1: 'Z6', Z6K1: 'Z437' },
				Z2K2: readJSON( testObjectsDir( 'and-Z437.json' ) )
			} );
		};
		const binaryBindCall = {
			Z1K1: 'Z7',
			Z7K1: 'Z430',
			Z430K1: 'Z437',
			Z430K2: makeTrue()
		};
		attemptOrchestration(
			/* testName= */ 'bind binary function',
			/* functionCall= */ binaryBindCall,
			/* expectedResultFile= */ testOutputsDir( 'bind-binary-expected.json' ),
			/* expectedErrorFile= */ null,
			setUpStubs
		);
	}

	{
		const noScrubs = readJSON( testInputsDir( 'no-implementations.json' ) );
		attemptOrchestration(
			/* testName= */ 'no implementations',
			/* functionCall= */ noScrubs,
			/* expectedResultFile= */ null,
			/* expectedErrorFile= */ testOutputsDir( 'no-implementations-expected.json' ),
			/* setUpStubs= */ null,
			/* expectedExtraMetadata= */ [],
			// Error gets returned before implementation is selected
			/* expectedMissingMetadata= */ [ 'implementationId', 'implementationType' ]
		);
	}

	{
		const setUpStubs = () => {
			evaluatorStub.setZId(
				'Z402',
				( zobject ) => makeMappedResultEnvelope(
					( parseInt( zobject.functionArguments.Z402K1.object.Z6K1 ) + 1 ).toString() ) );
			wikiStub.setZId( 'Z400', readJSON( testObjectsDir( 'scott-numeral-zero-Z400.json' ) ) );
			wikiStub.setZId( 'Z401', readJSON( testObjectsDir( 'scott-numeral-succ-Z401.json' ) ) );
			wikiStub.setZId( 'Z402', readJSON( testObjectsDir( 'string-numeral-increment-Z402.json' ) ) );
			wikiStub.setZId( 'Z403', readJSON( testObjectsDir( 'scott-numeral-convert-Z403.json' ) ) );
			wikiStub.setZId( 'Z404', readJSON( testObjectsDir( 'scott-numeral-ack-Z404.json' ) ) );
		};

		{
			const call = {
				Z1K1: 'Z7',
				Z7K1: 'Z402',
				Z402K1: '41'
			};
			attemptOrchestration(
				/* testName= */ 'Increment string numeral',
				/* functionCall= */ call,
				/* expectedResultFile= */ testOutputsDir( 'expected-42.json' ),
				/* expectedErrorFile= */ null,
				setUpStubs,
				/* expectedExtraMetadata= */ [],
				/* expectedMissingMetadata= */ [ 'implementationId' ]
			);
		}

		{
			const call = {
				Z1K1: 'Z7',
				Z7K1: 'Z403',
				Z403K1: 'Z400'
			};
			attemptOrchestration(
				/* testName= */ 'Scott numeral zero',
				/* functionCall= */ call,
				/* expectedResultFile= */ testOutputsDir( 'expected-0.json' ),
				/* expectedErrorFile= */ null,
				setUpStubs,
				/* expectedExtraMetadata= */ [],
				/* expectedMissingMetadata= */ [ 'implementationId' ]
			);
		}

		{
			const call = {
				Z1K1: 'Z7',
				Z7K1: 'Z403',
				Z403K1: {
					Z1K1: 'Z7',
					Z7K1: 'Z401',
					Z401K1: 'Z400'
				}
			};
			attemptOrchestration(
				/* testName= */ 'Scott numeral one',
				/* functionCall= */ call,
				/* expectedResultFile= */ testOutputsDir( 'expected-1.json' ),
				/* expectedErrorFile= */ null,
				setUpStubs,
				/* expectedExtraMetadata= */ [],
				/* expectedMissingMetadata= */ [ 'implementationId' ]
			);
		}

		{
			const call = {
				Z1K1: 'Z7',
				Z7K1: 'Z403',
				Z403K1: {
					Z1K1: 'Z7',
					Z7K1: 'Z401',
					Z401K1: {
						Z1K1: 'Z7',
						Z7K1: 'Z401',
						Z401K1: 'Z400'
					}
				}
			};
			attemptOrchestration(
				/* testName= */ 'Scott numeral two',
				/* functionCall= */ call,
				/* expectedResultFile= */ testOutputsDir( 'expected-2.json' ),
				/* expectedErrorFile= */ null,
				setUpStubs,
				/* expectedExtraMetadata= */ [],
				/* expectedMissingMetadata= */ [ 'implementationId' ]
			);
		}

		{
			const call = {
				Z1K1: 'Z7',
				Z7K1: 'Z403',
				Z403K1: {
					Z1K1: 'Z7',
					Z7K1: 'Z404',
					Z404K1: {
						Z1K1: 'Z7',
						Z7K1: 'Z401',
						Z401K1: 'Z400'
					},
					Z404K2: {
						Z1K1: 'Z7',
						Z7K1: 'Z401',
						Z401K1: 'Z400'
					}
				}
			};
			attemptOrchestration(
				/* testName= */ 'Scott numeral Ackermann(1, 1)',
				/* functionCall= */ call,
				/* expectedResultFile= */ testOutputsDir( 'expected-3.json' ),
				/* expectedErrorFile= */ null,
				setUpStubs,
				/* expectedExtraMetadata= */ [],
				/* expectedMissingMetadata= */ [ 'implementationId' ]
			);
		}
	}

	{
		const call = {
			Z1K1: 'Z7',
			Z7K1: 'Z803',
			Z803K1: {
				Z1K1: 'Z39',
				Z39K1: {
					Z1K1: 'Z6',
					Z6K1: 'Z4K3'
				}
			},
			Z803K2: 'Z40'
		};
		attemptOrchestration(
			/* testName= */ 'Built-ins are resolved when they are an argument to a function.',
			/* functionCall= */ call,
			/* expectedResultFile= */ testOutputsDir( 'expected-Z140.json' )
		);
	}

	{
		const call = {
			Z1K1: 'Z7',
			Z7K1: 'Z803',
			Z803K1: {
				Z1K1: 'Z39',
				Z39K1: {
					Z1K1: 'Z6',
					Z6K1: 'Z4K2'
				}
			},
			Z803K2: 'Z40'
		};
		attemptOrchestration(
			/* testName= */ 'Built-ins are still resolved when they are an argument to a function.',
			/* functionCall= */ call,
			/* expectedResultFile= */ testOutputsDir( 'expected-Z40-Z4K2.json' )
		);
	}

	{
		const setUpStubs = () => {
			evaluatorStub.setZId(
				'Z443',
				( zobject ) => makeMappedResultEnvelope(
					JSON.stringify( zobject.functionArguments.Z443K1.object ) ) );
			wikiStub.setZId( 'Z444', readJSON( testObjectsDir( 'Z444.json' ) ) );
			wikiStub.setZId( 'Z443', readJSON( testObjectsDir( 'Z443.json' ) ) );
			wikiStub.setZId( 'Z439', readJSON( testObjectsDir( 'Z439.json' ) ) );
			wikiStub.setZId( 'Z438', readJSON( testObjectsDir( 'Z438.json' ) ) );
			wikiStub.setZId( 'Z415', readJSON( testObjectsDir( 'Z415.json' ) ) );
		};
		const call = {
			Z1K1: 'Z7',
			Z7K1: 'Z443',
			Z443K1: {
				Z1K1: 'Z439',
				Z439K1: '111',
				Z439K2: [
					'Z438',
					{
						Z1K1: 'Z438',
						Z438K1: {
							Z1K1: 'Z415',
							Z415K1: '222'
						},
						Z438K2: '333',
						Z438K3: '444'
					}
				]
			}
		};
		attemptOrchestration(
			/* testName= */ 'Test with many on-wiki custom types',
			/* functionCall= */ call,
			/* expectedResultFile= */ testOutputsDir( 'expected-on-wiki-types.json' ),
			/* expectedErrorFile= */ null,
			setUpStubs
		);
	}

	{
		const setUpStubs = () => {
			wikiStub.setZId( 'Z405', readJSON( testObjectsDir( 'Z405-arg-value.json' ) ) );
		};
		const call = {
			Z1K1: 'Z7',
			Z7K1: 'Z801',
			Z801K1: [
				'Z6',
				'Z405',
				'less precious string'
			]
		};
		attemptOrchestration(
			/* testName= */ 'Test that non-top-level argument values are resolved',
			/* functionCall= */ call,
			/* expectedResultFile= */ testOutputsDir( 'expected-non-top-level-reference.json' ),
			/* expectedErrorFile= */ null,
			setUpStubs
		);
	}

	{
		const call = {
			Z1K1: 'Z7',
			Z7K1: {
				Z1K1: 'Z8',
				Z8K1: [
					'Z17',
					{
						Z1K1: 'Z17',
						Z17K1: {
							Z1K1: 'Z9',
							Z9K1: 'Z6'
						},
						Z17K2: 'Z406K1',
						Z17K3: {
							Z1K1: 'Z12',
							Z12K1: [ 'Z11' ]
						}
					}
				],
				Z8K2: {
					Z1K1: 'Z7',
					Z7K1: 'Z881',
					Z881K1: 'Z6'
				},
				Z8K3: [ 'Z20' ],
				Z8K4: [
					'Z14',
					{
						Z1K1: 'Z14',
						Z14K1: 'Z406',
						Z14K2: {
							Z1K1: 'Z7',
							Z7K1: 'Z801',
							Z801K1: [
								'Z6',
								{
									Z1K1: 'Z18',
									Z18K1: 'Z406K1'
								},
								'less precious string'
							]
						}
					}
				],
				Z8K5: 'Z406'
			},
			Z406K1: 'a darling string'
		};
		attemptOrchestration(
			/* testName= */ 'Test that non-top-level argument references are resolved',
			/* functionCall= */ call,
			/* expectedResultFile= */ testOutputsDir( 'expected-non-top-level-argref.json' ),
			/* expectedErrorFile= */ null,
			/* setUpStubs= */ null,
			/* expectedExtraMetadata= */ [],
			/* expectedMissingMetadata= */ [ 'implementationId' ]
		);
	}

	{
		const call = {
			Z1K1: 'Z7',
			Z7K1: {
				Z1K1: 'Z8',
				Z8K1: [
					'Z17'
				],
				Z8K2: {
					Z1K1: 'Z7',
					Z7K1: 'Z881',
					Z881K1: 'Z6'
				},
				Z8K3: [ 'Z20' ],
				Z8K4: [
					'Z14',
					{
						Z1K1: 'Z14',
						Z14K1: 'Z406',
						Z14K2: {
							Z1K1: 'Z7',
							Z7K1: 'Z801',
							Z801K1: [
								'Z6',
								{
									Z1K1: 'Z7',
									Z7K1: {
										Z1K1: 'Z8',
										Z8K1: [
											'Z17'
										],
										Z8K2: 'Z6',
										Z8K3: [ 'Z20' ],
										Z8K4: [
											'Z14',
											{
												Z1K1: 'Z14',
												Z14K1: 'Z407',
												Z14K2: 'a real lousy string, just a jerk'
											}
										],
										Z8K5: 'Z407'
									}
								},
								'less precious string'
							]
						}
					}
				],
				Z8K5: 'Z406'
			}
		};
		attemptOrchestration(
			/* testName= */ 'Test that non-top-level function calls are resolved',
			/* functionCall= */ call,
			/* expectedResultFile= */ testOutputsDir( 'expected-non-top-level-call.json' ),
			/* expectedErrorFile= */ null,
			/* setUpStubs= */ null,
			/* expectedExtraMetadata= */ [],
			/* expectedMissingMetadata= */ [ 'implementationId' ]
		);
	}

	{
		const setUpStubs = () => {
			wikiStub.setZId( 'Z415', {
				Z1K1: 'Z2',
				Z2K1: { Z1K1: 'Z6', Z6K1: 'Z400' },
				Z2K2: readJSON( testObjectsDir( 'Z415-reified-integer.json' ) )
			} );
		};
		const call = {
			Z1K1: 'Z7',
			Z7K1: 'Z805',
			Z805K1: {
				Z1K1: 'Z415',
				Z415K1: '1'
			}
		};
		attemptOrchestration(
			/* testName= */ 'Test that reify avoids infinite expansions',
			/* functionCall= */ call,
			/* expectedResultFile= */ testOutputsDir( 'expected-reified-integer.json' ),
			/* expectedErrorFile= */ null,
			setUpStubs
		);
	}

	{
		const call = {
			Z1K1: 'Z7',
			Z7K1: 'Z828',
			Z828K1: {
				Z1K1: 'Z99',
				Z99K1: {
					Z1K1: 'Z9',
					Z9K1: 'Z811'
				}
			}
		};
		attemptOrchestration(
			/* testName= */ 'Test that Z828 retrieves a Z2 for a built-in',
			/* functionCall= */ call,
			/* expectedResultFile= */ schemataDefinitionsDir( 'Z811.json' )
		);
	}

	{
		const setUpStubs = () => {
			wikiStub.setZId( 'Z415', readJSON( testObjectsDir( 'Z415.json' ) ) );
		};
		const call = {
			Z1K1: 'Z7',
			Z7K1: 'Z828',
			Z828K1: {
				Z1K1: 'Z99',
				Z99K1: {
					Z1K1: 'Z9',
					Z9K1: 'Z415'
				}
			}
		};
		attemptOrchestration(
			/* testName= */ 'Test that Z828 retrieves a Z2 from the mock wiki',
			/* functionCall= */ call,
			/* expectedResultFile= */ testOutputsDir( 'Z415.json' ),
			/* expectedErrorFile= */ null,
			/* setUpStubs= */ setUpStubs
		);
	}

	{
		// An exception is generated in wikiStub because it doesn't know about Z499
		const call = {
			Z1K1: 'Z7',
			Z7K1: 'Z828',
			Z828K1: {
				Z1K1: 'Z99',
				Z99K1: {
					Z1K1: 'Z9',
					Z9K1: 'Z499'
				}
			}
		};
		attemptOrchestration(
			/* testName= */ 'Test that Z828 catches an error thrown by dereferenceZObjects()',
			/* functionCall= */ call,
			/* expectedResultFile= */ null,
			/* expectedErrorFile= */ testOutputsDir( 'error_thrown_by_dereference_expected.json' )
		);
	}

	{
		// wikidataStub gets Q41607 from our test directory
		const call = {
			Z1K1: 'Z7',
			Z7K1: 'Z6821',
			Z6821K1: { Z1K1: 'Z6091', Z6091K1: 'Q41607' }
		};
		attemptOrchestration(
			/* testName= */ 'Test that Z6821 retrieves & instantiates a Wikidata item from mock wikidata',
			/* functionCall= */ call,
			/* expectedResultFile= */ testOutputsDir( 'Q41607_expected.json' ),
			/* expectedErrorFile= */ null,
			/* setUpStubs= */ null
		);
	}

	{
		// An exception is generated in wikidataStub because it doesn't know about Q9999
		const setUpStubs = () => {
			wikidataStub.setEntityId( 'Q9999', null, true );
		};
		const call = {
			Z1K1: 'Z7',
			Z7K1: 'Z6821',
			Z6821K1: { Z1K1: 'Z6091', Z6091K1: 'Q9999' }
		};
		attemptOrchestration(
			/* testName= */ 'Test that Z6821 catches an error thrown by dereferenceWikidataItem()',
			/* functionCall= */ call,
			/* expectedResultFile= */ null,
			/* expectedErrorFile= */ testOutputsDir( 'missing_item_error_expected.json' ),
			/* setUpStubs= */ setUpStubs
		);
	}

	{
		// wikidataStub gets P5137 from our test directory
		const call = {
			Z1K1: 'Z7',
			Z7K1: 'Z6822',
			Z6822K1: { Z1K1: 'Z6092', Z6092K1: 'P5137' }
		};
		attemptOrchestration(
			/* testName= */ 'Test that Z6822 retrieves & instantiates a Wikidata property from mock wikidata',
			/* functionCall= */ call,
			/* expectedResultFile= */ testOutputsDir( 'P5137_expected.json' ),
			/* expectedErrorFile= */ null,
			/* setUpStubs= */ null
		);
	}

	{
		// An exception is generated in wikidataStub because it doesn't know about P9999
		const setUpStubs = () => {
			wikidataStub.setEntityId( 'P9999', null, true );
		};
		const call = {
			Z1K1: 'Z7',
			Z7K1: 'Z6822',
			Z6822K1: { Z1K1: 'Z6092', Z6092K1: 'P9999' }
		};
		attemptOrchestration(
			/* testName= */ 'Test that Z6822 catches an error thrown by dereferenceWikidataProperty()',
			/* functionCall= */ call,
			/* expectedResultFile= */ null,
			/* expectedErrorFile= */ testOutputsDir( 'missing_property_error_expected.json' ),
			/* setUpStubs= */ setUpStubs
		);
	}

	{
		const setUpStubs = () => {
			wikidataStub.setEntityId( 'L3435', readJSON( testWikidataDir( 'L3435.json' ) ) );
		};
		const call = {
			Z1K1: 'Z7',
			Z7K1: 'Z6825',
			Z6825K1: { Z1K1: 'Z6095', Z6095K1: 'L3435' }
		};
		attemptOrchestration(
			/* testName= */ 'Test that Z6825 retrieves & instantiates a Wikidata lexeme from mock wikidata',
			/* functionCall= */ call,
			/* expectedResultFile= */ testOutputsDir( 'L3435_expected.json' ),
			/* expectedErrorFile= */ null,
			/* setUpStubs= */ setUpStubs
		);
	}

	{
		const setUpStubs = () => {
			wikidataStub.setEntityId( 'L3828', readJSON( testWikidataDir( 'L3828.json' ) ) );
		};
		const call = {
			Z1K1: 'Z7',
			Z7K1: 'Z6825',
			Z6825K1: { Z1K1: 'Z6095', Z6095K1: 'L3828' }
		};
		attemptOrchestration(
			/* testName= */ 'Test that Z6825 retrieves & instantiates a larger Wikidata lexeme from mock wikidata',
			/* functionCall= */ call,
			/* expectedResultFile= */ testOutputsDir( 'L3828_expected.json' ),
			/* expectedErrorFile= */ null,
			/* setUpStubs= */ setUpStubs
		);
	}

	{
		// An exception is generated in wikidataStub because it doesn't know about L9999
		const setUpStubs = () => {
			wikidataStub.setEntityId( 'L9999', null, true );
		};
		const call = {
			Z1K1: 'Z7',
			Z7K1: 'Z6825',
			Z6825K1: { Z1K1: 'Z6095', Z6095K1: 'L9999' }

		};
		attemptOrchestration(
			/* testName= */ 'Test that Z6825 catches an error thrown by dereferenceWikidataLexeme()',
			/* functionCall= */ call,
			/* expectedResultFile= */ null,
			/* expectedErrorFile= */ testOutputsDir( 'missing_lexeme_error_expected.json' ),
			/* setUpStubs= */ setUpStubs
		);
	}

	{
		const setUpStubs = () => {
			wikidataStub.setEntityId( 'L3435-F1', readJSON( testWikidataDir( 'L3435-F1.json' ) ) );
		};
		const call = {
			Z1K1: 'Z7',
			Z7K1: 'Z6824',
			Z6824K1: { Z1K1: 'Z6094', Z6094K1: 'L3435-F1' }
		};
		attemptOrchestration(
			/* testName= */ 'Test that Z6824 retrieves & instantiates a Wikidata lexeme form from mock wikidata',
			/* functionCall= */ call,
			/* expectedResultFile= */ testOutputsDir( 'L3435-F1_expected.json' ),
			/* expectedErrorFile= */ null,
			/* setUpStubs= */ setUpStubs
		);
	}

	{
		// An exception is generated in wikidataStub because it doesn't know about L9999-F1
		const setUpStubs = () => {
			wikidataStub.setEntityId( 'L9999-F1', null, true );
		};
		const call = {
			Z1K1: 'Z7',
			Z7K1: 'Z6824',
			Z6824K1: { Z1K1: 'Z6094', Z6094K1: 'L9999-F1' }
		};
		attemptOrchestration(
			/* testName= */ 'Test that Z6824 catches an error thrown by dereferenceWikidataLexemeForm()',
			/* functionCall= */ call,
			/* expectedResultFile= */ null,
			/* expectedErrorFile= */ testOutputsDir( 'missing_lexemeform_error_expected.json' ),
			/* setUpStubs= */ setUpStubs
		);
	}

	{
		const setUpStubs = () => {
			wikidataStub.setEntityId( 'L3435-S1', readJSON( testWikidataDir( 'L3435-S1.json' ) ) );
		};
		const call = {
			Z1K1: 'Z7',
			Z7K1: 'Z6826',
			Z6826K1: { Z1K1: 'Z6096', Z6096K1: 'L3435-S1' }
		};
		attemptOrchestration(
			/* testName= */ 'Test that Z6826 retrieves & instantiates a Wikidata lexeme sense from mock wikidata',
			/* functionCall= */ call,
			/* expectedResultFile= */ testOutputsDir( 'L3435-S1_expected.json' ),
			/* expectedErrorFile= */ null,
			/* setUpStubs= */ setUpStubs
		);
	}

	{
		// An exception is generated in wikidataStub because it doesn't know about L9999-S1
		const setUpStubs = () => {
			wikidataStub.setEntityId( 'L9999-S1', null, true );
		};
		const call = {
			Z1K1: 'Z7',
			Z7K1: 'Z6826',
			Z6826K1: { Z1K1: 'Z6096', Z6096K1: 'L9999-S1' }
		};
		attemptOrchestration(
			/* testName= */ 'Test that Z6826 catches an error thrown by dereferenceWikidataLexemeSense()',
			/* functionCall= */ call,
			/* expectedResultFile= */ null,
			/* expectedErrorFile= */ testOutputsDir( 'missing_lexemesense_error_expected.json' ),
			/* setUpStubs= */ setUpStubs
		);
	}

	{
		// Pretend there are no lexemes related to Q41607 by P5137
		const queryResponse = { query: { search: [] } };
		const setUpStubs = () => {
			wikidataQueryStub.setQueryResponse( 'P5137', 'Q41607', queryResponse );
		};
		const call = {
			Z1K1: 'Z7',
			Z7K1: 'Z6830',
			Z6830K1: { Z1K1: 'Z6091', Z6091K1: 'Q41607' },
			Z6830K2: { Z1K1: 'Z6092', Z6092K1: 'P5137' },
			Z6830K3: { Z1K1: 'Z60', Z60K1: 'en', Z60K2: { Z1K1: 'Z12', Z12K1: [ 'Z11' ] } }
		};
		attemptOrchestration(
			/* testName= */ 'Z6830 finds no lexemes related to a given item',
			/* functionCall= */ call,
			/* expectedResultFile= */ testOutputsDir( 'Z6830_no_lexemes_expected.json' ),
			/* expectedErrorFile= */ null,
			/* setUpStubs= */ setUpStubs
		);
	}

	{
		// Pretend these 3 lexemes (1 in English) are related to Q41607 by P5137
		const queryResponse =
			{ query: { search: [ { ns: 146, title: 'Lexeme:L3435' }, { ns: 146, title: 'Lexeme:L3828' },
				{ ns: 146, title: 'Lexeme:L31815' } ] } };
		const setUpStubs = () => {
			wikidataQueryStub.setQueryResponse( 'P5137', 'Q41607', queryResponse );
		};
		const call = {
			Z1K1: 'Z7',
			Z7K1: 'Z6830',
			Z6830K1: { Z1K1: 'Z6091', Z6091K1: 'Q41607' },
			Z6830K2: { Z1K1: 'Z6092', Z6092K1: 'P5137' },
			Z6830K3: { Z1K1: 'Z60', Z60K1: 'en', Z60K2: { Z1K1: 'Z12', Z12K1: [ 'Z11' ] } }
		};
		attemptOrchestration(
			/* testName= */ 'Z6830 finds a lexeme related to a given item',
			/* functionCall= */ call,
			/* expectedResultFile= */ testOutputsDir( 'Z6830_one_lexeme_expected.json' ),
			/* expectedErrorFile= */ null,
			/* setUpStubs= */ setUpStubs
		);
	}

	{
		// Set haswbstatement to return a 404
		const queryResponse = { query: { search: [] } };
		const setUpStubs = () => {
			wikidataQueryStub.setQueryResponse( 'P5137', 'Q41607', queryResponse, true );
		};
		const call = {
			Z1K1: 'Z7',
			Z7K1: 'Z6830',
			Z6830K1: { Z1K1: 'Z6091', Z6091K1: 'Q41607' },
			Z6830K2: { Z1K1: 'Z6092', Z6092K1: 'P5137' },
			Z6830K3: { Z1K1: 'Z60', Z60K1: 'en', Z60K2: { Z1K1: 'Z12', Z12K1: [ 'Z11' ] } }
		};
		attemptOrchestration(
			/* testName= */ 'Z6830 returns Z500 when there is a 404 from haswbstatement',
			/* functionCall= */ call,
			/* expectedResultFile= */ null,
			/* expectedErrorFile= */ testOutputsDir( 'Z6830_Z500_expected.json' ),
			/* setUpStubs= */ setUpStubs
		);
	}

	attemptOrchestration(
		/* testName= */ 'Function call to Z6801/Same wikidata item, with equal ZObjects',
		/* functionCall= */ readJSON( testInputsDir( 'Z6801_equal.json' ) ),
		/* expectedResultFile= */ testOutputsDir( 'expected_same.json' )
	);

	attemptOrchestration(
		/* testName= */ 'Function call to Z6801/Same wikidata item, with different ZObjects',
		/* functionCall= */ readJSON( testInputsDir( 'Z6801_different.json' ) ),
		/* expectedResultFile= */ testOutputsDir( 'expected_different.json' )
	);

	attemptOrchestration(
		/* testName= */ 'Function call to Z6801/Same wikidata item, where first argument is of the wrong type',
		/* functionCall= */ readJSON( testInputsDir( 'Z6801_first_arg_wrong_type.json' ) ),
		/* expectedResultFile= */ null,
		/* expectedErrorFile= */ testOutputsDir( 'Z6801_first_arg_wrong_type_expected.json' )
	);

	attemptOrchestration(
		/* testName= */ 'Function call to Z6801/Same wikidata item, where second argument is of the wrong type',
		/* functionCall= */ readJSON( testInputsDir( 'Z6801_second_arg_wrong_type.json' ) ),
		/* expectedResultFile= */ null,
		/* expectedErrorFile= */ testOutputsDir( 'Z6801_second_arg_wrong_type_expected.json' )
	);

	attemptOrchestration(
		/* testName= */ 'Function call to Z6801/Same wikidata item, where one argument is a reference and one is a literal',
		/* functionCall= */ readJSON( testInputsDir( 'Z6801_ref_vs_literal.json' ) ),
		/* expectedResultFile= */ null,
		/* expectedErrorFile= */ testOutputsDir( 'Z6801_ref_vs_literal_expected.json' )
	);

	attemptOrchestration(
		/* testName= */ 'Function call to Z6802/Same wikidata property, with equal ZObjects',
		/* functionCall= */ readJSON( testInputsDir( 'Z6802_equal.json' ) ),
		/* expectedResultFile= */ testOutputsDir( 'expected_same.json' )
	);

	attemptOrchestration(
		/* testName= */ 'Function call to Z6802/Same wikidata property, with different ZObjects',
		/* functionCall= */ readJSON( testInputsDir( 'Z6802_different.json' ) ),
		/* expectedResultFile= */ testOutputsDir( 'expected_different.json' )
	);

	attemptOrchestration(
		/* testName= */ 'Function call to Z6802/Same wikidata property, where first argument is of the wrong type',
		/* functionCall= */ readJSON( testInputsDir( 'Z6802_first_arg_wrong_type.json' ) ),
		/* expectedResultFile= */ null,
		/* expectedErrorFile= */ testOutputsDir( 'Z6802_first_arg_wrong_type_expected.json' )
	);

	attemptOrchestration(
		/* testName= */ 'Function call to Z6802/Same wikidata property, where second argument is of the wrong type',
		/* functionCall= */ readJSON( testInputsDir( 'Z6802_second_arg_wrong_type.json' ) ),
		/* expectedResultFile= */ null,
		/* expectedErrorFile= */ testOutputsDir( 'Z6802_second_arg_wrong_type_expected.json' )
	);

	attemptOrchestration(
		/* testName= */ 'Function call to Z6802/Same wikidata property, where one argument is a reference and one is a literal',
		/* functionCall= */ readJSON( testInputsDir( 'Z6802_ref_vs_literal.json' ) ),
		/* expectedResultFile= */ null,
		/* expectedErrorFile= */ testOutputsDir( 'Z6802_ref_vs_literal_expected.json' )
	);

	attemptOrchestration(
		/* testName= */ 'Function call to Z6803/Same wikidata statement, equal ZObjects case 1, item value',
		/* functionCall= */ readJSON( testInputsDir( 'Z6803_equal_1.json' ) ),
		/* expectedResultFile= */ testOutputsDir( 'expected_same.json' )
	);

	attemptOrchestration(
		/* testName= */ 'Function call to Z6803/Same wikidata statement, equal ZObjects case 1, keys jumbled',
		/* functionCall= */ readJSON( testInputsDir( 'Z6803_equal_1_keys_jumbled.json' ) ),
		/* expectedResultFile= */ testOutputsDir( 'expected_same.json' )
	);

	attemptOrchestration(
		/* testName= */ 'Function call to Z6803/Same wikidata statement, equal ZObjects case 2, string value',
		/* functionCall= */ readJSON( testInputsDir( 'Z6803_equal_2.json' ) ),
		/* expectedResultFile= */ testOutputsDir( 'expected_same.json' )
	);

	attemptOrchestration(
		/* testName= */ 'Function call to Z6803/Same wikidata statement, equal ZObjects case 3, Z11 value',
		/* functionCall= */ readJSON( testInputsDir( 'Z6803_equal_3.json' ) ),
		/* expectedResultFile= */ testOutputsDir( 'expected_same.json' )
	);

	attemptOrchestration(
		/* testName= */ 'Function call to Z6803/Same wikidata statement, different subjects',
		/* functionCall= */ readJSON( testInputsDir( 'Z6803_different_subjects.json' ) ),
		/* expectedResultFile= */ testOutputsDir( 'expected_different.json' )
	);

	attemptOrchestration(
		/* testName= */ 'Function call to Z6803/Same wikidata statement, different predicates',
		/* functionCall= */ readJSON( testInputsDir( 'Z6803_different_predicates.json' ) ),
		/* expectedResultFile= */ testOutputsDir( 'expected_different.json' )
	);

	attemptOrchestration(
		/* testName= */ 'Function call to Z6803/Same wikidata statement, different values',
		/* functionCall= */ readJSON( testInputsDir( 'Z6803_different_values.json' ) ),
		/* expectedResultFile= */ testOutputsDir( 'expected_different.json' )
	);

	attemptOrchestration(
		/* testName= */ 'Function call to Z6803/Same wikidata statement, different ranks',
		/* functionCall= */ readJSON( testInputsDir( 'Z6803_different_ranks.json' ) ),
		/* expectedResultFile= */ testOutputsDir( 'expected_different.json' )
	);

	attemptOrchestration(
		/* testName= */ 'Function call to Z6803/Same wikidata statement, where first argument is of the wrong type',
		/* functionCall= */ readJSON( testInputsDir( 'Z6803_first_arg_wrong_type.json' ) ),
		/* expectedResultFile= */ null,
		/* expectedErrorFile= */ testOutputsDir( 'Z6803_first_arg_wrong_type_expected.json' )
	);

	attemptOrchestration(
		/* testName= */ 'Function call to Z6803/Same wikidata statement, where second argument is of the wrong type',
		/* functionCall= */ readJSON( testInputsDir( 'Z6803_second_arg_wrong_type.json' ) ),
		/* expectedResultFile= */ null,
		/* expectedErrorFile= */ testOutputsDir( 'Z6803_second_arg_wrong_type_expected.json' )
	);

	attemptOrchestration(
		/* testName= */ 'Function call to Z6804/Same wikidata lexeme form, with equal ZObjects',
		/* functionCall= */ readJSON( testInputsDir( 'Z6804_equal.json' ) ),
		/* expectedResultFile= */ testOutputsDir( 'expected_same.json' )
	);

	attemptOrchestration(
		/* testName= */ 'Function call to Z6804/Same wikidata lexeme form, with different ZObjects',
		/* functionCall= */ readJSON( testInputsDir( 'Z6804_different.json' ) ),
		/* expectedResultFile= */ testOutputsDir( 'expected_different.json' )
	);

	attemptOrchestration(
		/* testName= */ 'Function call to Z6804/Same wikidata lexeme form, where first argument is of the wrong type',
		/* functionCall= */ readJSON( testInputsDir( 'Z6804_first_arg_wrong_type.json' ) ),
		/* expectedResultFile= */ null,
		/* expectedErrorFile= */ testOutputsDir( 'Z6804_first_arg_wrong_type_expected.json' )
	);

	attemptOrchestration(
		/* testName= */ 'Function call to Z6804/Same wikidata lexeme form, where second argument is of the wrong type',
		/* functionCall= */ readJSON( testInputsDir( 'Z6804_second_arg_wrong_type.json' ) ),
		/* expectedResultFile= */ null,
		/* expectedErrorFile= */ testOutputsDir( 'Z6804_second_arg_wrong_type_expected.json' )
	);

	attemptOrchestration(
		/* testName= */ 'Function call to Z6804/Same wikidata lexeme form, where one argument is a reference and one is a literal',
		/* functionCall= */ readJSON( testInputsDir( 'Z6804_ref_vs_literal.json' ) ),
		/* expectedResultFile= */ null,
		/* expectedErrorFile= */ testOutputsDir( 'Z6804_ref_vs_literal_expected.json' )
	);

	attemptOrchestration(
		/* testName= */ 'Function call to Z6805/Same wikidata lexeme, with equal ZObjects',
		/* functionCall= */ readJSON( testInputsDir( 'Z6805_equal.json' ) ),
		/* expectedResultFile= */ testOutputsDir( 'expected_same.json' )
	);

	attemptOrchestration(
		/* testName= */ 'Function call to Z6805/Same wikidata lexeme, with different ZObjects',
		/* functionCall= */ readJSON( testInputsDir( 'Z6805_different.json' ) ),
		/* expectedResultFile= */ testOutputsDir( 'expected_different.json' )
	);

	attemptOrchestration(
		/* testName= */ 'Function call to Z6805/Same wikidata lexeme, where first argument is of the wrong type',
		/* functionCall= */ readJSON( testInputsDir( 'Z6805_first_arg_wrong_type.json' ) ),
		/* expectedResultFile= */ null,
		/* expectedErrorFile= */ testOutputsDir( 'Z6805_first_arg_wrong_type_expected.json' )
	);

	attemptOrchestration(
		/* testName= */ 'Function call to Z6805/Same wikidata lexeme, where second argument is of the wrong type',
		/* functionCall= */ readJSON( testInputsDir( 'Z6805_second_arg_wrong_type.json' ) ),
		/* expectedResultFile= */ null,
		/* expectedErrorFile= */ testOutputsDir( 'Z6805_second_arg_wrong_type_expected.json' )
	);

	attemptOrchestration(
		/* testName= */ 'Function call to Z6805/Same wikidata lexeme, where one argument is a reference and one is a literal',
		/* functionCall= */ readJSON( testInputsDir( 'Z6805_ref_vs_literal.json' ) ),
		/* expectedResultFile= */ null,
		/* expectedErrorFile= */ testOutputsDir( 'Z6805_ref_vs_literal_expected.json' )
	);

	attemptOrchestration(
		/* testName= */ 'Function call to Z6806/Same wikidata lexeme sense, with equal ZObjects',
		/* functionCall= */ readJSON( testInputsDir( 'Z6806_equal.json' ) ),
		/* expectedResultFile= */ testOutputsDir( 'expected_same.json' )
	);

	attemptOrchestration(
		/* testName= */ 'Function call to Z6806/Same wikidata lexeme sense, with different ZObjects',
		/* functionCall= */ readJSON( testInputsDir( 'Z6806_different.json' ) ),
		/* expectedResultFile= */ testOutputsDir( 'expected_different.json' )
	);

	attemptOrchestration(
		/* testName= */ 'Function call to Z6806/Same wikidata lexeme sense, where first argument is of the wrong type',
		/* functionCall= */ readJSON( testInputsDir( 'Z6806_first_arg_wrong_type.json' ) ),
		/* expectedResultFile= */ null,
		/* expectedErrorFile= */ testOutputsDir( 'Z6806_first_arg_wrong_type_expected.json' )
	);

	attemptOrchestration(
		/* testName= */ 'Function call to Z6806/Same wikidata lexeme sense, where second argument is of the wrong type',
		/* functionCall= */ readJSON( testInputsDir( 'Z6806_second_arg_wrong_type.json' ) ),
		/* expectedResultFile= */ null,
		/* expectedErrorFile= */ testOutputsDir( 'Z6806_second_arg_wrong_type_expected.json' )
	);

	attemptOrchestration(
		/* testName= */ 'Function call to Z6806/Same wikidata lexeme sense, where one argument is a reference and one is a literal',
		/* functionCall= */ readJSON( testInputsDir( 'Z6806_ref_vs_literal.json' ) ),
		/* expectedResultFile= */ null,
		/* expectedErrorFile= */ testOutputsDir( 'Z6806_ref_vs_literal_expected.json' )
	);

	{
		const setUpStubs = () => {
			wikiStub.setZId( 'Z421', readJSON( testObjectsDir( 'Z421-kleenean.json' ) ) );
			wikiStub.setZId( 'Z422', readJSON( testObjectsDir( 'Z422-kleenean.json' ) ) );
		};

		{
			const call = {
				Z1K1: 'Z7',
				Z7K1: 'Z805',
				Z805K1: {
					Z1K1: 'Z421',
					Z421K1: 'Z422'
				}
			};
			attemptOrchestration(
				/* testName= */ 'Test that unresolved Z9s pass validation',
				/* functionCall= */ call,
				/* expectedResultFile= */ testOutputsDir( 'expected-reified-kleenean.json' ),
				/* expectedErrorFile = */ null,
				setUpStubs
			);
		}

		{
			const call = {
				Z1K1: 'Z7',
				Z7K1: 'Z801',
				Z801K1: {
					Z1K1: 'Z421',
					Z421K1: 'Z422'
				}
			};
			attemptOrchestration(
				/* testName= */ 'Test that cyclical references are handled kinda intelligently',
				/* functionCall= */ call,
				/* expectedResultFile= */ testOutputsDir( 'expected-echoed-kleenean-reference.json' ),
				/* expectedErrorFile = */ null,
				setUpStubs
			);
		}
	}

	{
		const setUpStubs = () => {
			wikiStub.setZId( 'Z431', readJSON( testObjectsDir( 'Z431.json' ) ) );
			wikiStub.setZId( 'Z432', readJSON( testObjectsDir( 'Z432.json' ) ) );
			wikiStub.setZId( 'Z441', readJSON( testObjectsDir( 'Z441.json' ) ) );
			wikiStub.setZId( 'Z442', readJSON( testObjectsDir( 'Z442.json' ) ) );
		};
		const call = {
			Z1K1: 'Z7',
			Z7K1: 'Z801',
			Z801K1: {
				Z1K1: 'Z431',
				Z431K1: 'Z432'
			}
		};
		attemptOrchestration(
			/* testName= */ 'Test that mutually recursive cyclical references are handled kinda intelligently',
			/* functionCall= */ call,
			/* expectedResultFile= */ testOutputsDir( 'expected-echoed-unkleenean-reference.json' ),
			/* expectedErrorFile = */ null,
			setUpStubs
		);
	}

	{
		const argument = {
			Z1K1: 'Z7',
			Z7K1: {
				Z1K1: 'Z8',
				Z8K1: [
					'Z17',
					{
						Z1K1: 'Z17',
						Z17K1: {
							Z1K1: 'Z9',
							Z9K1: 'Z6'
						},
						Z17K2: 'Z60606K1',
						Z17K3: {
							Z1K1: 'Z12',
							Z12K1: [ 'Z11' ]
						}
					}
				],
				Z8K2: {
					Z1K1: 'Z7',
					Z7K1: 'Z881',
					Z881K1: 'Z6'
				},
				Z8K3: [ 'Z20' ],
				Z8K4: [
					'Z14',
					{
						Z1K1: 'Z14',
						Z14K1: 'Z60606',
						Z14K2: {
							Z1K1: 'Z7',
							Z7K1: 'Z810',
							Z810K1: {
								Z1K1: 'Z18',
								Z18K1: 'Z60606K1'
							},
							Z810K2: [
								'Z6',
								{
									Z1K1: 'Z18',
									Z18K1: 'Z60606K1'
								}
							]
						}
					}
				],
				Z8K5: 'Z60606'
			},
			Z60606K1: 'meow'
		};
		const call = {
			Z1K1: 'Z7',
			Z7K1: {
				Z1K1: 'Z8',
				Z8K1: [
					'Z17'
				],
				Z8K2: {
					Z1K1: 'Z7',
					Z7K1: 'Z881',
					Z881K1: 'Z6'
				},
				Z8K3: [ 'Z20' ],
				Z8K4: [
					'Z14',
					{
						Z1K1: 'Z14',
						Z14K1: 'Z60607',
						Z14K2: {
							Z1K1: 'Z7',
							Z7K1: 'Z801',
							Z801K1: argument
						}
					}
				],
				Z8K5: 'Z60607'
			}
		};
		attemptOrchestration(
			/* testName= */ 'Test really tricksy deep-nested function calls in arguments',
			/* functionCall= */ call,
			/* expectedResultFile= */ testOutputsDir( 'expected-tricksy-deep-function-call.json' ),
			/* expectedErrorFile= */ null,
			/* setUpStubs= */ null,
			/* expectedExtraMetadata= */ [],
			/* expectedMissingMetadata= */ [ 'implementationId' ]
		);
	}

	attemptOrchestration(
		/* testName= */ 'function call for Z802 with reference to Z902',
		/* functionCall= */ readJSON( testInputsDir( 'Z802_false.json' ) ),
		/* expectedResultFile= */ testOutputsDir( 'Z902_false_expected.json' )
	);

	attemptOrchestration(
		/* testName= */ 'function call for the false Z902 (if), the dissembler',
		/* functionCall= */ readJSON( testInputsDir( 'Z902_false.json' ) ),
		/* expectedResultFile= */ testOutputsDir( 'Z902_false_expected.json' )
	);

	attemptOrchestration(
		/* testName= */ 'function call for the true Z902 (if), the good if',
		/* functionCall= */ readJSON( testInputsDir( 'Z902_true.json' ) ),
		/* expectedResultFile= */ testOutputsDir( 'Z902_true_expected.json' )
	);

	attemptOrchestration(
		/* testName= */ 'function call for Z903 (value by key)',
		/* functionCall= */ readJSON( testInputsDir( 'Z903.json' ) ),
		/* expectedResultFile= */ testOutputsDir( 'expected_funicle.json' )
	);

	attemptOrchestration(
		/* testName= */ 'function call for Z903 (value by key) with bad key',
		/* functionCall= */ readJSON( testInputsDir( 'Z903_bad_key.json' ) ),
		/* expectedResultFile= */ null,
		/* expectedErrorFile= */ testOutputsDir( 'Z903_bad_key_expected.json' )
	);

	attemptOrchestration(
		/* testName= */ 'function call for Z804',
		/* functionCall= */ readJSON( testInputsDir( 'Z804.json' ) ),
		/* expectedResultFile= */ testOutputsDir( 'Z804_expected.json' )
	);

	attemptOrchestration(
		/* testName= */ 'function call for Z805 with reference to Z905',
		/* functionCall= */ readJSON( testInputsDir( 'Z805.json' ) ),
		/* expectedResultFile= */ testOutputsDir( 'Z905_expected.json' )
	);

	attemptOrchestration(
		/* testName= */ 'function call for Z905 (reify)',
		/* functionCall= */ readJSON( testInputsDir( 'Z905.json' ) ),
		/* expectedResultFile= */ testOutputsDir( 'Z905_expected.json' )
	);

	attemptOrchestration(
		/* testName= */ 'function call for Z808 with reference to Z908',
		/* functionCall= */ readJSON( testInputsDir( 'Z808.json' ) ),
		/* expectedResultFile= */ testOutputsDir( 'Z908_expected.json' )
	);

	attemptOrchestration(
		/* testName= */ 'function call for Z908 (abstract)',
		/* functionCall= */ readJSON( testInputsDir( 'Z908.json' ) ),
		/* expectedResultFile= */ testOutputsDir( 'Z908_expected.json' )
	);

	attemptOrchestration(
		/* testName= */ 'function call (short form) for Z810/Cons onto empty List',
		/* functionCall= */ readJSON( testInputsDir( 'Z810.json' ) ),
		/* expectedResultFile= */ testOutputsDir( 'Z910_expected.json' )
	);

	attemptOrchestration(
		/* testName= */ 'function call (short form) for Z810/Cons onto empty Z881',
		/* functionCall= */ readJSON( testInputsDir( 'Z810_empty_Z881.json' ) ),
		/* expectedResultFile= */ testOutputsDir( 'Z910_empty_Z881_expected.json' )
	);

	attemptOrchestration(
		/* testName= */ 'function call (short form) for Z810/Cons onto non-empty Z881',
		/* functionCall= */ readJSON( testInputsDir( 'Z810_full_Z881.json' ) ),
		/* expectedResultFile= */ testOutputsDir( 'Z910_full_Z881_expected.json' )
	);

	attemptOrchestration(
		/* testName= */ 'function call for Z910/Cons onto empty List',
		/* functionCall= */ readJSON( testInputsDir( 'Z910.json' ) ),
		/* expectedResultFile= */ testOutputsDir( 'Z910_expected.json' )
	);

	attemptOrchestration(
		/* testName= */ 'function call for Z910/Cons onto empty Z881',
		/* functionCall= */ readJSON( testInputsDir( 'Z910_empty_Z881.json' ) ),
		/* expectedResultFile= */ testOutputsDir( 'Z910_empty_Z881_expected.json' )
	);

	attemptOrchestration(
		/* testName= */ 'function call for Z910/Cons onto non-empty Z881',
		/* functionCall= */ readJSON( testInputsDir( 'Z910_full_Z881.json' ) ),
		/* expectedResultFile= */ testOutputsDir( 'Z910_full_Z881_expected.json' )
	);

	attemptOrchestration(
		/* testName= */ 'function call (short form) for Z811/Head with non-empty List',
		/* functionCall= */ readJSON( testInputsDir( 'Z811.json' ) ),
		/* expectedResultFile= */ testOutputsDir( 'expected_arbitrary_zobject.json' )
	);

	attemptOrchestration(
		/* testName= */ 'function call (short form) for Z811/Head with non-empty Z881',
		/* functionCall= */ readJSON( testInputsDir( 'Z811_full_Z881.json' ) ),
		/* expectedResultFile= */ testOutputsDir( 'expected_traveler.json' )
	);

	attemptOrchestration(
		/* testName= */ 'function call (short form) for Z811/Head with empty Z881',
		/* functionCall= */ readJSON( testInputsDir( 'Z811_empty_Z881.json' ) ),
		/* expectedResultFile= */ null,
		/* expectedErrorFile= */ testOutputsDir( 'Z811-short-form-empty-Z881_expected-error.json' )
	);

	attemptOrchestration(
		/* testName= */ 'function call for Z911 (head) with non-empty List',
		/* functionCall= */ readJSON( testInputsDir( 'Z911.json' ) ),
		/* expectedResultFile= */ testOutputsDir( 'expected_arbitrary_zobject.json' )
	);

	attemptOrchestration(
		/* testName= */ 'function call for Z811/Head with reference to Z911 and non-empty Z881',
		/* functionCall= */ readJSON( testInputsDir( 'Z911_full_Z881.json' ) ),
		/* expectedResultFile= */ testOutputsDir( 'expected_traveler.json' )
	);

	attemptOrchestration(
		/* testName= */ 'function call for Z911 (head) with empty List',
		/* functionCall= */ readJSON( testInputsDir( 'Z911_empty.json' ) ),
		/* expectedResultFile= */ null,
		/* expectedErrorFile= */ testOutputsDir( 'Z911_empty_Z881_expected_error.json' )
	);

	attemptOrchestration(
		/* testName= */ 'function call for Z811/Head with reference to Z911 and empty Z881',
		/* functionCall= */ readJSON( testInputsDir( 'Z911_empty_Z881.json' ) ),
		/* expectedResultFile= */ null,
		/* expectedErrorFile= */ testOutputsDir( 'Z811-reference-to-Z911-empty-Z881_expected-error.json' )
	);

	attemptOrchestration(
		/* testName= */ 'function call (short form) for Z812/Tail with non-empty List',
		/* functionCall= */ readJSON( testInputsDir( 'Z812.json' ) ),
		/* expectedResultFile= */ testOutputsDir( 'expected-Z812.json' )
	);

	attemptOrchestration(
		/* testName= */ 'function call (short form) for Z812/Tail with non-empty Z881',
		/* functionCall= */ readJSON( testInputsDir( 'Z812_full_Z881.json' ) ),
		/* expectedResultFile= */ testOutputsDir( 'Z912_full_Z881_expected.json' )
	);

	attemptOrchestration(
		/* testName= */ 'function call (short form) for Z812/Tail with empty Z881',
		/* functionCall= */ readJSON( testInputsDir( 'Z812_empty_Z881.json' ) ),
		/* expectedResultFile= */ null,
		/* expectedErrorFile= */ testOutputsDir( 'Z812-short-form-empty-Z881_expected-error.json' )
	);

	attemptOrchestration(
		/* testName= */ 'function call for Z812/Tail with reference to Z912 and non-empty List',
		/* functionCall= */ readJSON( testInputsDir( 'Z912.json' ) ),
		/* expectedResultFile= */ testOutputsDir( 'expected-Z812.json' )
	);

	attemptOrchestration(
		/* testName= */ 'function call for Z812/Tail with reference to Z912 and non-empty Z881',
		/* functionCall= */ readJSON( testInputsDir( 'Z912_full_Z881.json' ) ),
		/* expectedResultFile= */ testOutputsDir( 'Z912_full_Z881_expected.json' )
	);

	attemptOrchestration(
		/* testName= */ 'function call for Z812/Tail with reference to Z912 and empty List',
		/* functionCall= */ readJSON( testInputsDir( 'Z912_empty.json' ) ),
		/* expectedResultFile= */ null,
		/* expectedErrorFile= */ testOutputsDir( 'Z912_empty_Z881_expected_error.json' )
	);

	attemptOrchestration(
		/* testName= */ 'function call for Z812/Tail with reference to Z912 and empty Z881',
		/* functionCall= */ readJSON( testInputsDir( 'Z912_empty_Z881.json' ) ),
		/* expectedResultFile= */ null,
		/* expectedErrorFile= */ testOutputsDir( 'Z812-reference-to-Z912-empty-Z881_expected-error.json' )
	);

	attemptOrchestration(
		/* testName= */ 'function call (short form) for Z813/Empty with an empty List (benjamin)',
		/* functionCall= */ readJSON( testInputsDir( 'Z813_empty_benjamin.json' ) ),
		/* expectedResultFile= */ testOutputsDir( 'expected_Z813_empty_benjamin.json' )
	);

	attemptOrchestration(
		/* testName= */ 'function call (short form) for Z813/Empty with an empty Z881',
		/* functionCall= */ readJSON( testInputsDir( 'Z813_empty_Z881.json' ) ),
		/* expectedResultFile= */ testOutputsDir( 'expected_Z813_empty_benjamin.json' )
	);

	attemptOrchestration(
		/* testName= */ 'function call (short form) for Z813/Empty with a non-empty Z881',
		/* functionCall= */ readJSON( testInputsDir( 'Z813_full_Z881.json' ) ),
		/* expectedResultFile= */ testOutputsDir( 'expected_Z813_full_benjamin.json' )
	);

	attemptOrchestration(
		/* testName= */ 'function call (short form) for Z813/Empty with a non-empty List (benjamin)',
		/* functionCall= */ readJSON( testInputsDir( 'Z813_full_Z881.json' ) ),
		/* expectedResultFile= */ testOutputsDir( 'expected_Z813_full_benjamin.json' )
	);

	attemptOrchestration(
		/* testName= */ 'function call for Z813/Empty with reference to Z913 and an empty List (benjamin)',
		/* functionCall= */ readJSON( testInputsDir( 'Z913_empty_benjamin.json' ) ),
		/* expectedResultFile= */ testOutputsDir( 'expected_Z813_empty_benjamin.json' )
	);

	attemptOrchestration(
		/* testName= */ 'function call for Z813/Empty with reference to Z913 and an empty Z881',
		/* functionCall= */ readJSON( testInputsDir( 'Z913_empty_Z881.json' ) ),
		/* expectedResultFile= */ testOutputsDir( 'expected_Z813_empty_benjamin.json' )
	);

	attemptOrchestration(
		/* testName= */ 'function call for Z813/Empty with reference to Z913 and a non-empty List (benjamin)',
		/* functionCall= */ readJSON( testInputsDir( 'Z913_full_benjamin.json' ) ),
		/* expectedResultFile= */ testOutputsDir( 'expected_Z813_full_benjamin.json' )
	);

	attemptOrchestration(
		/* testName= */ 'function call for Z813/Empty with reference to Z913 and an non-empty Z881',
		/* functionCall= */ readJSON( testInputsDir( 'Z913_full_Z881.json' ) ),
		/* expectedResultFile= */ testOutputsDir( 'expected_Z813_full_benjamin.json' )
	);

	attemptOrchestration(
		/* testName= */ 'function call for Z821 (first)',
		/* functionCall= */ readJSON( testInputsDir( 'Z821.json' ) ),
		/* expectedResultFile= */ testOutputsDir( 'expected_first_element.json' )
	);

	attemptOrchestration(
		/* testName= */ 'function call for Z821 (first) with reference to Z921',
		/* functionCall= */ readJSON( testInputsDir( 'Z921.json' ) ),
		/* expectedResultFile= */ testOutputsDir( 'expected_first_element.json' )
	);

	attemptOrchestration(
		/* testName= */ 'function call for Z822 (second)',
		/* functionCall= */ readJSON( testInputsDir( 'Z822.json' ) ),
		/* expectedResultFile= */ testOutputsDir( 'expected_second_element.json' )
	);

	attemptOrchestration(
		/* testName= */ 'function call for Z822 (second) with reference to Z922',
		/* functionCall= */ readJSON( testInputsDir( 'Z922.json' ) ),
		/* expectedResultFile= */ testOutputsDir( 'expected_second_element.json' )
	);

	attemptOrchestration(
		/* testName= */ 'function call for Z868',
		/* functionCall= */ readJSON( testInputsDir( 'Z868.json' ) ),
		/* expectedResultFile= */ testOutputsDir( 'Z968_expected.json' )
	);

	attemptOrchestration(
		/* testName= */ 'function call for Z968 (string to code points)',
		/* functionCall= */ readJSON( testInputsDir( 'Z968.json' ) ),
		/* expectedResultFile= */ testOutputsDir( 'Z968_expected.json' )
	);

	attemptOrchestration(
		/* testName= */ 'function call for Z968 (string to code points) with combined Emoji',
		/* functionCall= */ readJSON( testInputsDir( 'Z968_emoji.json' ) ),
		/* expectedResultFile= */ testOutputsDir( 'Z968_emoji_expected.json' )
	);

	{
		const call = readJSON( testInputsDir( 'Z872_filter_function_call.json' ) );
		attemptOrchestration(
			/* testName= */ 'find a letter using Z872 (filter)',
			/* functionCall= */ call,
			/* expectedResultFile= */ testOutputsDir( 'Z872_2_a.json' )
		);
	}

	{
		const call = {
			Z1K1: 'Z7',
			Z7K1: 'Z872',
			Z872K1: 'Z868',
			Z872K2: [
				'Z6', 'c', 'a', 't', 'e', 'r', 'p', 'i', 'l', 'l', 'a', 'r'
			]
		};
		attemptOrchestration(
			/* testName= */ 'Z872 (filter) but the function does not return Boolean',
			/* functionCall= */ call,
			/* expectedResultFile= */ null,
			/* expectedErrorFile= */ testOutputsDir( 'Z872_non_bool_expected.json' )
		);
	}

	{
		const call = {
			Z1K1: 'Z7',
			Z7K1: 'Z873',
			Z873K1: 'Z868',
			Z873K2: [
				'Z6',
				'acab',
				'baca'
			]
		};
		attemptOrchestration(
			/* testName= */ 'function call for Z873 (map)',
			/* functionCall= */ call,
			/* expectedResultFile= */ testOutputsDir( 'Z873_expected.json' )
		);
	}

	function getTypeFor( ZID, nextZID ) {
		const theType = {
			Z1K1: 'Z4',
			Z4K1: ZID,
			Z4K2: [
				'Z3',
				{
					Z1K1: 'Z3',
					Z3K1: nextZID,
					Z3K2: ZID + 'K1',
					Z3K3: {
						Z1K1: 'Z12',
						Z12K1: [
							'Z11',
							{
								Z1K1: 'Z11',
								Z11K1: 'Z1002',
								Z11K2: 'value'
							}
						]
					}
				}
			],
			Z4K3: 'Z831'
		};
		return normalize( theType ).Z22K1;
	}

	function getPersistentObjectFor( ZID, theObject ) {
		return {
			Z1K1: 'Z2',
			Z2K1: {
				Z1K1: 'Z6',
				Z6K1: ZID
			},
			Z2K2: theObject,
			Z2K3: {
				Z1K1: 'Z12',
				Z12K1: [ 'Z11' ]
			},
			Z2K4: {
				Z1K1: 'Z32',
				Z32K1: [ 'Z31' ]
			}
		};
	}

	function toZID( someInteger ) {
		return 'Z' + someInteger;
	}

	{
		// Create types with ZIDs from Z400 to Z448; each one will have a member
		// called Z4??K1, which will be of type Z4??, whose ZID will be one greater
		// than that of the current object. Also create persistent objects with ZIDs
		// from Z450 to Z458, with the property that the ZID of each object will
		// be exactly 50 greater than the ZID of its type.
		const setUpStubs = () => {
			for ( let index = 4000; index < 4200; ++index ) {
				const typeZID = toZID( index );
				const nextTypeZID = toZID( index + 1 );
				const objectZID = toZID( index + 200 );
				const nextObjectZID = toZID( index + 201 );
				const theType = getTypeFor( typeZID, nextTypeZID );
				const theObject = {
					Z1K1: typeZID,
					[ typeZID + 'K1' ]: nextObjectZID
				};
				wikiStub.setZId( typeZID, getPersistentObjectFor( typeZID, theType ) );
				wikiStub.setZId( objectZID, getPersistentObjectFor( objectZID, theObject ) );
			}
		};
		// Object Z4200 has type Z4000. It will point to its member (Z4000K1: Z4201),
		// which will likewise point to the next object in the chain (Z4001K1: Z4202),
		// etc., ad infinitum, if infinity were approximately 49.
		const call = {
			Z1K1: 'Z7',
			Z7K1: 'Z801',
			Z801K1: 'Z4200'
		};
		attemptOrchestration(
			/* testName= */ 'eager evaluation limit: the object is too deep',
			/* functionCall= */ call,
			/* expectedResultFile= */ null,
			/* expectedErrorFile= */ testOutputsDir( 'orchestrator_recursion_limit_expected.json' ),
			/* setUpStubs= */ setUpStubs
		);
	}

	{
		const theStrings = [ 'Z6' ];
		for ( let i = 0; i < 100; ++i ) {
			theStrings.push( String( i ) );
		}
		const call = {
			Z1K1: 'Z7',
			Z7K1: 'Z873',
			Z873K1: 'Z801',
			Z873K2: theStrings
		};
		const requestId = 'some-fake-id';
		attemptOrchestration(
			/* testName= */ 'orchestrator rate limit: too many inputs to a map!',
			/* functionCall= */ call,
			/* expectedResultFile= */ null,
			/* expectedErrorFile= */ testOutputsDir( 'orchestrator_rate_limit_expected.json' ),
			/* setUpStubs= */ null,
			/* expectedExtraMetadata= */ [],
			/* expectedMissingMetadata= */ [],
			/* implementationSelector= */ null,
			/* doValidate= */ true,
			/* skip= */ false,
			requestId
		);
	}

	{
		const call = {
			Z1K1: 'Z7',
			Z7K1: 'Z876',
			Z876K1: {
				Z1K1: 'Z8',
				Z8K1: [
					'Z17',
					{
						Z1K1: 'Z17',
						Z17K1: 'Z40',
						Z17K2: 'Z20000K1',
						Z17K3: {
							Z1K1: 'Z12',
							Z12K1: [ 'Z11' ]
						}
					},
					{
						Z1K1: 'Z17',
						Z17K1: 'Z40',
						Z17K2: 'Z20000K2',
						Z17K3: {
							Z1K1: 'Z12',
							Z12K1: [ 'Z11' ]
						}
					}
				],
				Z8K2: 'Z40',
				Z8K3: [ 'Z20' ],
				Z8K4: [
					'Z14',
					{
						Z1K1: 'Z14',
						Z14K1: 'Z20000',
						Z14K2: {
							Z1K1: 'Z7',
							Z7K1: 'Z802',
							Z802K1: {
								Z1K1: 'Z18',
								Z18K1: 'Z20000K1'
							},
							Z802K2: {
								Z1K1: 'Z18',
								Z18K1: 'Z20000K2'
							},
							Z802K3: {
								Z1K1: 'Z18',
								Z18K1: 'Z20000K1'
							}
						}
					}
				],
				Z8K5: 'Z20000'
			},
			Z876K2: [
				'Z40',
				{ Z1K1: 'Z40', Z40K1: 'Z41' },
				{ Z1K1: 'Z40', Z40K1: 'Z42' }
			],
			Z876K3: { Z1K1: 'Z40', Z40K1: 'Z41' }
		};
		attemptOrchestration(
			/* testName= */ 'implement "all" using Z876 (reduce): false',
			/* functionCall= */ call,
			/* expectedResultFile= */ testOutputsDir( 'Z876_all_false_expected.json' )
		);
	}

	{
		const call = {
			Z1K1: 'Z7',
			Z7K1: 'Z876',
			Z876K1: {
				Z1K1: 'Z8',
				Z8K1: [
					'Z17',
					{
						Z1K1: 'Z17',
						Z17K1: 'Z40',
						Z17K2: 'Z20000K1',
						Z17K3: {
							Z1K1: 'Z12',
							Z12K1: [ 'Z11' ]
						}
					},
					{
						Z1K1: 'Z17',
						Z17K1: 'Z40',
						Z17K2: 'Z20000K2',
						Z17K3: {
							Z1K1: 'Z12',
							Z12K1: [ 'Z11' ]
						}
					}
				],
				Z8K2: 'Z40',
				Z8K3: [ 'Z20' ],
				Z8K4: [
					'Z14',
					{
						Z1K1: 'Z14',
						Z14K1: 'Z20000',
						Z14K2: {
							Z1K1: 'Z7',
							Z7K1: 'Z802',
							Z802K1: {
								Z1K1: 'Z18',
								Z18K1: 'Z20000K1'
							},
							Z802K2: {
								Z1K1: 'Z18',
								Z18K1: 'Z20000K2'
							},
							Z802K3: {
								Z1K1: 'Z18',
								Z18K1: 'Z20000K1'
							}
						}
					}
				],
				Z8K5: 'Z20000'
			},
			Z876K2: [
				'Z40',
				{ Z1K1: 'Z40', Z40K1: 'Z41' },
				{ Z1K1: 'Z40', Z40K1: 'Z41' }
			],
			Z876K3: { Z1K1: 'Z40', Z40K1: 'Z41' }
		};
		attemptOrchestration(
			/* testName= */ 'implement "all" using Z876 (reduce): true',
			/* functionCall= */ call,
			/* expectedResultFile= */ testOutputsDir( 'Z876_all_true_expected.json' )
		);
	}

	{
		const call = {
			Z1K1: 'Z7',
			Z7K1: 'Z876',
			Z876K1: {
				Z1K1: 'Z8',
				Z8K1: [
					'Z17',
					{
						Z1K1: 'Z17',
						Z17K1: 'Z40',
						Z17K2: 'Z20000K1',
						Z17K3: {
							Z1K1: 'Z12',
							Z12K1: [ 'Z11' ]
						}
					},
					{
						Z1K1: 'Z17',
						Z17K1: 'Z40',
						Z17K2: 'Z20000K2',
						Z17K3: {
							Z1K1: 'Z12',
							Z12K1: [ 'Z11' ]
						}
					}
				],
				Z8K2: 'Z40',
				Z8K3: [ 'Z20' ],
				Z8K4: [
					'Z14',
					{
						Z1K1: 'Z14',
						Z14K1: 'Z20000',
						Z14K2: {
							Z1K1: 'Z7',
							Z7K1: 'Z802',
							Z802K1: {
								Z1K1: 'Z18',
								Z18K1: 'Z20000K1'
							},
							Z802K2: {
								Z1K1: 'Z18',
								Z18K1: 'Z20000K2'
							},
							Z802K3: {
								Z1K1: 'Z18',
								Z18K1: 'Z20000K1'
							}
						}
					}
				],
				Z8K5: 'Z20000'
			},
			Z876K2: [ 'Z40' ],
			Z876K3: 'ay'
		};
		attemptOrchestration(
			/* testName= */ 'implement "all" using Z876 (reduce): empty list',
			/* functionCall= */ call,
			/* expectedResultFile= */ testOutputsDir( 'Z876_all_empty_expected.json' )
		);
	}

	attemptOrchestration(
		/* testName= */ 'function call for Z883 (short form)',
		/* functionCall= */ readJSON( testInputsDir( 'Z883.json' ) ),
		/* expectedResultFile= */ testOutputsDir( 'Z883_expected.json' )
	);

	{
		const call = {
			Z1K1: 'Z7',
			Z7K1: 'Z885',
			Z885K1: 'Z511'
		};
		attemptOrchestration(
			/* testName= */ 'function call for Z885',
			/* functionCall= */ call,
			/* expectedResultFile= */ testOutputsDir( 'Z885_expected.json' )
		);
	}

	attemptOrchestration(
		/* testName= */ 'function call for Z886 (short form)',
		/* functionCall= */ readJSON( testInputsDir( 'Z886.json' ) ),
		/* expectedResultFile= */ testOutputsDir( 'expected_mus.json' )
	);

	attemptOrchestration(
		/* testName= */ 'function call for Z886 (short form) with Z881 input',
		/* functionCall= */ readJSON( testInputsDir( 'Z886_with_Z881.json' ) ),
		/* expectedResultFile= */ testOutputsDir( 'expected_mus.json' )
	);

	attemptOrchestration(
		/* testName= */ 'function call for Z986 (code points to string)',
		/* functionCall= */ readJSON( testInputsDir( 'Z986.json' ) ),
		/* expectedResultFile= */ testOutputsDir( 'expected_mus.json' )
	);

	attemptOrchestration(
		/* testName= */ 'function call for Z986 (code points to string) with Z881 input',
		/* functionCall= */ readJSON( testInputsDir( 'Z986_with_Z881.json' ) ),
		/* expectedResultFile= */ testOutputsDir( 'expected_mus.json' )
	);

	attemptOrchestration(
		/* testName= */ 'function call for Z986 (code points to string) with combining characters',
		/* functionCall= */ readJSON( testInputsDir( 'Z986_emoji.json' ) ),
		/* expectedResultFile= */ testOutputsDir( 'Z986_emoji_expected.json' )
	);

	attemptOrchestration(
		/* testName= */ 'function call for Z986 (code points to string) with combining characters, with Z881 input',
		/* functionCall= */ readJSON( testInputsDir( 'Z986_emoji_with_Z881.json' ) ),
		/* expectedResultFile= */ testOutputsDir( 'Z986_emoji_expected.json' )
	);

	attemptOrchestration(
		/* testName= */ 'function call for Z888 with reference to Z988',
		/* functionCall= */ readJSON( testInputsDir( 'Z888_same.json' ) ),
		/* expectedResultFile= */ testOutputsDir( 'expected_same.json' )
	);

	attemptOrchestration(
		/* testName= */ 'function call for Z988 (same), and the arguments are truly same',
		/* functionCall= */ readJSON( testInputsDir( 'Z988_same.json' ) ),
		/* expectedResultFile= */ testOutputsDir( 'expected_same.json' )
	);

	attemptOrchestration(
		/* testName= */ 'function call for Z988 (same), and lo, they are not same',
		/* functionCall= */ readJSON( testInputsDir( 'Z988_different.json' ) ),
		/* expectedResultFile= */ testOutputsDir( 'expected_different.json' )
	);

	attemptOrchestration(
		/* testName= */ 'non-normalized function call with array',
		/* functionCall= */ readJSON( testInputsDir( 'Z988_different_non-normalized.json' ) ),
		/* expectedResultFile= */ testOutputsDir( 'expected_different.json' )
	);

	attemptOrchestration(
		/* testName= */ 'function call for Z899 with reference to Z999',
		/* functionCall= */ readJSON( testInputsDir( 'Z899.json' ) ),
		/* expectedResultFile= */ testOutputsDir( 'expected_Z11.json' )
	);

	attemptOrchestration(
		/* testName= */ 'function call for Z999 (unquote)',
		/* functionCall= */ readJSON( testInputsDir( 'Z999.json' ) ),
		/* expectedResultFile= */ testOutputsDir( 'expected_Z11.json' )
	);

	// TODO (T341518): Catch this error more explicitly.
	attemptOrchestration(
		/* testName= */ 'function call for Z899 (unquote) with degenerate input',
		/* functionCall= */ readJSON( testInputsDir( 'Z899-degenerate.json' ) ),
		/* expectedResultFile= */ null,
		/* expectedErrorFile= */ testOutputsDir( 'Z899-degenerate_expected.json' )
	);

	attemptOrchestration(
		/* testName= */ 'function call for Z960 (language code to language)',
		/* functionCall= */ readJSON( testInputsDir( 'Z6_english.json' ) ),
		/* expectedResultFile= */ testOutputsDir( 'expected_Z6_english.json' )
	);

	attemptOrchestration(
		/* testName= */ 'function call for Z889/List equality with reference to Z989 and lists of different length',
		/* functionCall= */ readJSON( testInputsDir( 'Z989_different_length.json' ) ),
		/* expectedResultFile= */ testOutputsDir( 'expected_Z989_different_length.json' )
	);

	attemptOrchestration(
		/* testName= */ 'function call for Z889/List equality with reference to Z989 and lists with different elements',
		/* functionCall= */ readJSON( testInputsDir( 'Z989_different_elements.json' ) ),
		/* expectedResultFile= */ testOutputsDir( 'expected_Z989_different_elements.json' )
	);

	attemptOrchestration(
		/* testName= */ 'function call for Z889/List equality with reference to Z989 and equal lists',
		/* functionCall= */ readJSON( testInputsDir( 'Z989_equal.json' ) ),
		/* expectedResultFile= */ testOutputsDir( 'expected_Z989_equal.json' )
	);

	attemptOrchestration(
		/* testName= */ 'function call containing multilingual text with multiple languages (implicit test for Z212)',
		/* functionCall= */ readJSON( testInputsDir( 'call-with-multilingual-text-with-multiple-langs.json' ) ),
		/* expectedResultFile= */ testOutputsDir( 'expected_abc.json' ),
		/* expectedErrorFile= */ null,
		/* setUpStubs= */ null,
		/* expectedExtraMetadata= */ [],
		/* expectedMissingMetadata= */ [ 'implementationId' ]
	);

	attemptOrchestration(
		/* testName= */ 'function call with wrong typed implementation Z20 instead of a Z14 in Z8K4',
		/* functionCall= */ readJSON( testInputsDir( 'function-call-with-wrong-typed-implementation.json' ) ),
		/* expectedResultFile= */ null,
		/* expectedErrorFile= */ testOutputsDir( 'wrong-typed-implementation-expected.json' ),
		/* setUpStubs= */ null,
		/* expectedExtraMetadata= */ [],
		/* expectedMissingMetadata= */ [ 'implementationId', 'implementationType' ]
	);

	attemptOrchestration(
		/* testName= */ 'function call with empty object in Z14K2',
		/* functionCall= */ readJSON( testInputsDir( 'function-call-with-empty-Z14K2.json' ) ),
		/* expectedResultFile= */ null,
		/* expectedErrorFile= */ testOutputsDir( 'empty-Z14K2_expected.json' ),
		/* setUpStubs= */ null,
		/* expectedExtraMetadata= */ [],
		/* expectedMissingMetadata= */ [ 'implementationId', 'implementationType' ]
	);

	{
		const setUpStubs = () => {
			evaluatorStub.setZId( 'Z400', () => makeMappedResultEnvelope( { Z1K1: 'Z6', Z6K1: '13' }, null ) );
		};
		attemptOrchestration(
			/* testName= */ 'test use of Z61 objects',
			/* functionCall= */ readJSON( testInputsDir( 'zObject-function-call.json' ) ),
			/* expectedResultFile= */ testOutputsDir( 'zObject-orchestrated-result.json' ),
			/* expectedErrorFile= */ null,
			setUpStubs,
			/* expectedExtraMetadata= */ [],
			/* expectedMissingMetadata= */ [ 'implementationId' ]
		);
	}

	{
		const setUpStubs = () => {
			evaluatorStub.setZId(
				'Z499',
				( zobject ) => {
					const resultObject = {
						deserializer: zobject.functionArguments.Z499K1.deserializer,
						serializer: zobject.serializer
					};
					// The goal of this test is simply to verify that type converter
					// fields are populated. To effect that end, we create
					// a (hopefully deterministically sorted) string containin that
					// information. Note that the code snippets in Z499 are all lies
					// and are not used in this test.
					return makeMappedResultEnvelope( {
						Z1K1: {
							Z1K1: 'Z9',
							Z9K1: 'Z400'
						},
						Z400K1: {
							Z1K1: 'Z6',
							Z6K1: JSON.stringify( resultObject )
						}
					} );
				} );
			// Create type with custom type converters
			const Z400 = readJSON( testObjectsDir( 'Z400_type.json' ) );
			wikiStub.setZId( 'Z400', {
				Z1K1: 'Z2',
				Z2K1: { Z1K1: 'Z6', Z6K1: 'Z400' },
				Z2K2: Z400
			} );
		};
		const theFunctionCall = readJSON( testInputsDir( 'Z499_function_call.json' ) );
		attemptOrchestration(
			/* testName= */ 'Test with custom type converters',
			/* functionCall= */ theFunctionCall,
			/* expectedResultFile= */ testOutputsDir( 'custom_type_converters_expected.json' ),
			/* expectedErrorFile= */ null,
			setUpStubs,
			/* expectedExtraMetadata= */ [],
			/* expectedMissingMetadata= */ [ 'implementationId' ],
			/* implementationSelector= */ null,
			/* doValidate= */ true,
			/* skip= */ false,
			/* requestId= */ 'hello'
		);
	}

	{
		const setUpStubs = () => {
			evaluatorStub.setZId(
				'Z484',
				( zobject ) => {
					const resultObject = {
						to_code_K1: zobject.functionArguments.Z484K1.deserializer,
						to_code_K2: zobject.functionArguments.Z484K2.deserializer,
						from_code: zobject.serializer
					};
					return makeMappedResultEnvelope( {
						Z1K1: {
							Z1K1: 'Z9',
							Z9K1: 'Z481'
						},
						Z481K1: {
							Z1K1: 'Z6',
							Z6K1: JSON.stringify( resultObject )
						}
					} );
				} );
			// Create type with custom type converters.
			wikiStub.setZId( 'Z481', {
				Z1K1: 'Z2',
				Z2K1: { Z1K1: 'Z6', Z6K1: 'Z481' },
				Z2K2: readJSON( testObjectsDir( 'type-converters-Z481.json' ) )
			} );
			// Create custom type converters.
			wikiStub.setZId( 'Z482', {
				Z1K1: 'Z2',
				Z2K1: { Z1K1: 'Z6', Z6K1: 'Z482' },
				Z2K2: readJSON( testObjectsDir( 'type-converters-Z482.json' ) )
			} );
			wikiStub.setZId( 'Z483', {
				Z1K1: 'Z2',
				Z2K1: { Z1K1: 'Z6', Z6K1: 'Z483' },
				Z2K2: readJSON( testObjectsDir( 'type-converters-Z483.json' ) )
			} );
			// Create function and native-code implementation using types with
			// custom type converters.
			wikiStub.setZId( 'Z484', {
				Z1K1: 'Z2',
				Z2K1: { Z1K1: 'Z6', Z6K1: 'Z484' },
				Z2K2: readJSON( testObjectsDir( 'type-converters-Z484.json' ) )
			} );
			wikiStub.setZId( 'Z485', {
				Z1K1: 'Z2',
				Z2K1: { Z1K1: 'Z6', Z6K1: 'Z485' },
				Z2K2: readJSON( testObjectsDir( 'type-converters-Z485.json' ) )
			} );
		};
		attemptOrchestration(
			/* testName= */ 'Test with on-wiki custom type converters',
			/* functionCall= */ readJSON( testInputsDir( 'call-with-type-converters.json' ) ),
			/* expectedResultFile= */ testOutputsDir( 'on_wiki_type_converters_expected.json' ),
			/* expectedErrorFile= */ null,
			setUpStubs,
			/* expectedExtraMetadata= */ [],
			/* expectedMissingMetadata= */ [],
			/* implementationSelector= */ null,
			/* doValidate= */ true,
			/* skip= */ false,
			/* requestId= */ 'hello'
		);
	}

	{
		const setUpStubs = () => {
			evaluatorStub.setZId(
				'Z494',
				( zobject ) => {
					const resultObject = {
						to_code: zobject.functionArguments.Z494K1.deserializer,
						from_code: zobject.serializer
					};
					return makeMappedResultEnvelope( {
						Z1K1: 'Z6',
						Z6K1: JSON.stringify( resultObject )
					} );
				} );
			// Create type with custom type converters.
			wikiStub.setZId( 'Z481', {
				Z1K1: 'Z2',
				Z2K1: { Z1K1: 'Z6', Z6K1: 'Z481' },
				Z2K2: readJSON( testObjectsDir( 'type-converters-Z481.json' ) )
			} );
			// Create custom type converters.
			wikiStub.setZId( 'Z483', {
				Z1K1: 'Z2',
				Z2K1: { Z1K1: 'Z6', Z6K1: 'Z483' },
				Z2K2: readJSON( testObjectsDir( 'type-converters-Z483.json' ) )
			} );
			// Create function and native-code implementation using types with
			// custom type converters.
			wikiStub.setZId( 'Z494', {
				Z1K1: 'Z2',
				Z2K1: { Z1K1: 'Z6', Z6K1: 'Z494' },
				Z2K2: readJSON( testObjectsDir( 'type-converters-Z494.json' ) )
			} );
			wikiStub.setZId( 'Z495', {
				Z1K1: 'Z2',
				Z2K1: { Z1K1: 'Z6', Z6K1: 'Z495' },
				Z2K2: readJSON( testObjectsDir( 'type-converters-Z495.json' ) )
			} );
		};
		attemptOrchestration(
			/* testName= */ 'Test list input with on-wiki custom type converters',
			/* functionCall= */ readJSON( testInputsDir( 'call-with-type-converters-list-input.json' ) ),
			/* expectedResultFile= */ testOutputsDir( 'list_input_on_wiki_type_converters_expected.json' ),
			/* expectedErrorFile= */ null,
			setUpStubs
		);
	}

	{
		const setUpStubs = () => {
			evaluatorStub.setZId(
				'Z496',
				( zobject ) => {
					const resultObject = { from_code: zobject.serializer };
					return makeMappedResultEnvelope( {
						Z1K1: {
							Z1K1: 'Z7',
							Z7K1: 'Z881',
							Z881K1: 'Z481'
						},
						K1: {
							Z1K1: {
								Z1K1: 'Z9',
								Z9K1: 'Z481'
							},
							Z481K1: {
								Z1K1: 'Z6',
								Z6K1: JSON.stringify( resultObject )
							}
						},
						K2: {
							Z1K1: {
								Z1K1: 'Z7',
								Z7K1: 'Z881',
								Z881K1: 'Z481'
							}
						}
					} );
				} );
			// Create type with custom type converters.
			wikiStub.setZId( 'Z481', {
				Z1K1: 'Z2',
				Z2K1: { Z1K1: 'Z6', Z6K1: 'Z481' },
				Z2K2: readJSON( testObjectsDir( 'type-converters-Z481.json' ) )
			} );
			// Create custom type converters.
			wikiStub.setZId( 'Z482', {
				Z1K1: 'Z2',
				Z2K1: { Z1K1: 'Z6', Z6K1: 'Z482' },
				Z2K2: readJSON( testObjectsDir( 'type-converters-Z482.json' ) )
			} );
			// Create function and native-code implementation using types with
			// custom type converters.
			wikiStub.setZId( 'Z496', {
				Z1K1: 'Z2',
				Z2K1: { Z1K1: 'Z6', Z6K1: 'Z496' },
				Z2K2: readJSON( testObjectsDir( 'type-converters-Z496.json' ) )
			} );
			wikiStub.setZId( 'Z497', {
				Z1K1: 'Z2',
				Z2K1: { Z1K1: 'Z6', Z6K1: 'Z497' },
				Z2K2: readJSON( testObjectsDir( 'type-converters-Z497.json' ) )
			} );
		};
		attemptOrchestration(
			/* testName= */ 'Test list output with on-wiki custom type converters',
			/* functionCall= */ readJSON( testInputsDir( 'call-with-type-converters-list-output.json' ) ),
			/* expectedResultFile= */ testOutputsDir( 'list_output_on_wiki_type_converters_expected.json' ),
			/* expectedErrorFile= */ null,
			setUpStubs
		);
	}

	{
		const setUpStubs = () => {
			wikiStub.setZId( 'Z400', {
				Z1K1: 'Z2',
				Z2K1: { Z1K1: 'Z6', Z6K1: 'Z400' },
				Z2K2: readJSON( testObjectsDir( 'infinite-strings-Z400.json' ) )
			} );
			wikiStub.setZId( 'Z421', {
				Z1K1: 'Z2',
				Z2K1: { Z1K1: 'Z6', Z6K1: 'Z421' },
				Z2K2: readJSON( testObjectsDir( 'infinite-strings-Z421.json' ) )
			} );
			evaluatorStub.setZId(
				'Z400',
				() => makeMappedResultEnvelope( {
					Z1K1: 'Z6',
					Z6K1: 'ab'
				}, null )
			);
		};
		const giganticKeyCall = {
			Z1K1: 'Z7',
			Z7K1: 'Z400',
			Z400K1: 'a',
			Z400K2: 'b'
		};
		attemptOrchestration(
			/* testName= */ 'formerly gigantic zObjectKey',
			/* functionCall= */ giganticKeyCall,
			/* expectedResultFile= */ testOutputsDir( 'nested-metadata-gigantic-key_expected.json' ),
			/* expectedErrorFile= */ null,
			setUpStubs
		);
	}

	// Z823 returns TypedPair<Z1, TypedMap<Z6, Z1>>.  As Z823 is the only function whose
	// Z22K1/result includes a metadata map (and metadata maps contain nondeterministic values),
	// its test requires special handling; i.e., it cannot use attemptOrchestration
	// (We could use a modified form of attemptOrchestration, but let's continue to
	// call orchestrate() manually for this test as an example of that approach).
	it( 'function call for Z823/Get envelope', async () => {
		const input = readJSON( testInputsDir( 'Z823.json' ) );
		// This will be the first element of the typed pair.
		const expectedOutput = { Z1K1: 'Z6', Z6K1: 'arbitrary ZObject' };
		const invariants = getTestInvariants(
			{
				doValidate: true,
				remainingTime: 15,
				wikiUri: WIKI_URI,
				wikidataUri: WIKIDATA_URI,
				evaluatorUri: EVAL_URI,
				requestId: 'Z823-test'
			}
		);
		const envelope = await orchestrate( input, invariants );
		const Z9 = function ( ZID ) {
			return {
				Z1K1: 'Z9',
				Z9K1: ZID
			};
		};
		const pairType = {
			Z1K1: Z9( 'Z7' ),
			Z7K1: Z9( 'Z882' ),
			Z882K1: Z9( 'Z1' ),
			Z882K2: Z9( 'Z1' )
		};
		const resultType = envelope.Z22K1.Z1K1;
		for ( const key of [ 'Z1K1', 'Z882K1', 'Z882K2' ] ) {
			assert.deepEqual( pairType[ key ], resultType[ key ] );
		}
		assert.deepEqual( expectedOutput, envelope.Z22K1.K1 );
		// TODO (T323402): Check the content of Z22K2/metadata
		assert.isTrue( isZMap( envelope.Z22K2 ) );
		// TODO (T323402): Check the content of K2/metadata
		assert.isTrue( isZMap( envelope.Z22K1.K2 ) );
		assert.deepEqual( makeVoid(), getError( envelope ) );
		assert.strictEqual( undefined, getZMapValue( envelope.Z22K1.K2, 'errors' ) );
	} );

} );
