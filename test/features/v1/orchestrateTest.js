'use strict';

const { strictEqual } = require( 'assert' );
const assert = require( '../../utils/assert.js' );
const Server = require( '../../utils/server.js' );
const { getMissingZ5 } = require( '../../../function-schemata/javascript/test/testUtils.js' );

const fetch = require( '../../../lib/fetch.js' );

describe( 'orchestration endpoint', function () {

	this.timeout( 20000 );

	let uri = null;
	const server = new Server();

	before( async () => {
		await server.start();
		uri = `${ server.config.uri }wikifunctions.org/v1/evaluate/`;
	} );

	after( async () => await server.stop() );

	const assertContentType = function ( response, regex ) {
		const justTheHeaders = {
			headers: {
				'content-type': response.headers.get( 'content-type' )
			}
		};
		assert.contentType( justTheHeaders, regex );
	};

	const testZ5 = async function ( input, codes ) {
		const response = await fetch(
			uri,
			{
				method: 'POST',
				headers: {
					'content-type': 'application/json'
				},
				body: JSON.stringify( { zobject: input } )
			} );
		assert.status( response, 200 );
		assertContentType( response, 'application/json' );

		const Z22 = await response.json();
		const notFound = getMissingZ5( Z22, codes );
		strictEqual( notFound.size, 0 );
	};

	it( 'orchestration endpoint: record with list and invalid sub-record', async () => {
		await testZ5(
			{ Z1K1: 'Z8', K2: [ 'Test', 'Second test' ], Z2K1: { K2: 'Test' } },
			[ 'Z502', 'Z523' ]
		);
	} );

	it( 'orchestration endpoint: invalid zobject (int not string/list/record)', async () => {
		await testZ5(
			{ Z1K1: 'Z2', Z2K1: 2 },
			[ 'Z502', 'Z521' ]
		);
	} );

	it( 'orchestration endpoint: invalid zobject (float not string/list/record)', async () => {
		await testZ5(
			{ Z1K1: 'Z2', Z2K1: 2.0 },
			[ 'Z502', 'Z521' ]
		);
	} );

	it( 'orchestration endpoint: number in array', async () => {
		await testZ5(
			[ 2 ],
			[ 'Z521' ]
		);
	} );

	it( 'bad bad bad JSON', async () => {
		const doFetch = async ( someInput ) => await fetch(
			uri,
			{
				method: 'POST',
				headers: {
					'content-type': 'application/json'
				},
				body: someInput
			} );
		const goodInput = JSON.stringify( {
			zobject: {
				Z1K1: 'Z7',
				Z7K1: 'Z801',
				Z801K1: 'hello, yes'
			},
			doValidate: false
		} );
		let goodResult = await doFetch( goodInput );
		assert.status( goodResult, 200 );
		assertContentType( goodResult, 'application/json' );
		let goodEnvelope = await goodResult.json();
		assert.deepEqual( 'hello, yes', goodEnvelope.Z22K1 );

		const badInput = '{"key":"\\d"}';
		const badResult = await doFetch( badInput );
		assert.status( badResult, 500 );

		goodResult = await doFetch( goodInput );
		assert.status( goodResult, 200 );
		assertContentType( goodResult, 'application/json' );
		goodEnvelope = await goodResult.json();
		assert.deepEqual( 'hello, yes', goodEnvelope.Z22K1 );
	} );

} );
