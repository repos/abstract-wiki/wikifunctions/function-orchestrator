'use strict';

const { readJSON } = require( '../../../../src/utils.js' );
const { testInputsDir, testOutputsDir, testObjectsDir } = require( '../../../utils/testFileUtils.js' );
const { getServerWithMocks } = require( '../../../utils/mockUtils.js' );
const { attemptOrchestration } = require( '../mswTestRunner.js' );

describe( 'mockedServices – Generics tests', () => {

	const { wikiStub, mockServiceWorker } = getServerWithMocks();

	before( () => {
		mockServiceWorker.listen();
	} );

	afterEach( () => {
		mockServiceWorker.resetHandlers();
	} );

	after( () => {
		mockServiceWorker.close();
	} );

	{
		const setUpStubs = () => {
			wikiStub.setZId( 'Z401', {
				Z1K1: 'Z2',
				Z2K1: { Z1K1: 'Z6', Z6K1: 'Z401' },
				Z2K2: readJSON( testObjectsDir( 'Z401.json' ) )
			} );
			wikiStub.setZId( 'Z430', {
				Z1K1: 'Z2',
				Z2K1: { Z1K1: 'Z6', Z6K1: 'Z430' },
				Z2K2: readJSON( testObjectsDir( 'Z403-bad.json' ) )
			} );
		};

		const genericIf = readJSON( testInputsDir( 'generic-if.json' ) );
		genericIf.Z1802K2 = 'Z430';
		attemptOrchestration(
			/* testName= */ 'generic type validation error: bad list',
			/* functionCall= */ genericIf,
			/* expectedResultFile= */ null,
			/* expectedErrorFile= */ testOutputsDir( 'bad_generic_list_expected.json' ),
			setUpStubs,
			/* expectedExtraMetadata= */[],
			// Error gets returned before implementation is selected
			/* expectedMissingMetadata= */[ 'implementationId', 'implementationType' ],
			/* implementationSelector= */ null,
			/* doValidate= */ true,
			// TODO (T327412): Re-enable this test once type comparison is stricter.
			/* skip= */ true
		);
	}

	{
		const setUpStubs = () => {
			wikiStub.setZId( 'Z401', {
				Z1K1: 'Z2',
				Z2K1: { Z1K1: 'Z6', Z6K1: 'Z401' },
				Z2K2: readJSON( testObjectsDir( 'Z401-again.json' ) )
			} );
			wikiStub.setZId( 'Z430', {
				Z1K1: 'Z2',
				Z2K1: { Z1K1: 'Z6', Z6K1: 'Z430' },
				Z2K2: readJSON( testObjectsDir( 'Z430-bad-again.json' ) )
			} );
		};

		const genericPair = readJSON( testInputsDir( 'generic-pair.json' ) );
		genericPair.Z1802K2 = 'Z430';
		attemptOrchestration(
			/* testName= */ 'generic type validation error: bad pair',
			/* functionCall= */ genericPair,
			/* expectedResultFile= */ null,
			/* expectedErrorFile= */ testOutputsDir( 'bad_generic_pair_expected.json' ),
			setUpStubs,
			/* expectedExtraMetadata= */[],
			// Error gets returned before implementation is selected
			/* expectedMissingMetadata= */[ 'implementationId', 'implementationType' ],
			/* implementationSelector= */ null,
			/* doValidate= */ true,
			// TODO (T327412): Re-enable this test once type comparison is stricter.
			/* skip= */ true
		);
	}

	{
		const setUpStubs = () => {
			wikiStub.setZId( 'Z401', {
				Z1K1: 'Z2',
				Z2K1: { Z1K1: 'Z6', Z6K1: 'Z401' },
				Z2K2: readJSON( testObjectsDir( 'Z401.json' ) )
			} );
			wikiStub.setZId( 'Z403', {
				Z1K1: 'Z2',
				Z2K1: { Z1K1: 'Z6', Z6K1: 'Z403' },
				Z2K2: readJSON( testObjectsDir( 'Z403.json' ) )
			} );
		};
		const genericIf = readJSON( testInputsDir( 'generic-if.json' ) );
		genericIf.Z1802K2 = 'Z403';
		attemptOrchestration(
			/* testName= */ 'generic if',
			/* functionCall= */ genericIf,
			/* expectedResultFile= */ testOutputsDir( 'Z403-expanded.json' ),
			/* expectedErrorFile = */ null,
			setUpStubs
		);
	}

	{
		const setUpStubs = () => {
			wikiStub.setZId( 'Z401', {
				Z1K1: 'Z2',
				Z2K1: { Z1K1: 'Z6', Z6K1: 'Z401' },
				Z2K2: readJSON( testObjectsDir( 'Z401-again.json' ) )
			} );
			wikiStub.setZId( 'Z403', {
				Z1K1: 'Z2',
				Z2K1: { Z1K1: 'Z6', Z6K1: 'Z403' },
				Z2K2: readJSON( testObjectsDir( 'Z403-again.json' ) )
			} );
		};
		const genericPair = readJSON( testInputsDir( 'generic-pair.json' ) );
		genericPair.Z1802K2 = 'Z403';
		attemptOrchestration(
			/* testName= */ 'generic pair',
			/* functionCall= */ genericPair,
			/* expectedResultFile= */ testOutputsDir( 'Z403-expanded-again.json' ),
			/* expectedErrorFile= */ null,
			setUpStubs
		);
	}

	{
		const setUpStubs = () => {
			wikiStub.setZId( 'Z401', {
				Z1K1: 'Z2',
				Z2K1: { Z1K1: 'Z6', Z6K1: 'Z401' },
				Z2K2: readJSON( testObjectsDir( 'Z401-map.json' ) )
			} );
			wikiStub.setZId( 'Z403', {
				Z1K1: 'Z2',
				Z2K1: { Z1K1: 'Z6', Z6K1: 'Z403' },
				Z2K2: readJSON( testObjectsDir( 'Z403-map.json' ) )
			} );
			wikiStub.setZId( 'Z411', {
				Z1K1: 'Z2',
				Z2K1: { Z1K1: 'Z6', Z6K1: 'Z411' },
				Z2K2: readJSON( testObjectsDir( 'Z411-map.json' ) )
			} );
			wikiStub.setZId( 'Z421', {
				Z1K1: 'Z2',
				Z2K1: { Z1K1: 'Z6', Z6K1: 'Z421' },
				Z2K2: readJSON( testObjectsDir( 'Z421-map.json' ) )
			} );
		};
		const genericMap = readJSON( testInputsDir( 'generic-map.json' ) );
		genericMap.Z1802K2 = 'Z403';
		attemptOrchestration(
			/* testName= */ 'generic map',
			/* functionCall= */ genericMap,
			/* expectedResultFile= */ testOutputsDir( 'Z403-map-expanded.json' ),
			/* expectedErrorFile= */ null,
			setUpStubs
		);
	}

	{
		const functionCall = readJSON( testInputsDir( 'echo-ephemeral.json' ) );
		attemptOrchestration(
			/* testName= */ 'ephemeral resolution test',
			/* functionCall= */ functionCall,
			/* expectedResultFile= */ testOutputsDir( 'echo-ephemeral-expanded.json' )
		);
	}

} );
