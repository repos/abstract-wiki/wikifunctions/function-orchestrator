'use strict';

const assert = require( '../../../utils/assert.js' );
const { readJSON } = require( '../../../../src/utils.js' );
const { testObjectsDir, testOutputsDir } = require( '../../../utils/testFileUtils.js' );
const { attemptOrchestration } = require( '../mswTestRunner.js' );
const { getServerWithMocks } = require( '../../../utils/mockUtils.js' );

describe( 'mockedServices – fetch tests', () => {

	const { wikiStub, mockServiceWorker } = getServerWithMocks();
	let resolutionCounts, resolutionExpectations;

	before( () => {
		mockServiceWorker.listen();
	} );

	afterEach( () => {
		for ( const [ ZID, expectedCount ] of resolutionExpectations ) {
			assert.deepEqual( resolutionCounts.get( ZID ), expectedCount );
		}
		wikiStub.reset();
		mockServiceWorker.resetHandlers();
	} );

	after( () => {
		mockServiceWorker.close();
	} );

	{
		resolutionCounts = new Map();
		resolutionExpectations = new Map( [
			[ 'Z400', 1 ]
		] );
		resolutionCounts.set( 'Z400', 0 );
		const incrementResolutions = function () {
			const currentCount = resolutionCounts.get( 'Z400' );
			resolutionCounts.set( 'Z400', currentCount + 1 );
		};
		const setUpStubs = () => {
			console.log( 'gonna call setZId' );
			wikiStub.setZId(
				'Z400',
				{
					Z1K1: 'Z2',
					Z2K1: { Z1K1: 'Z6', Z6K1: 'Z400' },
					Z2K2: readJSON( testObjectsDir( 'Z400_type.json' ) )
				},
				/* error= */ false,
				/* ephemeral= */ true,
				/* delay= */ null,
				/* retrievalCallback= */ incrementResolutions
			);
		};
		const functionCall = {
			Z1K1: 'Z7',
			Z7K1: 'Z801',
			Z801K1: [
				'Z1',
				{
					Z1K1: 'Z400',
					Z400K1: 'noooo'
				},
				{
					Z1K1: 'Z400',
					Z400K1: 'the horror'
				},
				{
					Z1K1: 'Z400',
					Z400K1: 'THE HORROR'
				}
			]
		};
		attemptOrchestration(
			/* testName= */ 'only retrieves once',
			/* functionCall= */ functionCall,
			/* expectedResultFile= */ testOutputsDir( 'only-retrieve-once.json' ),
			/* expectedErrorFile= */ null,
			setUpStubs
		);
	}

} );
