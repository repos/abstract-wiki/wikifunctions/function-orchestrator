'use strict';

const assert = require( '../../../utils/assert.js' );

const {
	makeMappedResultEnvelope
} = require( '../../../../function-schemata/javascript/src/utils.js' );
const { isZ24, readJSON } = require( '../../../../src/utils.js' );
const { orchestrate } = require( '../../../../src/orchestrate.js' );

const { getServerWithMocks, getTestInvariants, EVAL_URI, WIKI_URI, WIKIDATA_URI } = require( '../../../utils/mockUtils.js' );
const { getFunctionCallForExpensiveTest, setStubsForExpensiveTest, testDataDir, testWikidataDir, writeJSON } = require( '../../../utils/testFileUtils.js' );
const { InvariantsWithTrace, wrapFunctionsWithTrace, wrapInvariantsMemberFunctions } = require( '../../../utils/visualizeCallTrees.js' );

describe( 'visualize function execution tree tests', () => {

	let writeTrees = false;

	const {
		evaluatorStub, mockServiceWorker, wikiStub, wikidataStub
	} = getServerWithMocks();

	before( () => {
		wrapFunctionsWithTrace();
		mockServiceWorker.listen();
		for ( const argument of process.argv ) {
			if ( argument === '--write-trees' ) {
				writeTrees = true;
				break;
			}
		}
	} );

	afterEach( () => {
		evaluatorStub.reset();
		wikiStub.reset();
		wikidataStub.reset();
		mockServiceWorker.resetHandlers();
	} );

	after( () => {
		mockServiceWorker.close();
	} );

	async function runTest( functionCall, expectedSize, fileName ) {
		const oldInvariants = getTestInvariants(
			{
				doValidate: true,
				remainingTime: 15,
				evalUri: EVAL_URI,
				wikiUri: WIKI_URI,
				wikidataUri: WIKIDATA_URI,
				requestId: 'visualization',
				useWikidata: true
			}
		);
		const invariants = new InvariantsWithTrace(
			/* traceTree= */ null,
			oldInvariants.resolver_,
			oldInvariants.evaluators_,
			oldInvariants.orchestratorConfig_,
			oldInvariants.getRemainingTime_,
			oldInvariants.requestId_,
			oldInvariants.logger_,
			oldInvariants.req_,
			oldInvariants.rateLimiter_
		);
		wrapInvariantsMemberFunctions( invariants );
		const result = await orchestrate( functionCall, invariants );
		assert.deepEqual( isZ24( result.Z22K1 ), false );

		const tree = invariants.getTree();
		tree.calculateSizes();
		if ( writeTrees ) {
			writeJSON( tree.asJSON(), fileName );
		}
		assert.deepEqual( expectedSize, tree.getSize() );
	}

	it( 'debug trace for simple Echo', async () => {
		const functionCall = {
			Z1K1: 'Z7',
			Z7K1: 'Z801',
			Z801K1: 'Z801'
		};
		await runTest( functionCall, 19, 'simple_echo_debug_trace.json' );
	} );

	it( 'debug trace when retrieving and LID', async () => {
		const functionCall = {
			Z1K1: 'Z7',
			Z7K1: 'Z6825',
			Z6825K1: {
				Z1K1: 'Z6095',
				Z6095K1: 'L3435'
			}
		};
		const L3435 = readJSON( testWikidataDir( 'L3435-multiple-lemmas.json' ) );
		wikidataStub.setEntityId( 'L3435', L3435 );
		await runTest( functionCall, 49, 'retrieve_lid_debug_trace.json' );
	} );

	it( 'debug trace for english indefinite noun phrase - expensive call', async () => {
		const thisTestDir = testDataDir( 'english_indefinite_noun_phrase' );
		const functionCall = getFunctionCallForExpensiveTest( thisTestDir );
		setStubsForExpensiveTest( thisTestDir, wikiStub, wikidataStub );
		evaluatorStub.setZId( 'Z407', () => makeMappedResultEnvelope( {
			Z1K1: 'Z6',
			Z6K1: 'a'
		} ) );
		evaluatorStub.setZId( 'Z405', () => makeMappedResultEnvelope( {
			Z1K1: 'Z6',
			Z6K1: 'spherical'
		} ) );
		evaluatorStub.setZId( 'Z402', () => makeMappedResultEnvelope( {
			Z1K1: 'Z6',
			Z6K1: 'onion'
		} ) );
		await runTest( functionCall, 204, 'english_indefinite_noun_expensive_debug_trace.json' );
	} );

} );
