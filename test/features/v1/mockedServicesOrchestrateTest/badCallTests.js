'use strict';

const { testInputsDir, testOutputsDir } = require( '../../../utils/testFileUtils.js' );
const { attemptOrchestration } = require( '../mswTestRunner.js' );
const { readJSON } = require( '../../../../src/utils.js' );
const { getServerWithMocks } = require( '../../../utils/mockUtils.js' );

describe( 'mockedServices – Bad call tests', () => {

	const { mockServiceWorker } = getServerWithMocks();

	before( () => {
		mockServiceWorker.listen();
	} );

	afterEach( () => {
		mockServiceWorker.resetHandlers();
	} );

	after( () => {
		mockServiceWorker.close();
	} );

	attemptOrchestration(
		/* testName= */ 'well-formed empty Z6 string',
		/* functionCall= */ { Z1K1: 'Z6', Z6K1: '' },
		/* expectedResultFile= */ null,
		/* expectedErrorFile= */ testOutputsDir( 'bad-call-test-well-formed-empty-Z6-string.json' ),
		/* setUpStubs= */ null,
		/* expectedExtraMetadata= */ [],
		/* expectedMissingMetadata= */ [ 'implementationId', 'implementationType' ]
	);

	attemptOrchestration(
		/* testName= */ 'return string literal',
		/* functionCall= */ 'Hello',
		/* expectedResultFile= */ null,
		/* expectedErrorFile= */ testOutputsDir( 'bad-call-test-return-string-literal.json' ),
		/* setUpStubs= */ null,
		/* expectedExtraMetadata= */ [],
		/* expectedMissingMetadata= */ [ 'implementationId', 'implementationType' ]
	);

	attemptOrchestration(
		/* testName= */ 'return string literal with space',
		/* functionCall= */ 'Hello World!',
		/* expectedResultFile= */ null,
		/* expectedErrorFile= */ testOutputsDir( 'bad-call-test-return-string-literal-with-space.json' ),
		/* setUpStubs= */ null,
		/* expectedExtraMetadata= */ [],
		/* expectedMissingMetadata= */ [ 'implementationId', 'implementationType' ]
	);

	attemptOrchestration(
		/* testName= */ 'empty Z6 string',
		/* functionCall= */ '',
		/* expectedResultFile= */ null,
		/* expectedErrorFile= */ testOutputsDir( 'bad-call-test-empty-Z6-string.json' ),
		/* setUpStubs= */ null,
		/* expectedExtraMetadata= */ [],
		/* expectedMissingMetadata= */ [ 'implementationId', 'implementationType' ]
	);

	attemptOrchestration(
		/* testName= */ 'messy string',
		/* functionCall= */ 'This is a [basic] complicated test {string}!',
		/* expectedResultFile= */ null,
		/* expectedErrorFile= */ testOutputsDir( 'bad-call-test-messy-string.json' ),
		/* setUpStubs= */ null,
		/* expectedExtraMetadata= */ [],
		/* expectedMissingMetadata= */ [ 'implementationId', 'implementationType' ]
	);

	attemptOrchestration(
		/* testName= */ 'empty list',
		/* functionCall= */ [],
		/* expectedResultFile= */ null,
		/* expectedErrorFile= */ testOutputsDir( 'bad-call-test-empty-list.json' ),
		/* setUpStubs= */ null,
		/* expectedExtraMetadata= */ [],
		/* expectedMissingMetadata= */ [ 'implementationId', 'implementationType' ]
	);

	attemptOrchestration(
		/* testName= */ 'singleton list of strings',
		/* functionCall= */ [ 'Test' ],
		/* expectedResultFile= */ null,
		/* expectedErrorFile= */ testOutputsDir( 'bad-call-test-singleton-list-of-strings.json' ),
		/* setUpStubs= */ null,
		/* expectedExtraMetadata= */ [],
		/* expectedMissingMetadata= */ [ 'implementationId', 'implementationType' ]
	);

	attemptOrchestration(
		/* testName= */ 'string multiple list',
		/* functionCall= */ [ 'Test', 'Test2', 'Test3' ],
		/* expectedResultFile= */ null,
		/* expectedErrorFile= */ testOutputsDir( 'bad-call-test-string-multiple-list.json' ),
		/* setUpStubs= */ null,
		/* expectedExtraMetadata= */ [],
		/* expectedMissingMetadata= */ [ 'implementationId', 'implementationType' ]
	);

	attemptOrchestration(
		/* testName= */ 'record singleton list',
		/* functionCall= */ [ { Z1K1: 'Z60', Z2K1: 'Test' } ],
		/* expectedResultFile= */ null,
		/* expectedErrorFile= */ testOutputsDir( 'bad-call-test-record-singleton-list.json' ),
		/* setUpStubs= */ null,
		/* expectedExtraMetadata= */ [],
		/* expectedMissingMetadata= */ [ 'implementationId', 'implementationType' ]
	);

	attemptOrchestration(
		/* testName= */ 'simple double-quoted string',
		/* functionCall= */ '"test"',
		/* expectedResultFile= */ null,
		/* expectedErrorFile= */ testOutputsDir( 'bad-call-test-simple-double-quoted-string.json' ),
		/* setUpStubs= */ null,
		/* expectedExtraMetadata= */ [],
		/* expectedMissingMetadata= */ [ 'implementationId', 'implementationType' ]
	);

	attemptOrchestration(
		/* testName= */ 'empty double-quoted string',
		/* functionCall= */ '""',
		/* expectedResultFile= */ null,
		/* expectedErrorFile= */ testOutputsDir( 'bad-call-test-empty-double-quoted-string.json' ),
		/* setUpStubs= */ null,
		/* expectedExtraMetadata= */ [],
		/* expectedMissingMetadata= */ [ 'implementationId', 'implementationType' ]
	);

	attemptOrchestration(
		/* testName= */ 'well formed Z6 object as string',
		/* functionCall= */ '{ "Z1K1": "Z6", "Z6K1": "" }',
		/* expectedResultFile= */ null,
		/* expectedErrorFile= */ testOutputsDir( 'bad-call-test-well-formed-Z6-object-as-string.json' ),
		/* setUpStubs= */ null,
		/* expectedExtraMetadata= */ [],
		/* expectedMissingMetadata= */ [ 'implementationId', 'implementationType' ]
	);

	attemptOrchestration(
		/* testName= */ 'messy double-quoted string',
		/* functionCall= */ '"This is a [basic] complicated test {string}!"',
		/* expectedResultFile= */ null,
		/* expectedErrorFile= */ testOutputsDir( 'bad-call-test-messy-double-quoted-string.json' ),
		/* setUpStubs= */ null,
		/* expectedExtraMetadata= */ [],
		/* expectedMissingMetadata= */ [ 'implementationId', 'implementationType' ]
	);

	attemptOrchestration(
		/* testName= */ 'string empty list',
		/* functionCall= */ '[]',
		/* expectedResultFile= */ null,
		/* expectedErrorFile= */ testOutputsDir( 'bad-call-test-string-empty-list.json' ),
		/* setUpStubs= */ null,
		/* expectedExtraMetadata= */ [],
		/* expectedMissingMetadata= */ [ 'implementationId', 'implementationType' ]
	);

	attemptOrchestration(
		/* testName= */ 'string singleton list of strings',
		/* functionCall= */ '["Test"]',
		/* expectedResultFile= */ null,
		/* expectedErrorFile= */ testOutputsDir( 'bad-call-test-string-singleton-list-of-strings.json' ),
		/* setUpStubs= */ null,
		/* expectedExtraMetadata= */ [],
		/* expectedMissingMetadata= */ [ 'implementationId', 'implementationType' ]
	);

	{
		attemptOrchestration(
			/* testName= */ 'input to Z804: missing keys',
			/* functionCall= */ readJSON( testInputsDir( 'Z804_missing_keys.json' ) ),
			/* expectedResultFile= */ null,
			/* expectedErrorFile= */ testOutputsDir( 'Z804_missing_keys_expected.json' )
		);
	}

} );
