'use strict';

const { readJSON } = require( '../../../../src/utils.js' );
const { testInputsDir, testOutputsDir, testObjectsDir } = require( '../../../utils/testFileUtils.js' );
const { getServerWithMocks } = require( '../../../utils/mockUtils.js' );
const { attemptOrchestration } = require( '../mswTestRunner.js' );

describe( 'mockedServices – Types tests', () => {

	const { wikiStub, mockServiceWorker } = getServerWithMocks();

	before( () => {
		mockServiceWorker.listen();
	} );

	afterEach( () => {
		wikiStub.reset();
		mockServiceWorker.resetHandlers();
	} );

	after( () => {
		mockServiceWorker.close();
	} );

	{
		const setUpStubs = () => {
			const Z498 = readJSON( testObjectsDir( 'Z498-type-with-identity.json' ) );
			wikiStub.setZId( 'Z498', {
				Z1K1: 'Z2',
				Z2K1: { Z1K1: 'Z6', Z6K1: 'Z498' },
				Z2K2: Z498
			} );
			const Z499 = readJSON( testObjectsDir( 'Z499-object-with-identity.json' ) );
			wikiStub.setZId( 'Z499', {
				Z1K1: 'Z2',
				Z2K1: { Z1K1: 'Z6', Z6K1: 'Z499' },
				Z2K2: Z499
			} );
		};
		const theFunctionCall = readJSON( testInputsDir( 'identity-field-not-expanded.json' ) );
		attemptOrchestration(
			/* testName= */ 'identity field is not expanded',
			/* functionCall= */ theFunctionCall,
			/* expectedResultFile= */ testOutputsDir( 'identity-field-not-expanded_expected.json' ),
			/* expectedErrorFile= */ null,
			setUpStubs
		);
	}

} );
