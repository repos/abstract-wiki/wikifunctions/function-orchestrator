'use strict';

const { readJSON } = require( '../../../../src/utils.js' );
const { testInputsDir, testOutputsDir } = require( '../../../utils/testFileUtils.js' );
const { attemptOrchestration } = require( '../mswTestRunner.js' );
const { getServerWithMocks } = require( '../../../utils/mockUtils.js' );

describe( 'mockedServices – Type error tests', () => {

	const { mockServiceWorker } = getServerWithMocks();

	before( () => {
		mockServiceWorker.listen();
	} );

	afterEach( () => {
		mockServiceWorker.resetHandlers();
	} );

	after( () => {
		mockServiceWorker.close();
	} );

	attemptOrchestration(
		/* testName= */ 'argument type error: argument type does not match declared type',
		/* functionCall= */ readJSON( testInputsDir( 'invalid_call_argument_not_of_declared_type.json' ) ),
		/* expectedResultFile= */ null,
		/* expectedErrorFile= */ testOutputsDir( 'invalid_call_argument_not_of_declared_type_expected.json' ),
		/* setUpStubs= */ null,
		/* expectedExtraMetadata= */ [],
		/* expectedMissingMetadata= */[ 'implementationId' ]
	);

	attemptOrchestration(
		/* testName= */ 'return value type error: return value type does not match declared type',
		/* functionCall= */ readJSON( testInputsDir( 'invalid_call_return_value_not_of_declared_type.json' ) ),
		/* expectedResultFile= */ null,
		/* expectedErrorFile= */ testOutputsDir( 'invalid_call_return_value_not_of_declared_type_expected.json' ),
		/* setUpStubs= */ null,
		/* expectedExtraMetadata= */ [],
		/* expectedMissingMetadata= */[ 'implementationId', 'implementationType' ]
	);

	{
		const mapCall = readJSON( testInputsDir( 'invalid_key_type_passed_to_Z883.json' ) );
		mapCall.Z883K1 = 'Z1';
		attemptOrchestration(
			/* testName= */ 'argument value error: invalid value for Z883K1 / key type passed to Z883 / Typed Map',
			/* functionCall= */ mapCall,
			/* expectedResultFile= */ null,
			/* expectedErrorFile= */ testOutputsDir( 'invalid_key_type_passed_to_Z883_expected.json' )
		);
	}

	attemptOrchestration(
		/* testName= */ 'input to composition type error: static validation is skipped',
		/* functionCall= */ readJSON( testInputsDir( 'skips_static_validation.json' ) ),
		/* expectedResultFile= */ null,
		/* expectedErrorFile= */ testOutputsDir( 'skips_static_validation_expected.json' ),
		/* setUpStubs= */ null,
		/* expectedExtraMetadata= */[],
		/* expectedMissingMetadata= */[ 'implementationId' ],
		/* implementationSelector= */ null,
		// TODO (T327413): Should be false? What is this testing?
		/* doValidate= */ true,
		// TODO (T327412): Re-enable this test once type comparison is stricter.
		/* skip= */ true
	);

} );
