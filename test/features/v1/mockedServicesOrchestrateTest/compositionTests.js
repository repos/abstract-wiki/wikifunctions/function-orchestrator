'use strict';

const {
	makeMappedResultEnvelope
} = require( '../../../../function-schemata/javascript/src/utils.js' );
const { readJSON } = require( '../../../../src/utils.js' );
const { testInputsDir, testOutputsDir, testObjectsDir } = require( '../../../utils/testFileUtils.js' );
const { getServerWithMocks } = require( '../../../utils/mockUtils.js' );
const { attemptOrchestration } = require( '../mswTestRunner.js' );

// For the 'scope is attached properly to all elements of list in composition' test.
function Z456( theZList, theObject ) {
	let prev = null;
	let result = theZList;
	let curr = theZList;
	while ( curr !== undefined ) {
		if ( curr.K1 === theObject ) {
			if ( prev === null ) {
				result = curr.K2;
			} else {
				prev.K2 = curr.K2;
			}
			break;
		}
		prev = curr;
		curr = curr.K2;
	}
	return makeMappedResultEnvelope( result, null );
}

describe( 'mockedServices – Composition call tests', () => {

	const { evaluatorStub, wikiStub, mockServiceWorker } = getServerWithMocks();

	before( () => {
		mockServiceWorker.listen();
	} );

	afterEach( () => {
		wikiStub.reset();
		evaluatorStub.reset();
		mockServiceWorker.resetHandlers();
	} );

	after( () => {
		mockServiceWorker.close();
	} );

	{
		const setUpStubs = () => {
			const Z422 = readJSON( testObjectsDir( 'Z422.json' ) );
			wikiStub.setZId( 'Z422', {
				Z1K1: 'Z2',
				Z2K1: { Z1K1: 'Z6', Z6K1: 'Z422' },
				Z2K2: Z422
			} );
		};
		const theFunctionCall = readJSON( testInputsDir( 'composition-returns-type.json' ) );
		const returnedType = readJSON( testObjectsDir( 'type-returned-by-composition.json' ) );
		// Set the argument to the composition (which internally calls "echo").
		theFunctionCall.Z7K1.Z8K4[ 1 ].Z14K2.Z801K1 = { ...returnedType };
		attemptOrchestration(
			/* testName= */ 'composition returns type',
			/* functionCall= */ theFunctionCall,
			/* expectedResultFile= */ testOutputsDir( 'composition-returns-type_expected.json' ),
			/* expectedErrorFile= */ null,
			setUpStubs,
			/* expectedExtraMetadata= */[],
			/* expectedMissingMetadata= */[ 'implementationId' ]
		);
	}

	{
		const setUpStubs = () => {
			const Z400 = readJSON( testInputsDir( 'generic-composition.json' ) );
			wikiStub.setZId( 'Z400', {
				Z1K1: 'Z2',
				Z2K1: { Z1K1: 'Z6', Z6K1: 'Z400' },
				Z2K2: Z400
			} );
			const Z401 = readJSON( testInputsDir( 'generic-composition-implementation.json' ) );
			wikiStub.setZId( 'Z401', {
				Z1K1: 'Z2',
				Z2K1: { Z1K1: 'Z6', Z6K1: 'Z401' },
				Z2K2: Z401
			} );
		};

		// A type containing K1: list of strings and K2: Boolean.
		const theType = {
			Z1K1: 'Z7',
			Z7K1: 'Z400',
			Z400K1: {
				Z1K1: 'Z7',
				Z7K1: 'Z881',
				Z881K1: 'Z6'
			},
			Z400K2: 'Z40'
		};

		// The input has the above-specified type.
		const goodInput = {
			Z1K1: theType,
			K1: [ 'Z6' ],
			K2: {
				Z1K1: 'Z40',
				Z40K1: 'Z42'
			}
		};

		// Call <Echo> (Z801) on the input.
		const goodFunctionCall = {
			Z1K1: 'Z7',
			Z7K1: {
				Z1K1: 'Z8',
				Z8K1: [
					'Z17'
				],
				Z8K2: theType,
				Z8K3: [ 'Z20' ],
				Z8K4: [
					'Z14',
					{
						Z1K1: 'Z14',
						Z14K1: 'Z402',
						Z14K2: {
							Z1K1: 'Z7',
							Z7K1: 'Z801',
							Z801K1: goodInput
						}
					}
				],
				Z8K5: 'Z402'
			}
		};

		attemptOrchestration(
			/* testName= */ 'good generic defined as composition',
			/* functionCall= */ goodFunctionCall,
			/* expectedResultFile= */ testOutputsDir( 'type-returned-by-generic-composition.json' ),
			/* expectedErrorFile= */ null,
			setUpStubs,
			/* expectedExtraMetadata= */[],
			/* expectedMissingMetadata= */[ 'implementationId' ]
		);

		// The input has the above-specified type but fails to be one.
		const badInput = {
			Z1K1: theType,
			K1: 'Not a list of Z6',
			K2: {
				Z1K1: 'Z40',
				Z40K1: 'Z42'
			}
		};

		// Call <Echo> (Z801) on the input.
		const badFunctionCall = {
			Z1K1: 'Z7',
			Z7K1: {
				Z1K1: 'Z8',
				Z8K1: [
					'Z17'
				],
				Z8K2: theType,
				Z8K3: [ 'Z20' ],
				Z8K4: [
					'Z14',
					{
						Z1K1: 'Z14',
						Z14K1: 'Z50002',
						Z14K2: {
							Z1K1: 'Z7',
							Z7K1: 'Z801',
							Z801K1: badInput
						}
					}
				],
				Z8K5: 'Z50002'
			}
		};

		attemptOrchestration(
			/* testName= */ 'bad generic defined as composition',
			/* functionCall= */ badFunctionCall,
			/* expectedResultFile= */ null,
			/* expectedErrorFile= */ testOutputsDir( 'bad_generic_composition_expected.json' ),
			setUpStubs,
			/* expectedExtraMetadata= */[],
			/* expectedMissingMetadata= */[ 'implementationId' ],
			/* implementationSelector= */ null,
			/* doValidate= */ true,
			// TODO (T327412): Re-enable this test once type comparison is stricter.
			/* skip= */ true
		);
	}

	{
		const setUpStubs = () => {
			wikiStub.setZId( 'Z437', readJSON( testObjectsDir( 'all_Z437.json' ) ) );
		};
		attemptOrchestration(
			/* testName= */ 'composition of all empty',
			/* functionCall= */ readJSON( testInputsDir( 'all_empty.json' ) ),
			/* expectedResultFile= */ testOutputsDir( 'all_empty_expected.json' ),
			/* expectedErrorFile= */ null,
			setUpStubs,
			/* expectedExtraMetadata= */[],
			/* expectedMissingMetadata= */[ 'implementationId' ]
		);
		attemptOrchestration(
			/* testName= */ 'composition of all: [true, true]',
			/* functionCall= */ readJSON( testInputsDir( 'all_true_true.json' ) ),
			/* expectedResultFile= */ testOutputsDir( 'all_true_true_expected.json' ),
			/* expectedErrorFile= */ null,
			setUpStubs,
			/* expectedExtraMetadata= */[],
			/* expectedMissingMetadata= */[ 'implementationId' ]
		);
		attemptOrchestration(
			/* testName= */ 'composition of all: [true, false]',
			/* functionCall= */ readJSON( testInputsDir( 'all_true_false.json' ) ),
			/* expectedResultFile= */ testOutputsDir( 'all_true_false_expected.json' ),
			/* expectedErrorFile= */ null,
			setUpStubs,
			/* expectedExtraMetadata= */[],
			/* expectedMissingMetadata= */[ 'implementationId' ]
		);
	}

	{
		const setUpStubs = () => {
			for ( const ZID of [ 'Z454', 'Z455', 'Z456', 'Z458', 'Z464' ] ) {
				wikiStub.setZId( ZID, readJSON( testObjectsDir( 'scope-attachment-' + ZID + '.json' ) ) );
			}
			evaluatorStub.setZId( 'Z456', ( zobject ) => {
				const listInput = zobject.functionArguments.Z456K1.object;
				const objectInput = zobject.functionArguments.Z456K2.object;
				return Z456( listInput, objectInput );
			} );
		};
		attemptOrchestration(
			/* testName= */ 'scope is attached properly to all elements of list in composition',
			/* functionCall= */ readJSON( testInputsDir( 'scope-attachment-call.json' ) ),
			/* expectedResultFile= */ testOutputsDir( 'scope-attachment-expected.json' ),
			/* expectedErrorFile= */ null,
			/* setUpStubs= */ setUpStubs
		);
	}

	attemptOrchestration(
		/* testName= */ 'composition',
		/* functionCall= */ readJSON( testInputsDir( 'composition.json' ) ),
		/* expectedResultFile= */ testOutputsDir( 'expected_composition.json' ),
		/* expectedErrorFile= */ null,
		/* setUpStubs= */ null,
		/* expectedExtraMetadata= */[],
		/* expectedMissingMetadata= */[ 'implementationId' ]
	);

	attemptOrchestration(
		/* testName= */ 'composition consisting of an argument reference',
		/* functionCall= */ readJSON( testInputsDir( 'composition_arg_only.json' ) ),
		/* expectedResultFile= */ testOutputsDir( 'expected_composition_arg_only.json' ),
		/* expectedErrorFile= */ null,
		/* setUpStubs= */ null,
		/* expectedExtraMetadata= */[],
		/* expectedMissingMetadata= */[ 'implementationId' ]
	);

	attemptOrchestration(
		/* testName= */ 'composition consisting of an argument reference again',
		/* functionCall= */ readJSON( testInputsDir( 'composition_arg_only_false.json' ) ),
		/* expectedResultFile= */ testOutputsDir( 'expected_composition_arg_only_false.json' ),
		/* expectedErrorFile= */ null,
		/* setUpStubs= */ null,
		/* expectedExtraMetadata= */[],
		/* expectedMissingMetadata= */[ 'implementationId' ]
	);

} );
