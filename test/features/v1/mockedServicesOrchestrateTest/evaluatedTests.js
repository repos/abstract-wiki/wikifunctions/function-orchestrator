'use strict';

const { makeMappedResultEnvelope } = require( '../../../../function-schemata/javascript/src/utils.js' );
const { readJSON, makeBoolean } = require( '../../../../src/utils.js' );
const { testInputsDir, testOutputsDir } = require( '../../../utils/testFileUtils.js' );
const { attemptOrchestration } = require( '../mswTestRunner.js' );
const { getServerWithMocks } = require( '../../../utils/mockUtils.js' );

describe( 'mockedServices – Evaluated function call tests', () => {

	const { evaluatorStub, mockServiceWorker } = getServerWithMocks();

	before( () => {
		mockServiceWorker.listen();
	} );

	afterEach( () => {
		evaluatorStub.reset();
		mockServiceWorker.resetHandlers();
	} );

	after( () => {
		mockServiceWorker.close();
	} );

	{
		// It's important NOT to copy this from the JSON file, because the
		// tenuous trustworthiness of JSON.(parse|stringify) is exactly what's
		// at issue here.
		const codeString = "import re\n\tZ440 = lambda x: bool(re.search(r'\\d', x))";
		const setUpStubs = () => {
			evaluatorStub.setCodeString(
				codeString,
				( functionCall ) => {
					const isSame = functionCall.function.codeString === codeString;
					return makeMappedResultEnvelope( makeBoolean( isSame ), null );
				}
			);
		};
		attemptOrchestration(
			/* testName= */ 'original request contains forbidden escape sequence \\d',
			/* functionCall= */ readJSON( testInputsDir( 'explicit-forbidden-escape-python.json' ) ),
			/* expectedResultFile= */ testOutputsDir( 'explicit-forbidden-escape-python_expected.json' ),
			/* expectedErrorFile= */ null,
			setUpStubs,
			/* expectedExtraMetadata= */ [],
			/* expectedMissingMetadata= */ [ 'implementationId' ]
		);
	}

	// TODO (T371572): Also test the case where the offending object is stored on
	// the wiki, i.e. make one of these implementations accessible via wikiStub.

	{
		// It's important NOT to copy this from the JSON file, because the
		// tenuous trustworthiness of JSON.(parse|stringify) is exactly what's
		// at issue here.
		const codeString = '( a ) => { return a.match( /\\d/g ); }';
		const setUpStubs = () => {
			evaluatorStub.setCodeString(
				codeString,
				( functionCall ) => {

					/* eslint-disable security/detect-eval-with-expression */
					/* eslint-disable no-eval */
					const Z440 = eval( codeString );
					/* eslint-enable no-eval */
					/* eslint-enable security/detect-eval-with-expression */
					const toMatch = functionCall.functionArguments.Z440K1.object.Z6K1;
					const doesMatch = Z440( toMatch ) && true;
					return makeMappedResultEnvelope( makeBoolean( doesMatch ), null );
				}
			);
		};
		attemptOrchestration(
			/* testName= */ 'forbidden escape sequence \\d: can we eval() it???',
			/* functionCall= */ readJSON( testInputsDir( 'explicit-forbidden-escape-js.json' ) ),
			/* expectedResultFile= */ testOutputsDir( 'explicit-forbidden-escape-js_expected.json' ),
			/* expectedErrorFile= */ null,
			setUpStubs,
			/* expectedExtraMetadata= */ [],
			/* expectedMissingMetadata= */ [ 'implementationId' ]
		);
	}

} );
