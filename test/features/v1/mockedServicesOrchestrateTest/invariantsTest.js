'use strict';

const { readJSON } = require( '../../../../src/utils.js' );
const { testInputsDir, testOutputsDir } = require( '../../../utils/testFileUtils.js' );
const { attemptOrchestration } = require( '../mswTestRunner.js' );
const { getServerWithMocks } = require( '../../../utils/mockUtils.js' );
const { makeMappedResultEnvelope } = require( '../../../../function-schemata/javascript/src/utils.js' );
const { FirstImplementationSelector } = require( '../../../../src/implementationSelector.js' );

describe( 'mockedServices – Invariants with Z61 reference objects map to correct evaluator', () => {
	const { evaluatorStub, mockServiceWorker } = getServerWithMocks();

	before( () => {
		mockServiceWorker.listen();
	} );

	afterEach( () => {
		mockServiceWorker.resetHandlers();
	} );

	after( () => {
		mockServiceWorker.close();
	} );

	attemptOrchestration( // TODO [wip]
		/* testName= */ 'NEWREF function call for Z986 (code points to string) with combining characters',
		/* functionCall= */ readJSON( testInputsDir( 'Z986_emoji.json' ) ),
		/* expectedResultFile= */ testOutputsDir( 'Z986_emoji_expected.json' ),
		/* expectedErrorFile= */ null,
		/* setUpStubs= */ null,
		/* expectedExtraMetadata= */ [],
		/* expectedMissingMetadata= */ [],
		/* implementationSelector = */ null,
		/* doValidate = */ true,
		/* skip = */ false,
		/* requestId = */ null,
		/* invariantZIDs = */ [ 'Z610' ]
	);

	attemptOrchestration(
		/* testName= */ 'python - use of string literal: expect error',
		/* functionCall= */ readJSON( testInputsDir( 'evaluated-with-499.json' ) ),
		/* expectedResultFile= */ null,
		/* expectedErrorFile= */ testOutputsDir( 'evaluated-with-499_expected.json' ),
		/* setUpStubs= */ null,
		/* expectedExtraMetadata= */ [],
		// Error gets returned before implementation metadata is recorded
		/* expectedMissingMetadata= */ [ 'implementationId', 'implementationType' ]
	);

	attemptOrchestration(
		/* testName= */ 'python - use of ZObj Reference: expect error',
		/* functionCall= */ readJSON( testInputsDir( 'evaluated-499-using-ref.json' ) ),
		/* expectedResultFile= */ null,
		/* expectedErrorFile= */ testOutputsDir( 'evaluated-with-499_expected.json' ),
		/* setUpStubs= */ null,
		/* expectedExtraMetadata= */ [],
		// Error gets returned before implementation is selected
		// Error gets returned before implementation metadata is recorded
		/* expectedMissingMetadata= */ [ 'implementationId', 'implementationType' ]
	);

	const setUpStubs = () => {
		evaluatorStub.setZId( 'Z410', ( unused ) => makeMappedResultEnvelope( { Z1K1: 'Z6', Z6K1: '13' }, null ) ); // eslint-disable-line no-unused-vars
	};

	attemptOrchestration(
		/* testName= */ 'python - use of ZObj Reference: expect success',
		/* functionCall= */ readJSON( testInputsDir( 'evaluated-using-ref.json' ) ),
		/* expectedResultFile= */ testOutputsDir( 'evaluated-13.json' ),
		/* expectedErrorFile= */ null,
		setUpStubs,
		/* expectedExtraMetadata= */ [],
		/* expectedMissingMetadata= */ [ 'implementationId' ],
		/* implementationSelector= */ new FirstImplementationSelector()
	);
} );
