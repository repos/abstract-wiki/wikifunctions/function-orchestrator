'use strict';

const assert = require( '../../../utils/assert.js' );

const {
	makeMappedResultEnvelope
} = require( '../../../../function-schemata/javascript/src/utils.js' );
const { isZ24, readJSON } = require( '../../../../src/utils.js' );
const { orchestrate } = require( '../../../../src/orchestrate.js' );

const { getServerWithMocks, getTestInvariants, EVAL_URI, WIKI_URI, WIKIDATA_URI } = require( '../../../utils/mockUtils.js' );
const { getFunctionCallForExpensiveTest, setStubsForExpensiveTest, testDataDir, testInputsDir, testObjectsDir, testWikidataDir } = require( '../../../utils/testFileUtils.js' );

// Lexeme L3435, augmented with a few more lemmas.
const L3435 = readJSON( testWikidataDir( 'L3435-multiple-lemmas.json' ) );
const L1882 = readJSON( testWikidataDir( 'L1882.json' ) );
const L1882F4 = readJSON( testWikidataDir( 'L1882-F4-est.json' ) );

const Z19243 = readJSON( testObjectsDir( 'Z19243.json' ) );
const Z19244 = readJSON( testObjectsDir( 'Z19244.json' ) );

class CallCounter {

	constructor( callable ) {
		this.callable_ = callable;
		this.callCount_ = 0;
		Object.defineProperty( this, 'callCount', {
			get: function () {
				return this.callCount_;
			}
		} );
	}

	callIt( ...args ) {
		this.callCount_ += 1;
		return this.callable_( ...args );
	}

}

function getInvariantsAndInjectedFunctions() {
	const { runTypeValidatorDynamic } = require( '../../../../src/validation.js' );
	const { eagerlyEvaluate, resolveDanglingReferences, execute } = require(
		'../../../../src/execute.js' );
	const countedFunctions = [
		runTypeValidatorDynamic, eagerlyEvaluate, resolveDanglingReferences, execute
	].map( ( func ) => new CallCounter( func ) );
	const [
		validateTypesWithCallCount,
		eagerlyEvaluateWithCallCount,
		resolveDanglingReferencesWithCallCount,
		executeWithCallCount
	] = countedFunctions.map( ( counter ) => counter.callIt.bind( counter ) );
	const invariants = getTestInvariants(
		{
			doValidate: true,
			remainingTime: 15,
			evalUri: EVAL_URI,
			wikiUri: WIKI_URI,
			wikidataUri: WIKIDATA_URI,
			requestId: 'call-count-test',
			useWikidata: true,
			validateTypesWithCallCount,
			eagerlyEvaluateWithCallCount,
			resolveDanglingReferencesWithCallCount,
			executeWithCallCount
		}
	);
	const [
		validateCounter, eagerCounter, resolveCounter, executeCounter
	] = countedFunctions;
	return {
		validateCounter, eagerCounter, resolveCounter, executeCounter, invariants
	};
}

describe( 'function call count test', () => {

	const {
		mockServiceWorker, wikiStub, wikidataStub
	} = getServerWithMocks();

	before( () => {
		mockServiceWorker.listen();
	} );

	afterEach( () => {
		wikiStub.reset();
		wikidataStub.reset();
		mockServiceWorker.resetHandlers();
	} );

	after( () => {
		mockServiceWorker.close();
	} );

	it( 'count calls when retrieving an LID', async () => {
		const {
			validateCounter, eagerCounter, resolveCounter, invariants
		} = getInvariantsAndInjectedFunctions();
		const functionCall = {
			Z1K1: 'Z7',
			Z7K1: 'Z6825',
			Z6825K1: {
				Z1K1: 'Z6095',
				Z6095K1: 'L3435'
			}
		};
		wikidataStub.setEntityId( 'L3435', L3435 );
		const envelope = await orchestrate( functionCall, invariants );
		assert.deepEqual( isZ24( envelope.Z22K1 ), false );
		assert.deepEqual( validateCounter.callCount, 1 );
		assert.deepEqual( eagerCounter.callCount, 2 );
		assert.deepEqual( resolveCounter.callCount, 0 );
	} );

	it( 'count calls when echoing a retrieved LID', async () => {
		const {
			validateCounter, eagerCounter, resolveCounter, invariants
		} = getInvariantsAndInjectedFunctions();
		const functionCall = {
			Z1K1: 'Z7',
			Z7K1: {
				Z1K1: 'Z8',
				Z8K1: [
					'Z17'
				],
				Z8K2: 'Z1',
				Z8K3: [
					'Z20'
				],
				Z8K4: [
					'Z14',
					{
						Z1K1: 'Z14',
						Z14K1: 'Z10000',
						Z14K2: {
							Z1K1: 'Z7',
							Z7K1: 'Z801',
							Z801K1: {
								Z1K1: 'Z7',
								Z7K1: 'Z6825',
								Z6825K1: {
									Z1K1: 'Z6095',
									Z6095K1: 'L3435'
								}
							}
						}
					}
				],
				Z8K5: 'Z10000'
			}
		};
		wikidataStub.setEntityId( 'L3435', L3435 );
		const envelope = await orchestrate( functionCall, invariants );
		assert.deepEqual( isZ24( envelope.Z22K1 ), false );
		assert.deepEqual( validateCounter.callCount, 3 );
		assert.deepEqual( eagerCounter.callCount, 4 );
		assert.deepEqual( resolveCounter.callCount, 0 );
	} );

	it( 'count calls when running a simple Echo', async () => {
		const {
			validateCounter, eagerCounter, resolveCounter, invariants
		} = getInvariantsAndInjectedFunctions();
		const functionCall = {
			Z1K1: 'Z7',
			Z7K1: 'Z801',
			Z801K1: 'oye'
		};
		wikidataStub.setEntityId( 'L3435', L3435 );
		const envelope = await orchestrate( functionCall, invariants );
		assert.deepEqual( isZ24( envelope.Z22K1 ), false );
		assert.deepEqual( validateCounter.callCount, 1 );
		assert.deepEqual( eagerCounter.callCount, 1 );
		assert.deepEqual( resolveCounter.callCount, 0 );
	} );

	it( 'count calls when Echoing a community-defined type', async () => {
		const {
			validateCounter, eagerCounter, resolveCounter, invariants
		} = getInvariantsAndInjectedFunctions();
		const integerType = readJSON( testObjectsDir( 'Z400_type.json' ) );
		const integerStoredObject = {
			Z1K1: 'Z2',
			Z2K1: { Z1K1: 'Z6', Z6K1: 'Z444' },
			Z2K2: integerType
		};
		wikiStub.setZId( 'Z400', integerStoredObject );
		const functionCall = {
			Z1K1: 'Z7',
			Z7K1: 'Z801',
			Z801K1: {
				Z1K1: 'Z400',
				Z400K1: '20'
			}
		};
		const envelope = await orchestrate( functionCall, invariants );
		assert.deepEqual( isZ24( envelope.Z22K1 ), false );
		assert.deepEqual( validateCounter.callCount, 1 );
		assert.deepEqual( eagerCounter.callCount, 2 );
		assert.deepEqual( resolveCounter.callCount, 0 );
	} );

	it( 'count calls for Z808/Abstract', async () => {
		const {
			validateCounter, eagerCounter, resolveCounter, invariants
		} = getInvariantsAndInjectedFunctions();
		const functionCall = readJSON( testInputsDir( 'Z808.json' ) );
		const envelope = await orchestrate( functionCall, invariants );
		assert.deepEqual( isZ24( envelope.Z22K1 ), false );
		assert.deepEqual( validateCounter.callCount, 2 );
		assert.deepEqual( eagerCounter.callCount, 24 );
		assert.deepEqual( resolveCounter.callCount, 0 );
	} );

	it( 'count calls for Z810/Cons', async () => {
		const {
			validateCounter, eagerCounter, resolveCounter, invariants
		} = getInvariantsAndInjectedFunctions();
		const functionCall = readJSON( testInputsDir( 'Z810.json' ) );
		const envelope = await orchestrate( functionCall, invariants );
		assert.deepEqual( isZ24( envelope.Z22K1 ), false );

		// This test is weirdly flaky;
		// sometimes this is 2 and sometimes it's 3.
		assert.deepEqual(
			validateCounter.callCount === 2 ||
                validateCounter.callCount === 3, true );
		assert.deepEqual( eagerCounter.callCount, 6 );
		assert.deepEqual( resolveCounter.callCount, 0 );
	} );

	it( 'count calls for Z813/Empty', async () => {
		const {
			validateCounter, eagerCounter, resolveCounter, invariants
		} = getInvariantsAndInjectedFunctions();
		const functionCall = readJSON( testInputsDir( 'Z913_full_benjamin.json' ) );
		const envelope = await orchestrate( functionCall, invariants );
		assert.deepEqual( isZ24( envelope.Z22K1 ), false );
		assert.deepEqual( validateCounter.callCount, 2 );
		assert.deepEqual( eagerCounter.callCount, 5 );
		assert.deepEqual( resolveCounter.callCount, 0 );
	} );

	it( 'count calls for Z823/Get envelope', async () => {
		const {
			validateCounter, eagerCounter, resolveCounter, invariants
		} = getInvariantsAndInjectedFunctions();
		const functionCall = readJSON( testInputsDir( 'Z823.json' ) );
		const envelope = await orchestrate( functionCall, invariants );
		assert.deepEqual( isZ24( envelope.Z22K1 ), false );
		assert.deepEqual( validateCounter.callCount, 14 );
		assert.deepEqual( eagerCounter.callCount, 21 );
		assert.deepEqual( resolveCounter.callCount, 0 );
	} );

	it( 'count calls when referencing a builtin zobject', async () => {
		const {
			validateCounter, eagerCounter, resolveCounter, executeCounter, invariants
		} = getInvariantsAndInjectedFunctions();
		const functionCall = {
			Z1K1: { Z1K1: 'Z9', Z9K1: 'Z7' },
			Z7K1: { Z1K1: 'Z9', Z9K1: 'Z881' },
			Z881K1: { Z1K1: 'Z9', Z9K1: 'Z1' }
		};
		const envelope = await orchestrate( functionCall, invariants );
		assert.deepEqual( isZ24( envelope.Z22K1 ), false );
		assert.deepEqual( eagerCounter.callCount, 2 );
		assert.deepEqual( validateCounter.callCount, 1 );
		assert.deepEqual( eagerCounter.callCount, 2 );
		assert.deepEqual( resolveCounter.callCount, 62 );
		assert.deepEqual( executeCounter.callCount, 15 );
	} );

	it( 'count calls when retrieving LID with a list of lexeme form references', async () => {
		const {
			validateCounter, eagerCounter, resolveCounter, executeCounter, invariants
		} = getInvariantsAndInjectedFunctions();
		const functionCall =
			{
				Z1K1: 'Z7',
				Z7K1: 'Z19243',
				Z19243K1: {
					Z1K1: 'Z7',
					Z7K1: 'Z6825',
					Z6825K1: {
						Z1K1: 'Z6095',
						Z6095K1: 'L1882'
					}
				},
				Z19243K2: [
					'Z6091',
					{
						Z1K1: 'Z6091',
						Z6091K1: 'Q682111'
					},
					{
						Z1K1: 'Z6091',
						Z6091K1: 'Q192613'
					},
					{
						Z1K1: 'Z6091',
						Z6091K1: 'Q110786'
					},
					{
						Z1K1: 'Z6091',
						Z6091K1: 'Q51929074'
					}
				]
			};
		wikiStub.setZId( 'Z19243', Z19243 );
		wikiStub.setZId( 'Z19244', Z19244 );
		wikidataStub.setEntityId( 'L1882-F4', L1882F4 );
		wikidataStub.setEntityId( 'L1882', L1882 );
		await orchestrate( functionCall, invariants );
		assert.deepEqual( validateCounter.callCount, 4 );
		assert.deepEqual( eagerCounter.callCount, 16 );
		assert.deepEqual( resolveCounter.callCount, 1 );
		assert.deepEqual( executeCounter.callCount, 545 );
	} );
} );

describe( 'expensive call count test', () => {

	const {
		evaluatorStub, mockServiceWorker, wikiStub, wikidataStub
	} = getServerWithMocks();

	before( () => {
		mockServiceWorker.listen();
	} );

	afterEach( () => {
		evaluatorStub.reset();
		wikiStub.reset();
		wikidataStub.reset();
		mockServiceWorker.resetHandlers();
	} );

	after( () => {
		mockServiceWorker.close();
	} );

	it( 'english indefinite noun phrase - expensive call', async () => {
		const {
			validateCounter, eagerCounter, resolveCounter, invariants
		} = getInvariantsAndInjectedFunctions();
		const thisTestDir = testDataDir( 'english_indefinite_noun_phrase' );
		const functionCall = getFunctionCallForExpensiveTest( thisTestDir );
		setStubsForExpensiveTest( thisTestDir, wikiStub, wikidataStub );
		evaluatorStub.setZId( 'Z407', () => makeMappedResultEnvelope( {
			Z1K1: 'Z6',
			Z6K1: 'a'
		} ) );
		evaluatorStub.setZId( 'Z405', () => makeMappedResultEnvelope( {
			Z1K1: 'Z6',
			Z6K1: 'spherical'
		} ) );
		evaluatorStub.setZId( 'Z402', () => makeMappedResultEnvelope( {
			Z1K1: 'Z6',
			Z6K1: 'onion'
		} ) );
		const envelope = await orchestrate( functionCall, invariants );
		assert.deepEqual( isZ24( envelope.Z22K1 ), false );
		assert.deepEqual( validateCounter.callCount, 12 );
		assert.deepEqual( eagerCounter.callCount, 31 );
		assert.deepEqual( resolveCounter.callCount, 1 );
	} );

} );
