'use strict';

const { readJSON } = require( '../../../../src/utils.js' );
const { testInputsDir, testOutputsDir } = require( '../../../utils/testFileUtils.js' );
const { attemptOrchestration } = require( '../mswTestRunner.js' );
const { getServerWithMocks } = require( '../../../utils/mockUtils.js' );

describe( 'mockedServices – Validation tests', () => {

	const { mockServiceWorker } = getServerWithMocks();

	before( () => {
		mockServiceWorker.listen();
	} );

	afterEach( () => {
		mockServiceWorker.resetHandlers();
	} );

	after( () => {
		mockServiceWorker.close();
	} );

	attemptOrchestration(
		/* testName= */ 'validation error: invalid argument key for function call',
		/* functionCall= */ readJSON( testInputsDir( 'invalid_call_argument_key.json' ) ),
		/* expectedResultFile= */ null,
		/* expectedErrorFile= */ testOutputsDir( 'invalid_call_argument_key_expected.json' ),
		/* setUpStubs= */ null,
		/* expectedExtraMetadata= */ [],
		// Error gets returned before implementation is selected
		/* expectedMissingMetadata= */ [ 'implementationId', 'implementationType' ]
	);

	attemptOrchestration(
		/* testName= */ 'validation error: invalid argument type for function call',
		/* functionCall= */ readJSON( testInputsDir( 'invalid_call_argument_type.json' ) ),
		/* expectedResultFile= */ null,
		/* expectedErrorFile= */ testOutputsDir( 'invalid_call_argument_type_expected.json' ),
		/* setUpStubs= */ null
	);

	attemptOrchestration(
		/* testName= */ 'validation error: argument mismatch error value',
		/* functionCall= */ readJSON( testInputsDir( 'argument_mismatch.json' ) ),
		/* expectedResultFile= */ null,
		/* expectedErrorFile= */ testOutputsDir( 'argument_mismatch_expected.json' ),
		/* setUpStubs= */ null,
		/* expectedExtraMetadata= */ [],
		// Error gets returned before implementation is selected
		/* expectedMissingMetadata= */ []
	);

	attemptOrchestration(
		/* testName= */ 'validation error: invalid argument type for function call but doValidate=false',
		/* functionCall= */ readJSON( testInputsDir( 'invalid_call_argument_type.json' ) ),
		/* expectedResultFile= */ testOutputsDir( 'invalid_call_argument_type_dovalidate_false_expected.json' ),
		/* expectedErrorFile= */ null,
		/* setUpStubs= */ null,
		/* expectedExtraMetadata= */ [],
		// Error gets returned before implementation is selected
		/* expectedMissingMetadata= */ [],
		/* implementationSelector= */ null,
		/* doValidate= */ false
	);

	attemptOrchestration(
		/* testName= */ 'validation error: invalid duplicated argument key in function definition',
		/* functionCall= */ readJSON( testInputsDir( 'invalid_key_duplicated.json' ) ),
		/* expectedResultFile= */ null,
		/* expectedErrorFile= */ testOutputsDir( 'invalid_key_duplicated_expected.json' ),
		/* setUpStubs= */ null,
		/* expectedExtraMetadata= */ [],
		// Error gets returned before implementation is selected
		/* expectedMissingMetadata= */ [ 'implementationId', 'implementationType' ]
	);

	attemptOrchestration(
		/* testName= */ 'validation error: invalid key for first argument in function definition',
		/* functionCall= */ readJSON( testInputsDir( 'invalid_key_first_name.json' ) ),
		/* expectedResultFile= */ null,
		/* expectedErrorFile= */ testOutputsDir( 'invalid_key_first_name_expected.json' ),
		/* setUpStubs= */ null,
		/* expectedExtraMetadata= */ [],
		// Error gets returned before implementation is selected
		/* expectedMissingMetadata= */ [ 'implementationId', 'implementationType' ]
	);

	attemptOrchestration(
		/* testName= */ 'validation error: invalid key for first argument in function definition but doValidate=false',
		/* functionCall= */ readJSON( testInputsDir( 'invalid_key_first_name.json' ) ),
		/* expectedResultFile= */ null,
		/* expectedErrorFile= */ testOutputsDir( 'dont_validate_invalid_key_first_name_expected.json' ),
		/* setUpStubs= */ null,
		/* expectedExtraMetadata= */ [],
		// Error gets returned before implementation is selected
		/* expectedMissingMetadata= */ [],
		/* implementationSelector= */ null,
		/* doValidate= */ false
	);

	attemptOrchestration(
		/* testName= */ 'validation error: invalid key name for argument in function definition',
		/* functionCall= */ readJSON( testInputsDir( 'invalid_key_name.json' ) ),
		/* expectedResultFile= */ null,
		/* expectedErrorFile= */ testOutputsDir( 'invalid_key_name_expected.json' ),
		/* setUpStubs= */ null,
		/* expectedExtraMetadata= */ [],
		// Error gets returned before implementation is selected
		/* expectedMissingMetadata= */ [ 'implementationId', 'implementationType' ]
	);

	attemptOrchestration(
		/* testName= */ 'validation error: invalid non-sequential key for argument in function definition',
		/* functionCall= */ readJSON( testInputsDir( 'invalid_key_nonsequential.json' ) ),
		/* expectedResultFile= */ null,
		/* expectedErrorFile= */ testOutputsDir( 'invalid_key_nonsequential_expected.json' ),
		/* setUpStubs= */ null,
		/* expectedExtraMetadata= */ [],
		// Error gets returned before implementation is selected
		/* expectedMissingMetadata= */ [ 'implementationId', 'implementationType' ]
	);

} );
