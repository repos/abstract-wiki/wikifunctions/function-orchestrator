'use strict';

const { readJSON } = require( '../../../../src/utils.js' );
const { testInputsDir, testObjectsDir, testOutputsDir } = require( '../../../utils/testFileUtils.js' );
const { attemptOrchestration } = require( '../mswTestRunner.js' );
const { getServerWithMocks } = require( '../../../utils/mockUtils.js' );

describe( 'mockedServices – Argumment error tests', () => {

	const { wikiStub, mockServiceWorker } = getServerWithMocks();

	before( () => {
		mockServiceWorker.listen();
	} );

	afterEach( () => {
		wikiStub.reset();
		mockServiceWorker.resetHandlers();
	} );

	after( () => {
		mockServiceWorker.close();
	} );

	{
		const setUpStubs = () => {
			wikiStub.setZId( 'Z422', {
				Z1K1: 'Z2',
				Z2K1: { Z1K1: 'Z6', Z6K1: 'Z422' },
				Z2K2: readJSON( testObjectsDir( 'misnamed-argument-Z422.json' ) )
			} );
		};
		attemptOrchestration(
			/* testName= */ 'argument name error: misnamed argument',
			/* functionCall= */ readJSON( testInputsDir( 'invalid_call_misnamed_argument.call.json' ) ),
			/* expectedResultFile= */ null,
			/* expectedErrorFile= */ testOutputsDir( 'invalid_call_misnamed_argument.expected.json' ),
			setUpStubs,
			/* expectedExtraMetadata= */[],
			/* expectedMissingMetadata= */[ 'implementationId' ]
		);
	}

	{
		const setUpStubs = () => {
			wikiStub.setZId( 'Z423', {
				Z1K1: 'Z2',
				Z2K1: { Z1K1: 'Z6', Z6K1: 'Z423' },
				Z2K2: readJSON( testObjectsDir( 'misnamed-argument-Z423.json' ) )
			} );
		};
		attemptOrchestration(
			/* testName= */ 'argument name error: list type misnamed argument',
			/* functionCall= */ readJSON( testInputsDir( 'invalid_call_misnamed_argument_list.call.json' ) ),
			/* expectedResultFile= */ null,
			/* expectedErrorFile= */ testOutputsDir( 'invalid_call_misnamed_argument_list.expected.json' ),
			setUpStubs,
			/* expectedExtraMetadata= */[],
			/* expectedMissingMetadata= */[ 'implementationId' ]
		);
	}

	{
		const setUpStubs = () => {
			wikiStub.setZId( 'Z422', {
				Z1K1: 'Z2',
				Z2K1: { Z1K1: 'Z6', Z6K1: 'Z422' },
				Z2K2: readJSON( testObjectsDir( 'misnamed-argument-Z422.json' ) )
			} );
		};
		attemptOrchestration(
			/* testName= */ 'argument error: missing argument',
			/* functionCall= */ readJSON( testInputsDir( 'invalid_call_missing_argument.call.json' ) ),
			/* expectedResultFile= */ null,
			/* expectedErrorFile= */ testOutputsDir( 'invalid_call_missing_argument.expected.json' ),
			setUpStubs,
			/* expectedExtraMetadata= */[],
			// Error gets returned before implementation is selected
			/* expectedMissingMetadata= */[ 'implementationId', 'implementationType' ]
		);
	}

} );
