'use strict';

const { makeMappedResultEnvelope } = require( '../../../../function-schemata/javascript/src/utils.js' );
const { readJSON } = require( '../../../../src/utils.js' );
const { testInputsDir, testObjectsDir, testOutputsDir } = require( '../../../utils/testFileUtils.js' );
const { attemptOrchestration } = require( '../mswTestRunner.js' );
const { getServerWithMocks } = require( '../../../utils/mockUtils.js' );
const { FirstImplementationSelector, RandomImplementationSelector } = require( '../../../../src/implementationSelector.js' );

class SnoopingImplementationSelector {

	constructor( resultList ) {
		this.resultList_ = resultList;
	}

	* generate( implementations ) {
		for ( const implementation of implementations ) {
			const keys = [ ...implementation.getZ14().keys() ];
			keys.sort();
			this.resultList_.push( keys.join( ',' ) );
		}
		for ( const implementation of implementations ) {
			yield implementation;
		}
	}
}

describe( 'mockedServices – Implementation selector tests', () => {

	const { evaluatorStub, mockServiceWorker, wikiStub } = getServerWithMocks();

	before( () => {
		mockServiceWorker.listen();
	} );

	afterEach( () => {
		evaluatorStub.reset();
		wikiStub.reset();
		mockServiceWorker.resetHandlers();
	} );

	after( () => {
		mockServiceWorker.close();
	} );

	// Function call to Z804

	attemptOrchestration(
		/* testName= */ 'function call for Z804, without a selector over-ride',
		/* functionCall= */ readJSON( testInputsDir( 'Z804.json' ) ),
		/* expectedResultFile= */ testOutputsDir( 'Z804_expected.json' )
	);

	attemptOrchestration(
		/* testName= */ 'function call for Z804, with FirstImplementationSelector',
		/* functionCall= */ readJSON( testInputsDir( 'Z804.json' ) ),
		/* expectedResultFile= */ testOutputsDir( 'Z804_expected.json' ),
		/* expectedErrorFile= */ null,
		/* setUpStubs= */ null,
		/* expectedExtraMetadata= */ [],
		/* expectedMissingMetadata= */ [],
		/* implementationSelector= */ new FirstImplementationSelector()
	);

	attemptOrchestration(
		/* testName= */ 'function call for Z804, with RandomImplementationSelector',
		/* functionCall= */ readJSON( testInputsDir( 'Z804.json' ) ),
		/* expectedResultFile= */ testOutputsDir( 'Z804_expected.json' ),
		/* expectedErrorFile= */ null,
		/* setUpStubs= */ null,
		/* expectedExtraMetadata= */ [],
		/* expectedMissingMetadata= */ [],
		/* implementationSelector= */ new RandomImplementationSelector()
	);

	// Evaluated function call

	{
		const setUpStubs = () => {
			evaluatorStub.setZId( 'Z410', ( unused ) => makeMappedResultEnvelope( { Z1K1: 'Z6', Z6K1: '13' }, null ) ); // eslint-disable-line no-unused-vars
		};
		attemptOrchestration(
			/* testName= */ 'evaluated function call, without a selector over-ride',
			/* functionCall= */ readJSON( testInputsDir( 'evaluated.json' ) ),
			/* expectedResultFile= */ testOutputsDir( 'evaluated-13.json' ),
			/* expectedErrorFile= */ null,
			setUpStubs,
			/* expectedExtraMetadata= */ [],
			/* expectedMissingMetadata= */ [ 'implementationId' ]
		);
	}

	{
		const setUpStubs = () => {
			evaluatorStub.setZId( 'Z410', ( unused ) => makeMappedResultEnvelope( { Z1K1: 'Z6', Z6K1: '13' }, null ) ); // eslint-disable-line no-unused-vars
		};
		attemptOrchestration(
			/* testName= */ 'evaluated function call, with FirstImplementationSelector',
			/* functionCall= */ readJSON( testInputsDir( 'evaluated.json' ) ),
			/* expectedResultFile= */ testOutputsDir( 'evaluated-13.json' ),
			/* expectedErrorFile= */ null,
			setUpStubs,
			/* expectedExtraMetadata= */ [],
			/* expectedMissingMetadata= */ [ 'implementationId' ],
			/* implementationSelector= */ new FirstImplementationSelector()
		);
	}

	{
		const setUpStubs = () => {
			evaluatorStub.setZId( 'Z410', ( unused ) => makeMappedResultEnvelope( { Z1K1: 'Z6', Z6K1: '13' }, null ) ); // eslint-disable-line no-unused-vars
		};
		attemptOrchestration(
			/* testName= */ 'evaluated function call, with RandomImplementationSelector',
			/* functionCall= */ readJSON( testInputsDir( 'evaluated.json' ) ),
			/* expectedResultFile= */ testOutputsDir( 'evaluated-13.json' ),
			/* expectedErrorFile= */ null,
			setUpStubs,
			/* expectedExtraMetadata= */ [],
			/* expectedMissingMetadata= */ [ 'implementationId' ],
			/* implementationSelector= */ new RandomImplementationSelector()
		);
	}

	{
		class SecondImplementationSelector {
			* generate( implementations ) {
				yield implementations[ 1 ];
				yield implementations[ 0 ];
				for ( let i = 2; i < implementations.length; ++i ) {
					yield implementations[ i ];
				}
			}
		}

		attemptOrchestration(
			/* testName= */ 'multiple implementations',
			/* functionCall= */ readJSON( testInputsDir( 'multiple-implementations.json' ) ),
			/* expectedResultFile= */ testOutputsDir( 'multiple-implementations_expected.json' ),
			/* expectedErrorFile= */ null,
			/* setUpStubs= */ null,
			/* expectedExtraMetadata= */ [],
			/* expectedMissingMetadata= */ [ 'implementationId' ],
			/* implementationSelector= */ new SecondImplementationSelector()
		);
	}

	attemptOrchestration(
		/* testName= */ 'first implementation\'s programming language is nonexistent',
		/* functionCall= */ readJSON( testInputsDir( 'first-implementation-programming-language-nonexistent.json' ) ),
		/* expectedResultFile= */ testOutputsDir( 'first-implementation-nonexistent_expected.json' ),
		/* expectedErrorFile= */ null,
		/* setUpStubs= */ null,
		/* expectedExtraMetadata= */ [],
		/* expectedMissingMetadata= */ [ 'implementationId' ],
		/* implementationSelector= */ new FirstImplementationSelector()
	);

	attemptOrchestration(
		/* testName= */ 'first implementation\'s evaluator is down',
		/* functionCall= */ readJSON( testInputsDir( 'first-implementation-evaluator-down.json' ) ),
		/* expectedResultFile= */ testOutputsDir( 'first-implementation-evaluator-down_expected.json' ),
		/* expectedErrorFile= */ null,
		/* setUpStubs= */ null,
		/* expectedExtraMetadata= */ [],
		/* expectedMissingMetadata= */ [ 'implementationId' ],
		/* implementationSelector= */ new FirstImplementationSelector()
	);

	{
		let ranZ491 = false;
		const setUpStubs = () => {
			wikiStub.setZId(
				'Z490',
				{
					Z1K1: 'Z2',
					Z2K1: { Z1K1: 'Z6', Z6K1: 'Z490' },
					Z2K2: readJSON( testObjectsDir( 'slow_implementation_Z490.json' ) )
				},
				/* error= */ false,
				/* ephemeral= */ true,
				/* delay= */ 1000
			);
			wikiStub.setZId( 'Z491', {
				Z1K1: 'Z2',
				Z2K1: { Z1K1: 'Z6', Z6K1: 'Z491' },
				Z2K2: readJSON( testObjectsDir( 'fast_implementation_Z491.json' ) )
			} );
			evaluatorStub.setCodeString( 'Z490', () => makeMappedResultEnvelope( { Z1K1: 'Z6', Z6K1: String( ranZ491 ) }, null ) );
			evaluatorStub.setCodeString( 'Z491', () => {
				ranZ491 = true;
				throw new Error( 'do not run me' );
			} );
		};
		const functionCall = readJSON( testInputsDir( 'evaluated-implementation-race.json' ) );
		attemptOrchestration(
			/* testName= */ 'implementation race condition',
			/* functionCall= */ functionCall,
			/* expectedResultFile= */ testOutputsDir( 'evaluated-implementation-race.json' ),
			/* expectedErrorFile= */ null,
			/* setUpStubs= */ setUpStubs,
			/* expectedExtraMetadata= */ [],
			/* expectedMissingMetadata= */ [],
			/* implementationSelector= */ new FirstImplementationSelector()
		);
	}

	{
		const proofsOfResolution = [];
		const implementationSelector = new SnoopingImplementationSelector( proofsOfResolution );
		const functionCall = readJSON( testInputsDir( 'resolved-implementation-proof.json' ) );
		const setUpStubs = () => {
			wikiStub.setZId(
				'Z490',
				{
					Z1K1: 'Z2',
					Z2K1: { Z1K1: 'Z6', Z6K1: 'Z491' },
					Z2K2: functionCall.Z7K1
				}
			);
			wikiStub.setZId(
				'Z491',
				{
					Z1K1: 'Z2',
					Z2K1: { Z1K1: 'Z6', Z6K1: 'Z491' },
					Z2K2: readJSON( testObjectsDir( 'prove_resolution_Z7_491.json' ) )
				}
			);
			wikiStub.setZId(
				'Z492',
				{
					Z1K1: 'Z2',
					Z2K1: { Z1K1: 'Z6', Z6K1: 'Z492' },
					Z2K2: readJSON( testObjectsDir( 'prove_resolution_Z9_492.json' ) )
				}
			);
			evaluatorStub.setCodeString( 'Z492', () => makeMappedResultEnvelope( { Z1K1: 'Z6', Z6K1: proofsOfResolution.join( '|' ) } ) );
		};
		attemptOrchestration(
			/* testName= */ 'prove that all implementations are resolved before running',
			/* functionCall= */ functionCall,
			/* expectedResultFile= */ testOutputsDir( 'resolved-implementation-proof.json' ),
			/* expectedErrorFile= */ null,
			/* setUpStubs= */ setUpStubs,
			/* expectedExtraMetadata= */ [],
			/* expectedMissingMetadata= */ [],
			/* implementationSelector= */ implementationSelector
		);
	}

} );
