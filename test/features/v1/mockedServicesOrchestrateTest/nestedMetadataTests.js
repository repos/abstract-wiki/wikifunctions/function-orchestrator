'use strict';

const assert = require( '../../../utils/assert.js' );
const canonicalize = require( '../../../../function-schemata/javascript/src/canonicalize.js' );
const { getServerWithMocks, getTestInvariants, EVAL_URI, WIKI_URI, WIKIDATA_URI } = require( '../../../utils/mockUtils.js' );
const { getZMapValue, makeTrue } = require( '../../../../function-schemata/javascript/src/utils.js' );
const { orchestrate } = require( '../../../../src/orchestrate.js' );
const { readJSON } = require( '../../../../src/utils.js' );
const { testInputsDir, testObjectsDir, testOutputsDir } = require( '../../../utils/testFileUtils.js' );
const { writeJSON } = require( '../../../utils/testFileUtils.js' );

// Determine whether to run in test or regeneration mode.
let regenerationMode = false;
for ( const argument of process.argv ) {
	if ( argument === '--regenerate-output' ) {
		regenerationMode = true;
		break;
	}
}

async function runNestedMetadataTest( functionCall, invariants, expectedNestedMetadataFile ) {
	const envelope = await orchestrate( functionCall, invariants );
	let actualNestedMetadata = getZMapValue(
		envelope.Z22K2,
		{
			Z1K1: 'Z6',
			Z6K1: 'nestedMetadata'
		}
	);
	if ( actualNestedMetadata !== undefined ) {
		actualNestedMetadata = canonicalize( actualNestedMetadata ).Z22K1;
	}
	if ( regenerationMode ) {
		if ( expectedNestedMetadataFile !== undefined ) {
			writeJSON( actualNestedMetadata, expectedNestedMetadataFile );
		}
		assert.isNull( null, '' ); // must assert something lest Mocha complain
	} else {
		let expectedNestedMetadata;
		if ( expectedNestedMetadataFile !== undefined ) {
			expectedNestedMetadata = readJSON( expectedNestedMetadataFile );
		}
		assert.deepEqual( actualNestedMetadata, expectedNestedMetadata );
	}
}

describe( 'nested metadata test', () => {

	const { wikiStub, evaluatorStub, mockServiceWorker } = getServerWithMocks();

	before( () => {
		mockServiceWorker.listen();
	} );

	afterEach( () => {
		wikiStub.reset();
		evaluatorStub.reset();
		mockServiceWorker.resetHandlers();
	} );

	after( () => {
		mockServiceWorker.close();
	} );

	it( 'nested metadata is added when feature flag is set', async () => {
		const invariants = getTestInvariants(
			{
				doValidate: true,
				remainingTime: 15,
				wikiUri: 'unimportant',
				wikidataUri: 'unimportant',
				evaluatorUri: 'unimportant',
				requestId: 'nested-metadata-test',
				addNestedMetadata: true
			}
		);
		const expectedNestedMetadataFile = testOutputsDir( 'nested-metadata_expected.json' );
		const functionCall = readJSON( testInputsDir( 'Z872_filter_function_call.json' ) );
		await runNestedMetadataTest( functionCall, invariants, expectedNestedMetadataFile );
	} );

	it( 'nested metadata is absent when feature flag is not set', async () => {
		const invariants = getTestInvariants(
			{
				doValidate: true,
				remainingTime: 15,
				wikiUri: 'unimportant',
				wikidataUri: 'unimportant',
				evaluatorUri: 'unimportant',
				requestId: 'nested-metadata-test'
				// addNestedMetadata is implicitly undefined/false-y
			}
		);
		const functionCall = readJSON( testInputsDir( 'Z872_filter_function_call.json' ) );
		await runNestedMetadataTest( functionCall, invariants );
	} );

	it( 'curried function call populates deeply-nested metadata', async () => {
		const setUpStubs = () => {
			wikiStub.setZId( 'Z488', {
				Z1K1: 'Z2',
				Z2K1: { Z1K1: 'Z6', Z6K1: 'Z488' },
				Z2K2: readJSON( testObjectsDir( 'curry-implementation-Z488.json' ) )
			} );
			wikiStub.setZId( 'Z487', {
				Z1K1: 'Z2',
				Z2K1: { Z1K1: 'Z6', Z6K1: 'Z487' },
				Z2K2: readJSON( testObjectsDir( 'curry-Z487.json' ) )
			} );
			wikiStub.setZId( 'Z486', {
				Z1K1: 'Z2',
				Z2K1: { Z1K1: 'Z6', Z6K1: 'Z486' },
				Z2K2: readJSON( testObjectsDir( 'curry-call-Z486.json' ) )
			} );
			wikiStub.setZId( 'Z437', {
				Z1K1: 'Z2',
				Z2K1: { Z1K1: 'Z6', Z6K1: 'Z437' },
				Z2K2: readJSON( testObjectsDir( 'and-Z437.json' ) )
			} );
		};
		const curryCall = {
			Z1K1: 'Z7',
			Z7K1: 'Z486',
			Z486K1: 'Z437',
			Z486K2: makeTrue(),
			Z486K3: makeTrue()
		};
		setUpStubs();
		const invariants = getTestInvariants(
			{
				doValidate: true,
				remainingTime: 15,
				wikiUri: WIKI_URI,
				wikidataUri: WIKIDATA_URI,
				evaluatorUri: EVAL_URI,
				requestId: 'nested-metadata-curry-test',
				addNestedMetadata: true
			}
		);
		const expectedNestedMetadataFile = testOutputsDir( 'curry-nested-metadata_expected.json' );
		await runNestedMetadataTest( curryCall, invariants, expectedNestedMetadataFile );
	} );

} );
