'use strict';

const assert = require( '../../utils/assert.js' );
const uuid = require( 'uuid' );
const { getZMapValue, getError, isZMap } =
	require( '../../../function-schemata/javascript/src/utils.js' );
const { orchestrate } = require( '../../../src/orchestrate.js' );
const { Invariants } = require( '../../../src/Invariants.js' );
const { isZ24 } = require( '../../../src/utils.js' );
const { RateLimitCache } = require( '../../../src/rateLimits.js' );
const { readJSON } = require( '../../../src/utils.js' );
const { writeJSON } = require( '../../utils/testFileUtils.js' );
const { getTestInvariants, EVAL_URI, WIKI_URI, WIKIDATA_URI } = require( '../../utils/mockUtils.js' );
const canonicalize = require( '../../../function-schemata/javascript/src/canonicalize.js' );

function createExpectation( expectedValue, failureString, doCanonicalize = false ) {
	if ( expectedValue === null ) {
		return function ( actualResult ) {
			// We use isZ24 instead of isVoid because it's better that Z24 not
			// be resolved to {Z1K1: Z21} when sending results to the frontend.
			assert.deepEqual( isZ24( actualResult ), true, failureString );
		};
	}
	if ( doCanonicalize ) {
		expectedValue = canonicalize( expectedValue ).Z22K1;
	}
	return function ( actualValue ) {
		assert.deepEqual( actualValue, expectedValue, failureString );
	};
}

/**
 * Orchestrate and test the resulting output, error, and/or metadata.
 *
 * @param {string} testName unique name to apppend to the test
 * @param {Object} functionCall zobject, input to the orchestrator
 * @param {Mixed} expectedResult zobject, successful output or null
 * @param {Mixed} expectedErrorValue Z5 for an error or null
 * @param {Function|null} setUpStubs a callback to run to set up stubs
 * @param {Array} expectedExtraMetadata array of expected extra metadata
 * @param {Array} expectedMissingMetadata array of expected missing metadata
 * @param {Mixed} implementationSelector an ImplementationSelector subclass or null
 * @param {Invariants} invariants
 * @param {boolean} skip whether to skip this test
 */
function attemptOrchestrationTestMode(
	testName,
	functionCall,
	expectedResult,
	expectedErrorValue,
	setUpStubs,
	expectedExtraMetadata,
	expectedMissingMetadata,
	implementationSelector,
	invariants,
	skip
) {

	( skip ? it.skip : it )(
		'orchestration test: ' + testName,
		async () => {
			if ( setUpStubs !== null ) {
				setUpStubs();
			}
			const resultExpectationFailure = testName + ' returns the expected output, if any';
			// There are still some expected result files in normal form, so canonicalize here.
			const resultExpectation = createExpectation(
				expectedResult, resultExpectationFailure, /* doCanonicalize= */ true );

			let result = {};
			let thrownError = null;

			try {
				result = await orchestrate( functionCall, invariants, implementationSelector );
				result = canonicalize( result ).Z22K1;
			} catch ( err ) {
				console.trace();
				console.log( err );
				thrownError = err;
			}
			assert.isNull( thrownError, testName + ' should not throw an execution/validation error' );

			resultExpectation( result.Z22K1 );

			assert.isTrue( isZMap( result.Z22K2 ), testName + ' returns a ZMap for Z22K2' );
			const responseError = getError( result );
			let errorExpectation;
			if ( expectedErrorValue === null ) {
				errorExpectation = createExpectation( expectedErrorValue, testName + ' should not be in an execution/validation error state' );
			} else {
				assert.isNotNull( responseError, testName + ' should be in an execution/validation error state' );
				errorExpectation = createExpectation( expectedErrorValue, testName + ' returns the expected error, if any' );
			}
			errorExpectation( responseError );

			// Note: Keep this list in sync with the key block in the orchestrate() function,
			// and calls to setMetadataValue and setMetadataValues in other places.
			const standardMetaData = [
				'orchestrationMemoryUsage',
				'orchestrationCpuUsage',
				'orchestrationStartTime',
				'orchestrationEndTime',
				'orchestrationDuration',
				'orchestrationHostname',
				'implementationId',
				'implementationType'
			];

			standardMetaData.forEach( ( key ) => {
				const metaDataValue = getZMapValue( result.Z22K2, key );
				if ( expectedMissingMetadata.includes( key ) ) {
					assert.deepEqual( metaDataValue, undefined, testName + ' should not have the `' + key + '` meta-data key set' );
				} else {
					assert.isDefined( metaDataValue, testName + ' should have the `' + key + '` meta-data key set' );
				}
			} );

			expectedExtraMetadata.forEach( ( key ) => {
				const metaDataValue = getZMapValue( result.Z22K2, key );
				assert.isDefined( metaDataValue, testName + ' should have the `' + key + '` meta-data key set' );
			} );
		}
	);

}

function attemptOrchestrationRegenerationMode(
	testName,
	functionCall,
	expectedResultFile,
	expectedErrorFile,
	setUpStubs,
	implementationSelector,
	invariants,
	skip
) {
	( skip ? it.skip : it )(
		'regenerating output for ' + testName,
		async () => {
			if ( setUpStubs !== null ) {
				setUpStubs();
			}

			// Run the orchestrator.
			let result;
			try {
				result = await orchestrate( functionCall, invariants, implementationSelector );
				result = canonicalize( result ).Z22K1;
			} catch ( err ) {
				assert.isNotNull( null, 'could not regenerate output for ' + testName );
				return;
			}

			// Write expected output, if any.
			if ( expectedResultFile !== null ) {
				writeJSON( result.Z22K1, expectedResultFile );
			}

			// Write expected error, if any.
			if ( expectedErrorFile !== null ) {
				writeJSON( getError( result ), expectedErrorFile );
			}

			assert.isNull( null, '' ); // must assert something lest Mocha complain
		}
	);
}

// Determine whether to run in test or regeneration mode.
let regenerationMode = false;
for ( const argument of process.argv ) {
	if ( argument === '--regenerate-output' ) {
		regenerationMode = true;
		break;
	}
}

/**
 * Orchestrate and test the resulting output, error, and/or metadata.
 *
 * If there is an expected result, it can be indicated with expectedResult
 * or expectedResultFile, but not both.  (If both are given, expectedResult
 * will be ignored.)  Similarly, for expectedErrorValue and expectedErrorFile.
 *
 * When regenerationMode = true and there is a value for expectedResultFile, the expected
 * result will be written to that file.  Similarly, for expectedErrorFile.
 *
 * @param {string} testName unique name to append to the test
 * @param {Object} functionCall zobject, input to the orchestrator
 * @param {Mixed} expectedResultFile null, or name of file containing successful output
 * @param {Mixed} expectedErrorFile null, or name of file containing Z5
 * @param {Function|null} setUpStubs a callback to run to set up stubs
 * @param {Array} expectedExtraMetadata array of expected extra metadata
 * @param {Array} expectedMissingMetadata array of expected missing metadata
 * @param {Mixed} implementationSelector an ImplementationSelector subclass or null
 * @param {boolean} doValidate whether to perform static validation
 * @param {boolean} skip whether to skip this test
 * @param {string} requestId request ID to inject for this request
 * @param {Array} invariantZIDs list of Z ids for supported programming languages
 * @param {Object} req requested object
 * @param {RateLimitCache} rateLimiter
 */
function attemptOrchestration(
	testName,
	functionCall,
	expectedResultFile = null,
	expectedErrorFile = null,
	setUpStubs = null,
	expectedExtraMetadata = [],
	expectedMissingMetadata = [],
	implementationSelector = null,
	doValidate = true,
	skip = false,
	requestId = null,
	invariantZIDs = [],
	req = null,
	rateLimiter = null
) {
	if ( requestId === null ) {
		requestId = uuid.v1();
	}
	const invariants = getTestInvariants( {
		doValidate,
		remainingTime: 15,
		wikiUri: WIKI_URI,
		wikidataUri: WIKIDATA_URI,
		evalUri: EVAL_URI,
		requestId,
		req,
		invariantZIDs,
		rateLimiter
	} );
	if ( regenerationMode ) {
		attemptOrchestrationRegenerationMode(
			testName, functionCall, expectedResultFile, expectedErrorFile,
			setUpStubs, implementationSelector, invariants, skip
		);
		return;
	}
	let expectedResult = null;
	let expectedErrorValue = null;
	if ( expectedResultFile !== null ) {
		expectedResult = readJSON( expectedResultFile );
	}
	if ( expectedErrorFile !== null ) {
		expectedErrorValue = readJSON( expectedErrorFile );
	}
	attemptOrchestrationTestMode(
		testName,
		functionCall,
		expectedResult,
		expectedErrorValue,
		setUpStubs,
		expectedExtraMetadata,
		expectedMissingMetadata,
		implementationSelector,
		invariants,
		skip
	);
}

module.exports = { attemptOrchestration, WIKI_URI, EVAL_URI };
