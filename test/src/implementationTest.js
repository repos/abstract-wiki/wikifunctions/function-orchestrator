'use strict';

const { makeMappedResultEnvelope } = require( '../../function-schemata/javascript/src/utils.js' );
const assert = require( '../utils/assert.js' );
const normalize = require( '../../function-schemata/javascript/src/normalize.js' );
const { Implementation } = require( '../../src/implementation.js' );
const { ZWrapper } = require( '../../src/ZWrapper.js' );
const { EmptyFrame } = require( '../../src/frame.js' );
const { getServerWithMocks, getTestInvariants, EVAL_URI, WIKI_URI, WIKIDATA_URI } = require( '../utils/mockUtils.js' );

function getNormalizedZWrapper( ZObject ) {
	return ZWrapper.create( normalize( ZObject ).Z22K1, new EmptyFrame() );
}

describe( 'Implementation test', () => {

	const { evaluatorStub, mockServiceWorker } = getServerWithMocks();

	before( async () => {
		mockServiceWorker.listen();
	} );

	afterEach( () => {
		evaluatorStub.reset();
		mockServiceWorker.resetHandlers();
	} );

	after( () => {
		mockServiceWorker.close();
	} );

	it( 'Evaluated implementation uses internal Z14', async () => {
		evaluatorStub.setZId( 'Z6800', () => makeMappedResultEnvelope( { Z1K1: 'Z6', Z6K1: 'None' }, null ) );
		const Z14 = getNormalizedZWrapper( {
			Z1K1: 'Z14',
			Z14K1: 'Z6800',
			Z14K3: {
				Z1K1: 'Z16',
				Z16K1: {
					Z1K1: 'Z61',
					Z61K1: 'python-3'
				},
				Z16K2: 'def Z6800():\n\treturn "None"'
			}
		} );

		const functionCall = getNormalizedZWrapper( {
			Z1K1: 'Z7',
			Z7K1: {
				Z1K1: 'Z8',
				Z8K1: [ 'Z17' ],
				Z8K2: 'Z1',
				Z8K3: [ 'Z20' ],
				Z8K4: [ 'Z14' ],
				Z8K5: 'Z6800'
			}
		} );

		const doValidate = false;
		const invariants = getTestInvariants(
			{
				doValidate,
				remainingTime: 100,
				wikiUri: WIKI_URI,
				wikidataUri: WIKIDATA_URI,
				evalUri: EVAL_URI,
				requestId: 'not-your-business'
			}
		);

		const evaluatedImplementation = await Implementation.create( Z14, invariants, doValidate );
		evaluatedImplementation.invariants = invariants;
		const executionResult = await evaluatedImplementation.execute(
			functionCall, /* argumentList= */ [] );
		assert.deepEqual( executionResult.Z22K1.Z6K1, 'None' );
	} );

} );
