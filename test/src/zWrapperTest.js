'use strict';

const assert = require( '../utils/assert.js' );
const { ZWrapper } = require( '../../src/ZWrapper.js' );
const { BaseFrame, EmptyFrame } = require( '../../src/frame.js' );
const { getError } = require( '../../function-schemata/javascript/src/utils.js' );

const NESTED_OBJECT_ = {
	Z1K1: {
		Z1K1: 'Z9',
		Z9K1: 'Z10000'
	},
	Z10000K1: {
		Z1K1: 'Z6',
		Z6K1: '!'
	}
};

describe( 'ZWrapper test', () => {

	it( 'ZWrapper class string construction', () => {
		const stringIsNotAZWrapper = ZWrapper.create( 'Hello I am a test string', new EmptyFrame() );

		assert.deepEqual( stringIsNotAZWrapper, 'Hello I am a test string' );
	} );

	it( 'ZWrapper class construction with scope', () => {
		const aReference = { Z1K1: 'Z9', Z9K1: 'Z9' };
		const aReferenceZWrapper = ZWrapper.create( aReference, new EmptyFrame() );
		assert.deepEqual( aReference, aReferenceZWrapper.asJSON() );
		assert.deepEqual( new Set( [ 'Z1K1', 'Z9K1' ] ), new Set( aReferenceZWrapper.keys() ) );
		assert.deepEqual( 'Z9', aReferenceZWrapper.Z1K1 );
		assert.deepEqual( aReferenceZWrapper.scope_ instanceof EmptyFrame, true );
	} );

	it( 'ZWrapper class construction without a scope has a parentPointer', () => {
		const aReference = { Z1K1: 'Z9', Z9K1: 'Z9' };
		const parentPointer = 'Domingo Montoya';
		const aReferenceZWrapper = ZWrapper.create( aReference, null, parentPointer );
		assert.deepEqual( aReference, aReferenceZWrapper.asJSON() );
		assert.deepEqual( new Set( [ 'Z1K1', 'Z9K1' ] ), new Set( aReferenceZWrapper.keys() ) );
		assert.deepEqual( 'Z9', aReferenceZWrapper.Z1K1 );
		assert.deepEqual( aReferenceZWrapper.scope_ instanceof EmptyFrame, false );
		assert.deepEqual( 'Domingo Montoya', aReferenceZWrapper.parent_ );
	} );

	it( 'ZWrapper class construction raises exception without scope and parentPointer', () => {
		const aReference = { Z1K1: 'Z9', Z9K1: 'Z9' };
		assert.throws( () => {
			ZWrapper.create( aReference, null, undefined );
		} );
	} );

	it( 'ZWrapper class construction respects isValidated and isFullyResolved', () => {
		const validAndResolvedWrapper = ZWrapper.create(
			NESTED_OBJECT_,
			/* scope= */ new EmptyFrame(),
			/* parentPointer= */ null,
			/* isValidated= */ true,
			/* isFullyResolved= */ true );
		assert.deepEqual( validAndResolvedWrapper.isValidated(), true );
		assert.deepEqual( validAndResolvedWrapper.isFullyResolved(), true );
		assert.deepEqual( validAndResolvedWrapper.Z1K1.isValidated(), true );
		assert.deepEqual( validAndResolvedWrapper.Z1K1.isFullyResolved(), true );
		assert.deepEqual( validAndResolvedWrapper.Z10000K1.isValidated(), true );
		assert.deepEqual( validAndResolvedWrapper.Z10000K1.isFullyResolved(), true );

		// isValidated and isFullyResolved default to false,
		// so this object should be invalid and unresolved.
		const therapyWrapper = ZWrapper.create(
			NESTED_OBJECT_,
			/* scope= */ new EmptyFrame(),
			/* parentPointer= */ null );
		assert.deepEqual( therapyWrapper.isValidated(), false );
		assert.deepEqual( therapyWrapper.isFullyResolved(), false );
		assert.deepEqual( therapyWrapper.Z1K1.isValidated(), false );
		assert.deepEqual( therapyWrapper.Z1K1.isFullyResolved(), false );
		assert.deepEqual( therapyWrapper.Z10000K1.isValidated(), false );
		assert.deepEqual( therapyWrapper.Z10000K1.isFullyResolved(), false );
	} );

	it( 'ZWrapper.invalidate() propagates up', () => {
		const theWrapper = ZWrapper.create(
			NESTED_OBJECT_,
			/* scope= */ new EmptyFrame(),
			/* parentPointer= */ null,
			/* isValidated= */ true,
			/* isFullyResolved= */ true );
		assert.deepEqual( theWrapper.isValidated(), true );
		assert.deepEqual( theWrapper.isFullyResolved(), true );
		assert.deepEqual( theWrapper.Z1K1.isValidated(), true );
		assert.deepEqual( theWrapper.Z1K1.isFullyResolved(), true );
		assert.deepEqual( theWrapper.Z10000K1.isValidated(), true );
		assert.deepEqual( theWrapper.Z10000K1.isFullyResolved(), true );

		// Now invalidate a child object.
		theWrapper.Z1K1.invalidate();
		assert.deepEqual( theWrapper.isValidated(), false );
		assert.deepEqual( theWrapper.isFullyResolved(), true );
		assert.deepEqual( theWrapper.Z1K1.isValidated(), false );
		assert.deepEqual( theWrapper.Z1K1.isFullyResolved(), true );
		// The other child should not be affected ...
		assert.deepEqual( theWrapper.Z10000K1.isValidated(), true );
		assert.deepEqual( theWrapper.Z10000K1.isFullyResolved(), true );
		// ... until explicitly invalidated.
		theWrapper.Z10000K1.invalidate();
		assert.deepEqual( theWrapper.Z10000K1.isValidated(), false );
	} );

	it( 'ZWrapper.unresolve() propagates up', () => {
		const theWrapper = ZWrapper.create(
			NESTED_OBJECT_,
			/* scope= */ new EmptyFrame(),
			/* parentPointer= */ null,
			/* isValidated= */ true,
			/* isFullyResolved= */ true );
		assert.deepEqual( theWrapper.isValidated(), true );
		assert.deepEqual( theWrapper.isFullyResolved(), true );
		assert.deepEqual( theWrapper.Z1K1.isValidated(), true );
		assert.deepEqual( theWrapper.Z1K1.isFullyResolved(), true );
		assert.deepEqual( theWrapper.Z10000K1.isValidated(), true );
		assert.deepEqual( theWrapper.Z10000K1.isFullyResolved(), true );

		// Now unresolve a child object.
		theWrapper.Z10000K1.unresolve();
		assert.deepEqual( theWrapper.isValidated(), true );
		assert.deepEqual( theWrapper.isFullyResolved(), false );
		assert.deepEqual( theWrapper.Z10000K1.isValidated(), true );
		assert.deepEqual( theWrapper.Z10000K1.isFullyResolved(), false );
		// The other child should not be affected ...
		assert.deepEqual( theWrapper.Z1K1.isValidated(), true );
		assert.deepEqual( theWrapper.Z1K1.isFullyResolved(), true );
		// ... until explicitly unresolved.
		theWrapper.Z1K1.unresolve();
		assert.deepEqual( theWrapper.Z1K1.isFullyResolved(), false );
	} );

	it( 'invalidation and unresolution heuristics', () => {
		const theWrapper = ZWrapper.create(
			NESTED_OBJECT_,
			/* scope= */ new EmptyFrame(),
			/* parentPointer= */ null,
			/* isValidated= */ true,
			/* isFullyResolved= */ true );
		const badWrapper = ZWrapper.create(
			NESTED_OBJECT_,
			/* scope= */ new EmptyFrame(),
			/* parentPointer= */ null,
			/* isValidated= */ false,
			/* isFullyResolved= */ false );
		const goodWrapper = ZWrapper.create(
			NESTED_OBJECT_,
			/* scope= */ new EmptyFrame(),
			/* parentPointer= */ null,
			/* isValidated= */ true,
			/* isFullyResolved= */ true );

		assert.deepEqual( theWrapper.isValidated(), true );
		assert.deepEqual( theWrapper.isFullyResolved(), true );

		// Original wrapper will be neither invalidated nor unresolved if
		// Z1K1 is set.
		theWrapper.setName( 'Z1K1', badWrapper );
		assert.deepEqual( theWrapper.isValidated(), true );
		assert.deepEqual( theWrapper.isFullyResolved(), true );

		// Original wrapper will be invalidated if another key is set.
		// Original wrapper will not be unresolved if the new value
		// is fully resolved.
		theWrapper.setName( 'Z10000K1', goodWrapper );
		assert.deepEqual( theWrapper.isValidated(), false );
		assert.deepEqual( goodWrapper.isFullyResolved(), true );
		assert.deepEqual( theWrapper.isFullyResolved(), true );

		// Original wrapper will be unresolved if the new value
		// is not fully resolved.
		theWrapper.setName( 'Z10000K1', badWrapper );
		assert.deepEqual( theWrapper.isValidated(), false );
		assert.deepEqual( badWrapper.isFullyResolved(), false );
		assert.deepEqual( theWrapper.isFullyResolved(), false );
	} );

	it( 'ZWrapper class construction with degenerate input', () => {
		const degenerateObject = { Z1K1: 'Z6', Z6K1: 13 };
		assert.throws( () => {
			ZWrapper.create( degenerateObject, new EmptyFrame() );
		} );
	} );

	it( 'ZWrapper resolution', async () => {
		const theTrueTrue = {
			Z1K1: {
				Z1K1: 'Z9',
				Z9K1: 'Z40'
			},
			Z40K1: {
				Z1K1: 'Z9',
				Z9K1: 'Z41'
			}
		};
		const georgieWrapper = ZWrapper.create( theTrueTrue, new EmptyFrame() );
		assert.deepEqual( theTrueTrue.Z1K1, georgieWrapper.original_.get( 'Z1K1' ).asJSON() );
		assert.deepEqual( false, georgieWrapper.resolved_.has( 'Z1K1' ) );

		await georgieWrapper.resolveKey( [ 'Z1K1' ], /* invariants= */ null );
		assert.deepEqual( theTrueTrue.Z1K1, georgieWrapper.resolved_.get( 'Z1K1' ).asJSON() );
	} );

	it( 'ZWrapper heuristic resolution', async () => {
		const functionallyTheTruth = {
			Z1K1: {
				Z1K1: 'Z9',
				Z9K1: 'Z40'
			},
			Z40K1: {
				Z1K1: 'Z7',
				Z7K1: {
					Z1K1: 'Z6',
					Z6K1: 'not a function'
				}
			}
		};
		const napoleonHill = ZWrapper.create( functionallyTheTruth, new EmptyFrame() );
		// This call used to throw an error because the heuristics identified the
		// Z40K1 here as a function call even though it is not remotely a valid
		// function call. The specific error is an implementation detail. That
		// means that this test is a change detector.
		//
		// Type-checking then got better, so the call to resolveKey didn't fail,
		// but calling asJSON() on the output did.
		//
		// This call now throws an error again because the heuristics have
		// become lax.
		assert.fails( napoleonHill.resolveKey( [ 'Z40K1' ], /* invariants= */ null ) );
	} );

	it( 'ZWrapper resolveKey for nonexistent key', async () => {
		const theGoodNumber = {
			Z1K1: {
				Z1K1: 'Z9',
				Z9K1: 'Z420'
			},
			Z420K1: {
				Z1K1: 'Z6',
				Z6K1: 'something innocuous'
			}
		};
		const theGoodWrapper = ZWrapper.create( theGoodNumber, new EmptyFrame() );
		const theGoodResult = ( await theGoodWrapper.resolveKey( [ 'Z420K2' ] ) ).asJSON();
		const expectedBadNumber = {
			Z1K1: {
				Z1K1: 'Z9',
				Z9K1: 'Z5'
			},
			Z5K1: {
				Z1K1: 'Z9',
				Z9K1: 'Z525'
			},
			Z5K2: {
				Z1K1: {
					Z1K1: {
						Z1K1: 'Z9',
						Z9K1: 'Z7'
					},
					Z7K1: {
						Z1K1: 'Z9',
						Z9K1: 'Z885'
					},
					Z885K1: {
						Z1K1: 'Z9',
						Z9K1: 'Z525'
					}
				},
				Z525K1: {
					Z1K1: {
						Z1K1: 'Z9',
						Z9K1: 'Z39'
					},
					Z39K1: {
						Z1K1: 'Z6',
						Z6K1: 'Z420K2'
					}
				}
			}
		};
		assert.deepEqual( expectedBadNumber, getError( theGoodResult ) );
	} );

	it( 'ZWrapper debugObject', () => {
		const nullZReferenceString = 'Z24';
		const nullZReference = { Z1K1: 'Z9', Z9K1: nullZReferenceString };
		const nullZWrapper = ZWrapper.create( nullZReference, new EmptyFrame() );
		assert.deepEqual(
			{ object_: nullZReferenceString, scope_: {} },
			nullZWrapper.debugObject()
		);

		const recursiveNullZWrapper = ZWrapper.create(
			nullZReference,
			new BaseFrame( new EmptyFrame() )
		);
		assert.deepEqual(
			{
				object_: nullZReferenceString,
				scope_: {
					lastFrame_: {}
				}
			},
			recursiveNullZWrapper.debugObject()
		);

		const emptyEnvelopeObject = {
			Z1K1: {
				Z1K1: 'Z9',
				Z9K1: 'Z22'
			},
			Z22K1: nullZReference,
			Z22K2: nullZReference
		};

		const baseZWrapper = ZWrapper.create(
			emptyEnvelopeObject, new BaseFrame( new EmptyFrame() ) );
		assert.deepEqual(
			{
				// The nullZReferences (normal form) become nullZReferenceStrings (canonical)
				object_: { Z1K1: 'Z22', Z22K1: nullZReferenceString, Z22K2: nullZReferenceString },
				scope_: {
					lastFrame_: {}
				}
			},
			baseZWrapper.debugObject()
		);
	} );

	it( 'ZWrapper asJSON, asJSONEphemeral respect correct keys', () => {
		const theObject = {
			Z1K1: {
				Z1K1: 'Z9',
				Z9K1: 'Z10000'
			},
			K1: {
				Z1K1: {
					Z1K1: 'Z9',
					Z9K1: 'Z18'
				},
				Z18K1: {
					Z1K1: 'Z9',
					Z9K1: 'Z10005'
				}
			},
			K2: {
				Z1K1: {
					Z1K1: 'Z9',
					Z9K1: 'Z18'
				},
				Z18K1: {
					Z1K1: 'Z9',
					Z9K1: 'Z10006'
				}
			}
		};
		const replacementStringOne = {
			Z1K1: 'Z6',
			Z6K1: 'diamond daimon monad i'
		};
		const replacementStringTwo = {
			Z1K1: 'Z6',
			Z6K1: 'adam kadmon in the sky'
		};
		const wrappedObject = ZWrapper.create( theObject, new EmptyFrame() );
		wrappedObject.setName( 'K1', replacementStringOne );
		wrappedObject.setNameEphemeral( 'K2', replacementStringTwo );

		// For asJSON, the non-ephemerally set key should be visible but not the ephemeral one.
		const actualJSON = wrappedObject.asJSON();
		const expectedJSON = { ...theObject };
		expectedJSON.K1 = replacementStringOne;
		assert.deepEqual( actualJSON, expectedJSON );

		// For asJSONEphemeral, both set keys should be visible.
		const actualJSONEphemeral = wrappedObject.asJSONEphemeral();
		const expectedJSONEphemeral = { ...theObject };
		expectedJSONEphemeral.K1 = replacementStringOne;
		expectedJSONEphemeral.K2 = replacementStringTwo;
		assert.deepEqual( actualJSONEphemeral, expectedJSONEphemeral );
	} );

} );
