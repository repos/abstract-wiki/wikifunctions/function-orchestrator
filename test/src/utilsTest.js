'use strict';

const assert = require( '../utils/assert.js' );
const {
	createSchema,
	generateError,
	getEvaluatorAndOrchestratorConfigsForEnvironment,
	getTimeoutForEnvironment,
	isError,
	isGenericListType,
	isGenericType,
	isRefOrString,
	makeBoolean,
	makeWrappedResultEnvelope,
	quoteZObject,
	responseEnvelopeContainsError,
	responseEnvelopeContainsValue,
	returnOnFirstError,
	traverseZList
} = require( '../../src/utils.js' );
const { ZWrapper } = require( '../../src/ZWrapper.js' );
const { EmptyFrame } = require( '../../src/frame.js' );
const {
	makeMappedResultEnvelope,
	makeEmptyZResponseEnvelopeMap,
	setZMapValue
} = require( '../../function-schemata/javascript/src/utils.js' );

// Collection of objects for testing.
const createdString = { Z1K1: 'Z6', Z6K1: 'created' };

const wrappedCreatedString = ZWrapper.create( createdString, new EmptyFrame() );

const tenThou = { Z1K1: 'Z9', Z9K1: 'Z10000' };

const wrappedTenThou = ZWrapper.create( tenThou, new EmptyFrame() );

const goodZ22 = makeMappedResultEnvelope(
	{ Z1K1: 'Z6', Z6K1: 'dull but reliable sigma string' }, null
);

const badZ22 = makeMappedResultEnvelope(
	null, generateError( 'extremely exciting but morally flawed error string' )
);

const listFunctionCall = {
	Z1K1: { Z1K1: 'Z9', Z9K1: 'Z7' },
	Z7K1: { Z1K1: 'Z9', Z9K1: 'Z881' },
	Z881K1: { Z1K1: 'Z9', Z9K1: 'Z1' }
};

const listType = {
	Z1K1: { Z1K1: 'Z9', Z9K1: 'Z4' },
	Z4K1: {
		Z1K1: { Z1K1: 'Z9', Z9K1: 'Z7' },
		Z7K1: { Z1K1: 'Z9', Z9K1: 'Z881' },
		Z881K1: { Z1K1: 'Z9', Z9K1: 'Z1' }
	},
	Z4K2: {
		Z1K1: {
			Z1K1: { Z1K1: 'Z9', Z9K1: 'Z7' },
			Z7K1: { Z1K1: 'Z9', Z9K1: 'Z881' },
			Z881K1: { Z1K1: 'Z9', Z9K1: 'Z3' }
		},
		K1: {
			Z1K1: { Z1K1: 'Z9', Z9K1: 'Z3' },
			Z3K1: { Z1K1: 'Z9', Z9K1: 'Z1' },
			Z3K2: { Z1K1: 'Z6', Z6K1: 'K1' },
			Z3K3: { Z1K1: 'Z9', Z9K1: 'Z1212' }
		},
		K2: {
			Z1K1: {
				Z1K1: { Z1K1: 'Z9', Z9K1: 'Z7' },
				Z7K1: { Z1K1: 'Z9', Z9K1: 'Z881' },
				Z881K1: { Z1K1: 'Z9', Z9K1: 'Z3' }
			}
		}
	},
	Z4K3: { Z1K1: 'Z9', Z9K1: 'Z1000' }
};

const embeddedListType = { ...listType };
embeddedListType.Z4K1 = { ...listType };

const embeddedType = {
	Z1K1: { Z1K1: 'Z9', Z9K1: 'Z4' },
	Z4K1: {
		Z1K1: { Z1K1: 'Z9', Z9K1: 'Z7' },
		Z7K1: { Z1K1: 'Z9', Z9K1: 'Z831' },
		Z831K1: { Z1K1: 'Z9', Z9K1: 'Z6' }
	},
	Z4K2: {
		Z1K1: {
			Z1K1: { Z1K1: 'Z9', Z9K1: 'Z7' },
			Z7K1: { Z1K1: 'Z9', Z9K1: 'Z881' },
			Z881K1: { Z1K1: 'Z9', Z9K1: 'Z3' }
		},
		K1: {
			Z1K1: { Z1K1: 'Z9', Z9K1: 'Z3' },
			Z3K1: { Z1K1: 'Z9', Z9K1: 'Z6' },
			Z3K2: { Z1K1: 'Z6', Z6K1: 'K1' },
			Z3K3: { Z1K1: 'Z9', Z9K1: 'Z1212' }
		},
		K2: {
			Z1K1: {
				Z1K1: { Z1K1: 'Z9', Z9K1: 'Z7' },
				Z7K1: { Z1K1: 'Z9', Z9K1: 'Z881' },
				Z881K1: { Z1K1: 'Z9', Z9K1: 'Z3' }
			}
		}
	},
	Z4K3: { Z1K1: 'Z9', Z9K1: 'Z1000' }
};

const someType = {
	Z1K1: { Z1K1: 'Z9', Z9K1: 'Z4' },
	Z4K1: { Z1K1: 'Z9', Z9K1: 'Z10000' },
	Z4K2: {
		Z1K1: {
			Z1K1: { Z1K1: 'Z9', Z9K1: 'Z7' },
			Z7K1: { Z1K1: 'Z9', Z9K1: 'Z881' },
			Z881K1: { Z1K1: 'Z9', Z9K1: 'Z3' }
		},
		K1: {
			Z1K1: { Z1K1: 'Z9', Z9K1: 'Z3' },
			Z3K1: { Z1K1: 'Z9', Z9K1: 'Z6' },
			Z3K2: { Z1K1: 'Z6', Z6K1: 'Z10000K1' },
			Z3K3: { Z1K1: 'Z9', Z9K1: 'Z1212' }
		},
		K2: {
			Z1K1: {
				Z1K1: { Z1K1: 'Z9', Z9K1: 'Z7' },
				Z7K1: { Z1K1: 'Z9', Z9K1: 'Z881' },
				Z881K1: { Z1K1: 'Z9', Z9K1: 'Z3' }
			},
			K1: {
				Z1K1: { Z1K1: 'Z9', Z9K1: 'Z3' },
				Z3K1: embeddedType,
				Z3K2: {
					Z1K1: 'Z6',
					Z6K1: 'Z10000K2'
				},
				Z3K3: {
					Z1K1: 'Z9',
					Z9K1: 'Z1212'
				}
			},
			K2: {
				Z1K1: {
					Z1K1: { Z1K1: 'Z9', Z9K1: 'Z7' },
					Z7K1: { Z1K1: 'Z9', Z9K1: 'Z881' },
					Z881K1: { Z1K1: 'Z9', Z9K1: 'Z3' }
				}
			}
		}
	},
	Z4K3: { Z1K1: 'Z9', Z9K1: 'Z1000' }
};

const objectOfSomeType = {
	Z1K1: someType,
	Z10000K1: createdString,
	Z10000K2: {
		Z1K1: embeddedType,
		K1: createdString
	}
};

class FakeLogger {

	constructor() {
		this.warnMessages_ = [];
	}

	static create() {
		const result = new FakeLogger();
		Object.defineProperty( result, 'warnMessages', {
			get: function () {
				return Object.freeze( this.warnMessages_ );
			}
		} );
		return result;
	}

	log( level, dataObject ) {
		this.warnMessages_.push( dataObject.message );
	}

}

const defaultTimeout = 20000;

describe( 'utils test', () => {

	// isRefOrString

	it( 'isRefOrString is true for ZWrapped Z6s', () => {
		assert.deepEqual( isRefOrString( wrappedCreatedString ), true );
	} );

	it( 'isRefOrString is true for unwrapped Z6s', () => {
		assert.deepEqual( isRefOrString( createdString ), true );
	} );

	it( 'isRefOrString is true for ZWrapped Z9s', () => {
		assert.deepEqual( isRefOrString( wrappedTenThou ), true );
	} );

	it( 'isRefOrString is true for unwrapped Z9s', () => {
		assert.deepEqual( isRefOrString( tenThou ), true );
	} );

	it( 'isRefOrString is false for ZWrapped whatever', () => {
		const theObject = ZWrapper.create( {
			Z1K1: { Z1K1: 'Z9', Z9K1: 'Z10000' },
			Z10000K1: { Z1K1: 'Z9', Z9K1: 'Z6' }
		}, new EmptyFrame() );
		assert.deepEqual( isRefOrString( theObject ), false );
	} );

	it( 'isRefOrString is false for unwrapped whatever', () => {
		const theObject = {
			Z1K1: { Z1K1: 'Z9', Z9K1: 'Z10000' },
			Z10000K1: { Z1K1: 'Z9', Z9K1: 'Z6' }
		};
		assert.deepEqual( isRefOrString( theObject ), false );
	} );

	// createSchema

	it( 'createSchema for ZWrapped Z9/Z6', () => {
		const schema = createSchema( wrappedTenThou );
		const result = schema.validate( tenThou );
		assert.deepEqual( result, true );
	} );

	it( 'createSchema for unwrapped Z9/Z6', () => {
		const schema = createSchema( tenThou );
		const result = schema.validate( tenThou );
		assert.deepEqual( result, true );
	} );

	it( 'createSchema for some type', () => {
		const schema = createSchema( objectOfSomeType );
		const result = schema.validate( objectOfSomeType );
		assert.deepEqual( result, true );
	} );

	// isError

	it( 'isError with canonical probable error', () => {
		const canonicalError = {
			Z1K1: 'Z5',
			Z5K1: 'nothing'
		};
		assert.deepEqual( isError( canonicalError ), true );
	} );

	it( 'isError with canonical normal error', () => {
		const canonicalError = {
			Z1K1: { Z1K1: 'Z9', Z9K1: 'Z5' },
			Z5K1: { Z1K1: 'Z6', Z6K1: 'nothing' }
		};
		assert.deepEqual( isError( canonicalError ), true );
	} );

	it( 'isError with non-error', () => {
		assert.deepEqual( isError( tenThou ), false );
	} );

	// isGenericListType

	it( 'isGenericListType: bare function call', () => {
		assert.deepEqual( isGenericListType( listFunctionCall ), true );
	} );

	it( 'isGenericListType: Z4 with function call in Z4K1', () => {
		assert.deepEqual( isGenericListType( listType ), true );
	} );

	it( 'isGenericListType: Z4 with Z4 with function call in Z4K1 in Z4K1', () => {
		assert.deepEqual( isGenericListType( embeddedListType ), true );
	} );

	it( 'isGenericListType: not a list', () => {
		assert.deepEqual( isGenericListType( embeddedType ), false );
	} );

	// isGenericType

	it( 'isGenericType with ZWrapped generic type', () => {
		const someList = ZWrapper.create( embeddedType.Z4K2, new EmptyFrame() );
		assert.deepEqual( isGenericType( someList ), true );
	} );

	it( 'isGenericType with generic type', () => {
		const someList = embeddedType.Z4K2;
		assert.deepEqual( isGenericType( someList ), true );
	} );

	it( 'isGenericType with not that', () => {
		assert.deepEqual( isGenericType( objectOfSomeType ), false );
	} );

	// responseEnvelopeContainsError

	it( 'responseEnvelopeContainsError finds nothing on undefined', async () => {
		assert.equal( responseEnvelopeContainsError(), false );
	} );

	it( 'responseEnvelopeContainsError finds nothing on an empty object', async () => {
		assert.equal( responseEnvelopeContainsError( {} ), false );
	} );

	it( 'responseEnvelopeContainsError finds nothing on a non-ZResponseEnvelope ZObject', async () => {
		assert.equal( responseEnvelopeContainsError( { Z1K1: 'Z6', Z6K1: 'Hello' } ), false );
	} );

	it( 'responseEnvelopeContainsError finds nothing on an undefined ZResponseEnvelope Map', async () => {
		assert.equal( responseEnvelopeContainsError( { Z1K1: 'Z22', Z22K1: 'Z1' } ), false );
	} );

	it( 'responseEnvelopeContainsError finds nothing on a null ZResponseEnvelope Map', async () => {
		assert.equal( responseEnvelopeContainsError( goodZ22 ), false );
	} );

	it( 'responseEnvelopeContainsError finds nothing on void ZResponseEnvelope Map', async () => {
		assert.equal( responseEnvelopeContainsError( makeMappedResultEnvelope( 'Z1', undefined ) ), false );
	} );

	it( 'responseEnvelopeContainsError finds nothing on empty ZResponseEnvelope Map', async () => {
		assert.equal( responseEnvelopeContainsError( makeMappedResultEnvelope( 'Z1', makeEmptyZResponseEnvelopeMap() ) ), false );
	} );

	it( 'responseEnvelopeContainsError finds nothing on error-less ZResponseEnvelope Map', async () => {
		const map = makeEmptyZResponseEnvelopeMap();
		setZMapValue( map, { Z1K1: 'Z6', Z6K1: 'hello' }, 'Z24' );
		assert.equal( responseEnvelopeContainsError( makeMappedResultEnvelope( 'Z1', map ) ), false );
	} );

	it( 'responseEnvelopeContainsError finds a direct error', async () => {
		assert.equal( responseEnvelopeContainsError( badZ22 ), true );
	} );

	it( 'responseEnvelopeContainsError finds nothing on error set to Z24', async () => {
		const map = makeEmptyZResponseEnvelopeMap();
		setZMapValue( map, { Z1K1: 'Z6', Z6K1: 'errors' }, 'Z24' );
		assert.equal( responseEnvelopeContainsError( makeMappedResultEnvelope( 'Z1', map ) ), false );
	} );

	it( 'responseEnvelopeContainsError works with ZWrappers', async () => {
		const goodWrapper = ZWrapper.create( goodZ22, new EmptyFrame() );
		const badWrapper = ZWrapper.create( badZ22, new EmptyFrame() );
		assert.equal( responseEnvelopeContainsError( goodWrapper ), false );
		assert.equal( responseEnvelopeContainsError( badWrapper ), true );
	} );

	// responseEnvelopeContainsValue

	it( 'responseEnvelopeContainsValue works with ZWrappers', async () => {
		const goodWrapper = ZWrapper.create( goodZ22, new EmptyFrame() );
		const badWrapper = ZWrapper.create( badZ22, new EmptyFrame() );
		assert.equal( responseEnvelopeContainsValue( goodWrapper ), true );
		assert.equal( responseEnvelopeContainsValue( badWrapper ), false );
	} );

	// makeBoolean

	it( 'makeBoolean true, canonical', () => {
		assert.deepEqual( makeBoolean( true, true ), { Z1K1: 'Z40', Z40K1: 'Z41' } );
	} );

	it( 'makeBoolean false, canonical', () => {
		assert.deepEqual( makeBoolean( false, true ), { Z1K1: 'Z40', Z40K1: 'Z42' } );
	} );

	it( 'makeBoolean true, normal', () => {
		assert.deepEqual(
			makeBoolean( true ),
			{
				Z1K1: { Z1K1: 'Z9', Z9K1: 'Z40' },
				Z40K1: { Z1K1: 'Z9', Z9K1: 'Z41' }
			} );
	} );

	it( 'makeBoolean false, normal', () => {
		assert.deepEqual(
			makeBoolean( false ),
			{
				Z1K1: { Z1K1: 'Z9', Z9K1: 'Z40' },
				Z40K1: { Z1K1: 'Z9', Z9K1: 'Z42' }
			} );
	} );

	// traverseZList

	it( 'traverseZList', () => {
		const resultList = [];
		const ZList = someType.Z4K2;
		const callback = ( tail ) => {
			resultList.push( tail.K1.Z3K2.Z6K1 );
		};
		traverseZList( ZList, callback );
		assert.deepEqual( resultList, [ 'Z10000K1', 'Z10000K2' ] );
	} );

	// returnOnFirstError

	it( 'returnOnFirstError encounters error in first function', async () => {
		const badFunction = () => badZ22;
		const goodFunction = () => goodZ22;
		const result = await returnOnFirstError(
			goodZ22, [
				[ badFunction, [], 'badFunction' ],
				[ goodFunction, [], 'goodFunction' ] ] );
		assert.deepEqual( result, badZ22 );
	} );

	it( 'returnOnFirstError encounters no errors', async () => {
		const theFunction = ( Z22K1 ) => {
			const localResult = { ...Z22K1 };
			localResult.Z6K1 = 'very ' + localResult.Z6K1;
			return makeMappedResultEnvelope( localResult, null );
		};
		const result = await returnOnFirstError(
			goodZ22, [
				[ theFunction, [], 'theFunction' ],
				[ theFunction, [], 'theFunction' ],
				[ theFunction, [], 'theFunction' ] ] );
		assert.deepEqual( result.Z22K1.Z6K1, 'very very very dull but reliable sigma string' );
	} );

	it( 'returnOnFirstError calls callback', async () => {
		let stoolPigeon = false;
		const goodFunction = () => goodZ22;
		const indicatorFunction = () => {
			stoolPigeon = true;
		};
		await returnOnFirstError(
			goodZ22, [ [ goodFunction, [], 'goodFunction' ] ], indicatorFunction );
		assert.deepEqual( stoolPigeon, true );
	} );

	it( 'returnOnFirstError omits Z22 if requested', async () => {
		let stoolPigeon = false;
		const indicatorFunction = ( firstArgument = null ) => {
			if ( firstArgument !== null ) {
				stoolPigeon = true;
			}
			return goodZ22;
		};
		await returnOnFirstError(
			goodZ22, [
				[ indicatorFunction, [], 'indicatorFunction' ]
			], /* callback= */null, /* addZ22= */false );
		assert.deepEqual( stoolPigeon, false );
	} );

	// quoteZObject

	it( 'quoteZObject ZWrapped', async () => {
		const quotedTenThou = quoteZObject( wrappedTenThou );
		const expected = {
			Z1K1: {
				Z1K1: 'Z9',
				Z9K1: 'Z99'
			},
			Z99K1: tenThou
		};
		assert.deepEqual( quotedTenThou.asJSON(), expected );
	} );

	it( 'quoteZObject unwrapped', async () => {
		const quotedTenThou = quoteZObject( tenThou );
		const expected = {
			Z1K1: {
				Z1K1: 'Z9',
				Z9K1: 'Z99'
			},
			Z99K1: tenThou
		};
		assert.deepEqual( quotedTenThou.asJSON(), expected );
	} );

	// makeWrappedResultEnvelope

	it( 'makeWrappedResultEnvelope, good version', async () => {
		const goodUnwrapped = makeMappedResultEnvelope(
			{ Z1K1: 'Z6', Z6K1: 'dull but reliable sigma string' }, null
		);
		const goodWrapped = makeWrappedResultEnvelope(
			{ Z1K1: 'Z6', Z6K1: 'dull but reliable sigma string' }, null
		);
		assert.deepEqual( goodWrapped.asJSON(), goodUnwrapped );
	} );

	it( 'makeWrappedResultEnvelope, bad version', async () => {
		const badUnwrapped = makeMappedResultEnvelope(
			null, generateError( 'extremely exciting but morally flawed error string' )
		);
		const badWrapped = makeWrappedResultEnvelope(
			null, generateError( 'extremely exciting but morally flawed error string' )
		);
		assert.deepEqual( badWrapped.asJSON(), badUnwrapped );
	} );

	// getEvaluatorAndOrchestratorConfigsForEnvironment: evaluatorConfigs

	it( 'getEvaluatorAndOrchestratorConfigsForEnvironment (evaluatorConfigs) with ORCHESTRATOR_CONFIG (overrides useReentrance)', async () => {
		const evaluatorConfigs = [
			{
				programmingLanguages: [ 'chef' ],
				evaluatorUri: 'nowhere',
				evaluatorWs: 'somewhere',
				useReentrance: false
			}
		];
		const environment = {
			ORCHESTRATOR_CONFIG: JSON.stringify( { evaluatorConfigs: evaluatorConfigs } )
		};
		const {
			evaluatorConfigs: actualConfigs
		} = getEvaluatorAndOrchestratorConfigsForEnvironment( environment, true );
		assert.deepEqual( actualConfigs, evaluatorConfigs );
	} );

	it( 'getEvaluatorAndOrchestratorConfigsForEnvironment (evaluatorConfigs) default', async () => {
		const expectedConfigs = [
			{
				programmingLanguages: [
					'javascript-es2020', 'javascript-es2019', 'javascript-es2018',
					'javascript-es2017', 'javascript-es2016', 'javascript-es2015',
					'javascript' ],
				evaluatorUri: 'nowhere',
				evaluatorWs: 'somewhere',
				useReentrance: true
			},
			{
				programmingLanguages: [
					'python-3-9', 'python-3-8', 'python-3-7', 'python-3',
					'python' ],
				evaluatorUri: 'nowhere',
				evaluatorWs: 'somewhere',
				useReentrance: true
			},
			{
				softwareLanguages: [
					'Z600', 'Z601', 'Z602', 'Z603', 'Z604', 'Z605', 'Z606', 'Z607', 'Z608' ],
				evaluatorUri: 'nowhere',
				evaluatorWs: 'somewhere',
				useReentrance: true
			},
			{
				softwareLanguages: [
					'Z610', 'Z611', 'Z612', 'Z613', 'Z614', 'Z615', 'Z620' ],
				evaluatorUri: 'nowhere',
				evaluatorWs: 'somewhere',
				useReentrance: true
			}
		];
		const environment = {
			FUNCTION_EVALUATOR_URL: 'nowhere',
			FUNCTION_EVALUATOR_WS: 'somewhere'
		};
		const {
			evaluatorConfigs: actualConfigs
		} = getEvaluatorAndOrchestratorConfigsForEnvironment( environment, true );
		assert.deepEqual( actualConfigs, expectedConfigs );
	} );

	// getEvaluatorAndOrchestratorConfigsForEnvironment: orchestratorConfig

	it( 'getEvaluatorAndOrchestratorConfigsForEnvironment (orchestratorConfig) with addNestedMetadata', async () => {
		const environment = {
			ORCHESTRATOR_CONFIG: JSON.stringify( {
				evaluatorConfigs: [],
				addNestedMetadata: true,
				generateFunctionsMetrics: false
			} )
		};
		const expectedConfig = {
			addNestedMetadata: true,
			generateFunctionsMetrics: false
		};
		const {
			orchestratorConfig: actualConfig
		} = getEvaluatorAndOrchestratorConfigsForEnvironment( environment, true );
		assert.deepEqual( actualConfig, expectedConfig );
	} );

	it( 'getEvaluatorAndOrchestratorConfigsForEnvironment (orchestratorConfig) without addNestedMetadata', async () => {
		const environment = {
			ORCHESTRATOR_CONFIG: JSON.stringify( {
				evaluatorConfigs: [],
				generateFunctionsMetrics: false
			} )
		};
		const expectedConfig = { generateFunctionsMetrics: false };
		const {
			orchestratorConfig: actualConfig
		} = getEvaluatorAndOrchestratorConfigsForEnvironment( environment, true );
		assert.deepEqual( actualConfig, expectedConfig );
	} );

	// getTimeoutForEnvironment

	it( 'getTimeoutForEnvironment happy path: ORCHESTRATOR_TIMEOUT_MS is set to a good value', () => {
		const environment = {
			ORCHESTRATOR_TIMEOUT_MS: '15'
		};
		const logger = FakeLogger.create();
		const timeout = getTimeoutForEnvironment( environment, defaultTimeout, logger );
		assert.deepEqual( timeout, 15 );
	} );

	it( 'getTimeoutForEnvironment sad path: ORCHESTRATOR_TIMEOUT_MS is set to a negative value', () => {
		const environment = {
			ORCHESTRATOR_TIMEOUT_MS: '-15'
		};
		const logger = FakeLogger.create();
		const timeout = getTimeoutForEnvironment( environment, defaultTimeout, logger );
		assert.deepEqual( timeout, defaultTimeout );
		assert.deepEqual( logger.warnMessages, [ 'Timeout must be greater than 0 ms; setting to default of 20000 ms.' ] );
	} );

	it( 'getTimeoutForEnvironment: ORCHESTRATOR_TIMEOUT_MS is unset', () => {
		const environment = {};
		const logger = FakeLogger.create();
		const timeout = getTimeoutForEnvironment( environment, defaultTimeout, logger );
		assert.deepEqual( timeout, defaultTimeout );
		assert.deepEqual( logger.warnMessages, [ 'Timeout must be greater than 0 ms; setting to default of 20000 ms.' ] );
	} );

	it( 'getTimeoutForEnvironment: ORCHESTRATOR_TIMEOUT_MS is set to a non-numeric value', () => {
		const environment = {
			ORCHESTRATOR_TIMEOUT_MS: 'twenty thousand'
		};
		const logger = FakeLogger.create();
		const timeout = getTimeoutForEnvironment( environment, defaultTimeout, logger );
		assert.deepEqual( timeout, defaultTimeout );
		assert.deepEqual( logger.warnMessages, [ 'ORCHESTRATOR_TIMEOUT_MS was not a numerical value: twenty thousand; setting to default of 20000 ms.' ] );
	} );
} );
