'use strict';

const { makeMappedResultEnvelope } = require( '../../function-schemata/javascript/src/utils.js' );
const assert = require( '../utils/assert.js' );
const { testInputsDir, testOutputsDir } = require( '../utils/testFileUtils.js' );
const { RateLimitCache } = require( '../../src/rateLimits.js' );
const { readJSON } = require( '../../src/utils.js' );
const { getServerWithMocks } = require( '../utils/mockUtils.js' );
const { attemptOrchestration } = require( '../features/v1/mswTestRunner.js' );

describe( 'rateLimits test', () => {
	const { wikiStub, evaluatorStub, mockServiceWorker } = getServerWithMocks();

	before( () => {
		mockServiceWorker.listen();
	} );

	afterEach( () => {
		wikiStub.reset();
		evaluatorStub.reset();
		mockServiceWorker.resetHandlers();
	} );

	after( () => {
		mockServiceWorker.close();
	} );

	{
		const environment = {
			FUNCTION_ORCHESTRATOR_RATE_LIMIT_TTL_SECONDS: '1'
		};
		const rateLimiter = new RateLimitCache(
			/* limit= */ 100,
			/* environment= */ environment
		);
		const requestId = 'shoulddisappear';

		const ttlTestEvaluatedFunction = async () => {
			const existences = [];
			existences.push( rateLimiter.idCounter_.has( requestId ) );
			// Sleep for 1 second to wait out the TTL.
			await ( new Promise( ( resolve ) => {
				setTimeout( resolve, 1000 );
			} ) );
			existences.push( rateLimiter.idCounter_.has( requestId ) );
			const result = JSON.stringify( existences );
			return makeMappedResultEnvelope(
				{
					Z1K1: 'Z6',
					Z6K1: result
				},
				null
			);
		};

		const setUpStubs = () => {
			evaluatorStub.setZId( 'Z420', ttlTestEvaluatedFunction );
		};
		attemptOrchestration(
			/* testName= */ 'check that rate limiter TTL is respected',
			/* functionCall= */ readJSON( testInputsDir( 'rate-limits.json' ) ),
			/* expectedResultFile= */ testOutputsDir( 'rate-limits-ttl-respected.json' ),
			/* expectedErrorFile= */ null,
			setUpStubs,
			/* expectedExtraMetadata= */ [],
			/* expectedMissingMetadata= */ [ 'implementationId' ],
			/* implementationSelector= */ null,
			/* doValidate= */ false,
			/* skip= */ false,
			requestId,
			/* invariantZIDs= */ [],
			/* req= */ null,
			/* rateLimiter= */ rateLimiter
		);
	}

	{
		const environment = {
			FUNCTION_ORCHESTRATOR_RATE_LIMIT_TTL_SECONDS: '1000'
		};
		const rateLimiter = new RateLimitCache(
			/* limit= */ 100,
			/* environment= */ environment
		);
		const requestId = 'shouldnotdisappear';

		const ttlTestEvaluatedFunction = async () => {
			const existences = [];
			existences.push( rateLimiter.idCounter_.has( requestId ) );
			const result = JSON.stringify( existences );
			return makeMappedResultEnvelope(
				{
					Z1K1: 'Z6',
					Z6K1: result
				},
				null
			);
		};

		const setUpStubs = () => {
			evaluatorStub.setZId( 'Z420', ttlTestEvaluatedFunction );
		};
		attemptOrchestration(
			/* testName= */ 'check that request ID is evicted after returning',
			/* functionCall= */ readJSON( testInputsDir( 'rate-limits.json' ) ),
			/* expectedResultFile= */ testOutputsDir( 'rate-limits-evicted.json' ),
			/* expectedErrorFile= */ null,
			setUpStubs,
			/* expectedExtraMetadata= */ [],
			/* expectedMissingMetadata= */ [ 'implementationId' ],
			/* implementationSelector= */ null,
			/* doValidate= */ false,
			/* skip= */ false,
			requestId,
			/* invariantZIDs= */ [],
			/* req= */ null,
			/* rateLimiter= */ rateLimiter
		);
		assert.deepEqual( rateLimiter.idCounter_.has( requestId ), false );
	}

} );
