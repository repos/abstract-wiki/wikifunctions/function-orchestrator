'use strict';

const assert = require( '../utils/assert.js' );
const { getServerWithMocks, WIKI_URI, WIKIDATA_URI } = require( '../utils/mockUtils.js' );

const { ReferenceResolver } = require( '../../src/fetchObject.js' );
const { makeVoid, getError } = require( '../../function-schemata/javascript/src/utils.js' );
const { readJSON } = require( '../../src/utils.js' );
const { testWikidataDir, testOutputsDir, schemataDefinitionsDir } = require( '../utils/testFileUtils' );
const { getMissingZ5 } = require( '../../function-schemata/javascript/test/testUtils' );
const normalize = require( '../../function-schemata/javascript/src/normalize.js' );

const storedZ400 = {
	Z1K1: { Z1K1: 'Z9', Z9K1: 'Z2' },
	Z2K1: { Z1K1: 'Z6', Z6K1: 'Z400' },
	Z2K2: { Z1K1: 'Z6', Z6K1: 'haaaaay' }
};

const storedZ420 = {
	Z1K1: { Z1K1: 'Z9', Z9K1: 'Z2' },
	Z2K1: { Z1K1: 'Z6', Z6K1: 'Z420' },
	Z2K2: { Z1K1: 'Z6', Z6K1: 'yaaaaay' }
};

// Lexeme L3435, as retrieved from Wikidata
const storedL3435 = readJSON( testWikidataDir( 'L3435.json' ) );
// Lexeme L3435, as a ZObject. Normalize because dereferenceLexeme returns normal form.
const expectedL3435 = normalize( readJSON( testOutputsDir( 'L3435_expected.json' ) ) ).Z22K1;
// Lexeme L3435, with 'lemmas' element missing
const storedL3435_no_lemmas = readJSON( testWikidataDir( 'L3435_no_lemmas.json' ) );
// Lexeme L3828, as retrieved from Wikidata
const storedL3828 = readJSON( testWikidataDir( 'L3828.json' ) );
// Lexeme L3828, as a ZObject. Normalize because dereferenceLexeme returns normal form.
const expectedL3828 = normalize( readJSON( testOutputsDir( 'L3828_expected.json' ) ) ).Z22K1;

// Lexeme form L3435-F1, as retrieved from Wikidata
const storedL3435_F1 = readJSON( testWikidataDir( 'L3435-F1.json' ) );
// Lexeme form L3435-F1, as a ZObject. Normalize because dereferenceLexemeForm returns normal form.
const expectedL3435_F1 = normalize( readJSON( testOutputsDir( 'L3435-F1_expected.json' ) ) ).Z22K1;
// Lexeme form L3435-F1, with 'id' element missing
const storedL3435_F1_no_id = readJSON( testWikidataDir( 'L3435-F1_no_id.json' ) );

// Lexeme sense L3435-S1, as retrieved from Wikidata
const storedL3435_S1 = readJSON( testWikidataDir( 'L3435-S1.json' ) );
// Lexeme sense L3435-S1, as a ZObject. Normalize because dereferenceLexemeForm returns normal form.
const expectedL3435_S1 = normalize( readJSON( testOutputsDir( 'L3435-S1_expected.json' ) ) ).Z22K1;
// Lexeme sense L3435-S1, with 'id' element missing
const storedL3435_S1_no_id = readJSON( testWikidataDir( 'L3435-S1_no_id.json' ) );

// Item Q41607, as retrieved from Wikidata
const storedQ41607 = readJSON( testWikidataDir( 'Q41607.json' ) );
// Item Q41607, as a ZObject. Normalize because dereferenceItem returns normal form.
const expectedQ41607 = normalize( readJSON( testOutputsDir( 'Q41607_expected.json' ) ) ).Z22K1;
// Item Q41607, with 'id' element missing
const storedQ41607_no_id = readJSON( testWikidataDir( 'Q41607_no_id.json' ) );

describe( 'fetchObject.js: ReferenceResolver test', () => {

	const { wikiStub, wikidataStub, wikidataQueryStub,
		mockServiceWorker } = getServerWithMocks();

	before( () => {
		mockServiceWorker.listen();
	} );

	afterEach( () => {
		wikiStub.reset();
		wikidataStub.reset();
		wikidataQueryStub.reset();
		mockServiceWorker.resetHandlers();
	} );

	after( () => {
		mockServiceWorker.close();
	} );

	it( 'respects logs', async () => {
		const setUpStubs = () => {
			wikiStub.setZId( 'Z400', storedZ400 );
			wikiStub.setZId( 'Z420', storedZ420 );
		};
		setUpStubs();
		const logs = [];
		const fakeLog = ( level, message ) => {
			logs.push( message );
		};
		const resolver = new ReferenceResolver(
			WIKI_URI,
			WIKIDATA_URI,
			/* wikifunctionsVirtualHost= */ null,
			/* wikidataVirtualHost= */ null,
			/* logThis= */ fakeLog );
		const resultMap = await resolver.dereferenceZObjects( [ 'Z400', 'Z420' ] );
		assert.deepEqual( resultMap.get( 'Z400' ).Z22K1, storedZ400 );
		assert.deepEqual( resultMap.get( 'Z420' ).Z22K1, storedZ420 );
		assert.deepEqual( logs, [
			'orchestrator start fetching ZIDs <Z400|Z420> from URI <http://thewiki/?action=wikilambda_fetch&format=json&uselang=content&zids=Z400%7CZ420> with host <null> ...',
			'... orchestrator final fetching ZIDs <Z400|Z420>'
		] );
	} );

	it( 'log times are spaced', async () => {
		const theDelay = 1000;
		const setUpStubs = () => {
			wikiStub.setZId(
				'Z400',
				storedZ400,
				/* error= */ false,
				/* ephemeral= */ true,
				/* delay= */ theDelay );
		};
		setUpStubs();
		const logs = [];
		const fakeLog = () => {
			logs.push( new Date() );
		};
		const resolver = new ReferenceResolver(
			WIKI_URI,
			WIKIDATA_URI,
			/* wikifunctionsVirtualHost= */ null,
			/* wikidataVirtualHost= */ null,
			/* logThis= */ fakeLog );
		await resolver.dereferenceZObjects( [ 'Z400' ] );
		const timeDelta = logs[ 1 ] = logs[ 0 ];
		assert.deepEqual( timeDelta > theDelay, true );
	} );

	it.skip( 'retrieves ZIDs when present, makes Z5 when not', async () => {
		// TODO (T370472): Fix MW stub and un-skip this test.
		const setUpStubs = () => {
			wikiStub.setZId( 'Z420', storedZ420 );
		};
		setUpStubs();
		const resolver = new ReferenceResolver( WIKI_URI, WIKIDATA_URI );
		const resultMap = await resolver.dereferenceZObjects( [ 'Z400', 'Z420' ] );
		assert.deepEqual( resultMap.get( 'Z420' ).Z22K1, storedZ420 );
		assert.deepEqual( resultMap.get( 'Z400' ).Z22K1, makeVoid() );
	} );

	it( 'retrieves Wikidata lexeme when present', async () => {
		const setUpStubs = () => {
			wikidataStub.setEntityId( 'L3435', storedL3435 );
		};
		setUpStubs();
		const resolver = new ReferenceResolver( WIKI_URI, WIKIDATA_URI, null, null, null, true );
		const result = await resolver.dereferenceLexeme( 'L3435' );
		assert.deepEqual( result.Z22K1, expectedL3435 );
	} );

	it( 'retrieves Wikidata lexeme with multiple senses', async () => {
		const setUpStubs = () => {
			wikidataStub.setEntityId( 'L3828', storedL3828 );
		};
		setUpStubs();
		const resolver = new ReferenceResolver( WIKI_URI, WIKIDATA_URI, null, null, null, true );
		const result = await resolver.dereferenceLexeme( 'L3828' );
		assert.deepEqual( result.Z22K1, expectedL3828 );
	} );

	it( 'Makes Z5 when useWikidata feature flag is false', async () => {
		const setUpStubs = () => {
			wikidataStub.setEntityId( 'L3435', storedL3435 );
		};
		setUpStubs();
		const resolver = new ReferenceResolver( WIKI_URI, WIKIDATA_URI, null, null, null, false );
		const result = await resolver.dereferenceLexeme( 'L3435' );
		assert.deepEqual( result.Z22K1, makeVoid() );
		const notFound = getMissingZ5( result.Z22K2, [ 'Z500' ] );
		assert.strictEqual( notFound.size, 0 );
	} );

	it( 'Makes Z5 when Wikidata lexeme ID is lexically invalid', async () => {
		const resolver = new ReferenceResolver( WIKI_URI, WIKIDATA_URI, null, null, null, true );
		const result = await resolver.dereferenceLexeme( 'L9999-F2' );
		assert.deepEqual( result.Z22K1, makeVoid() );
		// Z549 is invalid_zreference
		const notFound = getMissingZ5( result.Z22K2, [ 'Z549' ] );
		assert.strictEqual( notFound.size, 0 );
	} );

	it( 'Makes Z5 when Wikidata lexeme is not present', async () => {
		const setUpStubs = () => {
			wikidataStub.setEntityId( 'L9999', null, true );
		};
		setUpStubs();
		const resolver = new ReferenceResolver( WIKI_URI, WIKIDATA_URI, null, null, null, true );
		const result = await resolver.dereferenceLexeme( 'L9999' );
		assert.deepEqual( result.Z22K1, makeVoid() );
		// Z504 is zid_not_found
		const notFound = getMissingZ5( result.Z22K2, [ 'Z504' ] );
		assert.strictEqual( notFound.size, 0 );
	} );

	it( 'Makes Z5 when Wikidata lexeme is syntactically invalid', async () => {
		const setUpStubs = () => {
			wikidataStub.setEntityId( 'L3435', storedL3435_no_lemmas );
		};
		setUpStubs();
		const resolver = new ReferenceResolver( WIKI_URI, WIKIDATA_URI, null, null, null, true );
		const result = await resolver.dereferenceLexeme( 'L3435' );
		assert.deepEqual( result.Z22K1, makeVoid() );
		// Z548 is invalid_json
		const notFound = getMissingZ5( result.Z22K2, [ 'Z548' ] );
		assert.strictEqual( notFound.size, 0 );
	} );

	it( 'Returns a Z548/invalid_json when Wikidata fetch return is non-JSON', async () => {
		const setUpStubs = () => {
			wikidataStub.setEntityId( 'L1', 'upstream connect error or disconnect/reset before headers. retried and the latest reset reason: connection failure' );
		};
		setUpStubs();
		const resolver = new ReferenceResolver( WIKI_URI, WIKIDATA_URI, null, null, null, true );
		const result = await resolver.dereferenceLexeme( 'L1' );

		assert.deepEqual( result.Z22K1, makeVoid() );
		const foundError = getError( result );
		assert.notDeepEqual( foundError, makeVoid() );
		assert.strictEqual( foundError.Z5K1.Z9K1, 'Z548' /* invalid_json */ );
	} );

	it( 'retrieves Wikidata lexeme form when present', async () => {
		const setUpStubs = () => {
			wikidataStub.setEntityId( 'L3435-F1', storedL3435_F1 );
		};
		setUpStubs();
		const resolver = new ReferenceResolver( WIKI_URI, WIKIDATA_URI, null, null, null, true );
		const result = await resolver.dereferenceLexemeForm( 'L3435-F1' );
		assert.deepEqual( result.Z22K1, expectedL3435_F1 );
	} );

	it( 'Makes Z5 when Wikidata lexeme form ID is lexically invalid', async () => {
		const resolver = new ReferenceResolver( WIKI_URI, WIKIDATA_URI, null, null, null, true );
		const result = await resolver.dereferenceLexemeForm( 'L9999' );
		assert.deepEqual( result.Z22K1, makeVoid() );
		// Z549 is invalid_zreference
		const notFound = getMissingZ5( result.Z22K2, [ 'Z549' ] );
		assert.strictEqual( notFound.size, 0 );
	} );

	it( 'Makes Z5 when Wikidata lexeme form is not present', async () => {
		const setUpStubs = () => {
			wikidataStub.setEntityId( 'L9999-F2', null, true );
		};
		setUpStubs();
		const resolver = new ReferenceResolver( WIKI_URI, WIKIDATA_URI, null, null, null, true );
		const result = await resolver.dereferenceLexemeForm( 'L9999-F2' );
		assert.deepEqual( result.Z22K1, makeVoid() );
		// Z504 is zid_not_found
		const notFound = getMissingZ5( result.Z22K2, [ 'Z504' ] );
		assert.strictEqual( notFound.size, 0 );
	} );

	it( 'Makes Z5 when Wikidata lexeme form is syntactically invalid', async () => {
		const setUpStubs = () => {
			wikidataStub.setEntityId( 'L3435-F1', storedL3435_F1_no_id );
		};
		setUpStubs();
		const resolver = new ReferenceResolver( WIKI_URI, WIKIDATA_URI, null, null, null, true );
		const result = await resolver.dereferenceLexemeForm( 'L3435-F1' );
		assert.deepEqual( result.Z22K1, makeVoid() );
		// Z548 is invalid_json
		const notFound = getMissingZ5( result.Z22K2, [ 'Z548' ] );
		assert.strictEqual( notFound.size, 0 );
	} );

	it( 'retrieves Wikidata lexeme sense when present', async () => {
		const setUpStubs = () => {
			wikidataStub.setEntityId( 'L3435-S1', storedL3435_S1 );
		};
		setUpStubs();
		const resolver = new ReferenceResolver( WIKI_URI, WIKIDATA_URI, null, null, null, true );
		const result = await resolver.dereferenceLexemeSense( 'L3435-S1' );
		assert.deepEqual( result.Z22K1, expectedL3435_S1 );
	} );

	it( 'Makes Z5 when Wikidata lexeme sense ID is lexically invalid', async () => {
		const resolver = new ReferenceResolver( WIKI_URI, WIKIDATA_URI, null, null, null, true );
		const result = await resolver.dereferenceLexemeSense( 'L9999-F1' );
		assert.deepEqual( result.Z22K1, makeVoid() );
		// Z549 is invalid_zreference
		const notFound = getMissingZ5( result.Z22K2, [ 'Z549' ] );
		assert.strictEqual( notFound.size, 0 );
	} );

	it( 'Makes Z5 when Wikidata lexeme sense is not present', async () => {
		const setUpStubs = () => {
			wikidataStub.setEntityId( 'L9999-S2', null, true );
		};
		setUpStubs();
		const resolver = new ReferenceResolver( WIKI_URI, WIKIDATA_URI, null, null, null, true );
		const result = await resolver.dereferenceLexemeSense( 'L9999-S2' );
		assert.deepEqual( result.Z22K1, makeVoid() );
		// Z504 is zid_not_found
		const notFound = getMissingZ5( result.Z22K2, [ 'Z504' ] );
		assert.strictEqual( notFound.size, 0 );
	} );

	it( 'Makes Z5 when Wikidata lexeme sense is syntactically invalid', async () => {
		const setUpStubs = () => {
			wikidataStub.setEntityId( 'L3435-S1', storedL3435_S1_no_id );
		};
		setUpStubs();
		const resolver = new ReferenceResolver( WIKI_URI, WIKIDATA_URI, null, null, null, true );
		const result = await resolver.dereferenceLexemeSense( 'L3435-S1' );
		assert.deepEqual( result.Z22K1, makeVoid() );
		// Z548 is invalid_json
		const notFound = getMissingZ5( result.Z22K2, [ 'Z548' ] );
		assert.strictEqual( notFound.size, 0 );
	} );

	it( 'retrieves Wikidata item when present', async () => {
		const setUpStubs = () => {
			wikidataStub.setEntityId( 'Q41607', storedQ41607 );
		};
		setUpStubs();
		const resolver = new ReferenceResolver( WIKI_URI, WIKIDATA_URI, null, null, null, true );
		const result = await resolver.dereferenceItem( 'Q41607' );
		assert.deepEqual( result.Z22K1, expectedQ41607 );
	} );

	it( 'Makes Z5 when Wikidata item ID is lexically invalid', async () => {
		const resolver = new ReferenceResolver( WIKI_URI, WIKIDATA_URI, null, null, null, true );
		const result = await resolver.dereferenceItem( 'Q41607-31' );
		assert.deepEqual( result.Z22K1, makeVoid() );
		// Z549 is invalid_zreference
		const notFound = getMissingZ5( result.Z22K2, [ 'Z549' ] );
		assert.strictEqual( notFound.size, 0 );
	} );

	it( 'Makes Z5 when Wikidata item is not present', async () => {
		const setUpStubs = () => {
			wikidataStub.setEntityId( 'Q9999', null, true );
		};
		setUpStubs();
		const resolver = new ReferenceResolver( WIKI_URI, WIKIDATA_URI, null, null, null, true );
		const result = await resolver.dereferenceItem( 'Q9999' );
		assert.deepEqual( result.Z22K1, makeVoid() );
		// Z504 is zid_not_found
		const notFound = getMissingZ5( result.Z22K2, [ 'Z504' ] );
		assert.strictEqual( notFound.size, 0 );
	} );

	it( 'Makes Z5 when Wikidata item is syntactically invalid', async () => {
		const setUpStubs = () => {
			wikidataStub.setEntityId( 'Q41607', storedQ41607_no_id );
		};
		setUpStubs();
		const resolver = new ReferenceResolver( WIKI_URI, WIKIDATA_URI, null, null, null, true );
		const result = await resolver.dereferenceItem( 'Q41607' );
		assert.deepEqual( result.Z22K1, makeVoid() );
		// Z548 is invalid_json
		const notFound = getMissingZ5( result.Z22K2, [ 'Z548' ] );
		assert.strictEqual( notFound.size, 0 );
	} );

	it( 'retrieves Wikidata property when present', async () => {
		const resolver = new ReferenceResolver( WIKI_URI, WIKIDATA_URI, null, null, null, true );
		const result = await resolver.dereferenceProperty( 'P5137' );
		assert.deepEqual( result.Z22K1, normalize( readJSON( testOutputsDir( 'P5137_expected.json' ) ) ).Z22K1 );
	} );

	it( 'Makes Z5 when Wikidata property ID is lexically invalid', async () => {
		const resolver = new ReferenceResolver( WIKI_URI, WIKIDATA_URI, null, null, null, true );
		const result = await resolver.dereferenceProperty( 'P0' );
		assert.deepEqual( result.Z22K1, makeVoid() );
		// Z549 is invalid_zreference
		const notFound = getMissingZ5( result.Z22K2, [ 'Z549' ] );
		assert.strictEqual( notFound.size, 0 );
	} );

	it( 'Makes Z5 when Wikidata property is not present', async () => {
		const setUpStubs = () => {
			wikidataStub.setEntityId( 'P9999', null, true );
		};
		setUpStubs();
		const resolver = new ReferenceResolver( WIKI_URI, WIKIDATA_URI, null, null, null, true );
		const result = await resolver.dereferenceProperty( 'P9999' );
		assert.deepEqual( result.Z22K1, makeVoid() );
		// Z504 is zid_not_found
		const notFound = getMissingZ5( result.Z22K2, [ 'Z504' ] );
		assert.strictEqual( notFound.size, 0 );
	} );

	it( 'Makes Z5 when Wikidata property is syntactically invalid', async () => {
		const JsonForP5137 = readJSON( testWikidataDir( 'P5137.json' ) );
		delete JsonForP5137.id;
		const setUpStubs = () => {
			wikidataStub.setEntityId( 'P5137', JsonForP5137 );
		};
		setUpStubs();
		const resolver = new ReferenceResolver( WIKI_URI, WIKIDATA_URI, null, null, null, true );
		const result = await resolver.dereferenceProperty( 'P5137' );
		assert.deepEqual( result.Z22K1, makeVoid() );
		// Z548 is invalid_json
		const notFound = getMissingZ5( result.Z22K2, [ 'Z548' ] );
		assert.strictEqual( notFound.size, 0 );
	} );

	it( 'dereferenceWikidataEntities handles an empty-list input', async () => {
		const resolver = new ReferenceResolver( WIKI_URI, WIKIDATA_URI, null, null, null, true );
		const resultMap = await resolver.dereferenceWikidataEntities( [] );
		assert.deepEqual( resultMap, new Map() );
	} );

	it( 'dereferenceWikidataEntities retrieves a single wbgetentities result', async () => {
		const resolver = new ReferenceResolver( WIKI_URI, WIKIDATA_URI, null, null, null, true );
		const resultMap = await resolver.dereferenceWikidataEntities( [ 'L3435' ] );
		assert.deepEqual( resultMap.get( 'L3435' ).Z22K1, expectedL3435 );
	} );

	it( 'dereferenceWikidataEntities retrieves multiple wbgetentities results mixed with Z504s', async () => {
		// Indicate that these are missing
		const setUpStubs = () => {
			wikidataStub.setEntityId( 'L1', null, false, true );
			wikidataStub.setEntityId( 'Q41607', null, false, true );
		};
		setUpStubs();
		const resolver = new ReferenceResolver( WIKI_URI, WIKIDATA_URI, null, null, null, true );
		const resultMap = await resolver.dereferenceWikidataEntities( [ 'L3435', 'L1', 'L3435-F1', 'L3435-S1', 'Q41607' ] );
		assert.deepEqual( resultMap.get( 'L3435' ).Z22K1, expectedL3435 );
		assert.deepEqual( resultMap.get( 'L3435-F1' ).Z22K1, expectedL3435_F1 );
		assert.deepEqual( resultMap.get( 'L3435-S1' ).Z22K1, expectedL3435_S1 );
		let result = resultMap.get( 'L1' );
		assert.deepEqual( result.Z22K1, makeVoid() );
		// Z504 is zid_not_found
		let notFound = getMissingZ5( result.Z22K2, [ 'Z504' ] );
		assert.strictEqual( notFound.size, 0 );
		result = resultMap.get( 'Q41607' );
		assert.deepEqual( result.Z22K1, makeVoid() );
		notFound = getMissingZ5( result.Z22K2, [ 'Z504' ] );
		assert.strictEqual( notFound.size, 0 );
	} );

	it( 'dereferenceWikidataEntities returns an error for an invalid ID (G14)', async () => {
		const resolver = new ReferenceResolver( WIKI_URI, WIKIDATA_URI, null, null, null, true );
		const resultZ22 = await resolver.dereferenceWikidataEntities( [ 'L3435', 'L1', 'G14', 'L3435-S1', 'Q41607' ] );
		assert.deepEqual( resultZ22.Z22K1, makeVoid() );
		const notFound = getMissingZ5( resultZ22.Z22K2, [ 'Z549' ] );
		assert.strictEqual( notFound.size, 0 );
	} );

	it( 'dereferenceWikidataEntities handles a top-level error from wbgetentities', async () => {
		// Indicate that this returns a top-level error
		const setUpStubs = () => {
			wikidataStub.setEntityId( 'L34355555555', null, true, false );
		};
		setUpStubs();
		const resolver = new ReferenceResolver( WIKI_URI, WIKIDATA_URI, null, null, null, true );
		const resultZ22 = await resolver.dereferenceWikidataEntities( [ 'L3435', 'L1', 'L34355555555', 'L3435-S1', 'Q41607' ] );
		assert.deepEqual( resultZ22.Z22K1, makeVoid() );
		const notFound = getMissingZ5( resultZ22.Z22K2, [ 'Z500' ] );
		assert.strictEqual( notFound.size, 0 );
	} );

	it( 'findLexemesForItem returns empty list when there are no results from haswbstatement', async () => {
		// Set the response for findEntitiesByStatements
		const queryResponse = { query: { search: [] } };
		const setUpStubs = () => {
			wikidataQueryStub.setQueryResponse( 'P5137', 'Q41607', queryResponse );
		};
		setUpStubs();
		const resolver = new ReferenceResolver( WIKI_URI, WIKIDATA_URI, null, null, null, true );
		const language = normalize( readJSON( schemataDefinitionsDir( 'Z1002.json' ) ).Z2K2 ).Z22K1;
		const result = await resolver.findLexemesForItem( 'Q41607', 'P5137', language );
		const expected = normalize( [ 'Z6095' ] ).Z22K1;
		assert.deepEqual( result.Z22K1, expected );
	} );

	it( 'findLexemesForItem returns a result in English', async () => {
		// Set the response for findEntitiesByStatements
		const queryResponse =
			// Pretend that these 3 lexemes are related to Q41607 by P5137/'item for this lexeme'
			{ query: { search: [ { ns: 146, title: 'Lexeme:L3435' }, { ns: 146, title: 'Lexeme:L3828' },
				{ ns: 146, title: 'Lexeme:L31815' } ] } };
		const setUpStubs = () => {
			wikidataQueryStub.setQueryResponse( 'P5137', 'Q41607', queryResponse );
		};
		setUpStubs();
		const resolver = new ReferenceResolver( WIKI_URI, WIKIDATA_URI, null, null, null, true );
		const language = normalize( readJSON( schemataDefinitionsDir( 'Z1002.json' ) ).Z2K2 ).Z22K1;
		const result = await resolver.findLexemesForItem( 'Q41607', 'P5137', language );
		const expected = normalize( [ 'Z6095', { Z1K1: 'Z6095', Z6095K1: 'L3435' } ] ).Z22K1;
		assert.deepEqual( result.Z22K1, expected );
	} );

	it( 'findLexemesForItem returns a result in Finnish', async () => {
		// Set the response for findEntitiesByStatements
		const queryResponse =
			// Pretend these 3 lexemes are related to Q41607 by P5137/'item for this lexeme'
			{ query: { search: [ { ns: 146, title: 'Lexeme:L3435' }, { ns: 146, title: 'Lexeme:L3828' },
				{ ns: 146, title: 'Lexeme:L31815' } ] } };
		const setUpStubs = () => {
			wikidataQueryStub.setQueryResponse( 'P5137', 'Q41607', queryResponse );
		};
		setUpStubs();
		const resolver = new ReferenceResolver( WIKI_URI, WIKIDATA_URI, null, null, null, true );
		const language = normalize( readJSON( schemataDefinitionsDir( 'Z1051.json' ) ).Z2K2 ).Z22K1;
		const result = await resolver.findLexemesForItem( 'Q41607', 'P5137', language );
		const expected = normalize( [ 'Z6095', { Z1K1: 'Z6095', Z6095K1: 'L31815' } ] ).Z22K1;
		assert.deepEqual( result.Z22K1, expected );
	} );

	it( 'findLexemesForItem returns Z5 when there is a 404 from haswbstatement', async () => {
		// Set the response for findEntitiesByStatements
		const queryResponse = { query: { search: [] } };
		const setUpStubs = () => {
			wikidataQueryStub.setQueryResponse( 'P5137', 'Q41607', queryResponse, true );
		};
		setUpStubs();
		const resolver = new ReferenceResolver( WIKI_URI, WIKIDATA_URI, null, null, null, true );
		const language = normalize( readJSON( schemataDefinitionsDir( 'Z1002.json' ) ).Z2K2 ).Z22K1;
		const result = await resolver.findLexemesForItem( 'Q41607', 'P5137', language );
		assert.deepEqual( result.Z22K1, makeVoid() );
		const notFound = getMissingZ5( result.Z22K2, [ 'Z500' ] );
		assert.strictEqual( notFound.size, 0 );
	} );

	it( 'findLexemesForItem returns multiple results in Spanish from a large collection', async () => {
		// This tests that retrieveWikidataEntities correctly retrieves 50 at a time
		// TODO (T271776): When we use the inlanguage parameter in findEntitiesByStatements,
		//   repurpose this test to call dereferenceWikidataEntities instead of findLexemesForItem
		// Set the response for findEntitiesByStatements
		const searchResults = [];
		const n = 108;
		for ( let i = 1; i <= n; i++ ) {
			searchResults.push( { ns: 146, title: 'Lexeme:L' + i.toString() } );
		}
		wikidataQueryStub.setQueryResponse( 'P5137', 'Q41607', { query: { search: searchResults } } );
		const expected = [ 'Z6095' ];
		// Set n fake lexemes (as JSON from Wikidata), the response for retrieveWikidataEntities
		for ( let i = 1; i <= n; i++ ) {
			// Deep copy an English lexeme & give it a new identity
			const lexemeTemplate = JSON.parse( JSON.stringify( storedL3435 ) );
			const LID = 'L' + i.toString();
			lexemeTemplate.id = LID;
			wikidataStub.setEntityId( LID, lexemeTemplate );
			if ( i % 5 === 0 ) {
				// Make every 5th lexeme a Spanish one; put a Z6095 for it on expected
				lexemeTemplate.lemmas = { es: { language: 'es', value: 'paraguas' } };
				expected.push( { Z1K1: 'Z6095', Z6095K1: LID } );
			}
		}
		const resolver = new ReferenceResolver( WIKI_URI, WIKIDATA_URI, null, null, null, true );
		const language = normalize( readJSON( schemataDefinitionsDir( 'Z1003.json' ) ).Z2K2 ).Z22K1;
		const result = await resolver.findLexemesForItem( 'Q41607', 'P5137', language );
		assert.deepEqual( result.Z22K1, normalize( expected ).Z22K1 );
	} );
} );
