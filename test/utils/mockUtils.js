/**
 * Util classes and functions involving mocking. Used mainly for
 * testing and performance benchmark purposes.
 */

'use strict';

const fs = require( 'fs' );
const { readJSON } = require( '../../src/utils.js' );
const { Evaluator } = require( '../../src/Evaluator.js' );
const { EvaluatorError } = require( '../../src/implementation.js' );
const { Invariants } = require( '../../src/Invariants.js' );
const { ReferenceResolver } = require( '../../src/fetchObject.js' );
const path = require( 'path' );
const normalize = require( '../../function-schemata/javascript/src/normalize.js' );
const { getWrappedZObjectFromVersionedBinary } = require( '../../function-schemata/javascript/src/binaryFormatter.js' );

const { LoggerWrapper } = require( '../../function-schemata/javascript/src/LoggerWrapper.js' );

// This require() is apparently the secret stuff that makes MSW work with Node.
require( 'cross-fetch/polyfill' );
const { http, HttpResponse } = require( 'msw' );
const { setupServer } = require( 'msw/node' );

const WIKI_URI = 'http://thewiki';
const WIKIDATA_URI = 'http://thewikidata';
const EVAL_URI = 'http://theevaluator';

/**
 * A simple stub for mediaWiki API that holds fake ZObjects. When fake
 * ZObjects are not set, real ones from the file system are used.
 *
 * How to use: set up with your expected values in the tests/benchmarks, and have
 * mock REST handlers "query" from it.
 */
class MediaWikiStub {

	constructor() {
		this.wiki_ = new Map();
		this.zObjectDir_ = 'function-schemata/data/definitions/';
		this.reset();
	}

	reset() {
		this.ephemeral_ = new Map();
	}

	/**
	 * Set up a fake definition for a ZID.
	 *
	 * @param {string} zid The ZID you want to set up a fake for.
	 * @param {*} value The JSON definition for this zid.
	 * @param {boolean} error
	 * @param {boolean} ephemeral
	 * @param {number} delay how long to emulate delay in retrieving from MW
	 * @param {Function} retrievalCallback function to call upon object retrieval
	 */
	setZId(
		zid, value = null, error = false, ephemeral = true,
		delay = null, retrievalCallback = null ) {
		let theMap;
		if ( ephemeral ) {
			theMap = this.ephemeral_;
		} else {
			theMap = this.wiki_;
		}
		const valueToSet = {
			value: value,
			error: error
		};
		if ( delay ) {
			valueToSet.delay = delay;
		}
		if ( retrievalCallback !== null ) {
			valueToSet.retrievalCallback = retrievalCallback;
		}
		theMap.set( zid, valueToSet );
	}

	hasZid( zid ) {
		return this.wiki_.has( zid ) || this.ephemeral_.has( zid );
	}

	/**
	 * Gets the fake JSON object for a certain ZID. If the value was not
	 * previously set via setZid(), then the value will be set from the
	 * real directory and returned.
	 *
	 * @param {string} zid
	 * @return {*} A fake JSON representation of the ZID.
	 */
	getZId( zid ) {
		if ( this.ephemeral_.has( zid ) ) {
			return this.ephemeral_.get( zid );
		}
		if ( !( this.wiki_.has( zid ) ) ) {
			const filePath = path.join( this.zObjectDir_, zid + '.json' );
			// eslint-disable-next-line security/detect-non-literal-fs-filename
			if ( !fs.existsSync( filePath ) ) {
				throw new Error( `No persistent zobject available for ${ zid }` );
			}
			const jsonObj = readJSON( filePath );
			this.setZId( zid, jsonObj, false, false );
		}
		return this.wiki_.get( zid );
	}

}

/**
 * A simple stub for Wikidata's Linked Data API that holds fake Wikidata entities (as JSON objects).
 * When fake entities are not passed into setEntityId, they are retrieved from files in
 * this.entityDir_.
 *
 * See also:
 *   https://www.mediawiki.org/wiki/Wikibase/DataModel
 *   https://www.wikidata.org/wiki/Wikidata:Lexicographical_data/Documentation
 *   https://www.wikidata.org/wiki/Wikidata:Data_access
 *   https://www.mediawiki.org/wiki/Extension:WikibaseLexeme/Data_Model
 *   https://doc.wikimedia.org/WikibaseLexeme/master/php/md_docs_2topics_2json.html
 *
 * How to use: set up with your expected values in the tests/benchmarks, and have
 * mock REST handlers "query" from it.
 */
class WikidataStub {

	constructor() {
		this.wiki_ = new Map();
		this.entityDir_ = 'test/features/v1/test_data/wikidata/';
		this.reset();
	}

	reset() {
		this.ephemeral_ = new Map();
	}

	/**
	 * Set up a fake definition for a Wikidata entity.
	 *
	 * @param {string} entityId The Entity ID you want to set up a fake for.
	 * @param {*} value The JSON definition for this ID.
	 * @param {boolean} error
	 * @param {boolean} missing
	 * @param {boolean} ephemeral
	 * @param {number} delay how long to emulate delay in retrieving from MW
	 */
	setEntityId( entityId, value = null, error = false, missing = false,
		ephemeral = true, delay = null ) {
		let theMap;
		if ( ephemeral ) {
			theMap = this.ephemeral_;
		} else {
			theMap = this.wiki_;
		}
		const valueToSet = {
			value: value,
			error: error,
			missing: missing
		};
		if ( delay ) {
			valueToSet.delay = delay;
		}
		theMap.set( entityId, valueToSet );
	}

	hasEntityId( entityId ) {
		return this.wiki_.has( entityId ) || this.ephemeral_.has( entityId );
	}

	/**
	 * Gets the fake JSON object for a certain entityId.
	 *
	 * @param {string} entityId
	 * @return {*} A fake JSON representation of the entityId.
	 */
	getEntityId( entityId ) {
		if ( this.ephemeral_.has( entityId ) ) {
			return this.ephemeral_.get( entityId );
		}
		if ( !( this.wiki_.has( entityId ) ) ) {
			const filePath = path.join( this.entityDir_, entityId + '.json' );
			// eslint-disable-next-line security/detect-non-literal-fs-filename
			if ( !fs.existsSync( filePath ) ) {
				throw new Error( `No mocked entity available for ${ entityId }` );
			}
			const jsonObj = readJSON( filePath );
			this.setEntityId( entityId, jsonObj, false, false, false );
		}
		return this.wiki_.get( entityId );
	}

}

/**
 * A simple stub for Wikidata's Query API that holds fake search responses (as JSON objects).
 * When fake entities are not passed into setEntityId, they are retrieved from files in
 * this.responseDir_.
 *
 * N.B.: Currently, this only mocks responses for action=query searches with
 * srsearch=haswbstatement:PropertyId=EntityId.
 *
 * See also:
 *   https://phabricator.wikimedia.org/T378097 (includes a couple example searches in a comment)
 *   https://www.mediawiki.org/wiki/Help:Extension:WikibaseCirrusSearch
 *
 * How to use: set up with your expected values in the tests/benchmarks/responseDir_, and have
 * mock REST handlers "query" from it.
 */
class WikidataQueryStub {

	constructor() {
		this.wiki_ = new Map();
		this.responseDir_ = 'test/features/v1/test_data/wikidata/';
		this.reset();
	}

	reset() {
		this.ephemeral_ = new Map();
	}

	/**
	 * Set up a fake search response for a (PropertyId, EntityId) pair.
	 *
	 * @param {string} propertyId The Property ID you want to set up a fake for.
	 * @param {string} entityId The Entity ID you want to set up a fake for.
	 * @param {*} value The search response (JSON) for these IDs.
	 * @param {boolean} error
	 * @param {boolean} ephemeral
	 * @param {number} delay how long to emulate delay in retrieving from MW
	 */
	setQueryResponse( propertyId, entityId,
		value = null, error = false, ephemeral = true, delay = null ) {
		let theMap;
		if ( ephemeral ) {
			theMap = this.ephemeral_;
		} else {
			theMap = this.wiki_;
		}
		const valueToSet = {
			value: value,
			error: error
		};
		if ( delay ) {
			valueToSet.delay = delay;
		}
		const key = propertyId + '_' + entityId;
		theMap.set( key, valueToSet );
	}

	hasQueryResponse( propertyId, entityId ) {
		const key = propertyId + '_' + entityId;
		return this.wiki_.has( key ) || this.ephemeral_.has( key );
	}

	/**
	 * Gets the fake JSON object for a certain propertyId and himentityId.
	 *
	 * @param {string} propertyId
	 * @param {string} entityId
	 * @return {*} A fake JSON representation of the entityId.
	 */
	getQueryResponse( propertyId, entityId ) {
		const key = propertyId + '_' + entityId;
		if ( this.ephemeral_.has( key ) ) {
			return this.ephemeral_.get( key );
		}
		if ( !( this.wiki_.has( key ) ) ) {
			const filePath = path.join( this.responseDir_, 'query_response_' + key + '.json' );
			// eslint-disable-next-line security/detect-non-literal-fs-filename
			if ( !fs.existsSync( filePath ) ) {
				throw new Error( `No mocked query response available for ${ propertyId } + ${ entityId }` );
			}
			const jsonObj = readJSON( filePath );
			this.setQueryResponse( propertyId, entityId, jsonObj, false, false );
		}
		return this.wiki_.get( key );
	}

}

/**
 * A simple stub for the function evaluator API that holds
 * preset interactions (status code and callback) for ZIDs.
 *
 * How to use: set up with your expected values in the tests/benchmarks, and have
 * mock REST handlers "query" from it.
 */
class EvaluatorStub {
	constructor() {
		this.reset();
	}

	reset() {
		this.evaluatorByZId_ = new Map();
		this.evaluatorByCodeString_ = new Map();
	}

	/**
	 * Set up a fake evaluator result (callback + statusCode) for a ZID.
	 *
	 * @param {string} zid
	 * @param {Function} callback A function that performs the desired outcome.
	 * @param {number} statusCode The status code to return when this ZID is queried.
	 *     Default is 200.
	 * @param {boolean} doNormalize Whether to normalize after calling callback.
	 */
	setZId( zid, callback, statusCode = 200, doNormalize = true ) {
		let wrappedCallback;
		if ( doNormalize ) {
			wrappedCallback = async function ( input ) {
				return normalize( await callback( input ) ).Z22K1;
			};
		} else {
			wrappedCallback = callback;
		}
		this.evaluatorByZId_.set( zid, {
			statusCode: statusCode,
			callback: wrappedCallback
		} );
	}

	setCodeString( codeString, callback, statusCode = 200 ) {
		this.evaluatorByCodeString_.set( codeString, {
			statusCode: statusCode,
			callback: callback
		} );
	}

	/**
	 * Gets the fake evaluator status code and callback for a certain ZID.
	 * If the value was not previously set via setEvaluator, an error will
	 * be thrown.
	 *
	 * @param {string} zid
	 * @return {*} A fake evaluator response template consisted of two parts:
	 *     {statusCode: number, callback: Function}.
	 */
	getZId( zid ) {
		const result = this.evaluatorByZId_.get( zid );
		if ( result === undefined ) {
			throw new EvaluatorError( `The evaluator for ZID ${ zid } was never set!` );
		}
		return result;
	}

	getCodeString( codeString ) {
		const result = this.evaluatorByCodeString_.get( codeString );
		if ( result === undefined ) {
			throw new EvaluatorError( `The evaluator for code string ${ codeString } was never set!` );
		}
		return result;
	}
}

class TestLogger {
	constructor() {
		this.events_ = [];
	}

	log( ...args ) {
		this.events_.push( args );
	}

	child() {
		return new TestLogger();
	}
}

/**
 * Sets up the mocking for GET requests to a mediaWiki URI so that it will
 * return preset ZObjects saved in a stub.
 *
 * @param {string} mediaWikiUri The mediaWiki URI used in a orchestrator.
 * @param {MediaWikiStub} mediaWikiStub A stub that contains JSON definitions of ZIDs.
 * @return {*} A REST mock handler. Needed for server setup.
 */
function mockMediaWiki( mediaWikiUri, mediaWikiStub ) {
	return http.get( mediaWikiUri, async ( { request } ) => {
		// Gets all the ZIDs from the request.
		const url = new URL( request.url );
		const zids = url.searchParams.get( 'zids' );
		// Compose a map of ZID to JSON definition.
		let result = {};

		const delays = [];

		for ( const ZID of zids.split( '|' ) ) {
			const retrievedObject = mediaWikiStub.getZId( ZID );
			if ( retrievedObject.error ) {
				// TODO (T297507): This behavior reflects the current wikilambda_fetch
				// API; change it if that changes.
				result = { problem: 'bad' };
				break;
			}
			if ( retrievedObject.delay ) {
				delays.push(
					new Promise( ( resolve ) => {
						setTimeout( resolve, retrievedObject.delay );
					} ) );
			}
			const callback = retrievedObject.retrievalCallback;
			if ( callback ) {
				callback();
			}
			result[ ZID ] = {
				wikilambda_fetch: JSON.stringify( retrievedObject.value )
			};

		}
		if ( delays.length > 0 ) {
			await Promise.all( delays );
		}
		return HttpResponse.json( result, { status: 200 } );
	} );
}

/**
 * Given the (lexically valid) ID of an entity unknown to Wikidata, return the HTML
 * error string that will come back from Wikidata.
 *
 * @param {string} entityId The ID of the entity requested from Wikidata
 * @return {string} The HTML string returned for an unknown ID
 */
function notFoundText( entityId ) {
	const template = '<!DOCTYPE html>\n' +
	'<html><head><title>Not Found</title><meta name="color-scheme" content="light dark" /></head>\n' +
	'<body><h1>Not Found</h1><p>No entity with ID L3286234 was found.</p></body></html>';

	return template.replace( 'L3286234', entityId );
}

/**
 * Sets up the mocking for GET requests to a Wikidata URI so that it will
 * return preset Wikidata entities saved in a stub.
 *
 * @param {string} wikidataUri The Wikidata URI used in an orchestrator.
 * @param {WikidataStub} wikidataStub A stub that contains JSON definitions of Wikidata entities.
 * @return {*} A REST mock handler. Needed for server setup.
 */
function mockWikidata( wikidataUri, wikidataStub ) {
	return http.get( wikidataUri + '/wiki/Special:EntityData/*', async ( { request } ) => {
		// Get the entity Id from the request.
		const url = new URL( request.url );
		const entityId = url.pathname.match( /^\/wiki\/Special:EntityData\/(.*).json$/ )[ 1 ];

		const retrievedObject = wikidataStub.getEntityId( entityId );
		if ( retrievedObject.error ) {
			return HttpResponse.text( notFoundText( entityId ), { status: 404 } );
		}
		// The Linked Data API returns an entity map, even though there's only a single entity
		const entityMap = {};
		entityMap[ entityId ] = retrievedObject.value;
		return HttpResponse.json( { entities: entityMap }, { status: 200 } );
	} );
}

/**
 * Sets up the mocking for GET requests to the Wikidata action URI so that it will
 * return preset results saved in a stub.
 *
 * @param {string} wikidataUri The Wikidata URI used in an orchestrator.
 * @param {WikidataQueryStub} wikidataQueryStub Contains JSON blurbs of haswbstatement results.
 * @param {WikidataStub} wikidataStub Contains JSON definitions of Wikidata entities.
 * @return {*} A REST mock handler. Needed for server setup.
 */
function mockWikidataActionAPI( wikidataUri, wikidataQueryStub, wikidataStub ) {
	return http.get( wikidataUri + '/w/api.php', async ( { request } ) => {
		const url = new URL( request.url );
		if ( url.searchParams.get( 'action' ) === 'query' ) {
			// Get propertyId and entityId from the request.
			const srsearch = url.searchParams.get( 'srsearch' );
			const propertyId = srsearch.match( /^haswbstatement:(P[0-9]+)=(Q[0-9]+)$/ )[ 1 ];
			const entityId = srsearch.match( /^haswbstatement:(P[0-9]+)=(Q[0-9]+)$/ )[ 2 ];
			const retrievedObject = wikidataQueryStub.getQueryResponse( propertyId, entityId );
			if ( retrievedObject.error ) {
				return HttpResponse.text( '<html><head></head><body>File not found.\n' +
					'</body></html>', { status: 404 } );
			}
			return HttpResponse.json( retrievedObject.value, { status: 200 } );
		} else if ( url.searchParams.get( 'action' ) === 'wbgetentities' ) {
			// Gets all the IDs from the request.
			const ids = url.searchParams.get( 'ids' );
			// Compose a map of ID to JSON definition.
			const entityMap = {};
			const delays = [];

			for ( const ID of ids.split( '|' ) ) {
				const retrievedObject = wikidataStub.getEntityId( ID );
				if ( retrievedObject.error ) {
					// This comes back (with additional nested content) when any ID is invalid
					return HttpResponse.json( { error: { code: 'no-such-entity', info: `Could not find an entity with the ID "${ ID }"` } }, { status: 200 } );
				}
				if ( retrievedObject.delay ) {
					delays.push(
						new Promise( ( resolve ) => {
							setTimeout( resolve, retrievedObject.delay );
						} ) );
				}
				if ( retrievedObject.missing ) {
					entityMap[ ID ] = { id: ID, missing: '' };
				} else {
					entityMap[ ID ] = retrievedObject.value;
				}
			}
			if ( delays.length > 0 ) {
				await Promise.all( delays );
			}
			return HttpResponse.json( { entities: entityMap }, { status: 200 } );
		}
	} );
}

async function getBufferFromStream( stream ) {
	const parts = [];
	const reader = stream.getReader();
	while ( true ) {
		const { done, value } = await reader.read();
		if ( done ) {
			break;
		}
		parts.push( value );
	}
	const resultBuffer = new Uint8Array( parts.reduce( ( sum, part ) => sum + part.length, 0 ) );
	let pivot = 0;
	for ( const part of parts ) {
		resultBuffer.set( part, pivot );
		pivot += part.length;
	}
	return Buffer.from( resultBuffer );
}

/**
 * Sets up the mocking for POST requests to a evaluator URI so that it will
 * perform preset callbacks and return preset status codes.
 *
 * @param {string} evaluatorUri The evaluator URI used in a orchestrator.
 * @param {EvaluatorStub} evaluatorStub A stub that contains preset callbacks
 *     and status codes for ZIDs.
 * @return {*} A REST mock handler. Needed for server setup.
 */
function mockEvaluator( evaluatorUri, evaluatorStub ) {
	return http.post( evaluatorUri, async ( { request } ) => {
		const buffer = await getBufferFromStream( request.body );
		const functionCall = getWrappedZObjectFromVersionedBinary( buffer );
		let theStatusCode, theCallback;
		try {
			const ZID = functionCall.function.functionName;
			const { statusCode, callback } = evaluatorStub.getZId( ZID );
			[ theStatusCode, theCallback ] = [ statusCode, callback ];
		} catch ( e ) {
			const codeString = functionCall.function.codeString;
			const { statusCode, callback } = evaluatorStub.getCodeString( codeString );
			[ theStatusCode, theCallback ] = [ statusCode, callback ];
		}
		const value = await theCallback( functionCall );
		return HttpResponse.json( value, { status: theStatusCode } );
	} );
}

/**
 * Silently mock GET requests to the API running at :6254 and do nothing.
 *
 * @return {*} A REST mock handler. Needed for server setup.
 */
function mockLocalhost() {

	return http.get( 'http://localhost:6254/*', () => new HttpResponse( {} ) );
}

function getTestInvariants( invariantParameters ) {
	const {
		addNestedMetadata,
		doValidate,
		remainingTime,
		wikiUri,
		wikidataUri,
		evalUri,
		requestId,
		req,
		generateFunctionsMetrics,
		rateLimiter,
		validateTypesWithCallCount,
		eagerlyEvaluateWithCallCount,
		resolveDanglingReferencesWithCallCount,
		executeWithCallCount
	} = invariantParameters;
	let { invariantZIDs } = invariantParameters;
	// if !empty, dependency injection of Z61 ids from tests
	if ( invariantZIDs === undefined || invariantZIDs.length <= 0 ) {
		const softwareLanguages = [
			'Z600', 'Z601', 'Z602', 'Z603', 'Z604', 'Z605', 'Z606', 'Z607', 'Z608',
			'Z610', 'Z611', 'Z612', 'Z613', 'Z614', 'Z615', 'Z620'
		];
		invariantZIDs = [
			'javascript-es2020', 'javascript-es2019', 'javascript-es2018',
			'javascript-es2017', 'javascript-es2016', 'javascript-es2015',
			'javascript', 'python-3-9', 'python-3-8', 'python-3-7', 'python-3',
			'python'
		].concat( softwareLanguages );
	}
	const testLogger = new LoggerWrapper( new TestLogger() );
	const resolver = new ReferenceResolver( wikiUri, wikidataUri, null, null, null, true );

	const evaluators = [
		new Evaluator(
			{
				programmingLanguages: invariantZIDs,
				evaluatorUri: evalUri,
				evaluatorWs: null,
				useReentrance: false
			},
			testLogger
		)
	];
	const orchestratorConfig = { doValidate, addNestedMetadata, generateFunctionsMetrics };
	orchestratorConfig.generateFunctionsMetrics = false;
	function getRemainingTime() {
		return remainingTime;
	}
	const invariantConstructorArgs = [
		resolver, evaluators, orchestratorConfig, getRemainingTime, requestId, testLogger
	];
	invariantConstructorArgs.push( req || null );
	invariantConstructorArgs.push( rateLimiter || null );
	invariantConstructorArgs.push( validateTypesWithCallCount || null );
	invariantConstructorArgs.push( eagerlyEvaluateWithCallCount || null );
	invariantConstructorArgs.push( resolveDanglingReferencesWithCallCount || null );
	invariantConstructorArgs.push( executeWithCallCount || null );
	return new Invariants( ...invariantConstructorArgs );
}

function getServerWithMocks() {
	const wikiStub = new MediaWikiStub();
	const wikidataStub = new WikidataStub();
	const wikidataQueryStub = new WikidataQueryStub();
	const evaluatorStub = new EvaluatorStub();
	const mockServiceWorker = setupServer(
		mockMediaWiki( WIKI_URI, wikiStub ),
		mockWikidata( WIKIDATA_URI, wikidataStub ),
		mockWikidataActionAPI( WIKIDATA_URI, wikidataQueryStub, wikidataStub ),
		mockEvaluator( EVAL_URI, evaluatorStub ),
		mockLocalhost()
	);
	return { wikiStub, wikidataStub, wikidataQueryStub, evaluatorStub, mockServiceWorker };
}

module.exports = {
	MediaWikiStub,
	EvaluatorStub,
	WikidataStub,
	WikidataQueryStub,
	getServerWithMocks,
	getTestInvariants,
	mockMediaWiki,
	mockEvaluator,
	mockLocalhost,
	mockWikidata,
	mockWikidataActionAPI,
	EVAL_URI,
	WIKI_URI,
	WIKIDATA_URI
};
