'use strict';

const fs = require( 'fs' );
const path = require( 'path' );

const { readJSON } = require( '../../src/utils.js' );

/**
 * @param {string} directory The file directory that contains many ZObject json files.
 * @return {*} A map of ZObject name to JSON objects from a file directory.
 */
function readZObjectsFromDirectory( directory ) {
	const fileNameFilter = ( f ) => f.startsWith( 'Z' );
	// eslint-disable-next-line security/detect-non-literal-fs-filename
	const paths = fs.readdirSync( directory ).filter( fileNameFilter );

	const result = {};
	for ( const p of paths ) {
		result[ path.basename( p, '.json' ) ] = readJSON( path.join( directory, p ) );
	}

	return result;
}

function testDataDir( ...pathComponents ) {
	return path.join(
		path.dirname( path.dirname( __filename ) ),
		'features', 'v1', 'test_data', ...pathComponents );
}

function testInputsDir( ...pathComponents ) {
	return testDataDir( 'inputs', ...pathComponents );
}

function testObjectsDir( ...pathComponents ) {
	return testDataDir( 'objects', ...pathComponents );
}

function testWikidataDir( ...pathComponents ) {
	return testDataDir( 'wikidata', ...pathComponents );
}

function testOutputsDir( ...pathComponents ) {
	return testDataDir( 'outputs', ...pathComponents );
}

function schemataDefinitionsDir( ...pathComponents ) {
	return path.join(
		path.dirname( path.dirname( path.dirname( __filename ) ) ),
		'function-schemata', 'data', 'definitions', ...pathComponents );
}

function writeJSON( object, fileName ) {
	// eslint-disable-next-line security/detect-non-literal-fs-filename
	fs.writeFile( fileName, JSON.stringify( object, null, '\t' ) + '\n', () => {} );
}

function getFunctionCallForExpensiveTest( testSubdirectory ) {
	const functionCallFile = path.join( testSubdirectory, 'function_call.json' );
	return readJSON( functionCallFile );
}

function setStubsForExpensiveTest( testSubdirectory, wikiStub, wikidataStub ) {
	const wikilambdaDir = path.join( testSubdirectory, 'wikilambda' );
	// eslint-disable-next-line security/detect-non-literal-fs-filename
	for ( const fileName of fs.readdirSync( wikilambdaDir ) ) {
		const fullFileName = path.join( wikilambdaDir, fileName );
		const ZObject = readJSON( fullFileName );
		const ZID = fileName.replace( /\.json$/, '' );
		wikiStub.setZId( ZID, ZObject );
	}
	const wikidataDir = path.join( testSubdirectory, 'wikidata' );
	// eslint-disable-next-line security/detect-non-literal-fs-filename
	for ( const fileName of fs.readdirSync( wikidataDir ) ) {
		const fullFileName = path.join( wikidataDir, fileName );
		const wikidataObject = readJSON( fullFileName );
		const theID = fileName.replace( /\.json$/, '' );
		wikidataStub.setEntityId( theID, wikidataObject );
	}
}

module.exports = {
	getFunctionCallForExpensiveTest,
	readZObjectsFromDirectory,
	schemataDefinitionsDir,
	setStubsForExpensiveTest,
	testDataDir,
	testInputsDir,
	testObjectsDir,
	testWikidataDir,
	testOutputsDir,
	writeJSON
};
