'use strict';

const { Invariants } = require( '../../src/Invariants.js' );
const { ZWrapper } = require( '../../src/ZWrapper.js' );
const { isArray, isObject } = require( '../../function-schemata/javascript/src/utils.js' );

function limitedStringify( jsonObject, recursionDepth ) {
	if ( isArray( jsonObject ) ) {
		if ( recursionDepth <= 0 ) {
			return '[...]';
		}
		const children = [];
		for ( const child of jsonObject ) {
			children.push( limitedStringify( child, recursionDepth - 1 ) );
		}
		const parts = [
			'[',
			children.join( ',' ),
			']'
		];
		return parts.join( '' );
	}
	if ( isObject( jsonObject ) ) {
		if ( recursionDepth <= 0 ) {
			return '{...}';
		}
		const keys = Object.keys( jsonObject );
		keys.sort();
		const children = [];
		for ( const key of keys ) {
			const value = jsonObject[ key ];
			const stringifiedValue = limitedStringify( value, recursionDepth - 1 );
			children.push( limitedStringify( key, recursionDepth - 1 ) + ':' + stringifiedValue );
		}
		const parts = [
			'{',
			children.join( ',' ),
			'}'
		];
		return parts.join( '' );
	}

	// If the object is not an array or Object, it is terminal (string, Number,
	// null, etc.), and we can fall back on the normal JSON.stringify.
	return JSON.stringify( jsonObject );
}

class Tree {

	constructor() {
		this.labelComponents_ = null;
		this.functionName_ = null;
		this.children_ = [];
		this.size_ = null;
	}

	calculateSizes() {
		const children = this.children_;
		let totalSize = 1;
		for ( const child of children ) {
			child.calculateSizes();
			totalSize += child.getSize();
		}
		this.size_ = totalSize;
	}

	getSize() {
		return this.size_;
	}

	setLabelComponents( labelComponents ) {
		this.labelComponents_ = labelComponents;
	}

	setFunctionName( functionName ) {
		this.functionName_ = functionName;
	}

	newChild() {
		const child = new Tree();
		this.children_.push( child );
		return child;
	}

	asJSON() {
		const result = {};
		result.receivedArguments = this.labelComponents_;
		result.functionName = this.functionName_;
		result.size = this.size_;
		if ( this.children_.length > 0 ) {
			result.children = [];
			for ( const child of this.children_ ) {
				result.children.push( child.asJSON() );
			}
		}
		return result;
	}

}

class InvariantsWithTrace extends Invariants {

	constructor( traceTree, ...args ) {
		super( ...args );
		if ( traceTree === null ) {
			traceTree = new Tree();
		}
		this.traceTree_ = traceTree;
	}

	tracedCopy() {
		const copiedInvariants = new InvariantsWithTrace(
			this.traceTree_.newChild(),
			this.resolver_,
			this.evaluators_,
			this.orchestratorConfig_,
			this.getRemainingTime_,
			this.requestId_,
			this.logger_,
			this.req_,
			this.rateLimiter_,
			this.validateTypesWithCallCount,
			this.eagerlyEvaluateWithCallCount,
			this.resolveDanglingReferencesWithCallCount,
			this.executeWithCallCount );
		return copiedInvariants;
	}

	getTree() {
		return this.traceTree_;
	}

}

function wrapExecutionFunction( module, functionName ) {
	const originalFunction = module[ functionName ];
	function wrappedFunction( ...args ) {
		let newInvariants;
		const labelComponents = [];
		const wrappedArgs = [];
		try {
			for ( const arg of args ) {
				if ( arg instanceof InvariantsWithTrace ) {
					newInvariants = arg.tracedCopy();
					wrappedArgs.push( newInvariants );
					continue;
				}
				let toStringify;
				if ( arg instanceof ZWrapper ) {
					toStringify = arg.asJSON();
				} else if ( arg instanceof Set ) {
					toStringify = [ ...arg ];
				} else {
					toStringify = arg;
				}
				const label = limitedStringify( toStringify, 3 );
				labelComponents.push( label );
				wrappedArgs.push( arg );
			}
		} catch ( e ) {
			console.trace( e );
		}
		if ( newInvariants !== undefined ) {
			const tree = newInvariants.getTree();
			tree.setLabelComponents( labelComponents );
			tree.setFunctionName( functionName );
		}
		return originalFunction( ...wrappedArgs );
	}
	module[ functionName ] = wrappedFunction;
}

function wrapFunctionsWithTrace() {
	{
		const validateModule = require( '../../src/validation.js' );
		wrapExecutionFunction( validateModule, 'runTypeValidatorDynamic' );
	}
	{
		const executeModule = require( '../../src/execute.js' );
		wrapExecutionFunction( executeModule, 'eagerlyEvaluate' );
		wrapExecutionFunction( executeModule, 'execute' );
		wrapExecutionFunction( executeModule, 'resolveDanglingReferences' );
	}
	// TODO (T388054): Why doesn't this work?
	// wrapExecutionFunction( ZWrapper.prototype, 'resolveInternal_' );
	// TODO (T388054): figure out where else to monkey-patch.
}

function wrapResolverFunction( resolver, functionName, invariants ) {
	const originalFunction = resolver[ functionName ];
	function wrappedFunction( ...args ) {
		try {
			const child = invariants.getTree().newChild();
			const labels = args.map( ( arg ) => {
				let toStringify = arg;
				if ( arg instanceof Set ) {
					toStringify = [ ...arg ];
				}
				return limitedStringify( toStringify, 3 );
			} );
			child.setLabelComponents( labels );
			child.setFunctionName( functionName );
			return originalFunction.bind( resolver )( ...args );
		} catch ( e ) {
			console.trace( e );
			throw e;
		}
	}
	resolver[ functionName ] = wrappedFunction.bind( resolver );
}

function wrapInvariantsMemberFunctions( invariants ) {
	const resolver = invariants.resolver;
	wrapResolverFunction( resolver, 'fetchZIDs', invariants );
	wrapResolverFunction( resolver, 'retrieveWikidataEntities', invariants );
	wrapResolverFunction( resolver, 'retrieveWikidataEntityFromLODAPI', invariants );
}

module.exports = { InvariantsWithTrace, wrapFunctionsWithTrace, wrapInvariantsMemberFunctions };
